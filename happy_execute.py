from pypy.rlib.rarithmetic import intmask

from zend import *
import global_state
import targetphpstandalone as tphp
import happy_operators
import happy_util
import opcodes as ops
import read_apc_dump as rad
import zval_utils
import happy_hash
import happy_variables
import objects

def i_happy_is_true(op):
    if zval_utils.Z_TYPE_P(op) == IS_NULL:
        return 0
    elif (zval_utils.Z_TYPE_P(op) == IS_LONG or zval_utils.Z_TYPE_P(op) == IS_BOOL):
        return 1 if zval_utils.Z_LVAL_P(op) else 0
    elif zval_utils.Z_TYPE_P(op) == IS_DOUBLE:
        return 1 if zval_utils.Z_DVAL_P(op) else 0
    elif zval_utils.Z_TYPE_P(op) == IS_STRING:
        if zval_utils.Z_STRLEN_P(op) == 0:
            return 0
        if zval_utils.Z_STRLEN_P(op) == 1 and zval_utils.Z_STRVAL_P(op).get(0) == '0':
            return 0
        return 1
    elif zval_utils.Z_TYPE_P(op) == IS_OBJECT:
        # TODO: make it like Zend
        return 1
    raise Exception('Not implemented yet')

def INIT_PZVAL_COPY(z, v):
    z.assign(v.deref())
    zval_utils.Z_SET_TYPE_P(z, zval_utils.Z_TYPE_P(v))
    zval_utils.Z_SET_REFCOUNT_P(z, 1)
    zval_utils.Z_UNSET_ISREF_P(z)

def INIT_PZVAL(z):
    z.deref().refcount__gc = 1
    z.deref().is_ref__gc = 0

def FREE_OP_NODE(node, free_op):
    if node.get_op_type() == rad.APCFile.ZEND_znode.IS_CONST:
        pass
    elif node.get_op_type() == rad.APCFile.ZEND_znode.IS_TMP_VAR:
        zval_utils.zval_dtor(free_op.var())
    elif node.get_op_type() == rad.APCFile.ZEND_znode.IS_VAR:
        if not free_op.var().is_null():
            happy_variables.zval_ptr_dtor(free_op.var_ptr())
    elif node.get_op_type() == rad.APCFile.ZEND_znode.IS_CV:
        pass
    elif node.get_op_type() == rad.APCFile.ZEND_znode.IS_UNUSED:
        pass
    else:
        raise Exception('Should not happen')

def FREE_OP_VAR_PTR(should_free):
    if not should_free.var().is_null():
        happy_variables.zval_ptr_dtor(should_free.var_ptr())

def FREE_OP_IF_VAR(should_free):
    if not should_free.var().is_null() and not should_free.is_temp():
        happy_variables.zval_ptr_dtor(should_free.var_ptr())

def IS_TMP_FREE(should_free):
    return should_free.is_temp()

def IS_OP_TMP_FREE(node):
    return node.get_op_type() == rad.APCFile.ZEND_znode.IS_TMP_VAR

def DECODE_CTOR(ce):
    # TODO
    return ce

def zend_vm_stack_push(varptr):
    global_state.EG.argument_stack.append(varptr)

def zend_vm_stack_push_args(count):
    zv = zval_utils.make_empty_zval()
    pzv = zval_utils.zp_stack(zv)
    zval_utils.ZVAL_LONG(pzv, count)
    global_state.EG.argument_stack.append(pzv)
    return global_state.EG.argument_stack.top()

def zend_vm_stack_push_argc(count):
    zv = zval_utils.make_empty_zval()
    pzv = zval_utils.zp_stack(zv)
    zval_utils.ZVAL_LONG(pzv, count)
    global_state.EG.argument_stack.append(pzv)

def zend_vm_stack_free():
    # This takes care of deallocating memory for chained stacks in Zend; NOP in happy
    pass

def zend_vm_stack_free_int(ptr):
    global_state.EG.argument_stack.set_top(ptr)

def zend_vm_stack_get_arg(requested_arg):
    p = global_state.EG.argument_stack.getItem(
        global_state.EG.current_execute_data.prev_execute_data.function_state.arguments - 1)
    arg_count = zval_utils.Z_LVAL_P(p)

    if requested_arg > arg_count:
        return global_state.null_zval_ptr
    return global_state.EG.argument_stack.getItem(
        global_state.EG.current_execute_data.prev_execute_data.function_state.arguments -
        arg_count + requested_arg - 2)

def zend_vm_stack_clear_multiple():
    p = global_state.EG.argument_stack.getItem(
        global_state.EG.argument_stack.getLen() - 1)
    delete_count = zval_utils.Z_LVAL_P(p)
    top_idx = global_state.EG.argument_stack.top()
    for i in range(top_idx - 2, top_idx - 2 - intmask(delete_count), -1):
        q = global_state.EG.argument_stack.getItem(i)
        global_state.EG.argument_stack.setItem(i, global_state.null_zval_ptr)
        happy_variables.zval_ptr_dtor(zval_utils.zpp_stack(q))
    zend_vm_stack_free_int(top_idx - 1 - intmask(delete_count))

def zend_ptr_stack_3_push(stack, a, b, c):
    stack.append((a, b, c))

def zend_arg_types_stack_3_pop(arg_types_stack, execute_data):
    (execute_data.fbc, execute_data.current_object, execute_data.called_scope) = arg_types_stack.pop()

def zend_pzval_unlock_func(z, should_free, unref):
    if not zval_utils.Z_DELREF_P(z):
        zval_utils.Z_SET_REFCOUNT_P(z, 1)
        zval_utils.Z_UNSET_ISREF_P(z)
        should_free.set_var(z)
    else:
        should_free.set_var(global_state.null_zval_ptr)
        if unref and zval_utils.Z_ISREF_P(z) and zval_utils.Z_REFCOUNT_P(z) == 1:
            zval_utils.Z_UNSET_ISREF_P(z)

def PZVAL_LOCK(z):
    return zval_utils.Z_ADDREF_P(z)

def PZVAL_UNLOCK(z, f):
    return zend_pzval_unlock_func(z, f, 1)

def RETURN_VALUE_UNUSED(pzn):
    return pzn.EA_type & rad.APCFile.ZEND_znode.EXT_TYPE_UNUSED

def RETURN_VALUE_USED(opline):
    return not (opline.get_result().EA_type & rad.APCFile.ZEND_znode.EXT_TYPE_UNUSED)

def CV_OF_PTR(i):
    return global_state.EG.current_execute_data.CVs.address_of(i)

def CV_OF(i):
    return CV_OF_PTR(i)

def CV_DEF_OF(i):
    return global_state.EG.active_op_array.vars[i]

def get_zval_ptr_tmp(node, Ts, should_free):
    should_free.set_var(Ts[node.var / tphp.ZendExecutor.TEMP_SZ].tmp_var_ptr(), True)
    return should_free.var()

def get_zval_ptr_var(node, Ts, should_free):
    ptr = Ts[node.var / tphp.ZendExecutor.TEMP_SZ].get_ptr()

    if not ptr.is_null():
        PZVAL_UNLOCK(ptr, should_free)
    else:
        raise Exception('Not implemented yet')
    return ptr

def get_zval_cv_lookup(ptr, var, type):
    assert isinstance(ptr, objects.zval_ptr_ptr_ptr)
    cv = CV_DEF_OF(var)

    if (not global_state.EG.active_symbol_table) or\
       happy_hash.zend_hash_quick_find(global_state.EG.active_symbol_table, cv.name,
           cv.name.length() + 1, cv.hash_value, ptr) == FAILURE:
        if type == BP_VAR_W:
            zval_utils.Z_ADDREF(global_state.EG.uninitialized_zval())
            if not global_state.EG.active_symbol_table:
                ptr.assign(global_state.EG.current_execute_data.CVs.address_of_ext(var))
                ptr.deref().assign(global_state.EG.uninitialized_zval_ptr())
            else:
                happy_hash.zend_hash_quick_update(global_state.EG.active_symbol_table, cv.name,
                    cv.name.length() + 1, cv.hash_value,
                    global_state.EG.uninitialized_zval_ptr_ptr(), 0, ptr)
            return global_state.EG.current_execute_data.CVs.get(var)
        else:
            raise Exception('Not implemented!')
    return ptr.deref()

def get_zval_ptr_cv(node, type):
    ptr = CV_OF_PTR(node.var)

    if ptr.deref().is_null():
        return get_zval_cv_lookup(ptr, node.var, type).deref()

    return ptr.deref().deref()

def get_zval_ptr_var(node, Ts, should_free):
    ptr = Ts[node.var / tphp.ZendExecutor.TEMP_SZ].get_ptr()
    if not ptr.is_null():
        PZVAL_UNLOCK(ptr, should_free)
        return ptr
    else:
        raise Exception('Not implemented yet')

def _get_obj_zval_ptr_unused():
    if not global_state.EG.This().is_null():
        return global_state.EG.This()
    else:
        raise Exception('Not implemented!')

def _get_obj_zval_ptr_ptr_unused():
    if not global_state.EG.This().is_null():
        return global_state.EG.AddressOfThis()
    else:
        raise Exception('Not implemented!')

def get_zval_ptr_ptr_var(node, Ts, should_free):
    ptr_ptr = Ts[node.var / tphp.ZendExecutor.TEMP_SZ].get_ptr_ptr()

    if not ptr_ptr.is_null():
        PZVAL_UNLOCK(ptr_ptr.deref(), should_free)
    else:
        # string offset
        PZVAL_UNLOCK(Ts[node.var / tphp.ZendExecutor.TEMP_SZ].str, should_free)
    return ptr_ptr

def get_zval_ptr_ptr_cv(node, type):
    ptr = CV_OF_PTR(node.var)

    if ptr.deref().is_null():
        return get_zval_cv_lookup(ptr, node.var, type)

    return ptr.deref()

def get_zval_ptr_ptr(node, Ts, should_free, type):
    if node.get_op_type() == rad.APCFile.ZEND_znode.IS_CV:
        should_free.set_var(global_state.null_zval_ptr)
        return get_zval_ptr_ptr_cv(node, type)
    else:
        raise Exception('Not implemented yet')

def get_zval_ptr(node, Ts, should_free, type):
    if node.get_op_type() == rad.APCFile.ZEND_znode.IS_CONST:
        return zval_utils.zp_stack(node.constant)
    elif node.get_op_type() == rad.APCFile.ZEND_znode.IS_CV:
        should_free.set_var(global_state.null_zval_ptr)
        return get_zval_ptr_cv(node, type)
    elif node.get_op_type() == rad.APCFile.ZEND_znode.IS_VAR:
        return get_zval_ptr_var(node, Ts, should_free)
    elif node.get_op_type() == rad.APCFile.ZEND_znode.IS_TMP_VAR:
        return get_zval_ptr_tmp(node, Ts, should_free)
    else:
        raise Exception('Not implemented yet')

def GET_ZVAL_PTR_PTR(node, type, should_free, execute_data):
    if node.get_op_type() == rad.APCFile.ZEND_znode.IS_CV:
        return get_zval_ptr_ptr_cv(node, type)
    elif node.get_op_type() == rad.APCFile.ZEND_znode.IS_VAR:
        return get_zval_ptr_ptr_var(node, execute_data.Ts, should_free)
    elif node.get_op_type() == rad.APCFile.ZEND_znode.IS_UNUSED:
        return _get_obj_zval_ptr_ptr_unused()
    else:
        raise Exception('Not implemented yet')
    return op1

def GET_ZVAL_PTR(node, type, should_free, execute_data):
    if type != BP_VAR_R:
        raise Exception('Not implemented yet')
    if node.get_op_type() == rad.APCFile.ZEND_znode.IS_CONST:
        return zval_utils.zp_stack(node.constant)
    elif node.get_op_type() == rad.APCFile.ZEND_znode.IS_CV:
        return get_zval_ptr_cv(node, type)
    elif node.get_op_type() == rad.APCFile.ZEND_znode.IS_TMP_VAR:
        return get_zval_ptr_tmp(node, execute_data.Ts, should_free)
    elif node.get_op_type() == rad.APCFile.ZEND_znode.IS_VAR:
        return get_zval_ptr_var(node, execute_data.Ts, should_free)
    elif node.get_op_type() == rad.APCFile.ZEND_znode.IS_UNUSED:
        return global_state.null_zval_ptr
        # shouldn't happen
    raise Exception('Not implemented yet')

def GET_OBJ_ZVAL_PTR(node, type, should_free, execute_data):
    if node.get_op_type() == rad.APCFile.ZEND_znode.IS_UNUSED:
        return _get_obj_zval_ptr_unused()
    return GET_ZVAL_PTR(node, type, should_free, execute_data)

def GET_OBJ_ZVAL_PTR_PTR(node, type, should_free, execute_data):
    if node.get_op_type() == rad.APCFile.ZEND_znode.IS_UNUSED:
        return _get_obj_zval_ptr_ptr_unused()
    return GET_ZVAL_PTR_PTR(node, type, should_free, execute_data)

def zend_switch_free(T, extended_value):
    if not T.get_ptr().is_null():
        if extended_value & ZEND_FE_RESET_VARIABLE:
            zval_utils.Z_DELREF_P(T.get_ptr())
        happy_variables.zval_ptr_dtor(T.get_ptr_ptr())
    else:
        raise Exception('Not implemented yet')

def zend_assign_to_string_offset(T, value, value_type):
    assert isinstance(value, objects.zval_ptr)

    if zval_utils.Z_TYPE_P(T.str) == IS_STRING:
        if T.offset < 0:
            raise Exception('Not implemented yet')

        if T.offset >= zval_utils.Z_STRLEN_P(T.str):
            raise Exception('Not implemented yet')

        if zval_utils.Z_TYPE_P(value) != IS_STRING:
            tmp_ptr = zval_utils.zp_stack(zval_utils.zval_copy(value.deref()))

            if value_type != rad.APCFile.ZEND_znode.IS_TMP_VAR:
                happy_variables.zval_copy_ctor(tmp_ptr)
            happy_operators.convert_to_string(tmp_ptr)
            zval_utils.Z_STRVAL_P(T.str).assign(T.offset, zval_utils.Z_STRVAL(tmp_ptr.deref()).get(0))
        else:
            assert(T.offset > 0)
            zval_utils.Z_STRVAL_P(T.str).assign(T.offset, zval_utils.Z_STRVAL_P(value).get(0))
            if value_type == rad.APCFile.ZEND_znode.IS_TMP_VAR:
                raise Exception('Not implemented yet')
    else:
        raise Exception('Not implemented yet')
    return True

def zend_assign_to_variable(variable_ptr_ptr, value, is_tmp_var):
    assert isinstance(variable_ptr_ptr, objects.zval_ptr_ptr)
    variable_ptr = variable_ptr_ptr.deref()

    if variable_ptr == global_state.EG.error_zval_ptr:
        raise Exception('Not implemented yet')

    if (zval_utils.Z_TYPE_P(variable_ptr) == IS_OBJECT and
        zval_utils.Z_OBJ_HT_P(variable_ptr).set):
        raise Exception('Not implemented yet')

    if zval_utils.PZVAL_IS_REF(variable_ptr):
        if variable_ptr != value:
            refcount = zval_utils.Z_REFCOUNT_P(variable_ptr)

            garbage = zval_utils.zval_copy(variable_ptr.deref())
            variable_ptr.assign(value.deref())
            zval_utils.Z_SET_REFCOUNT_P(variable_ptr, refcount)
            zval_utils.Z_SET_ISREF_P(variable_ptr)
            if not is_tmp_var:
                zval_utils.i_zval_copy_ctor(variable_ptr)
            zval_utils.i_zval_dtor(garbage)
            return variable_ptr
    else:
        if zval_utils.Z_DELREF_P(variable_ptr) == 0:
            if not is_tmp_var:
                if variable_ptr == value:
                    raise Exception('Not implemented yet')
                elif zval_utils.PZVAL_IS_REF(value):
                    garbage = zval_utils.zval_copy(variable_ptr.deref())
                    variable_ptr.assign(value.deref())
                    INIT_PZVAL(variable_ptr)
                    happy_variables.zval_copy_ctor(variable_ptr)
                    zval_utils.i_zval_dtor(garbage)
                    return variable_ptr
                else:
                    zval_utils.Z_ADDREF_P(value)
                    variable_ptr_ptr.assign(value)
                    if variable_ptr != global_state.EG.uninitialized_zval_ptr():
                        zval_utils.zval_dtor(variable_ptr)
                    return value
            else:
                garbage = zval_utils.zval_copy(variable_ptr.deref())
                variable_ptr.assign(value.deref())
                INIT_PZVAL(variable_ptr)
                zval_utils.i_zval_dtor(garbage)
                return variable_ptr
        else:
            # we need to split
            if not is_tmp_var:
                if zval_utils.PZVAL_IS_REF(value) and\
                   zval_utils.Z_REFCOUNT_P(value) > 0:
                    variable_ptr = zval_utils.zp_stack(zval_utils.zval_copy(value.deref()))
                    variable_ptr_ptr.assign(variable_ptr)
                    zval_utils.Z_SET_REFCOUNT_P(variable_ptr, 1)
                    happy_variables.zval_copy_ctor(variable_ptr)
                else:
                    variable_ptr_ptr.assign(value)
                    zval_utils.Z_ADDREF_P(value)
            else:
                zval_utils.Z_SET_REFCOUNT_P(value, 1)
                variable_ptr = zval_utils.zp_stack(zval_utils.zval_copy(value.deref()))
                variable_ptr_ptr.assign(variable_ptr)
        zval_utils.Z_UNSET_ISREF_PP(variable_ptr_ptr)

    return variable_ptr_ptr.deref()

def zend_assign_to_object(result, object_ptr, property_name, value_op, Ts, opcode):
    assert isinstance(result, rad.APCFile.ZEND_znode)
    assert isinstance(object_ptr, objects.zval_ptr_ptr)
    assert isinstance(property_name, objects.zval_ptr)
    assert isinstance(value_op, rad.APCFile.ZEND_znode)
    object = object_ptr.deref()
    free_value = tphp.zend_free_op.make_new()
    value = get_zval_ptr(value_op, Ts, free_value, BP_VAR_R)
    retval = Ts[result.var / tphp.ZendExecutor.TEMP_SZ].get_ptr()

    if zval_utils.Z_TYPE_P(object) != IS_OBJECT:
        raise Exception('Not implemented yet')

    if value_op.get_op_type() == rad.APCFile.ZEND_znode.IS_TMP_VAR:
        raise Exception('Not implemented yet')
    elif value_op.get_op_type() == rad.APCFile.ZEND_znode.IS_CONST:
        orig_value = value

        value = zval_utils.zp_stack(zval_utils.zval_copy(orig_value.deref()))
        zval_utils.Z_UNSET_ISREF_P(value)
        zval_utils.Z_SET_REFCOUNT_P(value, 0)
        happy_variables.zval_copy_ctor(value)


    zval_utils.Z_ADDREF_P(value)
    if opcode == ops.ZEND_ASSIGN_OBJ:
        if not zval_utils.Z_OBJ_HT_P(object).write_property:
            raise Exception('Not implemented yet')
        zval_utils.Z_OBJ_HT_P(object).write_property(object, property_name, value)
    else:
        raise Exception('Not implemented yet')

    if not RETURN_VALUE_UNUSED(result) and not global_state.EG.exception:
        raise Exception('Not implemented yet')
    happy_variables.zval_ptr_dtor(zval_utils.zpp_stack(value))
    FREE_OP_IF_VAR(free_value)

def zend_assign_to_variable_reference(variable_ptr_ptr, value_ptr_ptr):
    assert isinstance(variable_ptr_ptr, objects.zval_ptr_ptr)
    assert isinstance(value_ptr_ptr, objects.zval_ptr_ptr)
    EG = global_state.EG

    variable_ptr = variable_ptr_ptr.deref()
    value_ptr = value_ptr_ptr.deref()

    if variable_ptr == EG.error_zval_ptr or value_ptr == EG.error_zval_ptr:
        raise Exception('Not implemented yet')
    elif variable_ptr != value_ptr:
        if not zval_utils.PZVAL_IS_REF(value_ptr):
            zval_utils.Z_DELREF_P(value_ptr)
            if zval_utils.Z_REFCOUNT_P(value_ptr) > 0:
                value_ptr_ptr.assign(zval_utils.zp_stack(zval_utils.make_empty_zval()))
                value_ptr_ptr.deref().assign(value_ptr.deref())
                value_ptr = value_ptr_ptr.deref()
                zval_utils.i_zval_copy_ctor(value_ptr)
            zval_utils.Z_SET_REFCOUNT_P(value_ptr, 1)
            zval_utils.Z_SET_ISREF_P(value_ptr)

        variable_ptr_ptr.assign(value_ptr)
        zval_utils.Z_ADDREF_P(value_ptr)

        happy_variables.zval_ptr_dtor(zval_utils.zpp_stack(variable_ptr))
    elif not zval_utils.Z_ISREF_P(variable_ptr):
        if variable_ptr_ptr == value_ptr_ptr:
            zval_utils.SEPARATE_ZVAL(variable_ptr_ptr)
        else:
            raise Exception('Not implemented yet')
        zval_utils.Z_SET_ISREF_PP(variable_ptr_ptr)

# this should modify object only if it's empty
def make_real_object(object_ptr):
    assert isinstance(object_ptr, objects.zval_ptr_ptr)
    if (zval_utils.Z_TYPE_PP(object_ptr) == IS_NULL or
        (zval_utils.Z_TYPE_PP(object_ptr) == IS_BOOL and zval_utils.Z_LVAL_PP(object_ptr) == 0) or
        (zval_utils.Z_TYPE_PP(object_ptr) == IS_STRING and zval_utils.Z_STRLEN_PP(object_ptr) == 0)):
        raise Exception('Not implemented yet')

def zend_get_target_symbol_table(opline):
    EG = global_state.EG

    if opline.get_op2().EA_type == ZEND_FETCH_LOCAL:
        if not EG.active_symbol_table:
            raise Exception('Not implemented yet')
        return EG.active_symbol_table
    elif opline.get_op2().EA_type == ZEND_FETCH_GLOBAL_LOCK:
        return EG.symbol_table
    elif opline.get_op2().EA_type == ZEND_FETCH_STATIC:
        if not EG.active_op_array.static_variables:
            raise Exception('Not implemented yet')
        return EG.active_op_array.static_variables
    else:
        raise Exception('Not implemented yet')
    return None

def zend_fetch_dimension_address_inner(ht, dim, type):
    retval_ptr = zval_utils.zppp_stack(global_state.null_zval_ptr_ptr)

    if dim.deref().type == IS_LONG:
        index = zval_utils.Z_LVAL_P(dim)
        if happy_hash.zend_hash_index_find(ht, index, retval_ptr) == FAILURE:
            if type == BP_VAR_W:
                new_zval = global_state.EG.uninitialized_zval_ptr()

                zval_utils.Z_ADDREF_P(new_zval)
                happy_hash.zend_hash_index_update(ht, index, zval_utils.zpp_stack(new_zval), 0, retval_ptr)
            else:
                raise Exception('Not implemented yet')
    elif dim.deref().type == IS_STRING:
        offset_key = zval_utils.Z_STRVAL_P(dim)
        offset_key_length = zval_utils.Z_STRLEN_P(dim)
        if happy_hash.zend_symtable_find(ht, offset_key, offset_key_length + 1, retval_ptr) == FAILURE:
            if type == BP_VAR_W:
                new_zval = global_state.EG.uninitialized_zval_ptr()

                zval_utils.Z_ADDREF_P(new_zval)
                happy_hash.zend_symtable_update(ht, offset_key, offset_key_length + 1,
                    zval_utils.zpp_stack(new_zval), 0, retval_ptr)
            else:
                raise Exception('Not implemented yet')
    else:
        raise Exception('Not implemented yet')
    return retval_ptr.deref()

def zend_fetch_dimension_address(result, container_ptr, dim, dim_is_tmp_var, type):
    assert isinstance(container_ptr, objects.zval_ptr_ptr)
    container = container_ptr.deref()
    retval_ptr = zval_utils.zppp_stack(global_state.null_zval_ptr_ptr)

    if zval_utils.Z_TYPE_P(container) == IS_ARRAY or zval_utils.Z_TYPE_P(container) == IS_NULL:
        if zval_utils.Z_TYPE_P(container) == IS_NULL:
            if container == global_state.EG.error_zval_ptr:
                raise Exception('Not implemented yet')
            elif type != BP_VAR_UNSET:
                if not zval_utils.PZVAL_IS_REF(container):
                    zval_utils.SEPARATE_ZVAL(container_ptr)
                    container = container_ptr.deref()
                zval_utils.zval_dtor(container)
                happy_util.array_init(container)
        else:
            if type != BP_VAR_UNSET and zval_utils.Z_REFCOUNT_P(container) > 1 and \
                not zval_utils.PZVAL_IS_REF(container):
                zval_utils.SEPARATE_ZVAL(container_ptr)
                container = container_ptr.deref()
        if dim.is_null():
            new_zval = global_state.EG.uninitialized_zval_ptr()

            zval_utils.Z_ADDREF_P(new_zval)
            if happy_hash.zend_hash_next_index_insert(happy_operators.Z_ARRVAL_P(container),
                zval_utils.zpp_stack(new_zval), 0, retval_ptr) == FAILURE:
                raise Exception('Not implemented yet')
        else:
            retval_ptr.assign(zend_fetch_dimension_address_inner(happy_operators.Z_ARRVAL_P(container), dim, type))
        result.set_ptr_ptr(retval_ptr.deref())
        zval_utils.PZVAL_LOCK(retval_ptr.deref().deref())
        return
    elif zval_utils.Z_TYPE_P(container) == IS_STRING:
        if type != BP_VAR_UNSET and zval_utils.Z_STRLEN_P(container) == 0:
            raise Exception('Not implemented yet')
        if dim.is_null():
            raise Exception('Not implemented yet')

        if zval_utils.Z_TYPE_P(dim) != IS_LONG:
            raise Exception('Not implemented yet')
        if type != BP_VAR_UNSET:
            zval_utils.SEPARATE_ZVAL_IF_NOT_REF(container_ptr)
        container = container_ptr.deref()
        result.str = container
        zval_utils.PZVAL_LOCK(container)
        result.offset = zval_utils.Z_LVAL_P(dim)
        result.set_ptr_ptr(global_state.null_zval_ptr_ptr)
        result.set_ptr(global_state.null_zval_ptr)
        return
    else:
        raise Exception('Not implemented yet')
    raise Exception('Not implemented yet')

def zend_fetch_dimension_address_read(result, container_ptr, dim, dim_is_tmp_var, type):
    assert isinstance(container_ptr, objects.zval_ptr_ptr)
    container = container_ptr.deref()

    if zval_utils.Z_TYPE_P(container) == IS_ARRAY:
        retval = zend_fetch_dimension_address_inner(happy_operators.Z_ARRVAL_P(container), dim, type)
        if result:
            zval_utils.AI_SET_PTR(result, retval.deref())
            zval_utils.PZVAL_LOCK(retval.deref())
        return
    else:
        raise Exception('Not implemented yet')

def zend_brk_cont(nest_levels_zval, array_offset, op_array, Ts):
    nest_levels = 0
    jmp_to = None

    if nest_levels_zval.deref().type != IS_LONG:
        raise Exception('Not implemented yet')
    else:
        nest_levels = nest_levels_zval.deref().lval
    original_nest_levels = nest_levels
    while nest_levels > 0:
        if array_offset == -1:
            raise Exception('Not implemented yet')
        jmp_to = op_array.brk_cont_array[array_offset]
        if nest_levels > 1:
            brk_opline = op_array.opcodes[jmp_to.brk.int_value]

            if brk_opline.opcode == ops.ZEND_SWITCH_FREE:
                raise Exception('Not implemented yet')
            elif brk_opline.opcode == ops.ZEND_FREE:
                raise Exception('Not implemented yet')
        array_offset = jmp_to.parent_elem.int_value
        nest_levels -= 1
    return jmp_to

def zend_lookup_class_ex(name, name_length, use_autoload):
    if not name or not name_length:
        return FAILURE, None

    lc_name = objects.MutableString(name.to_str().lower())

    if lc_name in global_state.EG.class_table:
        return SUCCESS, global_state.EG.class_table[lc_name]

    raise Exception('Not implemented yet')

def zend_fetch_class(class_name, class_name_len, fetch_type):
    use_autoload = (fetch_type & ZEND_FETCH_CLASS_NO_AUTOLOAD) == 0

    fetch_type &= ZEND_FETCH_CLASS_MASK

    if fetch_type == ZEND_FETCH_CLASS_SELF:
        if happy_util.is_null_struct(global_state.EG.scope):
            zend_error(E_ERROR, 'Cannot access self:: when no class scope is active')
        return global_state.EG.scope
    elif fetch_type == ZEND_FETCH_CLASS_PARENT:
        if happy_util.is_null_struct(global_state.EG.scope):
            raise Exception('Not implemented yet')
        if happy_util.is_null_struct(global_state.EG.scope.parent):
            raise Exception('Not implemented yet')
        return global_state.EG.scope.parent
    elif fetch_type == ZEND_FETCH_CLASS_STATIC:
        raise Exception('Not implemented yet')
    elif fetch_type == ZEND_FETCH_CLASS_AUTO:
        raise Exception('Not implemented yet')

    status, ce = zend_lookup_class_ex(class_name, class_name_len, use_autoload)
    if status == FAILURE:
        raise Exception('Not implemented yet')
    return ce
