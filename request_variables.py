from zend import *
import global_state
import happy_hash
import happy_variables
from objects import MutableString
import zval_utils


def request_build_argv(SG):
    argc = zval_utils.zp_stack(zval_utils.make_empty_zval())
    zval_utils.Z_SET_TYPE_P(argc, IS_LONG)
    zval_utils.Z_SET_LVAL_P(argc, SG.request_info.argc)

    if SG.request_info.argc:
        zval_utils.Z_ADDREF_P(argc)
        happy_hash.zend_hash_add(global_state.EG.symbol_table, MutableString('argc'), 4,
            zval_utils.zpp_stack(argc), 0, global_state.null_zval_ptr_ptr_ptr)
    happy_variables.zval_ptr_dtor(zval_utils.zpp_stack(argc))


def request_hash_environment(SG):
    request_build_argv(SG)
