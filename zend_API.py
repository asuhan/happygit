from zend import *
import global_state
import zval_utils
import happy_hash
import happy_operators
from happy_variables import zval_copy_property_ctor
import zend_objects
import objects

class zend_fcall_info:
    def __init__(self):
        self.function_table = None
        self.function_name = global_state.null_zval_ptr
        self.symbol_table = None
        self.retval_ptr_ptr = global_state.null_zval_ptr_ptr
        self.param_count = 0
        self.params = None
        self.object_ptr = global_state.null_zval_ptr
        self.no_separation = False

class zend_fcall_info_cache:
    def __init__(self):
        self.initialized = False
        self.function_handler = None
        self.calling_scope = None
        self.called_scope = None
        self.object_ptr = global_state.null_zval_ptr

def CE_STATIC_MEMBERS(ce):
    return ce.static_members

def zend_get_class_entry(zobject):
    assert isinstance(zobject, objects.zval_ptr)
    if zval_utils.Z_OBJ_HT_P(zobject).get_class_entry:
        return zval_utils.Z_OBJ_HT_P(zobject).get_class_entry(zobject)
    else:
        raise Exception('Not implemented yet')

def zend_get_object_classname(object):
    if not zval_utils.Z_OBJ_HT_P(object).get_class_name:
        raise Exception('Not implemented yet')
    err, class_name = zval_utils.Z_OBJ_HT_P(object).get_class_name(object, False)
    if err:
        raise Exception('Not implemented yet')
    return False, class_name

def zend_update_class_constants(class_type):
    # TODO
    pass

def object_and_properties_init(arg, class_type, properties):
    tmp_ptr = zval_utils.zpp_stack(global_state.null_zval_ptr)
    object_ptr = objects.zend_object_ptr(None)

    if class_type.ce_flags & (ZEND_ACC_INTERFACE | ZEND_ACC_IMPLICIT_ABSTRACT_CLASS
        | ZEND_ACC_EXPLICIT_ABSTRACT_CLASS):
        raise Exception('Not implemented yet')

    zend_update_class_constants(class_type)

    zval_utils.Z_SET_TYPE_P(arg, IS_OBJECT)
    if not class_type.create_object:
        zval_utils.Z_SET_OBJVAL_P(arg, zend_objects.zend_objects_new(object_ptr, class_type))
        if properties:
            raise Exception('Not implemented yet')
        else:
            # TODO
            object = object_ptr.deref()
            object.properties = happy_hash.zend_hash_init(None, 0, None, None, False)
            happy_hash.zend_hash_copy(object.properties, class_type.default_properties,
                zval_copy_property_ctor(class_type), tmp_ptr, 0)
    else:
        raise Exception('Not implemented yet')
    return SUCCESS

def object_init_ex(arg, class_type):
    return object_and_properties_init(arg, class_type, None)

def HASH_OF(p):
    if zval_utils.Z_TYPE_P(p) == IS_ARRAY:
        return happy_operators.Z_ARRVAL_P(p)
    elif zval_utils.Z_TYPE_P(p) == IS_OBJECT:
        raise Exception('Not implemented yet')
    return None
