#include <cstdio>

#include <runtime/base/string_data.h>

extern "C" {

HPHP::StringData *new_string_data(char *s) {
    HPHP::StringData *sd = NEW(HPHP::StringData)(s, HPHP::AttachString);
    sd->setRefCount(1);
    return sd;
}

void delete_string_data(HPHP::StringData *sd) {
    delete sd;
}

char string_data_get_char(HPHP::StringData *sd, int idx) {
    return sd->getChar(idx)->data()[0];
}

void string_data_set_char(HPHP::StringData *sd, int idx, char ch) {
    sd->setChar(idx, ch);
}

const char *string_data_to_str(HPHP::StringData *sd) {
    return sd->data();
}

HPHP::int64 string_data_get_hash(HPHP::StringData *sd) {
    return sd->hash();
}

HPHP::StringData *string_data_copy(HPHP::StringData *sd) {
    return sd->copy();
}

void string_data_append(HPHP::StringData *lsd, HPHP::StringData *rsd) {
    lsd->append(rsd->data(), rsd->size());
}

int string_data_get_length(HPHP::StringData *sd) {
    return sd->size();
}

struct NumericValue {
    HPHP::int64 lval;
    double dval;
};

int string_data_is_numeric(HPHP::StringData *sd,
                           struct NumericValue *val,
                           int allow_errors) {
    return sd->isNumericWithVal(val->lval, val->dval, allow_errors);
}

void string_data_increment(HPHP::StringData *sd) {
    sd->inc();
}

}

