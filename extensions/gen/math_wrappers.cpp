#include "happy_ext.h"
#include <runtime/base/complex_types.h>
#include <runtime/ext/ext_math.h>

using namespace HAPPY;

HAPPY_EXT(pi) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  if (param_count >= 0)   ASSIGN_RETURN_VALUE(HPHP::f_pi());
  EXT_EPILOGUE();
}

HAPPY_EXT(min) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  HPHP::Array p;
  for (int i = 1; i < param_count; i++)
    p.append(params[i]);
  if (param_count >= 1)   ASSIGN_RETURN_VALUE(HPHP::f_min(param_count, a0, p));
  EXT_EPILOGUE();
}

HAPPY_EXT(max) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  HPHP::Array p;
  for (int i = 1; i < param_count; i++)
    p.append(params[i]);
  if (param_count >= 1)   ASSIGN_RETURN_VALUE(HPHP::f_max(param_count, a0, p));
  EXT_EPILOGUE();
}

HAPPY_EXT(abs) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count >= 1)   ASSIGN_RETURN_VALUE(HPHP::f_abs(a0));
  EXT_EPILOGUE();
}

HAPPY_EXT(is_finite) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count >= 1)   ASSIGN_RETURN_VALUE(HPHP::f_is_finite(a0));
  EXT_EPILOGUE();
}

HAPPY_EXT(is_infinite) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count >= 1)   ASSIGN_RETURN_VALUE(HPHP::f_is_infinite(a0));
  EXT_EPILOGUE();
}

HAPPY_EXT(is_nan) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count >= 1)   ASSIGN_RETURN_VALUE(HPHP::f_is_nan(a0));
  EXT_EPILOGUE();
}

HAPPY_EXT(ceil) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count >= 1)   ASSIGN_RETURN_VALUE(HPHP::f_ceil(a0));
  EXT_EPILOGUE();
}

HAPPY_EXT(floor) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count >= 1)   ASSIGN_RETURN_VALUE(HPHP::f_floor(a0));
  EXT_EPILOGUE();
}

HAPPY_EXT(round) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count == 1)   ASSIGN_RETURN_VALUE(HPHP::f_round(a0));
  HPHP::Variant a1(params[1]);
  if (param_count >= 2)   ASSIGN_RETURN_VALUE(HPHP::f_round(a0, a1));
  EXT_EPILOGUE();
}

HAPPY_EXT(deg2rad) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count >= 1)   ASSIGN_RETURN_VALUE(HPHP::f_deg2rad(a0));
  EXT_EPILOGUE();
}

HAPPY_EXT(rad2deg) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count >= 1)   ASSIGN_RETURN_VALUE(HPHP::f_rad2deg(a0));
  EXT_EPILOGUE();
}

HAPPY_EXT(decbin) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count >= 1)   ASSIGN_RETURN_VALUE(HPHP::f_decbin(a0));
  EXT_EPILOGUE();
}

HAPPY_EXT(dechex) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count >= 1)   ASSIGN_RETURN_VALUE(HPHP::f_dechex(a0));
  EXT_EPILOGUE();
}

HAPPY_EXT(decoct) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count >= 1)   ASSIGN_RETURN_VALUE(HPHP::f_decoct(a0));
  EXT_EPILOGUE();
}

HAPPY_EXT(bindec) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count >= 1)   ASSIGN_RETURN_VALUE(HPHP::f_bindec(a0));
  EXT_EPILOGUE();
}

HAPPY_EXT(hexdec) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count >= 1)   ASSIGN_RETURN_VALUE(HPHP::f_hexdec(a0));
  EXT_EPILOGUE();
}

HAPPY_EXT(octdec) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count >= 1)   ASSIGN_RETURN_VALUE(HPHP::f_octdec(a0));
  EXT_EPILOGUE();
}

HAPPY_EXT(base_convert) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  HPHP::Variant a1(params[1]);
  HPHP::Variant a2(params[2]);
  if (param_count >= 3)   ASSIGN_RETURN_VALUE(HPHP::f_base_convert(a0, a1, a2));
  EXT_EPILOGUE();
}

HAPPY_EXT(pow) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  HPHP::Variant a1(params[1]);
  if (param_count >= 2)   ASSIGN_RETURN_VALUE(HPHP::f_pow(a0, a1));
  EXT_EPILOGUE();
}

HAPPY_EXT(exp) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count >= 1)   ASSIGN_RETURN_VALUE(HPHP::f_exp(a0));
  EXT_EPILOGUE();
}

HAPPY_EXT(expm1) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count >= 1)   ASSIGN_RETURN_VALUE(HPHP::f_expm1(a0));
  EXT_EPILOGUE();
}

HAPPY_EXT(log10) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count >= 1)   ASSIGN_RETURN_VALUE(HPHP::f_log10(a0));
  EXT_EPILOGUE();
}

HAPPY_EXT(log1p) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count >= 1)   ASSIGN_RETURN_VALUE(HPHP::f_log1p(a0));
  EXT_EPILOGUE();
}

HAPPY_EXT(log) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count == 1)   ASSIGN_RETURN_VALUE(HPHP::f_log(a0));
  HPHP::Variant a1(params[1]);
  if (param_count >= 2)   ASSIGN_RETURN_VALUE(HPHP::f_log(a0, a1));
  EXT_EPILOGUE();
}

HAPPY_EXT(cos) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count >= 1)   ASSIGN_RETURN_VALUE(HPHP::f_cos(a0));
  EXT_EPILOGUE();
}

HAPPY_EXT(cosh) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count >= 1)   ASSIGN_RETURN_VALUE(HPHP::f_cosh(a0));
  EXT_EPILOGUE();
}

HAPPY_EXT(sin) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count >= 1)   ASSIGN_RETURN_VALUE(HPHP::f_sin(a0));
  EXT_EPILOGUE();
}

HAPPY_EXT(sinh) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count >= 1)   ASSIGN_RETURN_VALUE(HPHP::f_sinh(a0));
  EXT_EPILOGUE();
}

HAPPY_EXT(tan) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count >= 1)   ASSIGN_RETURN_VALUE(HPHP::f_tan(a0));
  EXT_EPILOGUE();
}

HAPPY_EXT(tanh) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count >= 1)   ASSIGN_RETURN_VALUE(HPHP::f_tanh(a0));
  EXT_EPILOGUE();
}

HAPPY_EXT(acos) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count >= 1)   ASSIGN_RETURN_VALUE(HPHP::f_acos(a0));
  EXT_EPILOGUE();
}

HAPPY_EXT(acosh) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count >= 1)   ASSIGN_RETURN_VALUE(HPHP::f_acosh(a0));
  EXT_EPILOGUE();
}

HAPPY_EXT(asin) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count >= 1)   ASSIGN_RETURN_VALUE(HPHP::f_asin(a0));
  EXT_EPILOGUE();
}

HAPPY_EXT(asinh) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count >= 1)   ASSIGN_RETURN_VALUE(HPHP::f_asinh(a0));
  EXT_EPILOGUE();
}

HAPPY_EXT(atan) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count >= 1)   ASSIGN_RETURN_VALUE(HPHP::f_atan(a0));
  EXT_EPILOGUE();
}

HAPPY_EXT(atanh) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count >= 1)   ASSIGN_RETURN_VALUE(HPHP::f_atanh(a0));
  EXT_EPILOGUE();
}

HAPPY_EXT(atan2) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  HPHP::Variant a1(params[1]);
  if (param_count >= 2)   ASSIGN_RETURN_VALUE(HPHP::f_atan2(a0, a1));
  EXT_EPILOGUE();
}

HAPPY_EXT(hypot) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  HPHP::Variant a1(params[1]);
  if (param_count >= 2)   ASSIGN_RETURN_VALUE(HPHP::f_hypot(a0, a1));
  EXT_EPILOGUE();
}

HAPPY_EXT(fmod) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  HPHP::Variant a1(params[1]);
  if (param_count >= 2)   ASSIGN_RETURN_VALUE(HPHP::f_fmod(a0, a1));
  EXT_EPILOGUE();
}

HAPPY_EXT(sqrt) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count >= 1)   ASSIGN_RETURN_VALUE(HPHP::f_sqrt(a0));
  EXT_EPILOGUE();
}

HAPPY_EXT(getrandmax) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  if (param_count >= 0)   ASSIGN_RETURN_VALUE(HPHP::f_getrandmax());
  EXT_EPILOGUE();
}

HAPPY_EXT(srand) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  if (param_count == 0)   HPHP::f_srand();
  HPHP::Variant a0(params[0]);
  if (param_count >= 1)   HPHP::f_srand(a0);
  EXT_EPILOGUE();
}

HAPPY_EXT(rand) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  if (param_count == 0)   ASSIGN_RETURN_VALUE(HPHP::f_rand());
  HPHP::Variant a0(params[0]);
  if (param_count == 1)   ASSIGN_RETURN_VALUE(HPHP::f_rand(a0));
  HPHP::Variant a1(params[1]);
  if (param_count >= 2)   ASSIGN_RETURN_VALUE(HPHP::f_rand(a0, a1));
  EXT_EPILOGUE();
}

HAPPY_EXT(mt_getrandmax) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  if (param_count >= 0)   ASSIGN_RETURN_VALUE(HPHP::f_mt_getrandmax());
  EXT_EPILOGUE();
}

HAPPY_EXT(mt_srand) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  if (param_count == 0)   HPHP::f_mt_srand();
  HPHP::Variant a0(params[0]);
  if (param_count >= 1)   HPHP::f_mt_srand(a0);
  EXT_EPILOGUE();
}

HAPPY_EXT(mt_rand) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  if (param_count == 0)   ASSIGN_RETURN_VALUE(HPHP::f_mt_rand());
  HPHP::Variant a0(params[0]);
  if (param_count == 1)   ASSIGN_RETURN_VALUE(HPHP::f_mt_rand(a0));
  HPHP::Variant a1(params[1]);
  if (param_count >= 2)   ASSIGN_RETURN_VALUE(HPHP::f_mt_rand(a0, a1));
  EXT_EPILOGUE();
}

HAPPY_EXT(lcg_value) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  if (param_count >= 0)   ASSIGN_RETURN_VALUE(HPHP::f_lcg_value());
  EXT_EPILOGUE();
}


