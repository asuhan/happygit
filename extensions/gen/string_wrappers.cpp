#include "happy_ext.h"
#include <runtime/base/complex_types.h>
#include <runtime/ext/ext_string.h>

using namespace HAPPY;

HAPPY_EXT(addcslashes) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  HPHP::Variant a1(params[1]);
  if (param_count >= 2)   ASSIGN_RETURN_VALUE(HPHP::f_addcslashes(a0, a1));
  EXT_EPILOGUE();
}

HAPPY_EXT(stripcslashes) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count >= 1)   ASSIGN_RETURN_VALUE(HPHP::f_stripcslashes(a0));
  EXT_EPILOGUE();
}

HAPPY_EXT(addslashes) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count >= 1)   ASSIGN_RETURN_VALUE(HPHP::f_addslashes(a0));
  EXT_EPILOGUE();
}

HAPPY_EXT(stripslashes) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count >= 1)   ASSIGN_RETURN_VALUE(HPHP::f_stripslashes(a0));
  EXT_EPILOGUE();
}

HAPPY_EXT(bin2hex) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count >= 1)   ASSIGN_RETURN_VALUE(HPHP::f_bin2hex(a0));
  EXT_EPILOGUE();
}

HAPPY_EXT(hex2bin) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count >= 1)   ASSIGN_RETURN_VALUE(HPHP::f_hex2bin(a0));
  EXT_EPILOGUE();
}

HAPPY_EXT(nl2br) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count >= 1)   ASSIGN_RETURN_VALUE(HPHP::f_nl2br(a0));
  EXT_EPILOGUE();
}

HAPPY_EXT(quotemeta) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count >= 1)   ASSIGN_RETURN_VALUE(HPHP::f_quotemeta(a0));
  EXT_EPILOGUE();
}

HAPPY_EXT(str_shuffle) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count >= 1)   ASSIGN_RETURN_VALUE(HPHP::f_str_shuffle(a0));
  EXT_EPILOGUE();
}

HAPPY_EXT(strrev) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count >= 1)   ASSIGN_RETURN_VALUE(HPHP::f_strrev(a0));
  EXT_EPILOGUE();
}

HAPPY_EXT(strtolower) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count >= 1)   ASSIGN_RETURN_VALUE(HPHP::f_strtolower(a0));
  EXT_EPILOGUE();
}

HAPPY_EXT(strtoupper) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count >= 1)   ASSIGN_RETURN_VALUE(HPHP::f_strtoupper(a0));
  EXT_EPILOGUE();
}

HAPPY_EXT(ucfirst) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count >= 1)   ASSIGN_RETURN_VALUE(HPHP::f_ucfirst(a0));
  EXT_EPILOGUE();
}

HAPPY_EXT(ucwords) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count >= 1)   ASSIGN_RETURN_VALUE(HPHP::f_ucwords(a0));
  EXT_EPILOGUE();
}

HAPPY_EXT(strip_tags) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count == 1)   ASSIGN_RETURN_VALUE(HPHP::f_strip_tags(a0));
  HPHP::Variant a1(params[1]);
  if (param_count >= 2)   ASSIGN_RETURN_VALUE(HPHP::f_strip_tags(a0, a1));
  EXT_EPILOGUE();
}

HAPPY_EXT(trim) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count == 1)   ASSIGN_RETURN_VALUE(HPHP::f_trim(a0));
  HPHP::Variant a1(params[1]);
  if (param_count >= 2)   ASSIGN_RETURN_VALUE(HPHP::f_trim(a0, a1));
  EXT_EPILOGUE();
}

HAPPY_EXT(ltrim) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count == 1)   ASSIGN_RETURN_VALUE(HPHP::f_ltrim(a0));
  HPHP::Variant a1(params[1]);
  if (param_count >= 2)   ASSIGN_RETURN_VALUE(HPHP::f_ltrim(a0, a1));
  EXT_EPILOGUE();
}

HAPPY_EXT(rtrim) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count == 1)   ASSIGN_RETURN_VALUE(HPHP::f_rtrim(a0));
  HPHP::Variant a1(params[1]);
  if (param_count >= 2)   ASSIGN_RETURN_VALUE(HPHP::f_rtrim(a0, a1));
  EXT_EPILOGUE();
}

HAPPY_EXT(chop) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count == 1)   ASSIGN_RETURN_VALUE(HPHP::f_chop(a0));
  HPHP::Variant a1(params[1]);
  if (param_count >= 2)   ASSIGN_RETURN_VALUE(HPHP::f_chop(a0, a1));
  EXT_EPILOGUE();
}

HAPPY_EXT(explode) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  HPHP::Variant a1(params[1]);
  if (param_count == 2)   ASSIGN_RETURN_VALUE(HPHP::f_explode(a0, a1));
  HPHP::Variant a2(params[2]);
  if (param_count >= 3)   ASSIGN_RETURN_VALUE(HPHP::f_explode(a0, a1, a2));
  EXT_EPILOGUE();
}

HAPPY_EXT(implode) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count == 1)   ASSIGN_RETURN_VALUE(HPHP::f_implode(a0));
  HPHP::Variant a1(params[1]);
  if (param_count >= 2)   ASSIGN_RETURN_VALUE(HPHP::f_implode(a0, a1));
  EXT_EPILOGUE();
}

HAPPY_EXT(join) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count == 1)   ASSIGN_RETURN_VALUE(HPHP::f_join(a0));
  HPHP::Variant a1(params[1]);
  if (param_count >= 2)   ASSIGN_RETURN_VALUE(HPHP::f_join(a0, a1));
  EXT_EPILOGUE();
}

HAPPY_EXT(str_split) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count == 1)   ASSIGN_RETURN_VALUE(HPHP::f_str_split(a0));
  HPHP::Variant a1(params[1]);
  if (param_count >= 2)   ASSIGN_RETURN_VALUE(HPHP::f_str_split(a0, a1));
  EXT_EPILOGUE();
}

HAPPY_EXT(chunk_split) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count == 1)   ASSIGN_RETURN_VALUE(HPHP::f_chunk_split(a0));
  HPHP::Variant a1(params[1]);
  if (param_count == 2)   ASSIGN_RETURN_VALUE(HPHP::f_chunk_split(a0, a1));
  HPHP::Variant a2(params[2]);
  if (param_count >= 3)   ASSIGN_RETURN_VALUE(HPHP::f_chunk_split(a0, a1, a2));
  EXT_EPILOGUE();
}

HAPPY_EXT(str_replace) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  HPHP::Variant a1(params[1]);
  HPHP::Variant a2(params[2]);
  if (param_count == 3)   ASSIGN_RETURN_VALUE(HPHP::f_str_replace(a0, a1, a2));
  HPHP::Variant a3(params[3]);
  if (param_count >= 4)   ASSIGN_RETURN_VALUE(HPHP::f_str_replace(a0, a1, a2, vref(a3)));
  EXT_EPILOGUE();
}

HAPPY_EXT(str_ireplace) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  HPHP::Variant a1(params[1]);
  HPHP::Variant a2(params[2]);
  if (param_count == 3)   ASSIGN_RETURN_VALUE(HPHP::f_str_ireplace(a0, a1, a2));
  HPHP::Variant a3(params[3]);
  if (param_count >= 4)   ASSIGN_RETURN_VALUE(HPHP::f_str_ireplace(a0, a1, a2, vref(a3)));
  EXT_EPILOGUE();
}

HAPPY_EXT(substr_replace) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  HPHP::Variant a1(params[1]);
  HPHP::Variant a2(params[2]);
  if (param_count == 3)   ASSIGN_RETURN_VALUE(HPHP::f_substr_replace(a0, a1, a2));
  HPHP::Variant a3(params[3]);
  if (param_count >= 4)   ASSIGN_RETURN_VALUE(HPHP::f_substr_replace(a0, a1, a2, a3));
  EXT_EPILOGUE();
}

HAPPY_EXT(substr) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  HPHP::Variant a1(params[1]);
  if (param_count == 2)   ASSIGN_RETURN_VALUE(HPHP::f_substr(a0, a1));
  HPHP::Variant a2(params[2]);
  if (param_count >= 3)   ASSIGN_RETURN_VALUE(HPHP::f_substr(a0, a1, a2));
  EXT_EPILOGUE();
}

HAPPY_EXT(str_pad) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  HPHP::Variant a1(params[1]);
  if (param_count == 2)   ASSIGN_RETURN_VALUE(HPHP::f_str_pad(a0, a1));
  HPHP::Variant a2(params[2]);
  if (param_count == 3)   ASSIGN_RETURN_VALUE(HPHP::f_str_pad(a0, a1, a2));
  HPHP::Variant a3(params[3]);
  if (param_count >= 4)   ASSIGN_RETURN_VALUE(HPHP::f_str_pad(a0, a1, a2, a3));
  EXT_EPILOGUE();
}

HAPPY_EXT(str_repeat) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  HPHP::Variant a1(params[1]);
  if (param_count >= 2)   ASSIGN_RETURN_VALUE(HPHP::f_str_repeat(a0, a1));
  EXT_EPILOGUE();
}

HAPPY_EXT(wordwrap) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count == 1)   ASSIGN_RETURN_VALUE(HPHP::f_wordwrap(a0));
  HPHP::Variant a1(params[1]);
  if (param_count == 2)   ASSIGN_RETURN_VALUE(HPHP::f_wordwrap(a0, a1));
  HPHP::Variant a2(params[2]);
  if (param_count == 3)   ASSIGN_RETURN_VALUE(HPHP::f_wordwrap(a0, a1, a2));
  HPHP::Variant a3(params[3]);
  if (param_count >= 4)   ASSIGN_RETURN_VALUE(HPHP::f_wordwrap(a0, a1, a2, a3));
  EXT_EPILOGUE();
}

HAPPY_EXT(html_entity_decode) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count == 1)   ASSIGN_RETURN_VALUE(HPHP::f_html_entity_decode(a0));
  HPHP::Variant a1(params[1]);
  if (param_count == 2)   ASSIGN_RETURN_VALUE(HPHP::f_html_entity_decode(a0, a1));
  HPHP::Variant a2(params[2]);
  if (param_count >= 3)   ASSIGN_RETURN_VALUE(HPHP::f_html_entity_decode(a0, a1, a2));
  EXT_EPILOGUE();
}

HAPPY_EXT(htmlentities) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count == 1)   ASSIGN_RETURN_VALUE(HPHP::f_htmlentities(a0));
  HPHP::Variant a1(params[1]);
  if (param_count == 2)   ASSIGN_RETURN_VALUE(HPHP::f_htmlentities(a0, a1));
  HPHP::Variant a2(params[2]);
  if (param_count == 3)   ASSIGN_RETURN_VALUE(HPHP::f_htmlentities(a0, a1, a2));
  HPHP::Variant a3(params[3]);
  if (param_count >= 4)   ASSIGN_RETURN_VALUE(HPHP::f_htmlentities(a0, a1, a2, a3));
  EXT_EPILOGUE();
}

HAPPY_EXT(htmlspecialchars_decode) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count == 1)   ASSIGN_RETURN_VALUE(HPHP::f_htmlspecialchars_decode(a0));
  HPHP::Variant a1(params[1]);
  if (param_count >= 2)   ASSIGN_RETURN_VALUE(HPHP::f_htmlspecialchars_decode(a0, a1));
  EXT_EPILOGUE();
}

HAPPY_EXT(htmlspecialchars) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count == 1)   ASSIGN_RETURN_VALUE(HPHP::f_htmlspecialchars(a0));
  HPHP::Variant a1(params[1]);
  if (param_count == 2)   ASSIGN_RETURN_VALUE(HPHP::f_htmlspecialchars(a0, a1));
  HPHP::Variant a2(params[2]);
  if (param_count == 3)   ASSIGN_RETURN_VALUE(HPHP::f_htmlspecialchars(a0, a1, a2));
  HPHP::Variant a3(params[3]);
  if (param_count >= 4)   ASSIGN_RETURN_VALUE(HPHP::f_htmlspecialchars(a0, a1, a2, a3));
  EXT_EPILOGUE();
}

HAPPY_EXT(quoted_printable_encode) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count >= 1)   ASSIGN_RETURN_VALUE(HPHP::f_quoted_printable_encode(a0));
  EXT_EPILOGUE();
}

HAPPY_EXT(quoted_printable_decode) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count >= 1)   ASSIGN_RETURN_VALUE(HPHP::f_quoted_printable_decode(a0));
  EXT_EPILOGUE();
}

HAPPY_EXT(convert_uudecode) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count >= 1)   ASSIGN_RETURN_VALUE(HPHP::f_convert_uudecode(a0));
  EXT_EPILOGUE();
}

HAPPY_EXT(convert_uuencode) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count >= 1)   ASSIGN_RETURN_VALUE(HPHP::f_convert_uuencode(a0));
  EXT_EPILOGUE();
}

HAPPY_EXT(str_rot13) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count >= 1)   ASSIGN_RETURN_VALUE(HPHP::f_str_rot13(a0));
  EXT_EPILOGUE();
}

HAPPY_EXT(crc32) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count >= 1)   ASSIGN_RETURN_VALUE(HPHP::f_crc32(a0));
  EXT_EPILOGUE();
}

HAPPY_EXT(crypt) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count == 1)   ASSIGN_RETURN_VALUE(HPHP::f_crypt(a0));
  HPHP::Variant a1(params[1]);
  if (param_count >= 2)   ASSIGN_RETURN_VALUE(HPHP::f_crypt(a0, a1));
  EXT_EPILOGUE();
}

HAPPY_EXT(md5) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count == 1)   ASSIGN_RETURN_VALUE(HPHP::f_md5(a0));
  HPHP::Variant a1(params[1]);
  if (param_count >= 2)   ASSIGN_RETURN_VALUE(HPHP::f_md5(a0, a1));
  EXT_EPILOGUE();
}

HAPPY_EXT(sha1) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count == 1)   ASSIGN_RETURN_VALUE(HPHP::f_sha1(a0));
  HPHP::Variant a1(params[1]);
  if (param_count >= 2)   ASSIGN_RETURN_VALUE(HPHP::f_sha1(a0, a1));
  EXT_EPILOGUE();
}

HAPPY_EXT(convert_cyr_string) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  HPHP::Variant a1(params[1]);
  HPHP::Variant a2(params[2]);
  if (param_count >= 3)   ASSIGN_RETURN_VALUE(HPHP::f_convert_cyr_string(a0, a1, a2));
  EXT_EPILOGUE();
}

HAPPY_EXT(get_html_translation_table) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  if (param_count == 0)   ASSIGN_RETURN_VALUE(HPHP::f_get_html_translation_table());
  HPHP::Variant a0(params[0]);
  if (param_count == 1)   ASSIGN_RETURN_VALUE(HPHP::f_get_html_translation_table(a0));
  HPHP::Variant a1(params[1]);
  if (param_count >= 2)   ASSIGN_RETURN_VALUE(HPHP::f_get_html_translation_table(a0, a1));
  EXT_EPILOGUE();
}

HAPPY_EXT(hebrev) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count == 1)   ASSIGN_RETURN_VALUE(HPHP::f_hebrev(a0));
  HPHP::Variant a1(params[1]);
  if (param_count >= 2)   ASSIGN_RETURN_VALUE(HPHP::f_hebrev(a0, a1));
  EXT_EPILOGUE();
}

HAPPY_EXT(hebrevc) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count == 1)   ASSIGN_RETURN_VALUE(HPHP::f_hebrevc(a0));
  HPHP::Variant a1(params[1]);
  if (param_count >= 2)   ASSIGN_RETURN_VALUE(HPHP::f_hebrevc(a0, a1));
  EXT_EPILOGUE();
}

HAPPY_EXT(setlocale) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  HPHP::Variant a1(params[1]);
  HPHP::Array p;
  for (int i = 2; i < param_count; i++)
    p.append(params[i]);
  if (param_count >= 2)   ASSIGN_RETURN_VALUE(HPHP::f_setlocale(param_count, a0, a1, p));
  EXT_EPILOGUE();
}

HAPPY_EXT(localeconv) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  if (param_count >= 0)   ASSIGN_RETURN_VALUE(HPHP::f_localeconv());
  EXT_EPILOGUE();
}

HAPPY_EXT(nl_langinfo) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count >= 1)   ASSIGN_RETURN_VALUE(HPHP::f_nl_langinfo(a0));
  EXT_EPILOGUE();
}

HAPPY_EXT(printf) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  HPHP::Array p;
  for (int i = 1; i < param_count; i++)
    p.append(params[i]);
  if (param_count >= 1)   ASSIGN_RETURN_VALUE(HPHP::f_printf(param_count, a0, p));
  EXT_EPILOGUE();
}

HAPPY_EXT(vprintf) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  HPHP::Variant a1(params[1]);
  if (param_count >= 2)   ASSIGN_RETURN_VALUE(HPHP::f_vprintf(a0, a1));
  EXT_EPILOGUE();
}

HAPPY_EXT(sprintf) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  HPHP::Array p;
  for (int i = 1; i < param_count; i++)
    p.append(params[i]);
  if (param_count >= 1)   ASSIGN_RETURN_VALUE(HPHP::f_sprintf(param_count, a0, p));
  EXT_EPILOGUE();
}

HAPPY_EXT(vsprintf) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  HPHP::Variant a1(params[1]);
  if (param_count >= 2)   ASSIGN_RETURN_VALUE(HPHP::f_vsprintf(a0, a1));
  EXT_EPILOGUE();
}

HAPPY_EXT(sscanf) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  HPHP::Variant a1(params[1]);
  HPHP::Array p;
  for (int i = 2; i < param_count; i++)
    p.appendRef(params[i]);
  if (param_count >= 2)   ASSIGN_RETURN_VALUE(HPHP::f_sscanf(param_count, a0, a1, p));
  EXT_EPILOGUE();
}

HAPPY_EXT(chr) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count >= 1)   ASSIGN_RETURN_VALUE(HPHP::f_chr(a0));
  EXT_EPILOGUE();
}

HAPPY_EXT(ord) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count >= 1)   ASSIGN_RETURN_VALUE(HPHP::f_ord(a0));
  EXT_EPILOGUE();
}

HAPPY_EXT(money_format) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  HPHP::Variant a1(params[1]);
  if (param_count >= 2)   ASSIGN_RETURN_VALUE(HPHP::f_money_format(a0, a1));
  EXT_EPILOGUE();
}

HAPPY_EXT(number_format) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count == 1)   ASSIGN_RETURN_VALUE(HPHP::f_number_format(a0));
  HPHP::Variant a1(params[1]);
  if (param_count == 2)   ASSIGN_RETURN_VALUE(HPHP::f_number_format(a0, a1));
  HPHP::Variant a2(params[2]);
  if (param_count == 3)   ASSIGN_RETURN_VALUE(HPHP::f_number_format(a0, a1, a2));
  HPHP::Variant a3(params[3]);
  if (param_count >= 4)   ASSIGN_RETURN_VALUE(HPHP::f_number_format(a0, a1, a2, a3));
  EXT_EPILOGUE();
}

HAPPY_EXT(strcmp) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  HPHP::Variant a1(params[1]);
  if (param_count >= 2)   ASSIGN_RETURN_VALUE(HPHP::f_strcmp(a0, a1));
  EXT_EPILOGUE();
}

HAPPY_EXT(strncmp) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  HPHP::Variant a1(params[1]);
  HPHP::Variant a2(params[2]);
  if (param_count >= 3)   ASSIGN_RETURN_VALUE(HPHP::f_strncmp(a0, a1, a2));
  EXT_EPILOGUE();
}

HAPPY_EXT(strnatcmp) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  HPHP::Variant a1(params[1]);
  if (param_count >= 2)   ASSIGN_RETURN_VALUE(HPHP::f_strnatcmp(a0, a1));
  EXT_EPILOGUE();
}

HAPPY_EXT(strcasecmp) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  HPHP::Variant a1(params[1]);
  if (param_count >= 2)   ASSIGN_RETURN_VALUE(HPHP::f_strcasecmp(a0, a1));
  EXT_EPILOGUE();
}

HAPPY_EXT(strncasecmp) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  HPHP::Variant a1(params[1]);
  HPHP::Variant a2(params[2]);
  if (param_count >= 3)   ASSIGN_RETURN_VALUE(HPHP::f_strncasecmp(a0, a1, a2));
  EXT_EPILOGUE();
}

HAPPY_EXT(strnatcasecmp) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  HPHP::Variant a1(params[1]);
  if (param_count >= 2)   ASSIGN_RETURN_VALUE(HPHP::f_strnatcasecmp(a0, a1));
  EXT_EPILOGUE();
}

HAPPY_EXT(strcoll) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  HPHP::Variant a1(params[1]);
  if (param_count >= 2)   ASSIGN_RETURN_VALUE(HPHP::f_strcoll(a0, a1));
  EXT_EPILOGUE();
}

HAPPY_EXT(substr_compare) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  HPHP::Variant a1(params[1]);
  HPHP::Variant a2(params[2]);
  if (param_count == 3)   ASSIGN_RETURN_VALUE(HPHP::f_substr_compare(a0, a1, a2));
  HPHP::Variant a3(params[3]);
  if (param_count == 4)   ASSIGN_RETURN_VALUE(HPHP::f_substr_compare(a0, a1, a2, a3));
  HPHP::Variant a4(params[4]);
  if (param_count >= 5)   ASSIGN_RETURN_VALUE(HPHP::f_substr_compare(a0, a1, a2, a3, a4));
  EXT_EPILOGUE();
}

HAPPY_EXT(strchr) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  HPHP::Variant a1(params[1]);
  if (param_count >= 2)   ASSIGN_RETURN_VALUE(HPHP::f_strchr(a0, a1));
  EXT_EPILOGUE();
}

HAPPY_EXT(strrchr) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  HPHP::Variant a1(params[1]);
  if (param_count >= 2)   ASSIGN_RETURN_VALUE(HPHP::f_strrchr(a0, a1));
  EXT_EPILOGUE();
}

HAPPY_EXT(strstr) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  HPHP::Variant a1(params[1]);
  if (param_count == 2)   ASSIGN_RETURN_VALUE(HPHP::f_strstr(a0, a1));
  HPHP::Variant a2(params[2]);
  if (param_count >= 3)   ASSIGN_RETURN_VALUE(HPHP::f_strstr(a0, a1, a2));
  EXT_EPILOGUE();
}

HAPPY_EXT(stristr) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  HPHP::Variant a1(params[1]);
  if (param_count >= 2)   ASSIGN_RETURN_VALUE(HPHP::f_stristr(a0, a1));
  EXT_EPILOGUE();
}

HAPPY_EXT(strpbrk) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  HPHP::Variant a1(params[1]);
  if (param_count >= 2)   ASSIGN_RETURN_VALUE(HPHP::f_strpbrk(a0, a1));
  EXT_EPILOGUE();
}

HAPPY_EXT(strpos) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  HPHP::Variant a1(params[1]);
  if (param_count == 2)   ASSIGN_RETURN_VALUE(HPHP::f_strpos(a0, a1));
  HPHP::Variant a2(params[2]);
  if (param_count >= 3)   ASSIGN_RETURN_VALUE(HPHP::f_strpos(a0, a1, a2));
  EXT_EPILOGUE();
}

HAPPY_EXT(stripos) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  HPHP::Variant a1(params[1]);
  if (param_count == 2)   ASSIGN_RETURN_VALUE(HPHP::f_stripos(a0, a1));
  HPHP::Variant a2(params[2]);
  if (param_count >= 3)   ASSIGN_RETURN_VALUE(HPHP::f_stripos(a0, a1, a2));
  EXT_EPILOGUE();
}

HAPPY_EXT(strrpos) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  HPHP::Variant a1(params[1]);
  if (param_count == 2)   ASSIGN_RETURN_VALUE(HPHP::f_strrpos(a0, a1));
  HPHP::Variant a2(params[2]);
  if (param_count >= 3)   ASSIGN_RETURN_VALUE(HPHP::f_strrpos(a0, a1, a2));
  EXT_EPILOGUE();
}

HAPPY_EXT(strripos) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  HPHP::Variant a1(params[1]);
  if (param_count == 2)   ASSIGN_RETURN_VALUE(HPHP::f_strripos(a0, a1));
  HPHP::Variant a2(params[2]);
  if (param_count >= 3)   ASSIGN_RETURN_VALUE(HPHP::f_strripos(a0, a1, a2));
  EXT_EPILOGUE();
}

HAPPY_EXT(substr_count) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  HPHP::Variant a1(params[1]);
  if (param_count == 2)   ASSIGN_RETURN_VALUE(HPHP::f_substr_count(a0, a1));
  HPHP::Variant a2(params[2]);
  if (param_count == 3)   ASSIGN_RETURN_VALUE(HPHP::f_substr_count(a0, a1, a2));
  HPHP::Variant a3(params[3]);
  if (param_count >= 4)   ASSIGN_RETURN_VALUE(HPHP::f_substr_count(a0, a1, a2, a3));
  EXT_EPILOGUE();
}

HAPPY_EXT(strspn) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  HPHP::Variant a1(params[1]);
  if (param_count == 2)   ASSIGN_RETURN_VALUE(HPHP::f_strspn(a0, a1));
  HPHP::Variant a2(params[2]);
  if (param_count == 3)   ASSIGN_RETURN_VALUE(HPHP::f_strspn(a0, a1, a2));
  HPHP::Variant a3(params[3]);
  if (param_count >= 4)   ASSIGN_RETURN_VALUE(HPHP::f_strspn(a0, a1, a2, a3));
  EXT_EPILOGUE();
}

HAPPY_EXT(strcspn) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  HPHP::Variant a1(params[1]);
  if (param_count == 2)   ASSIGN_RETURN_VALUE(HPHP::f_strcspn(a0, a1));
  HPHP::Variant a2(params[2]);
  if (param_count == 3)   ASSIGN_RETURN_VALUE(HPHP::f_strcspn(a0, a1, a2));
  HPHP::Variant a3(params[3]);
  if (param_count >= 4)   ASSIGN_RETURN_VALUE(HPHP::f_strcspn(a0, a1, a2, a3));
  EXT_EPILOGUE();
}

HAPPY_EXT(strlen) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count >= 1)   ASSIGN_RETURN_VALUE(HPHP::f_strlen(a0));
  EXT_EPILOGUE();
}

HAPPY_EXT(count_chars) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count == 1)   ASSIGN_RETURN_VALUE(HPHP::f_count_chars(a0));
  HPHP::Variant a1(params[1]);
  if (param_count >= 2)   ASSIGN_RETURN_VALUE(HPHP::f_count_chars(a0, a1));
  EXT_EPILOGUE();
}

HAPPY_EXT(str_word_count) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count == 1)   ASSIGN_RETURN_VALUE(HPHP::f_str_word_count(a0));
  HPHP::Variant a1(params[1]);
  if (param_count == 2)   ASSIGN_RETURN_VALUE(HPHP::f_str_word_count(a0, a1));
  HPHP::Variant a2(params[2]);
  if (param_count >= 3)   ASSIGN_RETURN_VALUE(HPHP::f_str_word_count(a0, a1, a2));
  EXT_EPILOGUE();
}

HAPPY_EXT(levenshtein) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  HPHP::Variant a1(params[1]);
  if (param_count == 2)   ASSIGN_RETURN_VALUE(HPHP::f_levenshtein(a0, a1));
  HPHP::Variant a2(params[2]);
  if (param_count == 3)   ASSIGN_RETURN_VALUE(HPHP::f_levenshtein(a0, a1, a2));
  HPHP::Variant a3(params[3]);
  if (param_count == 4)   ASSIGN_RETURN_VALUE(HPHP::f_levenshtein(a0, a1, a2, a3));
  HPHP::Variant a4(params[4]);
  if (param_count >= 5)   ASSIGN_RETURN_VALUE(HPHP::f_levenshtein(a0, a1, a2, a3, a4));
  EXT_EPILOGUE();
}

HAPPY_EXT(similar_text) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  HPHP::Variant a1(params[1]);
  if (param_count == 2)   ASSIGN_RETURN_VALUE(HPHP::f_similar_text(a0, a1));
  HPHP::Variant a2(params[2]);
  if (param_count >= 3)   ASSIGN_RETURN_VALUE(HPHP::f_similar_text(a0, a1, vref(a2)));
  EXT_EPILOGUE();
}

HAPPY_EXT(soundex) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count >= 1)   ASSIGN_RETURN_VALUE(HPHP::f_soundex(a0));
  EXT_EPILOGUE();
}

HAPPY_EXT(metaphone) {
  EXT_PROLOGUE();  // TODO: assert on number of arguments
  HPHP::Variant a0(params[0]);
  if (param_count == 1)   ASSIGN_RETURN_VALUE(HPHP::f_metaphone(a0));
  HPHP::Variant a1(params[1]);
  if (param_count >= 2)   ASSIGN_RETURN_VALUE(HPHP::f_metaphone(a0, a1));
  EXT_EPILOGUE();
}


