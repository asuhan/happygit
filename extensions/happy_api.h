#ifndef __HAPPY_API_H__
#define __HAPPY_API_H__

namespace HAPPY {

struct HappyAPI {
    long (*get_type)(zval);
    zval (*deref)(zval);
    zval (*new_zval)();
    zval (*copy_zval)(zval);
    long (*to_int)(zval, long);
    double (*to_double)(zval);
    void* (*to_string)(zval);
    HashTable (*to_hash_table)(zval);
    zval (*to_zval_ref)(zval);
    zval (*set_null)(zval);
    zval (*set_int)(zval, long);
    zval (*set_double)(zval, double);
    zval (*set_string)(zval, void*);
    zval (*set_hash)(zval, HashTable);
    zval (*set_zval_ref)(zval, zval);
    long (*get_refcount)(zval);
    zval (*promote_to_ref)(zval);
    zval (*assign_zval)(zval, zval);
    char* (*lltype_malloc)(long);
    void* (*lltype_free)(char*);
    bool (*print_cstr)(char*);
    HashTable (*hash_new)();
    long (*hash_get_size)(HashTable);
    bool (*hash_exists)(HashTable, long, void*, bool);
    zval (*hash_get)(HashTable, long, void*, bool);
    bool (*hash_update)(HashTable, long, void*, bool, zval);
    zval (*hash_add)(HashTable, long, void*, bool, zval);
    bool (*hash_next_insert)(HashTable, zval);
    bool (*hash_del)(HashTable, long, void*, bool);
    HashTable (*hash_copy)(HashTable);
    HashPosition (*hash_get_pos)(HashTable, long, void*, bool);
    HashPosition (*hash_reset)(HashTable);
    HashPosition (*hash_end)(HashTable);
    HashPosition (*hash_move_forward)(HashTable, HashPosition);
    HashPosition (*hash_move_backward)(HashTable, HashPosition);
    zval (*hash_get_pos_data)(HashTable, HashPosition);
    void* (*hash_get_string_key)(HashTable, HashPosition);
    long (*hash_get_int_key)(HashTable, HashPosition);
    bool (*hash_renumber)(HashTable);
    bool (*hash_is_vector)(HashTable);
    bool (*hash_del_pos)(HashTable, HashPosition);
    bool (*hash_prepend)(HashTable, zval);
};


}

#endif // __HAPPY_API_H__
