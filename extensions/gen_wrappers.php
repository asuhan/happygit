<?php

require_once "hphp/idl/base.php";

function output_call($func, $call_args) {
  $call_args_str = implode(", ", $call_args);
  $call = "HPHP::f_{$func['name']}($call_args_str)";

  $ret  = $func['return'];
  if (is_null($ret)) {
    print "  $call;\n";
  } else {
    print "  ASSIGN_RETURN_VALUE($call);\n";
  }
}

$ext = $argv[1];
define('HPHP_TRIM_CHARLIST', "");
require_once "hphp/idl/$ext.idl.php";

print <<<EOT
#include "happy_ext.h"
#include <runtime/base/complex_types.h>
#include <runtime/ext/ext_$ext.h>

using namespace HAPPY;


EOT;

foreach ($funcs as $func) {
  print <<<EOT
HAPPY_EXT({$func['name']}) {
  EXT_PROLOGUE();
EOT;

  $args = $func['args'];
  $flags = $func['flags'];
  $call_args = array();
  print "  // TODO: assert on number of arguments\n";
  if ($flags & VarArgsMask) {
    $call_args[] = "param_count";
  }
  for ($i = 0; $i < count($args); $i++) {
    $arg_type = $args[$i]['type'];
    $has_default = array_key_exists('value', $args[$i]);
    if ($has_default) {
      // Output call with partial arguments
      print "  if (param_count == $i) ";
      output_call($func, $call_args);
    }

    print "  HPHP::Variant a$i(params[$i]);\n";
    if ($arg_type & Reference) {
      $call_args[] = "vref(a$i)";
    } else {
      $call_args[] = "a$i";
    }
  }
  if ($flags & VarArgsMask) {
    if ($flags & RefVariableArguments) {
      $app = "appendRef";
    } else {
      $app = "append";
    }
    print <<<EOT
  HPHP::Array p;
  for (int i = $i; i < param_count; i++)
    p.$app(params[i]);\n
EOT;
    $call_args[] = "p";
  }
  print "  if (param_count >= $i) ";
  output_call($func, $call_args);
  print "  EXT_EPILOGUE();\n}\n\n";
}
?>

