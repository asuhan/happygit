<?php

require_once "hphp/idl/base.php";

$ext = $argv[1];
define('HPHP_TRIM_CHARLIST', "");
require_once "hphp/idl/$ext.idl.php";

print "functions = [\n";

foreach ($funcs as $func) {
  print "  '{$func['name']}',\n";
}
print "]";
?>

