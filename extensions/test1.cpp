#include <cstdio>

#include "happy_ext.h"
#include <runtime/base/complex_types.h>
//#include <runtime/base/array/array_iterator.h>

using namespace HAPPY;

HAPPY_EXT(test1_fn1) {
    EXT_PROLOGUE();
    printf("test1_fn1: %d", param_count);
    for (int i = 0; i < param_count; i++) {
        HPHP::Variant v(params[i]);
        printf(" param%d: %p(%d) = %lld", i, params[i],
               HAPPY_API->get_type(params[i]), v.toInt64());
    }
    printf("\n");
    EXT_EPILOGUE();
}

HAPPY_EXT(test1_fn2) {
    EXT_PROLOGUE();
    printf("test1_fn2: %d", param_count);
    int64_t sum = 0;
    for (int i = 0; i < param_count; i++) {
        HPHP::Variant v(params[i]);
        printf(" param%d: %p(%d) = %lld", i, params[i],
               HAPPY_API->get_type(params[i]), v.toInt64());
        if (v.isInteger())
            sum += v.toInt64();
        /*
        if (v.isArray()) {
            for (HPHP::ArrayIter iter(v); iter; ++iter) {
                HPHP::Variant vv(iter.second());
                if (vv.isInteger())
                    sum += vv.toInt64();
            }
        }
        */
    }
    printf("\n");
    EXT_EPILOGUE();
}

HAPPY_EXT(plus1) {
  EXT_PROLOGUE();
  ASSERT(param_count == 1);
  if (return_value) {
    HPHP::Variant a0(params[0]);
    HPHP::Variant rv(return_value);
    rv.assign(a0.toInt64() + 1);
  }
  EXT_EPILOGUE();
}

