#ifndef __HAPPY_EXT_H__
#define __HAPPY_EXT_H__

#include <cassert>
#include <cstdlib>

namespace HAPPY {

struct HappyAPI;
struct zval_opaque;
struct HashTable_opaque;
struct HashPosition_opaque;

extern struct HappyAPI *HappyAPIFuncs;

typedef zval_opaque *zval;
typedef HashTable_opaque *HashTable;
typedef HashPosition_opaque *HashPosition;

#define ASSERT(x)  assert(x)

#define HAPPY_EXT(x)    extern "C" int happy_ext_ ## x (HAPPY::zval return_value,\
                                                        int param_count,\
                                                        HAPPY::zval *params)

#define HAPPY_API       (HAPPY::HappyAPIFuncs)

#define EXT_PROLOGUE()  

#define ASSIGN_RETURN_VALUE(v)    if (return_value) {\
                                    HPHP::Variant rv(return_value);\
                                    rv.operator=(v);\
                                  } else { (v); }

#define EXT_EPILOGUE()  do { clearCacheMaps(); return 0; } while (0)

char *happy_realloc(const char *p, size_t oldSize, size_t newSize);

extern void clearCacheMaps(); 

}

#include "happy_api.h"

#endif // __HAPPY_EXT_H__

