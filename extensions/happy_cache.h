#ifndef __HAPPY_CACHE_H__
#define __HAPPY_CACHE_H__

namespace HAPPY {

extern HPHP::Variant &getVariantForZVal(zval zv);
extern HPHP::CVarRef getCVarRefForZVal(zval zv);
extern HPHP::ArrayData *getArrayDataForHashTable(HashTable ht);

}

#endif // __HAPPY_CACHE_H__

