#include <cassert>
#include <cstdlib>
#include <cstring>
#include <algorithm>

#include <util/base.h>
#include <runtime/base/types.h>
#include <runtime/base/complex_types.h>
#include <runtime/base/array/happy_array.h>

#include <happy_ext.h>

namespace HAPPY {
    
struct HappyAPI *HappyAPIFuncs = NULL;

extern "C" void *init_happy_api(struct HappyAPI *api) {
    ASSERT(HappyAPIFuncs == NULL);
    HappyAPIFuncs = api;
    return NULL;
}

char *happy_realloc(const char *p, size_t oldSize, size_t newSize) {
    assert(newSize >= oldSize);
    char *q = HAPPY_API->lltype_malloc(newSize);
    memcpy(q, p, oldSize);
    HAPPY_API->lltype_free((char*)p);
    return q;
}

typedef HPHP::hphp_hash_map< zval, HPHP::Variant,
          HPHP::pointer_hash<zval_opaque> > ZValVarMap;
typedef HPHP::hphp_hash_map< HashTable, HPHP::HappyArray,
          HPHP::pointer_hash<HashTable_opaque> > HashArrayMap;
ZValVarMap zvalVarMap;
HashArrayMap htArrayMap;

void clearCacheMaps() {
    zvalVarMap.clear();
    htArrayMap.clear();
}

template< typename K, typename V, typename Map >
static typename Map::iterator cacheInsert(K key, Map &cache) {
    typename Map::iterator it = cache.find(key);
    if (it == cache.end()) {
        it = cache.insert(std::make_pair(key, V(key))).first;
    }
    return it;
}

HPHP::Variant &getVariantForZVal(zval zv) {
    ZValVarMap::iterator it = cacheInsert<
        zval, HPHP::Variant, ZValVarMap>(zv, zvalVarMap);
    return it->second;
}

HPHP::CVarRef getCVarRefForZVal(zval zv) {
    ZValVarMap::iterator it = cacheInsert<
        zval, HPHP::Variant, ZValVarMap>(zv, zvalVarMap);
    return it->second;
}

HPHP::ArrayData *getArrayDataForHashTable(HashTable ht) {
    HashArrayMap::iterator it = cacheInsert<
        HashTable, HPHP::HappyArray, HashArrayMap>(ht, htArrayMap);
    return &it->second;
}

}

