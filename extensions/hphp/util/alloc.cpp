/*
   +----------------------------------------------------------------------+
   | HipHop for PHP                                                       |
   +----------------------------------------------------------------------+
   | Copyright (c) 2010- Facebook, Inc. (http://www.facebook.com)         |
   +----------------------------------------------------------------------+
   | This source file is subject to version 3.01 of the PHP license,      |
   | that is bundled with this package in the file LICENSE, and is        |
   | available through the world-wide-web at the following url:           |
   | http://www.php.net/license/3_01.txt                                  |
   | If you did not receive a copy of the PHP license and are unable to   |
   | obtain it through the world-wide-web, please send a note to          |
   | license@php.net so we can mail you a copy immediately.               |
   +----------------------------------------------------------------------+
*/

#include <sys/mman.h>
#include <sys/user.h>
#include <stdlib.h>
#include <errno.h>
#include "alloc.h"
#include "util.h"

#include <happy_ext.h>

namespace HPHP { namespace Util {
///////////////////////////////////////////////////////////////////////////////

void* safe_malloc(size_t size) {
  void *ptr = HAPPY_API->lltype_malloc(size);

  if (ptr == NULL) {
    throw OutOfMemoryException(size);
  }
  return ptr;
}

void* safe_realloc(void *var, size_t oldSize, size_t newSize) {
  void *ptr = NULL;

  ptr = HAPPY::happy_realloc((char*)var, oldSize, newSize);
  if (ptr == NULL && newSize != 0) {
    throw OutOfMemoryException(newSize);
  }

  return ptr;
}

void safe_free(void *ptr) {
  if (!ptr) {
    HAPPY_API->lltype_free((char*)ptr);
  }
}

void flush_thread_caches() {
}

///////////////////////////////////////////////////////////////////////////////
}}
