/*
   +----------------------------------------------------------------------+
   | HipHop for PHP                                                       |
   +----------------------------------------------------------------------+
   | Copyright (c) 2010- Facebook, Inc. (http://www.facebook.com)         |
   +----------------------------------------------------------------------+
   | This source file is subject to version 3.01 of the PHP license,      |
   | that is bundled with this package in the file LICENSE, and is        |
   | available through the world-wide-web at the following url:           |
   | http://www.php.net/license/3_01.txt                                  |
   | If you did not receive a copy of the PHP license and are unable to   |
   | obtain it through the world-wide-web, please send a note to          |
   | license@php.net so we can mail you a copy immediately.               |
   +----------------------------------------------------------------------+
*/

#ifndef __INSIDE_HPHP_COMPLEX_TYPES_H__
#error Directly including 'type_variant.h' is prohibited. \
       Include 'complex_types.h' instead.
#endif

#ifndef __HPHP_VARIANT_H__
#define __HPHP_VARIANT_H__

#include <runtime/base/types.h>
#include <runtime/base/hphp_value.h>
#include <runtime/base/type_string.h>
//#include <runtime/base/type_object.h>
#include <runtime/base/type_array.h>
#include <runtime/base/memory/smart_allocator.h>
#include <runtime/base/array/array_data.h>

#include <happy_ext.h>
#include <happy_cache.h>

namespace HPHP {
///////////////////////////////////////////////////////////////////////////////

class ArrayIter;
class MutableArrayIter;

/**
 * Perhaps the most important class in the entire runtime. When type inference
 * fails to know type of a variable, or when certain coding requires reference
 * or other dynamic-ness, we have to use Variant as a fallback of a specific
 * type. This normally means slower coding. Conceptually, Variant == zval,
 * in terms of tasks it has to perform. Therefore, this class is taking similar
 * switch(type) approach Zend takes, and this class is pretty much the entire
 * Zend re-implementation in a C++ way, whereas other classes in this library
 * represent type-specialized implementation of the language.
 *
 * Variant is also the only way to implement references. A reference is a
 * strong binding between two variables, meaning they both point to the same
 * underlying data.
 *
 * In this class, strong binding is done through "pvar" member variable. All
 * others are for weak bindings. Primitive types can just make copies, but not
 * strings and arrays, which take a copy-on-write approach. This is done by
 * doing reference counting on pstr and parr members.
 *
 * In summary, we have really different approaches handling different types:
 *
 *           binding  copy-by-value copy-on-write  ref-counting
 *   num     weak      x (data)
 *   dbl     weak      x (data)
 *   str     weak      x (pointer)
 *   pstr    weak      x (pointer)        x             x
 *   parr    weak      x (pointer)        x             x
 *   pobj    weak      x (pointer)                      x
 *   pvar    strong    x (pointer)                      x
 */

#define null ((Variant()))

class Variant {
 public:
  friend class Array;
  friend class VariantVectorBase;

  /**
   * Variant does not formally derive from Countable, however it has a
   * _count field and implements all of the methods from Countable.
   */
  // IMPLEMENT_COUNTABLE_METHODS_NO_STATIC

  /**
   * Constructors. We can't really use template<T> here, since that will make
   * Variant being able to take many other external types, messing up those
   * operator overloads.
   */
  Variant() {
    // TODO: correct to set to null?
    m_zval = HAPPY_API->new_zval();
    HAPPY_API->set_null(m_zval);
  }

  enum NoInit { noInit };
  Variant(NoInit) : m_zval(NULL) {}

  Variant(HAPPY::zval zval) : m_zval(zval) {}

  Variant(bool v) {
    m_zval = HAPPY_API->new_zval();
    set(v);
  }

  Variant(int32 v) {
    m_zval = HAPPY_API->new_zval();
    set(v);
  }

  Variant(int64 v) {
    m_zval = HAPPY_API->new_zval();
    set(v);
  }

  Variant(double v) {
    m_zval = HAPPY_API->new_zval();
    set(v);
  }

  Variant(StringData *v) {
    m_zval = HAPPY_API->new_zval();
    set(v);
  }

  Variant(CStrRef v) {
    m_zval = HAPPY_API->new_zval();
    set(v);
  }

  Variant(CArrRef v) {
    m_zval = HAPPY_API->new_zval();
    set(v);
  }

  Variant(ArrayData *v) {
    m_zval = HAPPY_API->new_zval();
    set(v);
  }

  Variant(CVarRef v) {
    m_zval = HAPPY_API->copy_zval(v.m_zval);
  }

  Variant(CVarStrongBind v) {
    Variant vv(variant(v)); // FIXME: is this efficient???
    PromoteToRef(vv);
    // FIXME: call incRefCount()
    m_zval = HAPPY_API->new_zval();
    HAPPY_API->set_zval_ref(m_zval, vv.m_zval); 
  }

  Variant(CVarWithRefBind v);

 public:

  /**
   * Break bindings and set to null.
   */
  void unset() {
    //if (IS_REFCOUNTED_TYPE(getType())) destruct();
    // FIXME: set to null
    setNull();
  }

  /**
   * set to null without breaking bindings (if any), faster than v_a = null;
   */
  void setNull() {
    HAPPY_API->set_null(m_zval);
  }

  /**
   * Clear the original data, and set it to be the same as in v, and if
   * v is referenced, keep the reference.
   * In order to correctly copy circular arrays, even if v is the only
   * strong reference to arr, we still keep the reference.
   */
  Variant &setWithRef(CVarRef v, const ArrayData *arr = NULL);

  /**
   * Fast accessors that can be used by generated code when type inference can
   * prove that getType() will have a certain value at a given point in time
   */

///////////////////////////////////////////////////////////////////////////////
// int64

  inline ALWAYS_INLINE int64 asInt64Val() const {
    ASSERT(getRawType() == KindOfInt32 || getRawType() == KindOfInt64);
    return HAPPY_API->to_int(m_zval, 10);
  }

  inline ALWAYS_INLINE int64 toInt64Val() const {
    ASSERT(is(KindOfInt32) || is(KindOfInt64));
    return HAPPY_API->to_int(getRefZVal(), 10);
  }

///////////////////////////////////////////////////////////////////////////////
// double

  inline ALWAYS_INLINE double asDoubleVal() const {
    ASSERT(getRawType() == KindOfDouble);
    return HAPPY_API->to_double(m_zval);
  }

  inline ALWAYS_INLINE double toDoubleVal() const {
    ASSERT(is(KindOfDouble));
    return HAPPY_API->to_double(getRefZVal());
  }

///////////////////////////////////////////////////////////////////////////////
// boolean

  inline ALWAYS_INLINE bool asBooleanVal() const {
    ASSERT(getRawType() == KindOfBoolean);
    return asInt64Val() != 0;
  }

  inline ALWAYS_INLINE bool toBooleanVal() const {
    ASSERT(is(KindOfBoolean));
    return toInt64Val() != 0;
  }

///////////////////////////////////////////////////////////////////////////////
// string

  inline ALWAYS_INLINE const String & asCStrRef() const {
    DataType m_type = getRawType();
    ASSERT(m_type == KindOfString || m_type == KindOfStaticString);
    return *(const String*)(this);
  }

  inline ALWAYS_INLINE const String & toCStrRef() const {
    ASSERT(is(KindOfString) || is(KindOfStaticString));
    return *(const String*)(getRefCVar());
  }

  inline ALWAYS_INLINE String & asStrRef() {
    DataType m_type = getRawType();
    ASSERT(m_type == KindOfString || m_type == KindOfStaticString);
    return *(String*)(this);
  }

  inline ALWAYS_INLINE String & toStrRef() {
    ASSERT(is(KindOfString) || is(KindOfStaticString));
    return *(String*)(getRefVar());
  }

///////////////////////////////////////////////////////////////////////////////
// array

  inline ALWAYS_INLINE const Array & asCArrRef() const {
    ASSERT(getRawType() == KindOfArray);
    return *(const Array*)(this);
  }

  inline ALWAYS_INLINE const Array & toCArrRef() const {
    ASSERT(is(KindOfArray));
    return *(const Array*)(getRefCVar());
  }

  inline ALWAYS_INLINE Array & asArrRef() {
    ASSERT(getRawType() == KindOfArray);
    return *(Array*)(this);
  }

  inline ALWAYS_INLINE Array & toArrRef() {
    ASSERT(is(KindOfArray));
    return *(Array*)(getRefVar());
  }

  /**
   * Type testing functions
   */
  DataType getType() const {
    // FIXME: this is expensive: it does 2 calls: one to check if it's a Ref
    // then another call for the actual type
    return (DataType)HAPPY_API->get_type(getRefZVal());
  }
  DataType getRawType() const {
    return (DataType)HAPPY_API->get_type(m_zval);
  }
  bool is(DataType type) const {
    return getType() == type;
  }
  bool isInitialized() const {
    return getRawType() != KindOfUninit;
  }
  bool isNull() const {
    return getType() <= KindOfNull;
  }
  bool isBoolean() const {
    return getType() == KindOfBoolean;
  }
  bool isDouble() const {
    return getType() == KindOfDouble;
  }
  bool isString() const {
    DataType type = getType();
    return type == KindOfStaticString || type == KindOfString;
  }
  bool isInteger() const;
  bool isNumeric(bool checkString = false) const;
  DataType toNumeric(int64 &ival, double &dval, bool checkString = false)
    const;
  bool isScalar() const;
  bool isObject() const {
    return getType() == KindOfObject;
  }
  bool isIntVal() const {
    switch (getRawType()) {
      case KindOfUninit:
      case KindOfNull:
      case KindOfBoolean:
      case KindOfInt32:
      case KindOfInt64:
      case KindOfObject:
        return true;
      case KindOfVariant:
        return getRefCVar()->isIntVal();
      default:
        break;
    }
    return false;
  }
  bool isArray() const {
    return getType() == KindOfArray;
  }
  // Is "define('CONSTANT', <this value>)" legal?
  bool isAllowedAsConstantValue() const {
    return isNull() || isScalar();
  }
  bool isResource() const;

  bool getBoolean() const {
    ASSERT(getType() == KindOfBoolean);
    return getInt64() != 0;
  }
  int64 getInt64() const {
    ASSERT(getType() == KindOfInt32   ||
           getType() == KindOfInt64);
    return HAPPY_API->to_int(getRefZVal(), 10);
  }
  double getDouble() const {
    ASSERT(getType() == KindOfDouble);
    return HAPPY_API->to_double(getRefZVal());
  }

  /**
   * Operators
   */
  Variant &assign(CVarRef v);
  Variant &assignVal(CVarRef v) { return assign(v); }
  Variant &assignRef(CVarRef v);

  Variant &operator=(CVarRef v) {
    return assign(v);
  }
  Variant &operator=(RefResult v) { return assignRef(variant(v)); }
  Variant &operator=(CVarStrongBind v) { return assignRef(variant(v)); }
  Variant &operator=(CVarWithRefBind v) { return setWithRef(variant(v)); }

  template<typename T> Variant &operator=(const T &v) {
    set(v);
    return *this;
  }

  Variant  operator +  () const;
  Variant unary_plus() const { return Variant(*this).operator+();}
  Variant  operator +  (CVarRef v) const;
  Variant &operator += (CVarRef v);
  Variant &operator += (int32   n) { return operator+=((int64)n);}
  Variant &operator += (int64   n);
  Variant &operator += (double  n);

  Variant negate() const { return Variant(*this).operator-();}
  Variant  operator -  () const;
  Variant  operator -  (CVarRef v) const;
  Variant &operator -= (CVarRef v);
  Variant &operator -= (int32   n) { return operator-=((int64)n);}
  Variant &operator -= (int64   n);
  Variant &operator -= (double  n);

  Variant  operator *  (CVarRef v) const;
  Variant &operator *= (CVarRef v);
  Variant &operator *= (int32   n) { return operator*=((int64)n);}
  Variant &operator *= (int64   n);
  Variant &operator *= (double  n);

  Variant  operator /  (CVarRef v) const;
  Variant &operator /= (CVarRef v);
  Variant &operator /= (int32   n) { return operator/=((int64)n);}
  Variant &operator /= (int64   n);
  Variant &operator /= (double  n);

  int64    operator %  (CVarRef v) const;
  Variant &operator %= (CVarRef v);
  Variant &operator %= (int32   n) { return operator%=((int64)n);}
  Variant &operator %= (int64   n);
  Variant &operator %= (double  n);

  Variant  operator ~  () const;
  Variant  operator |  (CVarRef v) const;
  Variant &operator |= (CVarRef v);
  Variant  operator &  (CVarRef v) const;
  Variant &operator &= (CVarRef v);
  Variant  operator ^  (CVarRef v) const;
  Variant &operator ^= (CVarRef v);
  Variant &operator <<=(int64 n);
  Variant &operator >>=(int64 n);

  Variant &operator ++ ();
  Variant  operator ++ (int);
  Variant &operator -- ();
  Variant  operator -- (int);

  /**
   * These are convenient functions for writing extensions, since code
   * generation always uses explicit functions like same(), less() etc. that
   * are type specialized and unambiguous.
   */
  bool operator == (CVarRef v) const;
  bool operator != (CVarRef v) const;
  bool operator >= (CVarRef v) const;
  bool operator <= (CVarRef v) const;
  bool operator >  (CVarRef v) const;
  bool operator <  (CVarRef v) const;

  /**
   * Iterator functions. See array_iterator.h for end() and next().
   */
  ArrayIter begin(CStrRef context = null_string) const;
  // used by generated code
  MutableArrayIter begin(Variant *key, Variant &val,
                         CStrRef context = null_string);

  // Mutable iteration requires the most escalation.
  void escalate(bool mutableIteration = false);

  /**
   * Implicit type conversions. In general, we prefer explicit type conversion
   * functions. These are needed simply because Variant is a coerced type from
   * other types, and we need implicit type conversions to make our type
   * inference coding simpler (Expression::m_expectedType handling).
   */

  operator bool   () const { return toBoolean();}
  operator char   () const { return toByte();}
  operator short  () const { return toInt16();}
  operator int32  () const { return toInt32();}
  operator int64  () const { return toInt64();}
  operator double () const { return toDouble();}
  operator String () const;
  operator Array  () const;

  /**
   * Explicit type conversions
   */
  bool    toBoolean() const {
    if (getType() <= KindOfNull) return false;
    return toInt64() != 0;
  }
  char    toByte   () const { return (char)toInt64();}
  short   toInt16  (int base = 10) const { return (short)toInt64(base);}
  int32   toInt32  (int base = 10) const { return (int)toInt64(base);}
  int64   toInt64  () const {
    if (getRawType() <= KindOfNull) return 0;
    return HAPPY_API->to_int(getRefZVal(), 10);
  }
  int64   toInt64  (int base) const {
    if (getRawType() <= KindOfNull) return 0;
    return HAPPY_API->to_int(getRefZVal(), base);
  }
  double toDouble () const {
    return HAPPY_API->to_double(getRefZVal());
  }
  String toString () const {
    DataType type = getRawType();
    if (type == KindOfStaticString || type == KindOfString) {
      return getRawStringData();
    }
    return toStringHelper();
  }
  Array  toArray  () const {
    if (getRawType() == KindOfArray)
      return getRawArrayData();
    return toArrayHelper();
  }
   HAPPY::zval toZVal() const {
    return m_zval;
  }

  /**
   * Whether or not calling toKey() will throw a bad type exception
   */
  bool  canBeValidKey() const {
    switch (getType()) {
    case KindOfArray:  return false;
    case KindOfObject: return isResource();
    default:           return true;
    }
  }
  VarNR toKey   () const;

  /**
   * Comparisons
   */
  bool same(bool    v2) const;
  bool same(int32   v2) const;
  bool same(int64   v2) const;
  bool same(double  v2) const;
  bool same(litstr  v2) const;
  bool same(const StringData *v2) const;
  bool same(CStrRef v2) const;
  bool same(CArrRef v2) const;
  bool same(CVarRef v2) const;

  bool equal(bool    v2) const;
  bool equal(int32   v2) const;
  bool equal(int64   v2) const;
  bool equal(double  v2) const;
  bool equal(litstr  v2) const;
  bool equal(const StringData *v2) const;
  bool equal(CStrRef v2) const;
  bool equal(CArrRef v2) const;
  bool equal(CVarRef v2) const;

  bool equalAsStr(bool    v2) const;
  bool equalAsStr(int32   v2) const;
  bool equalAsStr(int64   v2) const;
  bool equalAsStr(double  v2) const;
  bool equalAsStr(litstr v2) const;
  bool equalAsStr(const StringData *v2) const;
  bool equalAsStr(CStrRef v2) const;
  bool equalAsStr(CArrRef v2) const;
  bool equalAsStr(CVarRef v2) const;

  bool less(bool    v2) const;
  bool less(int32   v2) const;
  bool less(int64   v2) const;
  bool less(double  v2) const;
  bool less(litstr  v2) const;
  bool less(const StringData *v2) const;
  bool less(CStrRef v2) const;
  bool less(CArrRef v2) const;
  bool less(CVarRef v2) const;

  bool more(bool    v2) const;
  bool more(int32   v2) const;
  bool more(int64   v2) const;
  bool more(double  v2) const;
  bool more(litstr  v2) const;
  bool more(const StringData *v2) const;
  bool more(CStrRef v2) const;
  bool more(CArrRef v2) const;
  bool more(CVarRef v2) const;

  /**
   * Offset functions
   */
  Variant rvalAtHelper(int64 offset, ACCESSPARAMS_DECL) const;
  Variant rvalAt(bool offset, ACCESSPARAMS_DECL) const;
  Variant rvalAt(int offset, ACCESSPARAMS_DECL) const {
    return rvalAt((int64)offset, flags);
  }
  Variant rvalAt(int64 offset, ACCESSPARAMS_DECL) const {
    DataType m_type = getRawType();
    if (m_type == KindOfArray) {
      return getRawArrayData()->get(offset, flags & AccessFlags::Error);
    }
    return rvalAtHelper(offset, flags);
  }
  Variant rvalAt(double offset, ACCESSPARAMS_DECL) const;
  Variant rvalAt(litstr offset, ACCESSPARAMS_DECL) const;
  Variant rvalAt(CStrRef offset, ACCESSPARAMS_DECL) const;
  Variant rvalAt(CVarRef offset, ACCESSPARAMS_DECL) const;

  template <typename T>
  CVarRef rvalRefHelper(T offset, CVarRef tmp, ACCESSPARAMS_DECL) const;
  CVarRef rvalRef(int offset, CVarRef tmp, ACCESSPARAMS_DECL) const {
    return rvalRef((int64)offset, tmp, flags);
  }

  CVarRef rvalRef(int64 offset, CVarRef tmp, ACCESSPARAMS_DECL) const {
    DataType m_type = getRawType();
    if (m_type == KindOfArray) {
      return getRawArrayData()->get(offset, flags & AccessFlags::Error);
    }
    return rvalRefHelper(offset, tmp, flags);
  }
  CVarRef rvalRef(bool offset, CVarRef tmp, ACCESSPARAMS_DECL) const;
  CVarRef rvalRef(double offset, CVarRef tmp, ACCESSPARAMS_DECL) const;
  CVarRef rvalRef(litstr offset, CVarRef tmp, ACCESSPARAMS_DECL) const;
  CVarRef rvalRef(CStrRef offset, CVarRef tmp, ACCESSPARAMS_DECL) const;
  CVarRef rvalRef(CVarRef offset, CVarRef tmp, ACCESSPARAMS_DECL) const;

  // for when we know its an array or null
  template <typename T>
  CVarRef rvalAtRefHelper(T offset, ACCESSPARAMS_DECL) const;
  CVarRef rvalAtRef(bool offset, ACCESSPARAMS_DECL) const {
    return rvalAtRefHelper((int64)offset, flags);
  }
  CVarRef rvalAtRef(int offset, ACCESSPARAMS_DECL) const {
    return rvalAtRefHelper((int64)offset, flags);
  }
  CVarRef rvalAtRef(double offset, ACCESSPARAMS_DECL) const;
  CVarRef rvalAtRef(int64 offset, ACCESSPARAMS_DECL) const {
    return rvalAtRefHelper(offset, flags);
  }
  CVarRef rvalAtRef(CStrRef offset, ACCESSPARAMS_DECL) const {
    return rvalAtRefHelper<CStrRef>(offset, flags);
  }
  CVarRef rvalAtRef(CVarRef offset, ACCESSPARAMS_DECL) const {
    return rvalAtRefHelper<CVarRef>(offset, flags);
  }
  const Variant operator[](bool    key) const { return rvalAt(key);}
  const Variant operator[](int     key) const { return rvalAt(key);}
  const Variant operator[](int64   key) const { return rvalAt(key);}
  const Variant operator[](double  key) const { return rvalAt(key);}
  const Variant operator[](litstr  key) const { return rvalAt(key);}
  const Variant operator[](CStrRef key) const { return rvalAt(key);}
  const Variant operator[](CArrRef key) const { return rvalAt(key);}
  const Variant operator[](CVarRef key) const { return rvalAt(key);}

  template<typename T>
  Variant &lval(const T &key) {
    DataType m_type = getRawType();
    if (m_type == KindOfVariant) {
      return getRefCVar()->lval(key);
    }

    ASSERT(m_type == KindOfArray);
    Variant *ret = NULL;
    ArrayData *arr = getRawArrayData();
    ArrayData *escalated = arr->lval(key, ret, arr->getCount() > 1);
    if (escalated) {
      set(escalated);
    }
    ASSERT(ret);
    return *ret;
  }

  Variant *lvalPtr(CStrRef key, bool forWrite, bool create);

  Variant &lvalAt();

  static Variant &lvalInvalid();
  static Variant &lvalBlackHole();

  Variant &lvalAt(bool    key, ACCESSPARAMS_DECL);
  Variant &lvalAt(int     key, ACCESSPARAMS_DECL);
  Variant &lvalAt(int64   key, ACCESSPARAMS_DECL);
  Variant &lvalAt(double  key, ACCESSPARAMS_DECL);
  Variant &lvalAt(litstr  key, ACCESSPARAMS_DECL);
  Variant &lvalAt(CStrRef key, ACCESSPARAMS_DECL);
  Variant &lvalAt(CVarRef key, ACCESSPARAMS_DECL);

  Variant &lvalRef(bool    key, Variant& tmp, ACCESSPARAMS_DECL);
  Variant &lvalRef(int     key, Variant& tmp, ACCESSPARAMS_DECL);
  Variant &lvalRef(int64   key, Variant& tmp, ACCESSPARAMS_DECL);
  Variant &lvalRef(double  key, Variant& tmp, ACCESSPARAMS_DECL);
  Variant &lvalRef(litstr  key, Variant& tmp, ACCESSPARAMS_DECL);
  Variant &lvalRef(CStrRef key, Variant& tmp, ACCESSPARAMS_DECL);
  Variant &lvalRef(CVarRef key, Variant& tmp, ACCESSPARAMS_DECL);

  Variant refvalAt(bool    key);
  Variant refvalAt(int     key);
  Variant refvalAt(int64   key);
  Variant refvalAt(double  key);
  Variant refvalAt(litstr  key, bool isString = false);
  Variant refvalAt(CStrRef key, bool isString = false);
  Variant refvalAt(CVarRef key);

  Variant argvalAt(bool byRef, bool    key) const;
  Variant argvalAt(bool byRef, int     key) const;
  Variant argvalAt(bool byRef, int64   key) const;
  Variant argvalAt(bool byRef, double  key) const;
  Variant argvalAt(bool byRef, litstr  key,
      bool isString = false) const;
  Variant argvalAt(bool byRef, CStrRef key,
      bool isString = false) const;
  Variant argvalAt(bool byRef, CVarRef key) const;

  template <typename T>
  inline ALWAYS_INLINE static CVarRef SetImpl(
    Variant *self, T key, CVarRef v, bool isKey);

  template <typename T>
  inline ALWAYS_INLINE static CVarRef SetRefImpl(
    Variant *self, T key, CVarRef v, bool isKey);

  CVarRef set(bool    key, CVarRef v);
  CVarRef set(int     key, CVarRef v) { return set((int64)key, v); }
  CVarRef set(int64   key, CVarRef v);
  CVarRef set(double  key, CVarRef v);
  CVarRef set(litstr  key, CVarRef v, bool isString = false) {
    return set(String(key), v, isString);
  }
  CVarRef set(CStrRef key, CVarRef v, bool isString = false);
  CVarRef set(CVarRef key, CVarRef v);

  CVarRef append(CVarRef v);

  CVarRef setRef(bool    key, CVarRef v);
  CVarRef setRef(int     key, CVarRef v) { return setRef((int64)key, v); }
  CVarRef setRef(int64   key, CVarRef v);
  CVarRef setRef(double  key, CVarRef v);
  CVarRef setRef(litstr  key, CVarRef v, bool isString = false) {
    return setRef(String(key), v, isString);
  }
  CVarRef setRef(CStrRef key, CVarRef v, bool isString = false);
  CVarRef setRef(CVarRef key, CVarRef v);

  CVarRef set(bool    key, RefResult v) { return setRef(key, variant(v)); }
  CVarRef set(int     key, RefResult v) { return setRef(key, variant(v)); }
  CVarRef set(int64   key, RefResult v) { return setRef(key, variant(v)); }
  CVarRef set(double  key, RefResult v) { return setRef(key, variant(v)); }
  CVarRef set(litstr  key, RefResult v, bool isString = false) {
    return setRef(key, variant(v), isString);
  }
  CVarRef set(CStrRef key, RefResult v, bool isString = false) {
    return setRef(key, variant(v), isString);
  }
  CVarRef set(CVarRef key, RefResult v) { return setRef(key, variant(v)); }

  CVarRef appendRef(CVarRef v);
  CVarRef append(RefResult v) { return appendRef(variant(v)); }

  CVarRef setOpEqual(int op, bool key, CVarRef v);
  CVarRef setOpEqual(int op, int key, CVarRef v) {
    return setOpEqual(op, (int64)key, v);
  }
  CVarRef setOpEqual(int op, int64 key, CVarRef v);
  CVarRef setOpEqual(int op, double key, CVarRef v);
  CVarRef setOpEqual(int op, litstr  key, CVarRef v, bool isString = false) {
    return setOpEqual(op, String(key), v, isString);
  }
  CVarRef setOpEqual(int op, CStrRef key, CVarRef v, bool isString = false);
  CVarRef setOpEqual(int op, CVarRef key, CVarRef v);
  CVarRef appendOpEqual(int op, CVarRef v);

  template<typename T, int op>
  T o_assign_op(CStrRef propName, CVarRef val, CStrRef context = null_string);

  void remove(bool    key) { removeImpl(key);}
  void remove(int     key) { removeImpl((int64)key);}
  void remove(int64   key) { removeImpl(key);}
  void remove(double  key) { removeImpl(key);}
  void remove(litstr  key, bool isString = false) {
    remove(String(key), isString);
  }
  void remove(CStrRef key, bool isString = false) {
    removeImpl(key, isString);
  }
  void remove(CVarRef key);

  void weakRemove(litstr key, bool isStr = false) {
    if (is(KindOfArray)) {// FIXME: ||
        // FIXME: (is(KindOfObject) && getObjectData()->o_instanceof("arrayaccess"))) {
      remove(key, isStr);
      return;
    }
    if (isString()) {
      // FIXME: raise_error("Cannot unset string offsets");
      return;
    }
  }

  void weakRemove(CStrRef key, bool isStr = false) {
    if (is(KindOfArray)) {// FIXME: ||
        // FIXME: (is(KindOfObject) && getObjectData()->o_instanceof("arrayaccess"))) {
      remove(key, isStr);
      return;
    }
    if (isString()) {
      // FIXME: raise_error("Cannot unset string offsets");
      return;
    }
  }

  template<typename T>
  void weakRemove(const T &key) {
    if (is(KindOfArray)) {// FIXME: ||
        // FIXME: (is(KindOfObject) && getObjectData()->o_instanceof("arrayaccess"))) {
      remove(key);
      return;
    }
    if (isString()) {
      // FIXME: raise_error("Cannot unset string offsets");
      return;
    }
  }

  /**
   * More array opeartions.
   */
  Variant pop();
  Variant dequeue();
  void prepend(CVarRef v);

  /**
   * Position-based iterations.
   */
  Variant array_iter_reset();
  Variant array_iter_prev();
  Variant array_iter_current() const;
  Variant array_iter_current_ref();
  Variant array_iter_next();
  Variant array_iter_end();
  Variant array_iter_key() const;
  Variant array_iter_each();

  StringData *getStringData() const {
    ASSERT(getType() == KindOfString || getType() == KindOfStaticString);
    return getRefCVar()->getRawStringData();
  }
  StringData *getStringDataOrNull() const {
    // This is a necessary evil because getStringData() returns
    // an undefined result if this is a null variant
    ASSERT(isNull() || is(KindOfString) || is(KindOfStaticString));
    return (getRawType() <= KindOfNull ? NULL : getRefCVar()->getRawStringData());
  }
  ArrayData *getArrayData() const {
    ASSERT(is(KindOfArray));
    return HAPPY::getArrayDataForHashTable(
      HAPPY_API->to_hash_table(getRefZVal()));
  }
  ArrayData *getArrayDataOrNull() const {
    // This is a necessary evil because getArrayData() returns
    // an undefined result if this is a null variant
    ASSERT(isNull() || is(KindOfArray));
    return (getType() <= KindOfNull ? NULL : getArrayData());
  }

  // FIXME: correct???
  int64 getNumData() const { return toInt64(); }

  /**
   * Based on the order in complex_types.h, TypedValue is defined before.
   * TypedValue is binary compatible with Variant
   */
  typedef struct Variant* TypedValueAccessor;
  TypedValueAccessor getTypedAccessor() const {
    return (TypedValueAccessor)getRefCVar();
  }
  static DataType GetAccessorType(TypedValueAccessor acc) {
    ASSERT(acc);
    return acc->getRawType();
  }
  static bool GetBoolean(TypedValueAccessor acc) {
    ASSERT(acc && acc->getRawType() == KindOfBoolean);
    return acc->asBooleanVal();
  }
  static int64 GetInt64(TypedValueAccessor acc) {
    ASSERT(acc);
    ASSERT(acc->getRawType() == KindOfInt32 ||
           acc->getRawType() == KindOfInt64);
    return acc->asInt64Val();
  }
  static double GetDouble(TypedValueAccessor acc) {
    ASSERT(acc && acc->getRawType() == KindOfDouble);
    return acc->asDoubleVal();
  }
  static bool IsString(TypedValueAccessor acc) {
    return acc->getRawType() == KindOfString || acc->getRawType() == KindOfStaticString;
  }
  static StringData *GetStringData(TypedValueAccessor acc) {
    ASSERT(acc);
    ASSERT(acc->getRawType() == KindOfString || acc->getRawType() == KindOfStaticString);
    return acc->getRawStringData();
  }
  static ArrayData *GetArrayData(TypedValueAccessor acc) {
    ASSERT(acc && acc->getRawType() == KindOfArray);
    return acc->getRawArrayData();
  }

  /**
   * The order of the data members is significant. The _count field must
   * be exactly FAST_REFCOUNT_OFFSET bytes from the beginning of the object.
   */
 protected:
  HAPPY::zval m_zval;
  mutable union Data {
    ObjectData  *pobj;
  } m_data;
 private:
  bool isPrimitive() const { return !IS_REFCOUNTED_TYPE(getType()); }
  void removeImpl(double key);
  void removeImpl(int64 key);
  void removeImpl(bool key);
  void removeImpl(CVarRef key, bool isString = false);
  void removeImpl(CStrRef key, bool isString = false);

  // FIXME: HPHP keeps these as private, which makes setting a Variant
  // to a primitive inefficient; it has to go through assign(Variant)
  // which requires a constructor call
  CVarRef set(bool    v) { HAPPY_API->set_int(getRefZVal(), (v ? 1 : 0)); }
  CVarRef set(int32   v) { HAPPY_API->set_int(getRefZVal(), v); }
  CVarRef set(int64   v) { HAPPY_API->set_int(getRefZVal(), v); }
  CVarRef set(double  v) { HAPPY_API->set_double(getRefZVal(), v); }
  CVarRef set(StringData *v);
  CVarRef set(CStrRef v) { return set(v.get()); }
  CVarRef set(ArrayData   *v);
  CVarRef set(CArrRef v) { return set(v.get()); }

  static inline ALWAYS_INLINE void PromoteToRef(CVarRef v) {
    ASSERT(&v != &null_variant);
    HAPPY_API->promote_to_ref(v.m_zval);
  }

  // only called from constructor
  void init(ObjectData *v);

  inline ALWAYS_INLINE
  void setWithRefHelper(CVarRef v, const ArrayData *arr, bool destroy) {
    ASSERT(this != &v);
    // FIXME: incRefCount()
    // FIXME: destruct()
    // FIXME: deref only if KindOfVariant && getCount() <= 1
    m_zval = HAPPY_API->copy_zval(v.m_zval);
  }

  inline ALWAYS_INLINE
  void constructWithRefHelper(CVarRef v, const ArrayData *arr) {
    // FIXME: _count = 0;
    setWithRefHelper(v, arr, false);
  }

  void split();  // breaking weak binding by making a real copy

  template<typename T>
  static inline ALWAYS_INLINE Variant &LvalAtImpl0(
      Variant *self, T key, Variant *tmp, bool blackHole, ACCESSPARAMS_DECL);

  template<typename T>
  inline ALWAYS_INLINE Variant &lvalAtImpl(T key, ACCESSPARAMS_DECL);

  template<typename T>
  Variant refvalAtImpl(const T &key) {
    DataType m_type = getRawType();
    if (m_type == KindOfVariant) {
      return getRefVar()->refvalAtImpl(key);
    }
    if (is(KindOfArray)) { // FIXME:  || isObjectConvertable()) {
      return strongBind(lvalAt(key));
    } else {
      return rvalAt(key);
    }
  }

  Variant refvalAtImpl(CStrRef key, bool isString = false);

  template<class T>
  Variant argvalAtImpl(bool byRef, const T &key) {
    DataType m_type = getRawType();
    if (m_type == KindOfVariant) {
      return getRefVar()->argvalAtImpl(byRef, key);
    }
    if (byRef && (m_type == KindOfArray)) { // FIXME: ||
                  // FIXME: isObjectConvertable())) {
      return strongBind(lvalAt(key));
    } else {
      return rvalAt(key);
    }
  }
  Variant argvalAtImpl(bool byRef, CStrRef key, bool isString = false);

  HAPPY::zval getRefZVal() const {
    if (getRawType() == KindOfVariant)
      return HAPPY_API->to_zval_ref(m_zval);
    return m_zval;
  }

  const Variant *getRefCVar() const {
    if (getRawType() == KindOfVariant) {
      return &HAPPY::getVariantForZVal(HAPPY_API->to_zval_ref(m_zval));
    }
    return this;
  }

  Variant *getRefVar() {
    if (getRawType() == KindOfVariant) {
      return &HAPPY::getVariantForZVal(HAPPY_API->to_zval_ref(m_zval));
    }
    return this;
  }

  StringData *getRawStringData() const {
    return static_cast<StringData*>(HAPPY_API->to_string(m_zval));
  }

  ArrayData *getRawArrayData() const {
    return HAPPY::getArrayDataForHashTable(HAPPY_API->to_hash_table(m_zval));
  }

 private:
  /**
   * Checks whether the LHS array needs to be copied for a *one-level*
   * array set, e.g., "$a[] = $v" or "$a['x'] = $v".
   *
   * Note:
   *  (1) The semantics is equivalent to having a temporary variable
   * holding to RHS value, i.e., "$tmp = $v; $a[] = $tmp". This is NOT
   * exactly the same as PHP 5.3, where "$a = &$b; $a = array(); $a = $b;"
   * creates a recursive array, although the final assignment is not
   * strong-binding.
   *  (2) It does NOT work with multi-level array set, i.e., "$a[][] = $v".
   * The compiler needs to generate a real temporary.
   */
  bool needCopyForSet(CVarRef v) {
    DataType m_type = getRawType(),
             v_m_type = v.getRawType();
    ArrayData *parr = getRawArrayData();
    ASSERT(m_type == KindOfArray);
    if (parr->getCount() > 1) return true;
    if (v_m_type == KindOfArray) return parr == v.getRawArrayData();
    if (v_m_type == KindOfVariant) {
      return parr == v.getRefCVar()->getRawArrayData();
    }
    return false;
  }

  bool needCopyForSetRef(CVarRef v) {
    DataType m_type = getRawType();
    ASSERT(m_type == KindOfArray);
    return getRawArrayData()->getCount() > 1;
  }

  String toStringHelper() const;
  Array  toArrayHelper() const;

  DataType convertToNumeric(int64 *lval, double *dval) const;
};

class VariantStrongBind  { private: Variant m_var; };

class VRefParamValue {
public:
  template <class T> VRefParamValue(const T &v) : m_var(v) {}

  VRefParamValue(RefResult v) : m_var(strongBind(v)) {}
  template <typename T>
  Variant &operator=(const T &v) const {
    m_var = v;
    return m_var;
  }
  VRefParamValue &operator=(const VRefParamValue &v) const {
    m_var = v.m_var;
    return *const_cast<VRefParamValue*>(this);
  }
  operator Variant&() const { return m_var; }
  Variant *operator&() const { return &m_var; }
  Variant *operator->() const { return &m_var; }

  operator bool   () const { return m_var.toBoolean();}
  operator int    () const { return m_var.toInt32();}
  operator int64  () const { return m_var.toInt64();}
  operator double () const { return m_var.toDouble();}
  operator String () const { return m_var.toString();}
  operator Array  () const { return m_var.toArray();}

  bool is(DataType type) const { return m_var.is(type); }
  bool isString() const { return m_var.isString(); }
  bool isNull() const { return m_var.isNull(); }

  bool toBoolean() const { return m_var.toBoolean(); }
  int64 toInt64() const { return m_var.toInt64(); }
  double toDouble() const { return m_var.toDouble(); }
  String toString() const { return m_var.toString(); }
  StringData *getStringData() const { return m_var.getStringData(); }
  Array toArray() const { return m_var.toArray(); }

  CVarRef append(CVarRef v) const { return m_var.append(v); }

  Variant pop() const { return m_var.pop(); }
  Variant dequeue() const { return m_var.dequeue(); }
  void prepend(CVarRef v) const { m_var.prepend(v); }

  bool isArray() const { return m_var.isArray(); }

  Variant array_iter_reset() const { return m_var.array_iter_reset(); }
  Variant array_iter_prev() const { return m_var.array_iter_prev(); }
  Variant array_iter_current() const { return m_var.array_iter_current(); }
  Variant array_iter_current_ref() const {
    return m_var.array_iter_current_ref();
  }
  Variant array_iter_next() const { return m_var.array_iter_next(); }
  Variant array_iter_end() const { return m_var.array_iter_end(); }
  Variant array_iter_key() const { return m_var.array_iter_key(); }
  Variant array_iter_each() const { return m_var.array_iter_each(); }

  Variant::TypedValueAccessor getTypedAccessor() const {
    return m_var.getTypedAccessor();
  }
private:
  mutable Variant m_var;
};

inline VRefParamValue vref(CVarRef v) {
  return strongBind(v);
}

inline VRefParam directRef(CVarRef v) {
  return *(VRefParamValue*)&v;
}

class VariantWithRefBind { private: Variant m_var; };

///////////////////////////////////////////////////////////////////////////////
// VarNR

/*
class VarNR : public Variant {
public:
  // Use to hold variant that do not need ref-counting

private:
  VarNR(litstr  v); // not implemented
  VarNR(const std::string & v); // not implemented

  const Variant *asVariant() const {
    return (const Variant*)this;
  }
  Variant *asVariant() {
    return (Variant*)this;
  }
};
*/

//typedef Variant VarNR;
inline const Variant Array::operator[](bool    key) const {
  return rvalAt(key);
}

inline const Variant Array::operator[](int     key) const {
  return rvalAt(key);
}

inline const Variant Array::operator[](int64   key) const {
  return rvalAt(key);
}

inline const Variant Array::operator[](double  key) const {
  return rvalAt(key);
}

inline const Variant Array::operator[](litstr  key) const {
  return rvalAt(key);
}

inline const Variant Array::operator[](CStrRef key) const {
  return rvalAt(key);
}

inline const Variant Array::operator[](CVarRef key) const {
  return rvalAt(key);
}

///////////////////////////////////////////////////////////////////////////////
}

#endif // __HPHP_VARIANT_H__
