/*
   +----------------------------------------------------------------------+
   | HipHop for PHP                                                       |
   +----------------------------------------------------------------------+
   | Copyright (c) 2010- Facebook, Inc. (http://www.facebook.com)         |
   | Copyright (c) 1998-2010 Zend Technologies Ltd. (http://www.zend.com) |
   +----------------------------------------------------------------------+
   | This source file is subject to version 2.00 of the Zend license,     |
   | that is bundled with this package in the file LICENSE, and is        |
   | available through the world-wide-web at the following url:           |
   | http://www.zend.com/license/2_00.txt.                                |
   | If you did not receive a copy of the Zend license and are unable to  |
   | obtain it through the world-wide-web, please send a note to          |
   | license@zend.com so we can mail you a copy immediately.              |
   +----------------------------------------------------------------------+
*/
#define INLINE_VARIANT_HELPER 1 // for selected inlining

#include <happy_ext.h>
#include <runtime/base/array/happy_array.h>
#include <runtime/base/array/array_init.h>
#include <runtime/base/array/array_iterator.h>
#include <runtime/base/complex_types.h>

namespace HPHP {

///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// static members

// FIXME: StaticEmptyHappyArray StaticEmptyHappyArray::s_theEmptyArray;

///////////////////////////////////////////////////////////////////////////////
// construction/destruciton

HappyArray::~HappyArray() {
}

void HappyArray::release() {
  // FIXME: needed?
}

///////////////////////////////////////////////////////////////////////////////
// iterations

ssize_t HappyArray::size() const {
  return HAPPY_API->hash_get_size(m_ht);
}

void HappyArray::iter_dirty_set() const {
}

void HappyArray::iter_dirty_reset() const {
}

void HappyArray::iter_dirty_check() const {
}

ssize_t HappyArray::iter_begin() const {
  HAPPY::HashPosition p = HAPPY_API->hash_reset(m_ht);
  if (!p) {
    return ArrayData::invalid_index;
  }
  return reinterpret_cast<ssize_t>(p);
}

ssize_t HappyArray::iter_end() const {
  HAPPY::HashPosition p = HAPPY_API->hash_end(m_ht);
  if (!p) {
    return ArrayData::invalid_index;
  }
  return reinterpret_cast<ssize_t>(p);
}

ssize_t HappyArray::iter_advance(ssize_t prev) const {
  HAPPY::HashPosition p = HAPPY_API->hash_move_forward(m_ht,
    reinterpret_cast<HAPPY::HashPosition>(prev));
  if (!p) {
    return ArrayData::invalid_index;
  }
  return reinterpret_cast<ssize_t>(p);
}

ssize_t HappyArray::iter_rewind(ssize_t prev) const {
  HAPPY::HashPosition p = HAPPY_API->hash_move_backward(m_ht,
    reinterpret_cast<HAPPY::HashPosition>(prev));
  if (!p) {
    return ArrayData::invalid_index;
  }
  return reinterpret_cast<ssize_t>(p);
}

Variant HappyArray::getKey(ssize_t pos) const {
  ASSERT(pos && pos != ArrayData::invalid_index);
  StringData *skey = (StringData*)HAPPY_API->hash_get_string_key(m_ht,
    reinterpret_cast<HAPPY::HashPosition>(pos));
  if (skey != NULL)
    return skey;
  
  int64 ikey = HAPPY_API->hash_get_int_key(m_ht,
    reinterpret_cast<HAPPY::HashPosition>(pos));
  return ikey;
}

Variant HappyArray::getValue(ssize_t pos) const {
  ASSERT(pos && pos != ArrayData::invalid_index);
  return HAPPY::getVariantForZVal(HAPPY_API->hash_get_pos_data(m_ht,
    reinterpret_cast<HAPPY::HashPosition>(pos)));
}

CVarRef HappyArray::getValueRef(ssize_t pos) const {
  ASSERT(pos && pos != ArrayData::invalid_index);
  return HAPPY::getCVarRefForZVal(HAPPY_API->hash_get_pos_data(m_ht,
    reinterpret_cast<HAPPY::HashPosition>(pos)));
}

bool HappyArray::isVectorData() const {
  return HAPPY_API->hash_is_vector(m_ht);
}

Variant HappyArray::reset() {
  HAPPY::HashPosition p = HAPPY_API->hash_reset(m_ht);
  if (p) {
    m_pos = reinterpret_cast<ssize_t>(p);
    return getValueRef(m_pos);
  }
  m_pos = ArrayData::invalid_index;
  return false;
}

Variant HappyArray::prev() {
  if (m_pos && m_pos != ArrayData::invalid_index) {
    HAPPY::HashPosition p = HAPPY_API->hash_move_backward(m_ht,
      reinterpret_cast<HAPPY::HashPosition>(m_pos));
    if (p) {
      m_pos = reinterpret_cast<ssize_t>(p);
      return getValueRef(m_pos);
    }
  }
  m_pos = ArrayData::invalid_index;
  return false;
}

Variant HappyArray::next() {
  if (m_pos && m_pos != ArrayData::invalid_index) {
    HAPPY::HashPosition p = HAPPY_API->hash_move_forward(m_ht,
      reinterpret_cast<HAPPY::HashPosition>(m_pos));
    if (p) {
      m_pos = reinterpret_cast<ssize_t>(p);
      return getValueRef(m_pos);
    }
  }
  m_pos = ArrayData::invalid_index;
  return false;
}

Variant HappyArray::end() {
  HAPPY::HashPosition p = HAPPY_API->hash_end(m_ht);
  if (p) {
    m_pos = reinterpret_cast<ssize_t>(p);
    return getValueRef(m_pos);
  }
  m_pos = ArrayData::invalid_index;
  return false;
}

Variant HappyArray::key() const {
  if (m_pos) {
    StringData *skey = (StringData*)HAPPY_API->hash_get_string_key(m_ht,
      reinterpret_cast<HAPPY::HashPosition>(m_pos));
    if (skey != NULL)
      return skey;
    
    int64 ikey = HAPPY_API->hash_get_int_key(m_ht,
      reinterpret_cast<HAPPY::HashPosition>(m_pos));
    return ikey;
  }
  return null;
}

Variant HappyArray::value(ssize_t &pos) const {
  if(pos && pos != ArrayData::invalid_index) {
    return getValueRef(pos);
  }
  pos = ArrayData::invalid_index;
  return false;
}

Variant HappyArray::current() const {
  if (m_pos) {
    return getValueRef(m_pos);
  }
  return false;
}

static StaticString s_value("value");
static StaticString s_key("key");

Variant HappyArray::each() {
  if (m_pos) {
    ArrayInit init(4);
    Key *p = reinterpret_cast<Key *>(m_pos);
    Variant key = getKey(m_pos);
    Variant value = getValue(m_pos);
    init.set(1, value);
    init.set(s_value, value, true);
    init.set(0, key);
    init.set(s_key, key, true);
    // FIXME: m_pos = (ssize_t)p->pListNext;
    return Array(init.create());
  }
  return false;
}

///////////////////////////////////////////////////////////////////////////////
// lookups

bool HappyArray::exists(int64 k) const {
  return HAPPY_API->hash_exists(m_ht, k, NULL, false);
}

bool HappyArray::exists(litstr k) const {
  return exists(String(k));
}

bool HappyArray::exists(CStrRef k) const {
  return HAPPY_API->hash_exists(m_ht, 0, k.get(), true);
}

typedef Variant::TypedValueAccessor TypedValueAccessor;

inline static bool isIntKey(TypedValueAccessor tva) {
  return Variant::GetAccessorType(tva) <= KindOfInt64;
}

inline static int64 getIntKey(TypedValueAccessor tva) {
  return Variant::GetInt64(tva);
}

inline static StringData *getStringKey(TypedValueAccessor tva) {
  return Variant::GetStringData(tva);
}

bool HappyArray::exists(CVarRef k) const {
  TypedValueAccessor tva = k.getTypedAccessor();
  if (isIntKey(tva)) return HAPPY_API->hash_exists(m_ht, getIntKey(tva), NULL, false);
  ASSERT(k.isString());
  StringData *key = getStringKey(tva);
  return HAPPY_API->hash_exists(m_ht, 0, key, true);
}

bool HappyArray::idxExists(ssize_t idx) const {
  return (idx && idx != ArrayData::invalid_index);
}

CVarRef HappyArray::get(int64 k, bool error /* = false */) const {
  HAPPY::zval zval = HAPPY_API->hash_get(m_ht, k, NULL, false);
  if (zval) {
    return HAPPY::getCVarRefForZVal(zval);
  }
  if (error) {
    // FIXME: raise_notice("Undefined index: %lld", k);
  }
  return null_variant;
}

CVarRef HappyArray::get(litstr k, bool error /* = false */) const {
  return get(String(k), error);
}

CVarRef HappyArray::get(CStrRef k, bool error /* = false */) const {
  StringData *key = k.get();
  HAPPY::zval zval = HAPPY_API->hash_get(m_ht, 0, key, true);
  if (zval) {
    return HAPPY::getCVarRefForZVal(zval);
  }
  if (error) {
    // FIXME: raise_notice("Undefined index: %s", k.data());
  }
  return null_variant;
}

CVarRef HappyArray::get(CVarRef k, bool error /* = false */) const {
  HAPPY::zval zval;
  TypedValueAccessor tva = k.getTypedAccessor();
  if (isIntKey(tva)) {
    zval = HAPPY_API->hash_get(m_ht, k.asInt64Val(), NULL, false);
  } else {
    ASSERT(k.isString());
    StringData *strkey = getStringKey(tva);
    zval = HAPPY_API->hash_get(m_ht, 0, strkey, true);
  }
  if (zval) {
    return HAPPY::getCVarRefForZVal(zval);
  }
  if (error) {
    // FIXME: raise_notice("Undefined index: %s", k.toString().data());
  }
  return null_variant;
}

ssize_t HappyArray::getIndex(int64 k) const {
  HAPPY::HashPosition p = HAPPY_API->hash_get_pos(m_ht, k, NULL, false);
  if (p) {
    return (ssize_t)p;
  }
  return ArrayData::invalid_index;
}

ssize_t HappyArray::getIndex(litstr k) const {
  return getIndex(String(k));
}

ssize_t HappyArray::getIndex(CStrRef k) const {
  HAPPY::HashPosition p = HAPPY_API->hash_get_pos(m_ht, 0, k.get(), true);
  if (p) {
    return (ssize_t)p;
  }
  return ArrayData::invalid_index;
}

ssize_t HappyArray::getIndex(CVarRef k) const {
  TypedValueAccessor tva = k.getTypedAccessor();
  HAPPY::HashPosition p;
  if (isIntKey(tva)) {
    p = HAPPY_API->hash_get_pos(m_ht, getIntKey(tva), NULL, false);
  } else {
    ASSERT(k.isString());
    StringData *key = getStringKey(tva);
    p = HAPPY_API->hash_get_pos(m_ht, 0, key, true);
  }
  if (p) {
    return (ssize_t)p;
  }
  return ArrayData::invalid_index;
}

bool HappyArray::nextInsert(CVarRef data) {
  return HAPPY_API->hash_next_insert(m_ht, data.toZVal());
}

bool HappyArray::nextInsertRef(CVarRef data) {
  Variant vr(strongBind(data));
  return nextInsert(vr);
}

bool HappyArray::nextInsertWithRef(CVarRef data) {
  Variant vr(withRefBind(data));
  return nextInsert(vr);
}

bool HappyArray::addLvalImpl(int64 h, Variant **pDest,
                            bool doFind /* = true */) {
  ASSERT(pDest != NULL);
  if (doFind) {
    HAPPY::zval zval = HAPPY_API->hash_get(m_ht, h, NULL, false);
    if (zval) {
      *pDest = &HAPPY::getVariantForZVal(zval);
      return false;
    }
  }
  HAPPY::zval zval = HAPPY_API->hash_add(m_ht, h, NULL, false, NULL);
  ASSERT(zval != NULL);
  *pDest = &HAPPY::getVariantForZVal(zval);
  return true;
}

bool HappyArray::addLvalImpl(StringData *key, int64 h, Variant **pDest,
                            bool doFind /* = true */) {
  ASSERT(key != NULL && pDest != NULL);
  if (doFind) {
    HAPPY::zval zval = HAPPY_API->hash_get(m_ht, 0, key, true);
    if (zval) {
      *pDest = &HAPPY::getVariantForZVal(zval);
      return false;
    }
  }
  HAPPY::zval zval = HAPPY_API->hash_add(m_ht, 0, key, true, NULL);
  ASSERT(zval != NULL);
  *pDest = &HAPPY::getVariantForZVal(zval);
  return true;
}

ArrayData *HappyArray::lval(int64 k, Variant *&ret, bool copy,
                           bool checkExist /* = false */) {
  if (!copy) {
    addLvalImpl(k, &ret);
    return NULL;
  }
  if (!checkExist) {
    HappyArray *a = copyImpl();
    a->addLvalImpl(k, &ret);
    return a;
  }

  HAPPY::zval zval = HAPPY_API->hash_get(m_ht, k, NULL, false);
  if (zval) {
    Variant &v = getVariantForZVal(zval);
    if (true /* FIXME: v.isReferenced() || v.isObject() */) {
      ret = &v;
      return NULL;
    }
  }
  HappyArray *a = copyImpl();
  a->addLvalImpl(k, &ret, zval);
  return a;
}

ArrayData *HappyArray::lval(CStrRef k, Variant *&ret, bool copy,
                           bool checkExist /* = false */) {
  StringData *key = k.get();
  int64 prehash = key->hash();
  if (!copy) {
    addLvalImpl(key, prehash, &ret);
    return NULL;
  }
  if (!checkExist) {
    HappyArray *a = copyImpl();
    a->addLvalImpl(key, prehash, &ret);
    return a;
  }

  HAPPY::zval zval = HAPPY_API->hash_get(m_ht, 0, key, true);
  if (zval) {
    Variant &v = getVariantForZVal(zval);
    if (true /* FIXME: v.isReferenced() || v.isObject() */) {
      ret = &v;
      return NULL;
    }
  }
  HappyArray *a = copyImpl();
  a->addLvalImpl(key, prehash, &ret, zval);
  return a;
}

ArrayData *HappyArray::lvalPtr(CStrRef k, Variant *&ret, bool copy,
                              bool create) {
  StringData *key = k.get();
  int64 prehash = key->hash();
  HappyArray *a = 0, *t = this;
  if (UNLIKELY(copy)) {
    a = t = copyImpl();
  }

  if (create) {
    t->addLvalImpl(key, prehash, &ret);
  } else {
    HAPPY::zval zval = HAPPY_API->hash_get(m_ht, 0, k.get(), true);
    if (zval) {
      ret = &getVariantForZVal(zval);
    } else {
      ret = NULL;
    }
  }
  return a;
}

ArrayData *HappyArray::lvalPtr(int64 k, Variant *&ret, bool copy,
                              bool create) {
  HappyArray *a = 0, *t = this;
  if (UNLIKELY(copy)) {
    a = t = copyImpl();
  }

  if (create) {
    t->addLvalImpl(k, &ret);
  } else {
    HAPPY::zval zval = HAPPY_API->hash_get(m_ht, k, NULL, false);
    if (zval) {
      ret = &getVariantForZVal(zval);
    } else {
      ret = NULL;
    }
  }
  return a;
}

ArrayData *HappyArray::lval(litstr k, Variant *&ret, bool copy,
                           bool checkExist /* = false */) {
  String s(k, AttachLiteral);
  return lval(s, ret, copy, checkExist);
}

ArrayData *HappyArray::lval(CVarRef k, Variant *&ret, bool copy,
                           bool checkExist /* = false */) {
  TypedValueAccessor tva = k.getTypedAccessor();
  if (isIntKey(tva)) {
    return lval(getIntKey(tva), ret, copy, checkExist);
  } else {
    ASSERT(k.isString());
    return lval(k.toKey(), ret, copy, checkExist);
  }
}

ArrayData *HappyArray::lvalNew(Variant *&ret, bool copy) {
  if (UNLIKELY(copy)) {
    HappyArray *a = copyImpl();
    if (!a->nextInsert(null)) {
      ret = &(Variant::lvalBlackHole());
      return a;
    }
    // FIXME: works???
    ssize_t p = iter_end();
    ret = const_cast<Variant*>(&getValueRef(p));
    return a;
  }
  if (!nextInsert(null)) {
    ret = &(Variant::lvalBlackHole());
    return NULL;
  }
  // FIXME: works???
  ssize_t p = iter_end();
  ret = const_cast<Variant*>(&getValueRef(p));
  return NULL;
}

bool HappyArray::addValWithRef(int64 h, CVarRef data) {
  HAPPY::zval zval = HAPPY_API->hash_get(m_ht, h, NULL, false);
  if (zval)
    return false;
  
  Variant vr(withRefBind(data));
  zval = HAPPY_API->hash_add(m_ht, h, NULL, false, vr.toZVal());
  return zval != NULL;
}

bool HappyArray::addValWithRef(StringData *key, CVarRef data) {
  HAPPY::zval zval = HAPPY_API->hash_get(m_ht, 0, key, true);
  if (zval) {
    return false;
  }

  Variant vr(withRefBind(data));
  zval = HAPPY_API->hash_add(m_ht, 0, key, true, vr.toZVal());
  return zval != NULL;
}

bool HappyArray::update(int64 h, CVarRef data) {
  HAPPY::zval zval = HAPPY_API->hash_get(m_ht, h, NULL, false);
  if (zval) {
    getVariantForZVal(zval).assign(data);
    return true;
  }
  zval = HAPPY_API->hash_add(m_ht, h, NULL, false, data.toZVal());
  return zval != NULL;
}

bool HappyArray::update(StringData *key, CVarRef data) {
  HAPPY::zval zval = HAPPY_API->hash_get(m_ht, 0, key, true);
  if (zval) {
    getVariantForZVal(zval).assign(data);
    return true;
  }
  zval = HAPPY_API->hash_add(m_ht, 0, key, true, data.toZVal());
  return zval != NULL;
}

bool HappyArray::updateRef(int64 h, CVarRef data) {
  HAPPY::zval zval = HAPPY_API->hash_get(m_ht, h, NULL, false);
  if (zval) {
    getVariantForZVal(zval).assignRef(data);
    return true;
  }

  Variant vr(strongBind(data));
  zval = HAPPY_API->hash_add(m_ht, h, NULL, false, vr.toZVal());
  return zval != NULL;
}

bool HappyArray::updateRef(StringData *key, CVarRef data) {
  HAPPY::zval zval = HAPPY_API->hash_get(m_ht, 0, key, true);
  if (zval) {
    getVariantForZVal(zval).assignRef(data);
    return true;
  }

  Variant vr(strongBind(data));
  zval = HAPPY_API->hash_add(m_ht, 0, key, true, vr.toZVal());
  return zval != NULL;
}

ArrayData *HappyArray::set(int64 k, CVarRef v, bool copy) {
  if (UNLIKELY(copy)) {
    HappyArray *a = copyImpl();
    a->update(k, v);
    return a;
  }
  update(k, v);
  return NULL;
}

ArrayData *HappyArray::set(CStrRef k, CVarRef v, bool copy) {
  if (UNLIKELY(copy)) {
    HappyArray *a = copyImpl();
    a->update(k.get(), v);
    return a;
  }
  update(k.get(), v);
  return NULL;
}

ArrayData *HappyArray::set(CVarRef k, CVarRef v, bool copy) {
  TypedValueAccessor tva = k.getTypedAccessor();
  if (isIntKey(tva)) {
    if (UNLIKELY(copy)) {
      HappyArray *a = copyImpl();
      a->update(getIntKey(tva), v);
      return a;
    }
    update(getIntKey(tva), v);
    return NULL;
  } else {
    ASSERT(k.isString());
    StringData *sd = getStringKey(tva);
    if (UNLIKELY(copy)) {
      HappyArray *a = copyImpl();
      a->update(sd, v);
      return a;
    }
    update(sd, v);
    return NULL;
  }
}

ArrayData *HappyArray::setRef(int64 k, CVarRef v, bool copy) {
  if (UNLIKELY(copy)) {
    HappyArray *a = copyImpl();
    a->updateRef(k, v);
    return a;
  }
  updateRef(k, v);
  return NULL;
}

ArrayData *HappyArray::setRef(CStrRef k, CVarRef v, bool copy) {
  if (UNLIKELY(copy)) {
    HappyArray *a = copyImpl();
    a->updateRef(k.get(), v);
    return a;
  }
  updateRef(k.get(), v);
  return NULL;
}

ArrayData *HappyArray::setRef(CVarRef k, CVarRef v, bool copy) {
  TypedValueAccessor tva = k.getTypedAccessor();
  if (isIntKey(tva)) {
    if (UNLIKELY(copy)) {
      HappyArray *a = copyImpl();
      a->updateRef(getIntKey(tva), v);
      return a;
    }
    updateRef(getIntKey(tva), v);
    return NULL;
  } else {
    ASSERT(k.isString());
    StringData *sd = getStringKey(tva);
    if (UNLIKELY(copy)) {
      HappyArray *a = copyImpl();
      a->updateRef(sd, v);
      return a;
    }
    updateRef(sd, v);
    return NULL;
  }
}

ArrayData *HappyArray::add(int64 k, CVarRef v, bool copy) {
  ASSERT(!exists(k));
  if (UNLIKELY(copy)) {
    HappyArray *result = copyImpl();
    result->add(k, v, false);
    return result;
  }
  HAPPY_API->hash_add(m_ht, k, NULL, false, v.toZVal());
  return NULL;
}

ArrayData *HappyArray::add(CStrRef k, CVarRef v, bool copy) {
  ASSERT(!exists(k));
  if (UNLIKELY(copy)) {
    HappyArray *result = copyImpl();
    result->add(k, v, false);
    return result;
  }
  HAPPY_API->hash_add(m_ht, 0, k.get(), true, v.toZVal());
  return NULL;
}

ArrayData *HappyArray::add(CVarRef k, CVarRef v, bool copy) {
  ASSERT(!exists(k));
  TypedValueAccessor tva = k.getTypedAccessor();
  if (isIntKey(tva)) return add(getIntKey(tva), v, copy);
  ASSERT(k.isString());
  return add(k.toKey(), v, copy);
}

ArrayData *HappyArray::addLval(int64 k, Variant *&ret, bool copy) {
  ASSERT(!exists(k));
  if (UNLIKELY(copy)) {
    HappyArray *result = copyImpl();
    result->addLvalImpl(k, &ret, false);
    return result;
  }
  addLvalImpl(k, &ret, false);
  return NULL;
}

ArrayData *HappyArray::addLval(CStrRef k, Variant *&ret, bool copy) {
  ASSERT(!exists(k));
  if (UNLIKELY(copy)) {
    HappyArray *result = copyImpl();
    result->addLvalImpl(k.get(), k->hash(), &ret, false);
    return result;
  }
  addLvalImpl(k.get(), k->hash(), &ret, false);
  return NULL;
}

ArrayData *HappyArray::addLval(CVarRef k, Variant *&ret, bool copy) {
  ASSERT(!exists(k));
  TypedValueAccessor tva = k.getTypedAccessor();
  if (isIntKey(tva)) return addLval(getIntKey(tva), ret, copy);
  ASSERT(k.isString());
  return addLval(k.toKey(), ret, copy);
}

///////////////////////////////////////////////////////////////////////////////
// delete

ArrayData *HappyArray::remove(int64 k, bool copy) {
  if (UNLIKELY(copy)) {
    HappyArray *a = copyImpl();
    a->remove(k, false);
    return a;
  }
  HAPPY_API->hash_del(m_ht, k, NULL, false);
  return NULL;
}

ArrayData *HappyArray::remove(CStrRef k, bool copy) {
  int64 prehash = k->hash();
  if (UNLIKELY(copy)) {
    HappyArray *a = copyImpl();
    a->remove(k, false);
    return a;
  }
  HAPPY_API->hash_del(m_ht, 0, k.get(), true); 
  return NULL;
}

ArrayData *HappyArray::remove(CVarRef k, bool copy) {
  TypedValueAccessor tva = k.getTypedAccessor();
  if (isIntKey(tva)) {
    if (UNLIKELY(copy)) {
      HappyArray *a = copyImpl();
      a->remove(k, false);
      return a;
    }
    HAPPY_API->hash_del(m_ht, k.asInt64Val(), NULL, false);
    return NULL;
  } else {
    ASSERT(k.isString());
    if (UNLIKELY(copy)) {
      HappyArray *a = copyImpl();
      a->remove(k, false);
      return a;
    }
    StringData *key = getStringKey(tva);
    HAPPY_API->hash_del(m_ht, 0, key, true);
    return NULL;
  }
}

ArrayData *HappyArray::copy() const {
  return copyImpl();
}

inline ALWAYS_INLINE HappyArray *HappyArray::copyImplHelper(bool sma) const {
  HAPPY::HashTable new_ht = HAPPY_API->hash_copy(m_ht);
  // FIXME: use sma???
  return new HappyArray(new_ht);
}

ArrayData *HappyArray::nonSmartCopy() const {
  return copyImplHelper(false);
}

HappyArray *HappyArray::copyImpl() const {
  return copyImplHelper(true);
}

ArrayData *HappyArray::append(CVarRef v, bool copy) {
  if (UNLIKELY(copy)) {
    HappyArray *a = copyImpl();
    a->nextInsert(v);
    return a;
  }
  nextInsert(v);
  return NULL;
}

ArrayData *HappyArray::appendRef(CVarRef v, bool copy) {
  if (UNLIKELY(copy)) {
    HappyArray *a = copyImpl();
    a->nextInsertRef(v);
    return a;
  }
  nextInsertRef(v);
  return NULL;
}

ArrayData *HappyArray::appendWithRef(CVarRef v, bool copy) {
  if (UNLIKELY(copy)) {
    HappyArray *a = copyImpl();
    a->nextInsertWithRef(v);
    return a;
  }
  nextInsertWithRef(v);
  return NULL;
}

ArrayData *HappyArray::append(const ArrayData *elems, ArrayOp op, bool copy) {
  if (UNLIKELY(copy)) {
    HappyArray *a = copyImpl();
    a->append(elems, op, false);
    return a;
  }

  if (op == Plus) {
    for (ArrayIter it(elems); !it.end(); it.next()) {
      Variant key = it.first();
      CVarRef value = it.secondRef();
      if (key.isNumeric()) {
        addValWithRef(key.toInt64(), value);
      } else {
        addValWithRef(key.getStringData(), value);
      }
    }
  } else {
    ASSERT(op == Merge);
    for (ArrayIter it(elems); !it.end(); it.next()) {
      Variant key = it.first();
      CVarRef value = it.secondRef();
      if (key.isNumeric()) {
        nextInsertWithRef(value);
      } else {
        Variant *p;
        StringData *sd = key.getStringData();
        addLvalImpl(sd, sd->hash(), &p, true);
        p->setWithRef(value);
      }
    }
  }
  return NULL;
}

ArrayData *HappyArray::pop(Variant &value) {
  if (getCount() > 1) {
    HappyArray *a = copyImpl();
    a->pop(value);
    return a;
  }
  // TODO: invalidate strong iterators
  HAPPY::HashPosition p = HAPPY_API->hash_end(m_ht);
  if (p) {
    // TODO: direct assignment of zval???
    value = getCVarRefForZVal(HAPPY_API->hash_get_pos_data(m_ht, p));
    HAPPY_API->hash_del_pos(m_ht, p);
    renumber();
  } else {
    value = null;
  }
  reset();  
  return NULL;
}

ArrayData *HappyArray::dequeue(Variant &value) {
  if (getCount() > 1) {
    HappyArray *a = copyImpl();
    a->dequeue(value);
    return a;
  }
  // TODO: invalidate strong iterators
  HAPPY::HashPosition p = HAPPY_API->hash_reset(m_ht);
  if (p) {
    // TODO: direct assignment of zval???
    value = getCVarRefForZVal(HAPPY_API->hash_get_pos_data(m_ht, p));
    HAPPY_API->hash_del_pos(m_ht, p);
    renumber();
  } else {
    value = null;
  }
  reset();  
  return NULL;
}

ArrayData *HappyArray::prepend(CVarRef v, bool copy) {
  if (UNLIKELY(copy)) {
    HappyArray *a = copyImpl();
    a->prepend(v, false);
    return a;
  }
  // TODO: invalidate strong iterators
  HAPPY_API->hash_prepend(m_ht, v.toZVal());
  renumber();
  reset();
  return NULL;
}

void HappyArray::renumber() {
  HAPPY_API->hash_renumber(m_ht);
}

void HappyArray::onSetStatic() {
  // FIXME: implement
}

void HappyArray::onSetEvalScalar() {
  // FIXME: implement
}

void HappyArray::getFullPos(FullPos &fp) {
  ASSERT(fp.container == (ArrayData*)this);
  fp.pos = m_pos;
  if (!fp.pos) {
    // Record that there is a strong iterator out there
    // that is past the end
    // FIXME: m_flag |= StrongIteratorPastEnd;
  }
}

bool HappyArray::setFullPos(const FullPos &fp) {
  ASSERT(fp.container == (ArrayData*)this);
  if (fp.pos) {
    m_pos = fp.pos;
    return true;
  }
  return false;
}

CVarRef HappyArray::currentRef() {
  ASSERT(m_pos);
  return getValueRef(m_pos);
}

CVarRef HappyArray::endRef() {
  ASSERT(m_pos);
  HAPPY::HashPosition p = HAPPY_API->hash_end(m_ht);
  return getValueRef(reinterpret_cast<ssize_t>(p));
}

///////////////////////////////////////////////////////////////////////////////
}
