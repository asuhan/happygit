/*
   +----------------------------------------------------------------------+
   | HipHop for PHP                                                       |
   +----------------------------------------------------------------------+
   | Copyright (c) 2010- Facebook, Inc. (http://www.facebook.com)         |
   +----------------------------------------------------------------------+
   | This source file is subject to version 3.01 of the PHP license,      |
   | that is bundled with this package in the file LICENSE, and is        |
   | available through the world-wide-web at the following url:           |
   | http://www.php.net/license/3_01.txt                                  |
   | If you did not receive a copy of the PHP license and are unable to   |
   | obtain it through the world-wide-web, please send a note to          |
   | license@php.net so we can mail you a copy immediately.               |
   +----------------------------------------------------------------------+
*/

#ifndef __HPHP_SMART_ALLOCATOR_H__
#define __HPHP_SMART_ALLOCATOR_H__

#include <util/base.h>

namespace HPHP {

///////////////////////////////////////////////////////////////////////////////
/**
 * If a class is using SmartAllocator, all "new" and "delete" should be done
 * through these two macros in a form like this,
 *
 *   MyClass *obj = NEW(MyClass)(...);
 *   DELETE(MyClass)(obj);
 */

#define NEW(T) new T
#define NEWOBJ(T) new T
#define DELETE(T) delete
#define DELETEOBJ(NS,T,OBJ) delete OBJ
#define RELEASEOBJ(NS,T,OBJ) ::operator delete(OBJ)
#define SWEEPOBJ(T) delete this

  ///////////////////////////////////////////////////////////////////////////////
/**
 * To use this allocator, simply add DECLARE_SMART_ALLOCATION macro to .h and
 * add IMPLEMENT_SMART_ALLOCATION macro to .cpp. For example,
 *
 * class MyClass {
 *   DECLARE_SMART_ALLOCATION(MyClass, SmartAllocatorImpl::NoCallbacks);
 * };
 *
 * IMPLEMENT_SMART_ALLOCATION(MyClass, SmartAllocatorImpl::NoCallbacks);
 */

#define DECLARE_SMART_ALLOCATION(T, F)                                  \
  public:                                                               \
  void release();                                                       \

#define IMPLEMENT_SMART_ALLOCATION(T, F)                                \
  void T::release() {                                                   \
    DELETE(T)(this);                                                    \
  }                                                                     \

#define IMPLEMENT_SMART_ALLOCATION_CLS(C, T, F)                         \
  void C::T::release() {                                                \
    DELETE(T)(this);                                                    \
  }                                                                     \

#define DECLARE_SMART_ALLOCATION_NOCALLBACKS(T)                         \
  DECLARE_SMART_ALLOCATION(T, SmartAllocatorImpl::NoCallbacks);         \
  bool calculate(int &size) {                                           \
    ASSERT(false);                                                      \
    return false;                                                       \
  }                                                                     \
  void restore(const char *&data) {                                     \
    ASSERT(false);                                                      \
  }                                                                     \
  void sweep() {                                                        \
  }                                                                     \

#define IMPLEMENT_SMART_ALLOCATION_NOCALLBACKS(T)                       \
  IMPLEMENT_SMART_ALLOCATION(T, SmartAllocatorImpl::NoCallbacks)        \

#define IMPLEMENT_SMART_ALLOCATION_NOCALLBACKS_CLS(C, T)                \
  IMPLEMENT_SMART_ALLOCATION_CLS(C, T, SmartAllocatorImpl::NoCallbacks) \

///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////
// This allocator is for known and fixed sized classes, like StringData or
// ArrayData.

///////////////////////////////////////////////////////////////////////////////
// This allocator is for unknown but fixed sized classes, like ObjectData.
// NS::T::s_T_initializer allows private inner classes to be initialized,
// this is completely hidden by using a nested private llocatorInitializer

#define DECLARE_OBJECT_ALLOCATION_NO_SWEEP(T)                           \
  public:                                                               \
  static void *ObjAllocatorInitSetup;                                   \
  inline ALWAYS_INLINE void operator delete(void *p) {                  \
    RELEASEOBJ(NS, T, p);                                               \
  }

#define DECLARE_OBJECT_ALLOCATION(T)                                    \
  DECLARE_OBJECT_ALLOCATION_NO_SWEEP(T)                                 \
  virtual void sweep();                                                 \

#define IMPLEMENT_OBJECT_ALLOCATION_NO_DEFAULT_SWEEP_CLS(NS,T)          \
  void *NS::T::ObjAllocatorInitSetup =                                  \
    ObjectAllocatorInitSetup<NS::T>();

#define IMPLEMENT_OBJECT_ALLOCATION_NO_DEFAULT_SWEEP(T)                 \
    IMPLEMENT_OBJECT_ALLOCATION_NO_DEFAULT_SWEEP_CLS(HPHP,T)

#define IMPLEMENT_OBJECT_ALLOCATION_CLS(NS,T)                           \
  IMPLEMENT_OBJECT_ALLOCATION_NO_DEFAULT_SWEEP_CLS(NS,T);               \
  void NS::T::sweep() {                                                 \
    SWEEPOBJ(T);                                                        \
  }

#define IMPLEMENT_OBJECT_ALLOCATION(T) IMPLEMENT_OBJECT_ALLOCATION_CLS(HPHP,T)

///////////////////////////////////////////////////////////////////////////////
}


///////////////////////////////////////////////////////////////////////////////

#endif // __HPHP_SMART_ALLOCATOR_H__
