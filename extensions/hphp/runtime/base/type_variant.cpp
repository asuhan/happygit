/*
   +----------------------------------------------------------------------+
   | HipHop for PHP                                                       |
   +----------------------------------------------------------------------+
   | Copyright (c) 2010- Facebook, Inc. (http://www.facebook.com)         |
   +----------------------------------------------------------------------+
   | This source file is subject to version 3.01 of the PHP license,      |
   | that is bundled with this package in the file LICENSE, and is        |
   | available through the world-wide-web at the following url:           |
   | http://www.php.net/license/3_01.txt                                  |
   | If you did not receive a copy of the PHP license and are unable to   |
   | obtain it through the world-wide-web, please send a note to          |
   | license@php.net so we can mail you a copy immediately.               |
   +----------------------------------------------------------------------+
*/
#include <happy_ext.h>
#include <runtime/base/array/array_init.h>
#include <runtime/base/complex_types.h>
#include <runtime/base/comparisons.h>
#include <runtime/base/string_data.h>
#include <runtime/base/zend/zend_functions.h>
#include <runtime/base/zend/zend_string.h>
#include <runtime/base/array/array_iterator.h>
#include <util/parser/hphp.tab.hpp>

using namespace std;

namespace HPHP {

// FIXME: implement some delayed initialization for this
const Variant null_variant = Variant(Variant::noInit);
const VarNR &null_varNR = null_variant;

///////////////////////////////////////////////////////////////////////////////
// static strings

static StaticString s_s("s");
static StaticString s_scalar("scalar");
static StaticString s_array("Array");
static StaticString s_1("1");
///////////////////////////////////////////////////////////////////////////////
// local helpers

static int64 ToKey(bool i) { return (int64)i; }
static int64 ToKey(int64 i) { return i; }
static VarNR ToKey(CStrRef s) { return s.toKey(); }
static VarNR ToKey(CVarRef v) { return v.toKey(); }

Variant::Variant(CVarWithRefBind v) {
  constructWithRefHelper(variant(v), 0);
}

Variant &Variant::assign(CVarRef v) {
  if (&v == this)
    return *this;
  HAPPY_API->assign_zval(getRefZVal(), v.getRefZVal());
  return *this;
}

Variant &Variant::assignRef(CVarRef v) {
  PromoteToRef(v);
  // TODO: inc refCount on target
  HAPPY_API->set_zval_ref(m_zval, v.m_zval);
}

Variant &Variant::setWithRef(CVarRef v, const ArrayData *arr /* = NULL */) {
  setWithRefHelper(v, arr, IS_REFCOUNTED_TYPE(getType()));
  return *this;
}

CVarRef Variant::set(StringData *v) {
  Variant *self = getRefVar();
  if (UNLIKELY(!v)) {
    self->setNull();
  } else {
    v->incRefCount();
    // FIXME: if (IS_REFCOUNTED_TYPE(self->getType())) self->destruct();
    HAPPY_API->set_string(self->m_zval, v);
  }
  return *this;
}

CVarRef Variant::set(ArrayData *v) {
  Variant *self = getRefVar();
  if (UNLIKELY(!v)) {
    self->setNull();
  } else {
    v->incRefCount();
    // FIXME: if (IS_REFCOUNTED_TYPE(self->m_type)) self->destruct();
    HAPPY_API->set_hash(self->m_zval, v->getHashTable());
  }
  return *this;
}

void Variant::split() {
  switch (getType()) {
  case KindOfVariant: getRefVar()->split();     break;
  // copy-on-write
  case KindOfStaticString:
  case KindOfString:
  {
    StringData *pstr = getRawStringData();
    int len = pstr->size();
    const char *copy = string_duplicate(pstr->data(), len);
    set(NEW(StringData)(copy, len, AttachString));
    break;
  }
  // FIXME: case KindOfArray:   set(getRawArrayData()->copy()); break;
  default:
    break;
  }
}

///////////////////////////////////////////////////////////////////////////////
// informational

bool Variant::isInteger() const {
  switch (getRawType()) {
    case KindOfInt32:
    case KindOfInt64:
      return true;
    case KindOfVariant:
      return getRefCVar()->isInteger();
    default:
      break;
  }
  return false;
}

bool Variant::isNumeric(bool checkString /* = false */) const {
  int64 ival;
  double dval;
  DataType t = toNumeric(ival, dval, checkString);
  return t == KindOfInt64 || t == KindOfDouble;
}

DataType Variant::toNumeric(int64 &ival, double &dval,
    bool checkString /* = false */) const {
  DataType t = getRawType();
  switch (t) {
  case KindOfInt32:
  case KindOfInt64:
    ival = getInt64();
    return KindOfInt64;
  case KindOfDouble:
    dval = toDouble();
    return KindOfDouble;
  case KindOfStaticString:
  case KindOfString:
    if (checkString) {
      return getRawStringData()->toNumeric(ival, dval);
    }
    break;
  case KindOfVariant:
    return getRefCVar()->toNumeric(ival, dval, checkString);
  default:
    break;
  }
  return t;
}

bool Variant::isScalar() const {
  switch (getType()) {
  case KindOfUninit:
  case KindOfNull:
  case KindOfArray:
  case KindOfObject:
    return false;
  default:
    break;
  }
  return true;
}

bool Variant::isResource() const {
  // FIXME
  return false;
}

///////////////////////////////////////////////////////////////////////////////
// array operations

Variant Variant::pop() {
  DataType m_type = getRawType();
  if (m_type == KindOfVariant) {
    return getRefVar()->pop();
  }
  if (!is(KindOfArray)) {
    // FIXME: throw_bad_type_exception("expecting an array");
    return null_variant;
  }

  Variant ret;
  ArrayData *newarr = getArrayData()->pop(ret);
  if (newarr) {
    set(newarr);
  }
  return ret;
}

Variant Variant::dequeue() {
  DataType m_type = getRawType();
  if (m_type == KindOfVariant) {
    return getRefVar()->dequeue();
  }
  if (!is(KindOfArray)) {
    // FIXME: throw_bad_type_exception("expecting an array");
    return null_variant;
  }

  Variant ret;
  ArrayData *newarr = getArrayData()->dequeue(ret);
  if (newarr) {
    set(newarr);
  }
  return ret;
}

void Variant::prepend(CVarRef v) {
  DataType m_type = getRawType();
  if (m_type == KindOfVariant) {
    getRefVar()->prepend(v);
    return;
  }
  if (isNull()) {
    set(Array::Create());
  }

  if (is(KindOfArray)) {
    ArrayData *arr = getArrayData();
    ArrayData *newarr = arr->prepend(v, (arr->getCount() > 1));
    if (newarr) {
      set(newarr);
    }
  } else {
    // FIXME: throw_bad_type_exception("expecting an array");
  }
}

Variant Variant::array_iter_reset() {
  if (is(KindOfArray)) {
    ArrayData *arr = getArrayData();
    if (arr->getCount() > 1 && !arr->isHead()
     && !arr->noCopyOnWrite()) {
      arr = arr->copy();
      set(arr);
      ASSERT(arr == getArrayData());
    }
    return arr->reset();
  }
  // FIXME: throw_bad_type_exception("expecting an array");
  return false;
}

Variant Variant::array_iter_prev() {
  if (is(KindOfArray)) {
    ArrayData *arr = getArrayData();
    if (arr->getCount() > 1 && !arr->isInvalid()
     && !arr->noCopyOnWrite()) {
      arr = arr->copy();
      set(arr);
      ASSERT(arr == getArrayData());
    }
    return arr->prev();
  }
  // FIXME: throw_bad_type_exception("expecting an array");
  return false;
}

Variant Variant::array_iter_current() const {
  if (is(KindOfArray)) {
    return getArrayData()->current();
  }
  // FIXME: throw_bad_type_exception("expecting an array");
  return false;
}

Variant Variant::array_iter_current_ref() {
  if (is(KindOfArray)) {
    escalate(true);
    ArrayData *arr = getArrayData();
    if (arr->getCount() > 1 && !arr->noCopyOnWrite()) {
      arr = arr->copy();
      set(arr);
      ASSERT(arr == getArrayData());
    }
    return strongBind(arr->currentRef());
  }
  // FIXME: throw_bad_type_exception("expecting an array");
  return false;
}

Variant Variant::array_iter_next() {
  if (is(KindOfArray)) {
    ArrayData *arr = getArrayData();
    if (arr->getCount() > 1 && !arr->isInvalid()
     && !arr->noCopyOnWrite()) {
      arr = arr->copy();
      set(arr);
      ASSERT(arr == getArrayData());
    }
    return arr->next();
  }
  // FIXME: throw_bad_type_exception("expecting an array");
  return false;
}

Variant Variant::array_iter_end() {
  if (is(KindOfArray)) {
    ArrayData *arr = getArrayData();
    if (arr->getCount() > 1 && !arr->isTail()
     && !arr->noCopyOnWrite()) {
      arr = arr->copy();
      set(arr);
      ASSERT(arr == getArrayData());
    }
    return arr->end();
  }
  // FIXME: throw_bad_type_exception("expecting an array");
  return false;
}

Variant Variant::array_iter_key() const {
  if (is(KindOfArray)) {
    return getArrayData()->key();
  }
  // FIXME: throw_bad_type_exception("expecting an array");
  return false;
}

Variant Variant::array_iter_each() {
  if (is(KindOfArray)) {
    ArrayData *arr = getArrayData();
    if (arr->getCount() > 1 && !arr->isInvalid()
     && !arr->noCopyOnWrite()) {
      arr = arr->copy();
      set(arr);
      ASSERT(arr == getArrayData());
    }
    return arr->each();
  }
  // FIXME: throw_bad_type_exception("expecting an array");
  return null_variant;
}

inline DataType Variant::convertToNumeric(int64 *lval, double *dval) const {
  StringData *s = getStringData();
  ASSERT(s);
  return s->isNumericWithVal(*lval, *dval, 1);
}

///////////////////////////////////////////////////////////////////////////////
// unary plus
Variant Variant::operator+() const {
  if (is(KindOfArray)) {
    // FIXME: throwBadArrayOperandException();
  }
  if (isDouble()) {
    return toDouble();
  }
  if (isIntVal()) {
    return toInt64();
  }
  if (isString()) {
    int64 lval; double dval;
    DataType ret = convertToNumeric(&lval, &dval);
    if (ret == KindOfDouble) {
      return dval;
    }
    if (ret == KindOfInt64) {
      return lval;
    }
    return toInt64();
  }

  ASSERT(false);
  return 0;
}

///////////////////////////////////////////////////////////////////////////////
// add or array append

Variant Variant::operator+(CVarRef var) const {
  if (isIntVal() && var.isIntVal()) {
    return toInt64() + var.toInt64();
  }
  int na = is(KindOfArray) + var.is(KindOfArray);
  if (na == 2) {
    return toCArrRef() + var.toCArrRef();
  } else if (na) {
    throw BadArrayMergeException();
  }
  if (isDouble() || var.isDouble()) {
    return toDouble() + var.toDouble();
  }
  if (isString()) {
    int64 lval; double dval;
    DataType ret = convertToNumeric(&lval, &dval);
    if (ret == KindOfDouble) {
      return dval + var.toDouble();
    }
  }
  if (var.isString()) {
    int64 lval; double dval;
    DataType ret = var.convertToNumeric(&lval, &dval);
    if (ret == KindOfDouble) {
      return toDouble() + dval;
    }
  }
  return toInt64() + var.toInt64();
}

Variant &Variant::operator+=(CVarRef var) {
  if (isIntVal() && var.isIntVal()) {
    set(toInt64() + var.toInt64());
    return *this;
  }
  int na = is(KindOfArray) + var.is(KindOfArray);
  if (na == 2) {
    ArrayData *arr1 = getArrayData();
    ArrayData *arr2 = var.getArrayData();
    if (arr1 == NULL || arr2 == NULL) {
      throw BadArrayMergeException();
    }
    if (arr2->empty() || arr1 == arr2) return *this;
    if (arr1->empty()) {
      set(arr2);
      return *this;
    }
    ArrayData *escalated = arr1->append(arr2, ArrayData::Plus,
                                        arr1->getCount() > 1);
    if (escalated) set(escalated);
    return *this;
  } else if (na) {
    throw BadArrayMergeException();
  }
  if (isDouble() || var.isDouble()) {
    set(toDouble() + var.toDouble());
    return *this;
  }
  if (isString()) {
    int64 lval; double dval;
    DataType ret = convertToNumeric(&lval, &dval);
    if (ret == KindOfDouble) {
      set(dval + var.toDouble());
      return *this;
    }
  }
  if (var.isString()) {
    int64 lval; double dval;
    DataType ret = var.convertToNumeric(&lval, &dval);
    if (ret == KindOfDouble) {
      set(toDouble() + dval);
      return *this;
    }
  }
  set(toInt64() + var.toInt64());
  return *this;
}

Variant &Variant::operator+=(int64 n) {
  if (isIntVal()) {
    set(toInt64() + n);
    return *this;
  }
  if (isDouble()) {
    set(toDouble() + n);
    return *this;
  }
  if (is(KindOfArray)) {
    throw BadArrayMergeException();
  }
  if (isString()) {
    int64 lval; double dval;
    DataType ret = convertToNumeric(&lval, &dval);
    if (ret == KindOfDouble) {
      set(dval + n);
      return *this;
    }
  } else {
    ASSERT(false);
  }
  set(toInt64() + n);
  return *this;
}

Variant &Variant::operator+=(double n) {
  if (is(KindOfArray)) {
    throw BadArrayMergeException();
  }
  set(toDouble() + n);
  return *this;
}

///////////////////////////////////////////////////////////////////////////////
// minus

Variant Variant::operator-() const {
  if (is(KindOfArray)) {
    throw BadArrayOperandException();
  }
  if (isDouble()) {
    return -toDouble();
  } else if (isIntVal()) {
    return -toInt64();
  } else {
    if (isString()) {
      int64 lval; double dval;
      DataType ret = convertToNumeric(&lval, &dval);
      if (ret == KindOfDouble) {
        return -dval;
      } else if (ret == KindOfInt64) {
        return -lval;
      } else {
        return -toInt64();
      }
    } else {
      ASSERT(false);
    }
  }
  return *this;
}

Variant Variant::operator-(CVarRef var) const {
  if (is(KindOfArray) || var.is(KindOfArray)) {
    throw BadArrayOperandException();
  }
  if (isDouble() || var.isDouble()) {
    return toDouble() - var.toDouble();
  }
  if (isIntVal() && var.isIntVal()) {
    return toInt64() - var.toInt64();
  }
  if (isString()) {
    int64 lval; double dval;
    DataType ret = convertToNumeric(&lval, &dval);
    if (ret == KindOfDouble) {
      return dval - var.toDouble();
    }
  }
  if (var.isString()) {
    int64 lval; double dval;
    DataType ret = var.convertToNumeric(&lval, &dval);
    if (ret == KindOfDouble) {
      return toDouble() - dval;
    }
  }
  return toInt64() - var.toInt64();
}

Variant &Variant::operator-=(CVarRef var) {
  if (is(KindOfArray) || var.is(KindOfArray)) {
    throw BadArrayOperandException();
  }
  if (isDouble() || var.isDouble()) {
    set(toDouble() - var.toDouble());
  } else if (isIntVal() && var.isIntVal()) {
    set(toInt64() - var.toInt64());
  } else {
    if (isString()) {
      int64 lval; double dval;
      DataType ret = convertToNumeric(&lval, &dval);
      if (ret == KindOfDouble) {
        set(dval - var.toDouble());
        return *this;
      }
    }
    if (var.isString()) {
      int64 lval; double dval;
      DataType ret = var.convertToNumeric(&lval, &dval);
      if (ret == KindOfDouble) {
        set(toDouble() - dval);
        return *this;
      }
    }
    set(toInt64() - var.toInt64());
  }
  return *this;
}

Variant &Variant::operator-=(int64 n) {
  if (is(KindOfArray)) {
    throw BadArrayOperandException();
  }
  if (isDouble()) {
    set(toDouble() - n);
  } else if (isIntVal()) {
    set(toInt64() - n);
  } else {
    if (isString()) {
      int64 lval; double dval;
      DataType ret = convertToNumeric(&lval, &dval);
      if (ret == KindOfDouble) {
        set(dval - n);
        return *this;
      }
    } else {
      ASSERT(false);
    }
    set(toInt64() - n);
  }
  return *this;
}

Variant &Variant::operator-=(double n) {
  if (is(KindOfArray)) {
    throw BadArrayOperandException();
  }
  set(toDouble() - n);
  return *this;
}

///////////////////////////////////////////////////////////////////////////////
// multiply

Variant Variant::operator*(CVarRef var) const {
  if (is(KindOfArray) || var.is(KindOfArray)) {
    throw BadArrayOperandException();
  }
  if (isDouble() || var.isDouble()) {
    return toDouble() * var.toDouble();
  }
  if (isIntVal() && var.isIntVal()) {
    return toInt64() * var.toInt64();
  }
  if (isString()) {
    int64 lval; double dval;
    DataType ret = convertToNumeric(&lval, &dval);
    if (ret == KindOfDouble) {
      return dval * var.toDouble();
    }
  }
  if (var.isString()) {
    int64 lval; double dval;
    DataType ret = var.convertToNumeric(&lval, &dval);
    if (ret == KindOfDouble) {
      return toDouble() * dval;
    }
  }
  return toInt64() * var.toInt64();
}

Variant &Variant::operator*=(CVarRef var) {
  if (is(KindOfArray) || var.is(KindOfArray)) {
    throw BadArrayOperandException();
  }
  if (isDouble() || var.isDouble()) {
    set(toDouble() * var.toDouble());
  } else if (isIntVal() && var.isIntVal()) {
    set(toInt64() * var.toInt64());
  } else {
    if (isString()) {
      int64 lval; double dval;
      DataType ret = convertToNumeric(&lval, &dval);
      if (ret == KindOfDouble) {
        set(dval * var.toDouble());
        return *this;
      }
    }
    if (var.isString()) {
      int64 lval; double dval;
      DataType ret = var.convertToNumeric(&lval, &dval);
      if (ret == KindOfDouble) {
        set(toDouble() * dval);
        return *this;
      }
    }
    set(toInt64() * var.toInt64());
  }
  return *this;
}

Variant &Variant::operator*=(int64 n) {
  if (is(KindOfArray)) {
    throw BadArrayOperandException();
  }
  if (isDouble()) {
    set(toDouble() * n);
  } else if (isIntVal()) {
    set(toInt64() * n);
  } else {
    if (isString()) {
      int64 lval; double dval;
      DataType ret = convertToNumeric(&lval, &dval);
      if (ret == KindOfDouble) {
        set(dval * n);
        return *this;
      }
    } else {
      ASSERT(false);
    }
    set(toInt64() * n);
  }
  return *this;
}

Variant &Variant::operator*=(double n) {
  if (is(KindOfArray)) {
    throw BadArrayOperandException();
  }
  set(toDouble() * n);
  return *this;
}

///////////////////////////////////////////////////////////////////////////////
// divide

Variant Variant::operator/(CVarRef var) const {
  if (is(KindOfArray) || var.is(KindOfArray)) {
    throw BadArrayOperandException();
  }
  int64 lval; double dval; bool int1 = true;
  int64 lval2; double dval2; bool int2 = true;

  if (isDouble()) {
    dval = toDouble();
    int1 = false;
  } else if (isIntVal()) {
    lval = toInt64();
  } else if (isString()) {
    DataType ret = convertToNumeric(&lval, &dval);
    if (ret == KindOfDouble) {
      int1 = false;
    } else if (ret != KindOfInt64) {
      lval = 0;
    }
  } else {
    ASSERT(false);
  }
  if (var.isDouble()) {
    dval2 = var.toDouble();
    int2 = false;
  } else if (var.isIntVal()) {
    lval2 = var.toInt64();
  } else if (var.isString()) {
    DataType ret = var.convertToNumeric(&lval2, &dval2);
    if (ret == KindOfDouble) {
      int2 = false;
    } else if (ret != KindOfInt64) {
      lval2 = 0;
    }
  } else {
    ASSERT(false);
  }

  if ((int2 && lval2 == 0) || (!int2 && dval2 == 0)) {
    // FIXME: raise_warning("Division by zero");
    return false;
  }

  if (int1 && int2) {
    if (lval % lval2 == 0) {
      return lval / lval2;
    } else {
      return (double)lval / lval2;
    }
  } else if (int1 && !int2) {
    return lval / dval2;
  } else if (!int1 && int2) {
    return dval / lval2;
  } else {
    return dval / dval2;
  }
}

Variant &Variant::operator/=(CVarRef var) {
  if (is(KindOfArray) || var.is(KindOfArray)) {
    throw BadArrayOperandException();
  }
  int64 lval; double dval; bool int1 = true;
  int64 lval2; double dval2; bool int2 = true;

  if (isDouble()) {
    dval = toDouble();
    int1 = false;
  } else if (isIntVal()) {
    lval = toInt64();
  } else if (isString()) {
    DataType ret = convertToNumeric(&lval, &dval);
    if (ret == KindOfDouble) {
      int1 = false;
    } else if (ret != KindOfInt64) {
      lval = 0;
    }
  } else {
    ASSERT(false);
  }
  if (var.isDouble()) {
    dval2 = var.toDouble();
    int2 = false;
  } else if (var.isIntVal()) {
    lval2 = var.toInt64();
  } else if (var.isString()) {
    DataType ret = var.convertToNumeric(&lval2, &dval2);
    if (ret == KindOfDouble) {
      int2 = false;
    } else if (ret != KindOfInt64) {
      lval2 = 0;
    }
  } else {
    ASSERT(false);
  }

  if ((int2 && lval2 == 0) || (!int2 && dval2 == 0)) {
    // FIXME: raise_warning("Division by zero");
    set(false);
    return *this;
  }

  if (int1 && int2) {
    if (lval % lval2 == 0) {
      set(lval / lval2);
    } else {
      set((double)lval / lval2);
    }
  } else if (int1 && !int2) {
    set(lval / dval2);
  } else if (!int1 && int2) {
    set(dval / lval2);
  } else {
    set(dval / dval2);
  }
  return *this;
}

Variant &Variant::operator/=(int64 n) {
  if (is(KindOfArray)) {
    throw BadArrayOperandException();
  }
  if (n == 0) {
    // FIXME: raise_warning("Division by zero");
    set(false);
    return *this;
  }

  if (isIntVal() && toInt64() % n == 0) {
    set(toInt64() / n);
  } else if (isDouble()) {
    set(toDouble() / n);
  } else {
    if (isString()) {
      int64 lval; double dval;
      DataType ret = convertToNumeric(&lval, &dval);
      if (ret == KindOfInt64 && lval % n == 0) {
        set(lval / n);
        return *this;
      }
    }
    set(toDouble() / n);
  }
  return *this;
}

Variant &Variant::operator/=(double n) {
  if (is(KindOfArray)) {
    throw BadArrayOperandException();
  }
  if (n == 0.0) {
    // FIXME: raise_warning("Division by zero");
    set(false);
    return *this;
  }
  set(toDouble() / n);
  return *this;
}

///////////////////////////////////////////////////////////////////////////////
// modulus

int64 Variant::operator%(CVarRef var) const {
  int64 lval = toInt64();
  int64 lval2 = var.toInt64();
  if (lval2 == 0) {
    // FIXME: raise_warning("Division by zero");
    return false;
  }
  return lval % lval2;
}

Variant &Variant::operator%=(CVarRef var) {
  int64 lval = toInt64();
  int64 lval2 = var.toInt64();
  if (lval2 == 0) {
    // FIXME: raise_warning("Division by zero");
    set(false);
    return *this;
  }
  set(lval % lval2);
  return *this;
}

Variant &Variant::operator%=(int64 n) {
  if (n == 0) {
    // FIXME: raise_warning("Division by zero");
    set(false);
    return *this;
  }
  set(toInt64() % n);
  return *this;
}

Variant &Variant::operator%=(double n) {
  if ((int64)n == 0) {
    // FIXME: raise_warning("Division by zero");
    set(false);
    return *this;
  }
  set(toInt64() % (int64)n);
  return *this;
}

///////////////////////////////////////////////////////////////////////////////
// bitwise

Variant Variant::operator~() const {
  switch (getType()) {
  case KindOfInt32:
  case KindOfInt64:
    return ~toInt64();
  case KindOfDouble:
    return ~(int64)(toDouble());
  case KindOfStaticString:
  case KindOfString:
    return ~toString();
  default:
    break;
  }
  throw InvalidOperandException("only numerics and strings can be negated");
}

Variant Variant::operator|(CVarRef v) const {
  if (isString() && v.isString()) {
    return toString() | v.toString();
  }
  return toInt64() | v.toInt64();
}

Variant Variant::operator&(CVarRef v) const {
  if (isString() && v.isString()) {
    return toString() & v.toString();
  }
  return toInt64() & v.toInt64();
}

Variant Variant::operator^(CVarRef v) const {
  if (isString() && v.isString()) {
    return toString() ^ v.toString();
  }
  return toInt64() ^ v.toInt64();
}

Variant &Variant::operator|=(CVarRef v) {
  if (isString() && v.isString()) {
    set(toString() | v.toString());
  } else {
    set(toInt64() | v.toInt64());
  }
  return *this;
}

Variant &Variant::operator&=(CVarRef v) {
  if (isString() && v.isString()) {
    set(toString() & v.toString());
  } else {
    set(toInt64() & v.toInt64());
  }
  return *this;
}

Variant &Variant::operator^=(CVarRef v) {
  if (isString() && v.isString()) {
    set(toString() ^ v.toString());
  } else {
    set(toInt64() ^ v.toInt64());
  }
  return *this;
}

Variant &Variant::operator<<=(int64 n) {
  set(toInt64() << n);
  return *this;
}

Variant &Variant::operator>>=(int64 n) {
  set(toInt64() >> n);
  return *this;
}

///////////////////////////////////////////////////////////////////////////////
// increment/decrement

Variant &Variant::operator++() {
  switch (getType()) {
  case KindOfUninit:
  case KindOfNull:   set(1LL); break;
  case KindOfInt32:
  case KindOfInt64:  set(toInt64() + 1);  break;
  case KindOfDouble: set(toDouble() + 1); break;
  case KindOfStaticString:
  case KindOfString:
    {
      if (getStringData()->empty()) {
        set("1");
      } else {
        int64 lval; double dval;
        DataType ret = convertToNumeric(&lval, &dval);
        switch (ret) {
        case KindOfInt64:  set(lval + 1); break;
        case KindOfDouble: set(dval + 1); break;
        case KindOfUninit:
        case KindOfNull:
          split();
          getStringData()->inc(); break;
        default:
          ASSERT(false);
          break;
        }
      }
    }
    break;
  default:
    break;
  }
  return *this;
}

Variant Variant::operator++(int) {
  Variant ret(*this);
  operator++();
  return ret;
}

Variant &Variant::operator--() {
  switch (getType()) {
  case KindOfInt32:
  case KindOfInt64:  set(toInt64() - 1);  break;
  case KindOfDouble: set(toDouble() - 1); break;
  case KindOfStaticString:
  case KindOfString:
    {
      if (getStringData()->empty()) {
        set(-1LL);
      } else {
        int64 lval; double dval;
        DataType ret = convertToNumeric(&lval, &dval);
        switch (ret) {
        case KindOfInt64:  set(lval - 1);   break;
        case KindOfDouble: set(dval - 1);   break;
        case KindOfUninit:
        case KindOfNull:   /* do nothing */ break;
        default:
          ASSERT(false);
          break;
        }
      }
    }
    break;
  default:
    break;
  }
  return *this;
}

Variant Variant::operator--(int) {
  Variant ret(*this);
  operator--();
  return ret;
}

///////////////////////////////////////////////////////////////////////////////
// iterator functions

ArrayIter Variant::begin(CStrRef context /* = null_string */) const {
  if (is(KindOfArray)) {
    return ArrayIter(getArrayData());
  }
  // FIXME: raise_warning("Invalid argument supplied for foreach()");
  return ArrayIter();
}

MutableArrayIter Variant::begin(Variant *key, Variant &val,
                                CStrRef context /* = null_string */) {
  return MutableArrayIter(this, key, val);
}

void Variant::escalate(bool mutableIteration /* = false */) {
  TypedValueAccessor tva = getTypedAccessor();
  if (GetAccessorType(tva) == KindOfArray) {
    ArrayData *arr = GetArrayData(tva);
    ArrayData *esc = arr->escalate(mutableIteration);
    if (arr != esc) set(esc);
  }
}

///////////////////////////////////////////////////////////////////////////////
// type conversions

String Variant::toStringHelper() const {
  switch (getRawType()) {
  case KindOfUninit:
  case KindOfNull:    return empty_string;
  case KindOfBoolean: return toInt64() ? s_1 : empty_string;
  case KindOfDouble:  return toDouble();
  case KindOfStaticString:
  case KindOfString:
    ASSERT(false); // Should be done in caller
    return getRawStringData();
  case KindOfVariant: return getRefCVar()->toString();
  default:
    break;
  }
  return toInt64();
}

Array Variant::toArrayHelper() const {
  switch (getRawType()) {
  case KindOfUninit:
  case KindOfNull:    return Array::Create();
  case KindOfInt64:   return Array::Create(toInt64());
  case KindOfStaticString:
  case KindOfString:  return Array::Create(getRawStringData());
  case KindOfArray:   return getRawArrayData();
  // FIXME: case KindOfObject:  return m_data.pobj->o_toArray();
  case KindOfVariant: return getRefCVar()->toArray();
  default:
    break;
  }
  return Array::Create(*this);
}

Variant Variant::toKey() const {
  DataType type = getRawType();
  if (type == KindOfString || type == KindOfStaticString) {
    int64 n;
    StringData *pstr = getRawStringData();
    if (pstr->isStrictlyInteger(n)) {
      return n;
    } else {
      return pstr;
    }
  }
  switch (type) {
  case KindOfUninit:
  case KindOfNull:
    return empty_string;
  case KindOfBoolean:
  case KindOfInt32:
  case KindOfInt64:
    return toInt64();
  case KindOfDouble:
    return toDouble();
  case KindOfObject:
    if (isResource()) {
      return toInt64();
    }
    break;
  case KindOfVariant:
    return getRefCVar()->toKey();
  default:
    break;
  }
  // FIXME: throw_bad_type_exception("Invalid type used as key");
  return null_varNR;
}

Variant::operator String() const {
  return toString();
}

Variant::operator Array() const {
  return toArray();
}

///////////////////////////////////////////////////////////////////////////////
// comparisons

bool Variant::same(bool v2) const {
  return isBoolean() && HPHP::equal(v2, getBoolean());
}

bool Variant::same(int v2) const {
  return same((int64)v2);
}

bool Variant::same(int64 v2) const {
  TypedValueAccessor acc = getTypedAccessor();
  switch (GetAccessorType(acc)) {
  case KindOfInt32:
  case KindOfInt64:
    return HPHP::equal(v2, GetInt64(acc));
  default:
    break;
  }
  return false;
}

bool Variant::same(double v2) const {
  return isDouble() && HPHP::equal(v2, getDouble());
}

bool Variant::same(litstr v2) const {
  StringData sd2(v2);
  return same(&sd2);
}

bool Variant::same(const StringData *v2) const {
  bool null1 = isNull();
  bool null2 = (v2 == NULL);
  if (null1 && null2) return true;
  if (null1 || null2) return false;
  return isString() && HPHP::same(getStringData(), v2);
}

bool Variant::same(CStrRef v2) const {
  return same(v2.get());
}

bool Variant::same(CArrRef v2) const {
  bool null1 = isNull();
  bool null2 = v2.isNull();
  if (null1 && null2) return true;
  if (null1 || null2) return false;
  return is(KindOfArray) && Array(getArrayData()).same(v2);
}

bool Variant::same(CVarRef v2) const {
  bool null1 = isNull();
  bool null2 = v2.isNull();
  if (null1 && null2) return true;
  if (null1 || null2) return false;

  TypedValueAccessor acc = getTypedAccessor();
  switch (GetAccessorType(acc)) {
  case KindOfInt32:
  case KindOfInt64: {
    TypedValueAccessor acc2 = v2.getTypedAccessor();
    switch (GetAccessorType(acc2)) {
    case KindOfInt32:
    case KindOfInt64:
      return HPHP::equal(GetInt64(acc), GetInt64(acc2));
    default:
      break;
    }
    break;
  }
  default:
    break;
  }
  return getType() == v2.getType() && equal(v2);
}

///////////////////////////////////////////////////////////////////////////////

#define UNWRAP(reverse)                                                    \
  TypedValueAccessor acc = getTypedAccessor();                             \
  switch (GetAccessorType(acc)) {                                          \
  case KindOfUninit:                                                       \
  case KindOfNull:    return HPHP::reverse(v2, false);                     \
  case KindOfBoolean: return HPHP::reverse(v2, GetBoolean(acc));           \
  case KindOfInt32:                                                        \
  case KindOfInt64:   return HPHP::reverse(v2, GetInt64(acc));             \
  case KindOfDouble:  return HPHP::reverse(v2, GetDouble(acc));            \
  case KindOfStaticString:                                                 \
  case KindOfString:  return HPHP::reverse(v2, GetStringData(acc));        \
  case KindOfArray:   return HPHP::reverse(v2, Array(GetArrayData(acc)));  \
  default:                                                                 \
    ASSERT(false);                                                         \
    break;                                                                 \
  }                                                                        \
  return false;                                                            \

// "null" needs to convert to "" before comparing with a string
#define UNWRAP_STR(reverse)                                                \
  TypedValueAccessor acc = getTypedAccessor();                             \
  switch (GetAccessorType(acc)) {                                          \
  case KindOfUninit:                                                       \
  case KindOfNull:    return HPHP::reverse(v2, empty_string);              \
  case KindOfBoolean: return HPHP::reverse(v2, GetBoolean(acc));           \
  case KindOfInt32:                                                        \
  case KindOfInt64:   return HPHP::reverse(v2, GetInt64(acc));             \
  case KindOfDouble:  return HPHP::reverse(v2, GetDouble(acc));            \
  case KindOfStaticString:                                                 \
  case KindOfString:  return HPHP::reverse(v2, GetStringData(acc));        \
  case KindOfArray:   return HPHP::reverse(v2, Array(GetArrayData(acc)));  \
  default:                                                                 \
    ASSERT(false);                                                         \
    break;                                                                 \
  }                                                                        \
  return false;                                                            \

// Array needs to convert to "Array" and Object to String
#define UNWRAP_STRING(reverse)                                             \
  TypedValueAccessor acc = getTypedAccessor();                             \
  switch (GetAccessorType(acc)) {                                          \
  case KindOfUninit:                                                       \
  case KindOfNull:    return HPHP::reverse(v2, empty_string);              \
  case KindOfBoolean: return HPHP::reverse(v2, GetBoolean(acc));           \
  case KindOfInt32:                                                        \
  case KindOfInt64:   return HPHP::reverse(v2, GetInt64(acc));             \
  case KindOfDouble:  return HPHP::reverse(v2, GetDouble(acc));            \
  case KindOfStaticString:                                                 \
  case KindOfString:  return HPHP::reverse(v2, GetStringData(acc));        \
  case KindOfArray:   return HPHP::reverse(v2, s_array);                   \
  default:                                                                 \
    ASSERT(false);                                                         \
    break;                                                                 \
  }                                                                        \
  return false;                                                            \

// "null" needs to convert to "" before comparing with a string
#define UNWRAP_VAR(forward, reverse)                                       \
  TypedValueAccessor acc = getTypedAccessor();                             \
  switch (GetAccessorType(acc)) {                                          \
  case KindOfUninit:                                                       \
  case KindOfNull:                                                         \
    if (v2.isString()) {                                                   \
      return HPHP::reverse(v2.getStringData(), empty_string);              \
    }                                                                      \
    return HPHP::reverse(v2, false);                                       \
  case KindOfBoolean: return HPHP::reverse(v2, GetBoolean(acc));           \
  case KindOfInt32:                                                        \
  case KindOfInt64:   return HPHP::reverse(v2, GetInt64(acc));             \
  case KindOfDouble:  return HPHP::reverse(v2, GetDouble(acc));            \
  case KindOfStaticString:                                                 \
  case KindOfString:  return HPHP::reverse(v2, GetStringData(acc));        \
  case KindOfArray:                                                        \
    if (v2.isArray()) {                                                    \
      return Array(GetArrayData(acc)).forward(Array(v2.getArrayData()));   \
    }                                                                      \
    return HPHP::reverse(v2, Array(GetArrayData(acc)));                    \
  default:                                                                 \
    ASSERT(false);                                                         \
    break;                                                                 \
  }                                                                        \
  return false;                                                            \

// array comparison is directional when they are uncomparable
// also, ">" is implemented as "!<=" in Zend
#define UNWRAP_ARR(forward, reverse)                                       \
  TypedValueAccessor acc = getTypedAccessor();                             \
  switch (GetAccessorType(acc)) {                                          \
  case KindOfUninit:                                                       \
  case KindOfNull:    return HPHP::reverse(v2, false);                     \
  case KindOfBoolean: return HPHP::reverse(v2, GetBoolean(acc));           \
  case KindOfInt32:                                                        \
  case KindOfInt64:   return HPHP::reverse(v2, GetInt64(acc));             \
  case KindOfDouble:  return HPHP::reverse(v2, GetDouble(acc));            \
  case KindOfStaticString:                                                 \
  case KindOfString:  return HPHP::reverse(v2, GetStringData(acc));        \
  case KindOfArray:   return Array(GetArrayData(acc)).forward(v2);         \
  default:                                                                 \
    ASSERT(false);                                                         \
    break;                                                                 \
  }                                                                        \
  return false;                                                            \

bool Variant::equal(bool    v2) const { UNWRAP(equal);}
bool Variant::equal(int     v2) const { UNWRAP(equal);}
bool Variant::equal(int64   v2) const { UNWRAP(equal);}
bool Variant::equal(double  v2) const { UNWRAP(equal);}
bool Variant::equal(litstr  v2) const { UNWRAP_STR(equal);}
bool Variant::equal(const StringData *v2) const { UNWRAP_STR(equal);}
bool Variant::equal(CStrRef v2) const { UNWRAP_STR(equal);}
bool Variant::equal(CArrRef v2) const { UNWRAP(equal);}
bool Variant::equal(CVarRef v2) const { UNWRAP_VAR(equal,equal);}

bool Variant::equalAsStr(bool    v2) const { UNWRAP_STRING(equalAsStr);}
bool Variant::equalAsStr(int     v2) const { UNWRAP_STRING(equalAsStr);}
bool Variant::equalAsStr(int64   v2) const { UNWRAP_STRING(equalAsStr);}
bool Variant::equalAsStr(double  v2) const { UNWRAP_STRING(equalAsStr);}
bool Variant::equalAsStr(litstr  v2) const { UNWRAP_STRING(equalAsStr);}
bool Variant::equalAsStr(const StringData *v2) const {
  UNWRAP_STRING(equalAsStr);
}
bool Variant::equalAsStr(CStrRef  v2) const { UNWRAP_STRING(equalAsStr);}
bool Variant::equalAsStr(CArrRef  v2) const { UNWRAP_STRING(equalAsStr);}
bool Variant::equalAsStr(CVarRef  v2) const { UNWRAP_STRING(equalAsStr);}

bool Variant::less(bool    v2) const { UNWRAP(more);}
bool Variant::less(int     v2) const { UNWRAP(more);}
bool Variant::less(int64   v2) const { UNWRAP(more);}
bool Variant::less(double  v2) const { UNWRAP(more);}
bool Variant::less(litstr  v2) const { UNWRAP_STR(more);}
bool Variant::less(const StringData *v2) const { UNWRAP_STR(more);}
bool Variant::less(CStrRef v2) const { UNWRAP_STR(more);}
bool Variant::less(CArrRef v2) const { UNWRAP_ARR(less,more);}
bool Variant::less(CVarRef v2) const { UNWRAP_VAR(less,more);}

bool Variant::more(bool    v2) const { UNWRAP(less);}
bool Variant::more(int     v2) const { UNWRAP(less);}
bool Variant::more(int64   v2) const { UNWRAP(less);}
bool Variant::more(double  v2) const { UNWRAP(less);}
bool Variant::more(litstr  v2) const { UNWRAP_STR(less);}
bool Variant::more(const StringData *v2) const { UNWRAP_STR(less);}
bool Variant::more(CStrRef v2) const { UNWRAP_STR(less);}
bool Variant::more(CArrRef v2) const { UNWRAP_ARR(more,less);}
bool Variant::more(CVarRef v2) const { UNWRAP_VAR(more,less);}

///////////////////////////////////////////////////////////////////////////////
// offset functions

#define IMPLEMENT_RVAL_INTEGRAL                                         \
  DataType m_type = getRawType();                                       \
  if (m_type == KindOfArray) {                                          \
    return getRawArrayData()->get(ToKey(offset), flags & AccessFlags::Error); \
  }                                                                     \
  switch (m_type) {                                                     \
    case KindOfStaticString:                                            \
    case KindOfString:                                                  \
      return getRawStringData()->getChar((int)offset);                  \
    case KindOfVariant:                                                 \
      return getRefCVar()->rvalAt(offset, flags);                       \
    case KindOfUninit:                                                  \
    case KindOfNull:                                                    \
      break;                                                            \
    default:                                                            \
      if ((flags & AccessFlags::Error) &&                               \
          !(flags & AccessFlags::NoHipHop)) {                           \
        /* FIXME: raise_bad_offset_notice();*/                                      \
      }                                                                 \
      break;                                                            \
  }                                                                     \
  return null_variant;

Variant Variant::rvalAt(bool offset, ACCESSPARAMS_IMPL) const {
  IMPLEMENT_RVAL_INTEGRAL
}
Variant Variant::rvalAt(double offset, ACCESSPARAMS_IMPL) const {
  IMPLEMENT_RVAL_INTEGRAL
}

Variant Variant::rvalAtHelper(int64 offset, ACCESSPARAMS_IMPL) const {
  DataType m_type = getRawType();
  switch (m_type) {
  case KindOfStaticString:
  case KindOfString:
    return getRawStringData()->getChar((int)offset);
  case KindOfVariant:
    return getRefCVar()->rvalAt(offset, flags);
  case KindOfUninit:
  case KindOfNull:
    break;
  default:
    if ((flags & AccessFlags::Error) && !(flags & AccessFlags::NoHipHop)) {
      // FIXME: raise_bad_offset_notice();
    }
    break;
  }
  return null_variant;
}

Variant Variant::rvalAt(litstr offset, ACCESSPARAMS_IMPL) const {
  DataType m_type = getRawType();
  if (m_type == KindOfArray) {
    bool error = flags & AccessFlags::Error;
    if (flags & AccessFlags::Key) {
      return getRawArrayData()->get(offset, error);
    }
    int64 n;
    int len = strlen(offset);
    if (!is_strictly_integer(offset, len, n)) {
      return getRawArrayData()->get(offset, error);
    } else {
      return getRawArrayData()->get(n, error);
    }
  }
  switch (m_type) {
  case KindOfStaticString:
  case KindOfString:
    return getRawStringData()->getChar(StringData(offset).toInt32());
  case KindOfVariant:
    return getRefCVar()->rvalAt(offset, flags);
  case KindOfUninit:
  case KindOfNull:
    break;
  default:
    if ((flags & AccessFlags::Error) && !(flags & AccessFlags::NoHipHop)) {
      // FIXME: raise_bad_offset_notice();
    }
    break;
  }
  return null_variant;
}

Variant Variant::rvalAt(CStrRef offset, ACCESSPARAMS_IMPL) const {
  DataType m_type = getRawType();
  if (m_type == KindOfArray) {
    bool error = flags & AccessFlags::Error;
    if (flags & AccessFlags::Key) {
      return getRawArrayData()->get(offset, error);
    }
    if (offset.isNull()) return getRawArrayData()->get(empty_string, error);
    int64 n;
    if (!offset->isStrictlyInteger(n)) {
      return getRawArrayData()->get(offset, error);
    } else {
      return getRawArrayData()->get(n, error);
    }
  }
  switch (m_type) {
  case KindOfStaticString:
  case KindOfString:
    return getRawStringData()->getChar(offset.toInt32());
  case KindOfVariant:
    return getRefCVar()->rvalAt(offset, flags);
  case KindOfUninit:
  case KindOfNull:
    break;
  default:
    if ((flags & AccessFlags::Error) && !(flags & AccessFlags::NoHipHop)) {
      // FIXME: raise_bad_offset_notice();
    }
    break;
  }
  return null_variant;
}

Variant Variant::rvalAt(CVarRef offset, ACCESSPARAMS_IMPL) const {
  DataType m_type = getRawType();
  if (m_type == KindOfArray) {
    // Fast path for KindOfArray
    switch (offset.getRawType()) {
    case KindOfUninit:
    case KindOfNull:
      return getRawArrayData()->get(empty_string, flags & AccessFlags::Error);
    case KindOfBoolean:
    case KindOfInt64:
      return getRawArrayData()->get(offset.asInt64Val(), flags & AccessFlags::Error);
    case KindOfDouble:
      return getRawArrayData()->get((int64)offset.asDoubleVal(),
                              flags & AccessFlags::Error);
    case KindOfStaticString:
    case KindOfString: {
      int64 n;
      if (offset.getRawStringData()->isStrictlyInteger(n)) {
        return getRawArrayData()->get(n, flags & AccessFlags::Error);
      } else {
        return getRawArrayData()->get(offset.asCStrRef(), flags & AccessFlags::Error);
      }
    }
    case KindOfArray:
      // FIXME: throw_bad_type_exception("Invalid type used as key");
      break;
    case KindOfVariant:
      return rvalAt(*(offset.getRefCVar()), flags);
    default:
      ASSERT(false);
      break;
    }
    return null_variant;
  }
  switch (m_type) {
  case KindOfStaticString:
  case KindOfString:
    return getRawStringData()->getChar(offset.toInt32());
  case KindOfVariant:
    return getRefCVar()->rvalAt(offset, flags);
  case KindOfUninit:
  case KindOfNull:
    break;
  default:
    if ((flags & AccessFlags::Error) && !(flags & AccessFlags::NoHipHop)) {
      // FIXME: raise_bad_offset_notice();
    }
    break;
  }
  return null_variant;
}

template <typename T>
CVarRef Variant::rvalRefHelper(T offset, CVarRef tmp, ACCESSPARAMS_IMPL) const {
  DataType m_type = getRawType();
  switch (m_type) {
  case KindOfStaticString:
  case KindOfString:
    const_cast<Variant&>(tmp) = getRawStringData()->getChar(HPHP::toInt32(offset));
    return tmp;
  case KindOfVariant:
    return getRefCVar()->rvalRef(offset, tmp, flags);
  case KindOfUninit:
  case KindOfNull:
    break;
  default:
    if ((flags & AccessFlags::Error) && !(flags & AccessFlags::NoHipHop)) {
      // FIXME: raise_bad_offset_notice();
    }
    break;
  }
  return null_variant;
}

template CVarRef
Variant::rvalRefHelper(int64 offset, CVarRef tmp, ACCESSPARAMS_IMPL) const;

CVarRef Variant::rvalRef(bool offset, CVarRef tmp, ACCESSPARAMS_IMPL) const {
  DataType m_type = getRawType();
  if (m_type == KindOfArray) {
    return getRawArrayData()->get(ToKey(offset), flags & AccessFlags::Error);
  }
  return rvalRefHelper(offset, tmp, flags);
}

CVarRef Variant::rvalRef(double offset, CVarRef tmp, ACCESSPARAMS_IMPL) const {
  DataType m_type = getRawType();
  if (m_type == KindOfArray) {
    return getRawArrayData()->get(ToKey(offset), flags & AccessFlags::Error);
  }
  return rvalRefHelper(offset, tmp, flags);
}

CVarRef Variant::rvalRef(litstr offset, CVarRef tmp, ACCESSPARAMS_IMPL) const {
  DataType m_type = getRawType();
  if (m_type == KindOfArray) {
    bool error = flags & AccessFlags::Error;
    if (flags & AccessFlags::Key) return getRawArrayData()->get(offset, error);
    int64 n;
    int len = strlen(offset);
    if (!is_strictly_integer(offset, len, n)) {
      return getRawArrayData()->get(offset, error);
    } else {
      return getRawArrayData()->get(n, error);
    }
  }
  return rvalRefHelper(offset, tmp, flags);
}

CVarRef Variant::rvalRef(CStrRef offset, CVarRef tmp, ACCESSPARAMS_IMPL) const {
  DataType m_type = getRawType();
  if (m_type == KindOfArray) {
    bool error = flags & AccessFlags::Error;
    if (flags & AccessFlags::Key) return getRawArrayData()->get(offset, error);
    if (offset.isNull()) return getRawArrayData()->get(empty_string, error);
    int64 n;
    if (!offset->isStrictlyInteger(n)) {
      return getRawArrayData()->get(offset, error);
    } else {
      return getRawArrayData()->get(n, error);
    }
  }
  return rvalRefHelper(offset, tmp, flags);
}

CVarRef Variant::rvalRef(CVarRef offset, CVarRef tmp, ACCESSPARAMS_IMPL) const {
  DataType m_type = getRawType();
  if (m_type == KindOfArray) {
    // Fast path for KindOfArray
    switch (offset.getRawType()) {
    case KindOfUninit:
    case KindOfNull:
      return getRawArrayData()->get(empty_string, flags & AccessFlags::Error);
    case KindOfBoolean:
    case KindOfInt64:
      return getRawArrayData()->get(offset.asInt64Val(), flags & AccessFlags::Error);
    case KindOfDouble:
      return getRawArrayData()->get((int64)offset.asDoubleVal(),
                              flags & AccessFlags::Error);
    case KindOfStaticString:
    case KindOfString: {
      int64 n;
      if (offset.getRawStringData()->isStrictlyInteger(n)) {
        return getRawArrayData()->get(n, flags & AccessFlags::Error);
      } else {
        return getRawArrayData()->get(offset.asCStrRef(), flags & AccessFlags::Error);
      }
    }
    case KindOfArray:
      // FIXME: throw_bad_type_exception("Invalid type used as key");
      break;
    case KindOfVariant:
      return rvalRef(*(offset.getRefCVar()), tmp, flags);
    default:
      ASSERT(false);
      break;
    }
    return null_variant;
  }
  return rvalRefHelper(offset, tmp, flags);
}

template <typename T>
CVarRef Variant::rvalAtRefHelper(T offset, ACCESSPARAMS_IMPL) const {
  DataType m_type = getRawType();
  if (LIKELY(m_type == KindOfArray)) {
    return asCArrRef().rvalAtRef(offset, flags);
  }
  if (LIKELY(m_type == KindOfVariant)) {
    return getRefCVar()->rvalAtRefHelper<T>(offset, flags);
  }
  return null_variant;
}

template
CVarRef Variant::rvalAtRefHelper<int64>(int64 offset, ACCESSPARAMS_DECL) const;
template
CVarRef Variant::rvalAtRefHelper<CStrRef>(CStrRef offset,
                                          ACCESSPARAMS_DECL) const;
template
CVarRef Variant::rvalAtRefHelper<CVarRef>(CVarRef offset,
                                          ACCESSPARAMS_DECL) const;
CVarRef Variant::rvalAtRef(double offset, ACCESSPARAMS_IMPL) const {
  return rvalAtRefHelper(HPHP::toInt64(offset), flags);
}

template <typename T>
class LvalHelper {};

template<>
class LvalHelper<int64> {
public:
  typedef int64 KeyType;
  static bool CheckKey(KeyType k) { return true; };
  static const bool CheckParams = false;
};

template<>
class LvalHelper<bool> : public LvalHelper<int64> {};

template<>
class LvalHelper<double> : public LvalHelper<int64> {};

template<>
class LvalHelper<CStrRef> {
public:
  typedef VarNR KeyType;
  static bool CheckKey(const KeyType &k) { return true; };
  static const bool CheckParams = true;
};

template<>
class LvalHelper<CVarRef> {
public:
  typedef VarNR KeyType;
  static bool CheckKey(const KeyType &k) { return !k.isNull(); };
  static const bool CheckParams = true;
};

template<typename T>
Variant& Variant::LvalAtImpl0(
    Variant *self, T key, Variant *tmp, bool blackHole, ACCESSPARAMS_IMPL) {
head:
  DataType self_m_type = self->getRawType();
  if (self_m_type == KindOfArray) {
    ArrayData *arr = self->getRawArrayData();
    ArrayData *escalated;
    Variant *ret = NULL;
    if (LvalHelper<T>::CheckParams && flags & AccessFlags::Key) {
      escalated = arr->lval(key, ret, arr->getCount() > 1,
                            flags & AccessFlags::CheckExist);
    } else {
      typename LvalHelper<T>::KeyType k(ToKey(key));
      if (LvalHelper<T>::CheckKey(k)) {
        escalated =
          arr->lval(k, ret, arr->getCount() > 1,
                    flags & AccessFlags::CheckExist);
      } else {
        if (blackHole) ret = &lvalBlackHole();
        else           ret = tmp;
        escalated = 0;
      }
    }
    if (escalated) {
      self->set(escalated);
    }
    ASSERT(ret);
    return *ret;
  }
  if (self_m_type == KindOfVariant) {
    self = self->getRefVar();
    goto head;
  }
  return lvalInvalid();
}

template<typename T>
Variant& Variant::lvalAtImpl(T key, ACCESSPARAMS_IMPL) {
  return Variant::LvalAtImpl0<T>(this, key, NULL, true, flags);
}

Variant &Variant::lvalAt(bool    key, ACCESSPARAMS_IMPL) {
  return lvalAtImpl(key, flags);
}
Variant &Variant::lvalAt(int     key, ACCESSPARAMS_IMPL) {
  return lvalAt((int64)key, flags);
}
Variant &Variant::lvalAt(int64   key, ACCESSPARAMS_IMPL) {
  return lvalAtImpl(key, flags);
}
Variant &Variant::lvalAt(double  key, ACCESSPARAMS_IMPL) {
  return lvalAtImpl(key, flags);
}
Variant &Variant::lvalAt(litstr  ckey, ACCESSPARAMS_IMPL) {
  String key(ckey);
  return lvalAt(key, flags);
}
Variant &Variant::lvalAt(CStrRef key, ACCESSPARAMS_IMPL) {
  return lvalAtImpl<CStrRef>(key, flags);
}
Variant &Variant::lvalAt(CVarRef k, ACCESSPARAMS_IMPL) {
  return lvalAtImpl<CVarRef>(k, flags);
}

Variant &Variant::lvalRef(bool    key, Variant& tmp, ACCESSPARAMS_IMPL) {
  return LvalAtImpl0(this, key, &tmp, false, flags);
}
Variant &Variant::lvalRef(int     key, Variant& tmp, ACCESSPARAMS_IMPL) {
  return lvalRef((int64)key, tmp, flags);
}
Variant &Variant::lvalRef(int64   key, Variant& tmp, ACCESSPARAMS_IMPL) {
  return LvalAtImpl0(this, key, &tmp, false, flags);
}
Variant &Variant::lvalRef(double  key, Variant& tmp, ACCESSPARAMS_IMPL) {
  return LvalAtImpl0(this, key, &tmp, false, flags);
}
Variant &Variant::lvalRef(litstr ckey, Variant& tmp, ACCESSPARAMS_IMPL) {
  String key(ckey);
  return lvalRef(key, tmp, flags);
}
Variant &Variant::lvalRef(CStrRef key, Variant& tmp, ACCESSPARAMS_IMPL) {
  return Variant::LvalAtImpl0<CStrRef>(this, key, &tmp, false, flags);
}
Variant &Variant::lvalRef(CVarRef k, Variant& tmp, ACCESSPARAMS_IMPL) {
  return Variant::LvalAtImpl0<CVarRef>(this, k, &tmp, false, flags);
}

Variant *Variant::lvalPtr(CStrRef key, bool forWrite, bool create) {
  Variant *t = getRefVar();
  if (t->getRawType() == KindOfArray) {
    return t->asArrRef().lvalPtr(key, forWrite, create);
  }
  return NULL;
}

Variant &Variant::lvalAt() {
  DataType m_type = getRawType();
  switch (m_type) {
  case KindOfUninit:
  case KindOfNull:
    set(ArrayData::Create());
    break;
  case KindOfBoolean:
    if (!toBoolean()) {
      set(ArrayData::Create());
    } else {
      // FIXME: throw_bad_type_exception("[] operator not supported for this type");
      return lvalBlackHole();
    }
    break;
  case KindOfArray:
    break;
  case KindOfVariant:
    return getRefVar()->lvalAt();
  case KindOfStaticString:
  case KindOfString:
    if (getStringData()->empty()) {
      set(ArrayData::Create());
      break;
    }
    // fall through to throw
  default:
    // FIXME: throw_bad_type_exception("[] operator not supported for this type");
    return lvalBlackHole();
  }

  ASSERT(m_type == KindOfArray);
  Variant *ret = NULL;
  ArrayData *arr = getRawArrayData();
  ArrayData *escalated = arr->lvalNew(ret, arr->getCount() > 1);
  if (escalated) {
    set(escalated);
  }
  ASSERT(ret);
  return *ret;
}

Variant &Variant::lvalInvalid() {
  // FIXME: throw_bad_type_exception("not array objects");
  return lvalBlackHole();
}

Variant &Variant::lvalBlackHole() {
  // FIXME
  return const_cast<Variant&>(null_variant);
}

Variant Variant::refvalAt(bool    key) {
  return refvalAtImpl(key);
}
Variant Variant::refvalAt(int     key) {
  return refvalAtImpl(key);
}
Variant Variant::refvalAt(int64   key) {
  return refvalAtImpl(key);
}
Variant Variant::refvalAt(double  key) {
  return refvalAtImpl(key);
}
Variant Variant::refvalAt(litstr  key, bool isString /* = false */) {
  return refvalAtImpl(key, isString);
}
Variant Variant::refvalAt(CStrRef key, bool isString /* = false */) {
  return refvalAtImpl(key, isString);
}
Variant Variant::refvalAt(CVarRef key) {
  return refvalAtImpl(key);
}

Variant Variant::refvalAtImpl(CStrRef key, bool isString /* = false */) {
  DataType m_type = getRawType();
  if (m_type == KindOfVariant) {
    return getRefVar()->refvalAtImpl(key, isString);
  }
  if (is(KindOfArray)) { // FIXME:  || isObjectConvertable()) {
    return strongBind(lvalAt(key, AccessFlags::IsKey(isString)));
  } else {
    return rvalAt(key, AccessFlags::IsKey(isString));
  }
}

Variant Variant::argvalAt(bool byRef, bool key) const {
  return const_cast<Variant*>(this)->argvalAtImpl(byRef, key);
}
Variant Variant::argvalAt(bool byRef, int key) const {
  return const_cast<Variant*>(this)->argvalAtImpl(byRef, key);
}
Variant Variant::argvalAt(bool byRef, int64 key) const {
  return const_cast<Variant*>(this)->argvalAtImpl(byRef, key);
}
Variant Variant::argvalAt(bool byRef, double key) const {
  return const_cast<Variant*>(this)->argvalAtImpl(byRef, key);
}
Variant Variant::argvalAt(bool byRef, litstr key,
    bool isString /* = false */) const {
  return const_cast<Variant*>(this)->argvalAtImpl(byRef, key, isString);
}
Variant Variant::argvalAt(bool byRef, CStrRef key,
    bool isString /* = false */) const {
  return const_cast<Variant*>(this)->argvalAtImpl(byRef, key, isString);
}
Variant Variant::argvalAt(bool byRef, CVarRef key) const {
  return const_cast<Variant*>(this)->argvalAtImpl(byRef, key);
}

Variant Variant::argvalAtImpl(bool byRef, CStrRef key,
    bool isString /* = false */) {
  DataType m_type = getRawType();
  if (m_type == KindOfVariant) {
    return getRefVar()->argvalAtImpl(byRef, key, isString);
  }
  if (byRef && (is(KindOfArray))) { // FIXME: || isObjectConvertable())) {
    return strongBind(lvalAt(key, AccessFlags::IsKey(isString)));
  } else {
    return rvalAt(key, AccessFlags::IsKey(isString));
  }
}

#define OPEQUAL(op, l, r)                                               \
  switch (op) {                                                         \
  /* FIXME: case T_CONCAT_EQUAL: concat_assign((l), r); break;*/                    \
  case T_PLUS_EQUAL:   ((l) += r);            break;                    \
  case T_MINUS_EQUAL:  ((l) -= r);            break;                    \
  case T_MUL_EQUAL:    ((l) *= r);            break;                    \
  case T_DIV_EQUAL:    ((l) /= r);            break;                    \
  case T_MOD_EQUAL:    ((l) %= r);            break;                    \
  case T_AND_EQUAL:    ((l) &= r);            break;                    \
  case T_OR_EQUAL:     ((l) |= r);            break;                    \
  case T_XOR_EQUAL:    ((l) ^= r);            break;                    \
  case T_SL_EQUAL:     ((l) <<= r);           break;                    \
  case T_SR_EQUAL:     ((l) >>= r);           break;                    \
  default:                                                              \
    throw FatalErrorException(0, "invalid operator %d", op);            \
  }                                                                     \

#define IMPLEMENT_SETAT_OPEQUAL                                         \
check_array:                                                            \
  DataType m_type = getRawType();                                       \
  if (m_type == KindOfArray) {                                          \
    Variant *cv = NULL;                                                 \
    ArrayData *escalated =                                              \
      getRawArrayData()->lval(ToKey(key), cv, (getRawArrayData()->getCount() > 1)); \
    if (escalated) {                                                    \
      set(escalated);                                                   \
    }                                                                   \
    ASSERT(cv);                                                         \
    OPEQUAL(op, *cv, v);                                                \
    return *cv;                                                         \
  }                                                                     \
  switch (m_type) {                                                     \
  case KindOfBoolean:                                                   \
    if (toBoolean()) {                                                  \
      /* FIXME: throw_bad_type_exception("not array objects"); */                  \
      break;                                                            \
    }                                                                   \
    /* Fall through */                                                  \
  case KindOfUninit:                                                    \
  case KindOfNull:                                                      \
    set(ArrayData::Create(ToKey(key), null));                           \
    goto check_array;                                                   \
  case KindOfVariant:                                                   \
    getRefVar()->setOpEqual(op, key, v);                                \
    break;                                                              \
  case KindOfStaticString:                                              \
  case KindOfString: {                                                  \
    String s = toString();                                              \
    if (s.empty()) {                                                    \
      set(ArrayData::Create(ToKey(key), null));                         \
      goto check_array;                                                 \
    }                                                                   \
    /* FIXME: throw_bad_type_exception("not array objects");*/                      \
    break;                                                              \
  }                                                                     \
  default:                                                              \
    /* FIXME: throw_bad_type_exception("not array objects");*/                      \
    break;                                                              \
  }                                                                     \
  return v;                                                             \

template <typename T>
inline ALWAYS_INLINE CVarRef Variant::SetImpl(Variant *self, T key,
                                              CVarRef v, bool isKey) {
  retry:
  if (LIKELY(self->getRawType() == KindOfArray)) {
    ArrayData *escalated;
    if (LvalHelper<T>::CheckParams && isKey) {
      escalated = self->getRawArrayData()->set(key, v, self->needCopyForSet(v));
    } else {
      typename LvalHelper<T>::KeyType k(ToKey(key));
      if (!LvalHelper<T>::CheckKey(k)) return lvalBlackHole();
      escalated = self->getRawArrayData()->set(k, v, self->needCopyForSet(v));
    }
    if (escalated) {
      self->set(escalated);
    }
    return v;
  }
  switch (self->getRawType()) {
  case KindOfBoolean:
    if (self->asBooleanVal()) {
      // FIXME: throw_bad_type_exception("not array objects");
      break;
    }
    /* Fall through */
  case KindOfUninit:
  case KindOfNull:
  create:
    if (LvalHelper<T>::CheckParams && isKey) {
      self->set(ArrayData::Create(key, v));
    } else {
      typename LvalHelper<T>::KeyType k(ToKey(key));
      if (!LvalHelper<T>::CheckKey(k)) return lvalBlackHole();
      self->set(ArrayData::Create(k, v));
    }
    break;
  case KindOfVariant:
    self = self->getRefVar();
    goto retry;
  case KindOfStaticString:
  case KindOfString: {
    StringData *s = self->getRawStringData();
    if (s->empty()) {
      goto create;
    }
    StringData *es = StringData::Escalate(s);
    es->set(key, v.toString());
    if (es != s) self->set(es);
    break;
  }
  default:
    // FIXME: throw_bad_type_exception("not array objects");
    break;
  }
  return v;
}

CVarRef Variant::set(bool key, CVarRef v) {
  return SetImpl(this, key, v, false);
}

CVarRef Variant::set(int64 key, CVarRef v) {
  return SetImpl(this, key, v, false);
}

CVarRef Variant::set(double key, CVarRef v) {
  return SetImpl(this, key, v, false);
}

CVarRef Variant::set(CStrRef key, CVarRef v, bool isString /* = false */) {
  return SetImpl<CStrRef>(this, key, v, isString);
}

CVarRef Variant::set(CVarRef key, CVarRef v) {
  return SetImpl<CVarRef>(this, key, v, false);
}

CVarRef Variant::append(CVarRef v) {
  DataType m_type = getRawType();
  switch (m_type) {
  case KindOfUninit:
  case KindOfNull:
    set(ArrayData::Create(v));
    break;
  case KindOfBoolean:
    if (!toBoolean()) {
      set(ArrayData::Create(v));
    } else {
      // FIXME: throw_bad_type_exception("[] operator not supported for this type");
    }
    break;
  case KindOfArray:
    {
      ArrayData *escalated = getRawArrayData()->append(v, needCopyForSet(v));
      if (escalated) {
        set(escalated);
      }
    }
    break;
  case KindOfVariant:
    getRefVar()->append(v);
    break;
  case KindOfStaticString:
  case KindOfString:
    if (getStringData()->empty()) {
      set(ArrayData::Create(v));
      return v;
    }
    // fall through to throw
  default:
    // FIXME: throw_bad_type_exception("[] operator not supported for this type");
      break;
  }
  return v;
}

template <typename T>
inline ALWAYS_INLINE CVarRef Variant::SetRefImpl(Variant *self, T key,
                                                 CVarRef v, bool isKey) {
  retry:
  if (LIKELY(self->getRawType() == KindOfArray)) {
    ArrayData *escalated;
    if (LvalHelper<T>::CheckParams && isKey) {
      escalated = self->getRawArrayData()->setRef(key, v, self->needCopyForSetRef(v));
    } else {
      typename LvalHelper<T>::KeyType k(ToKey(key));
      if (!LvalHelper<T>::CheckKey(k)) return lvalBlackHole();
      escalated = self->getRawArrayData()->setRef(k, v, self->needCopyForSetRef(v));
    }
    if (escalated) {
      self->set(escalated);
    }
    return v;
  }
  switch (self->getRawType()) {
  case KindOfBoolean:
    if (self->asBooleanVal()) {
      // FIXME: throw_bad_type_exception("not array objects");
      break;
    }
    /* Fall through */
  case KindOfUninit:
  case KindOfNull:
  create:
    if (LvalHelper<T>::CheckParams && isKey) {
      self->set(ArrayData::CreateRef(key, v));
    } else {
      typename LvalHelper<T>::KeyType k(ToKey(key));
      if (!LvalHelper<T>::CheckKey(k)) return lvalBlackHole();
      self->set(ArrayData::CreateRef(k, v));
    }
    break;
  case KindOfVariant:
    self = self->getRefVar();
    goto retry;
  case KindOfStaticString:
  case KindOfString: {
    if (self->getRawStringData()->empty()) {
      goto create;
    }
    // FIXME: throw_bad_type_exception("binding assignment to stringoffset");
    break;
  }
  default:
    // FIXME: throw_bad_type_exception("not array objects");
    break;
  }
  return v;
}

CVarRef Variant::setRef(bool key, CVarRef v) {
  return SetRefImpl(this, key, v, false);
}

CVarRef Variant::setRef(int64 key, CVarRef v) {
  return SetRefImpl(this, key, v, false);
}

CVarRef Variant::setRef(double key, CVarRef v) {
  return SetRefImpl(this, key, v, false);
}

CVarRef Variant::setRef(CStrRef key, CVarRef v, bool isString /* = false */) {
  return SetRefImpl<CStrRef>(this, key, v, isString);
}

CVarRef Variant::setRef(CVarRef key, CVarRef v) {
  return SetRefImpl<CVarRef>(this, key, v, false);
}

CVarRef Variant::appendRef(CVarRef v) {
  DataType m_type = getRawType();
  switch (m_type) {
  case KindOfUninit:
  case KindOfNull:
    set(ArrayData::CreateRef(v));
    break;
  case KindOfBoolean:
    if (!toBoolean()) {
      set(ArrayData::CreateRef(v));
    } else {
      // FIXME: throw_bad_type_exception("[] operator not supported for this type");
    }
    break;
  case KindOfArray:
    {
      ArrayData *escalated = getRawArrayData()->appendRef(v, needCopyForSetRef(v));
      if (escalated) {
        set(escalated);
      }
    }
    break;
  case KindOfVariant:
    getRefVar()->appendRef(v);
    break;
  case KindOfStaticString:
  case KindOfString:
    if (getStringData()->empty()) {
      set(ArrayData::CreateRef(v));
      return v;
    }
    // fall through to throw
  default:
    // FIXME: throw_bad_type_exception("[] operator not supported for this type");
    break;
  }
  return v;
}

CVarRef Variant::setOpEqual(int op, bool key, CVarRef v) {
  IMPLEMENT_SETAT_OPEQUAL;
}

CVarRef Variant::setOpEqual(int op, int64 key, CVarRef v) {
  IMPLEMENT_SETAT_OPEQUAL;
}

CVarRef Variant::setOpEqual(int op, double key, CVarRef v) {
  IMPLEMENT_SETAT_OPEQUAL;
}

CVarRef Variant::setOpEqual(int op, CStrRef key, CVarRef v,
                            bool isString /* = false */) {
check_array:
  DataType m_type = getRawType();
  if (m_type == KindOfArray) {
    Variant *cv = NULL;
    ArrayData *escalated;
    if (isString) {
      escalated =
        getRawArrayData()->lval(key, cv, (getRawArrayData()->getCount() > 1));
    } else {
      escalated =
        getRawArrayData()->lval(ToKey(key), cv, (getRawArrayData()->getCount() > 1));
    }
    if (escalated) {
      set(escalated);
    }
    ASSERT(cv);
    OPEQUAL(op, *cv, v);
    return *cv;
  }
  switch (m_type) {
  case KindOfBoolean:
    if (toBoolean()) {
      // FIXME: throw_bad_type_exception("not array objects");
      break;
    }
    /* Fall through */
  case KindOfUninit:
  case KindOfNull:
    if (isString) {
      set(ArrayData::Create(key, null));
    } else {
      set(ArrayData::Create(ToKey(key), null));
    }
    goto check_array;
  case KindOfVariant:
    return getRefVar()->setOpEqual(op, key, v, isString);
  case KindOfStaticString:
  case KindOfString: {
    String s = toString();
    if (s.empty()) {
      if (isString) {
        set(ArrayData::Create(key, null));
      } else {
        set(ArrayData::Create(ToKey(key), null));
      }
      goto check_array;
    }
    // FIXME: throw_bad_type_exception("not array objects");
    break;
  }
  default:
    // FIXME: throw_bad_type_exception("not array objects");
    break;
  }
  return v;
}

CVarRef Variant::setOpEqual(int op, CVarRef key, CVarRef v) {
check_array:
  DataType m_type = getRawType();
  if (m_type == KindOfArray) {
    Variant *cv = NULL;
    VarNR k(ToKey(key));
    if (k.isNull()) return lvalBlackHole();
    ArrayData *escalated =
      getRawArrayData()->lval(k, cv, (getRawArrayData()->getCount() > 1));
    if (escalated) {
      set(escalated);
    }
    ASSERT(cv);
    OPEQUAL(op, *cv, v);
    return *cv;
  }
  switch (m_type) {
  case KindOfBoolean:
    if (toBoolean()) {
      // FIXME: throw_bad_type_exception("not array objects");
      break;
    }
    /* Fall through */
  case KindOfUninit:
  case KindOfNull: {
    VarNR k(ToKey(key));
    if (k.isNull()) return lvalBlackHole();
    set(ArrayData::Create(k, null));
    goto check_array;
  }
  case KindOfVariant:
    return getRefVar()->setOpEqual(op, key, v);
  case KindOfStaticString:
  case KindOfString: {
    String s = toString();
    if (s.empty()) {
      VarNR k(ToKey(key));
      if (k.isNull()) return lvalBlackHole();
      set(ArrayData::Create(k, null));
      goto check_array;
    }
    // FIXME: throw_bad_type_exception("not array objects");
    break;
  }
  default:
    // FIXME: throw_bad_type_exception("not array objects");
    break;
  }
  return v;
}

CVarRef Variant::appendOpEqual(int op, CVarRef v) {
check_array:
  DataType m_type = getRawType();
  if (m_type == KindOfArray) {
    Variant *cv = NULL;
    ArrayData *escalated =
      getRawArrayData()->lvalNew(cv, getRawArrayData()->getCount() > 1);
    if (escalated) {
      set(escalated);
    }
    ASSERT(cv);
    switch (op) {
    // FIXME: case T_CONCAT_EQUAL: return concat_assign((*cv), v);
    case T_PLUS_EQUAL:   return ((*cv) += v);
    case T_MINUS_EQUAL:  return ((*cv) -= v);
    case T_MUL_EQUAL:    return ((*cv) *= v);
    case T_DIV_EQUAL:    return ((*cv) /= v);
    case T_MOD_EQUAL:    return ((*cv) %= v);
    case T_AND_EQUAL:    return ((*cv) &= v);
    case T_OR_EQUAL:     return ((*cv) |= v);
    case T_XOR_EQUAL:    return ((*cv) ^= v);
    case T_SL_EQUAL:     return ((*cv) <<= v);
    case T_SR_EQUAL:     return ((*cv) >>= v);
    default:
      throw FatalErrorException(0, "invalid operator %d", op);
    }
    return v;
  }
  switch (m_type) {
  case KindOfUninit:
  case KindOfNull:
    set(ArrayData::Create());
    goto check_array;
  case KindOfBoolean:
    if (!toBoolean()) {
      set(ArrayData::Create());
      goto check_array;
    } else {
      // FIXME: throw_bad_type_exception("[] operator not supported for this type");
    }
    break;
  case KindOfVariant:
    getRefVar()->appendOpEqual(op, v);
    break;
  case KindOfStaticString:
  case KindOfString:
    if (getStringData()->empty()) {
      set(ArrayData::Create());
      goto check_array;
    }
    // fall through to throw
  default:
    // FIXME: throw_bad_type_exception("[] operator not supported for this type");
    break;
  }
  return v;
}

void Variant::removeImpl(double key) {
  switch (getType()) {
  case KindOfUninit:
  case KindOfNull:
    break;
  case KindOfArray:
    {
      ArrayData *arr = getArrayData();
      if (arr) {
        ArrayData *escalated = arr->remove(ToKey(key), (arr->getCount() > 1));
        if (escalated) {
          set(escalated);
        }
      }
    }
    break;
  default:
    lvalInvalid();
    break;
  }
}

void Variant::removeImpl(int64 key) {
  switch (getType()) {
  case KindOfUninit:
  case KindOfNull:
    break;
  case KindOfArray:
    {
      ArrayData *arr = getArrayData();
      if (arr) {
        ArrayData *escalated = arr->remove(key, (arr->getCount() > 1));
        if (escalated) {
          set(escalated);
        }
      }
    }
    break;
  default:
    lvalInvalid();
    break;
  }
}

void Variant::removeImpl(bool key) {
  switch (getType()) {
  case KindOfUninit:
  case KindOfNull:
    break;
  case KindOfArray:
    {
      ArrayData *arr = getArrayData();
      if (arr) {
        ArrayData *escalated = arr->remove(ToKey(key), (arr->getCount() > 1));
        if (escalated) {
          set(escalated);
        }
      }
    }
    break;
  default:
    lvalInvalid();
    break;
  }
}

void Variant::removeImpl(CVarRef key, bool isString /* false */) {
  switch (getType()) {
  case KindOfUninit:
  case KindOfNull:
    break;
  case KindOfArray:
    {
      ArrayData *arr = getArrayData();
      if (arr) {
        ArrayData *escalated;
        if (isString) {
          escalated = arr->remove(key, (arr->getCount() > 1));
        } else {
          const VarNR &k = key.toKey();
          if (k.isNull()) return;
          escalated = arr->remove(k, (arr->getCount() > 1));
        }
        if (escalated) {
          set(escalated);
        }
      }
    }
    break;
  default:
    lvalInvalid();
    break;
  }
}

void Variant::removeImpl(CStrRef key, bool isString /* false */) {
  switch (getType()) {
  case KindOfUninit:
  case KindOfNull:
    break;
  case KindOfArray:
    {
      ArrayData *arr = getArrayData();
      if (arr) {
        ArrayData *escalated;
        if (isString) {
          escalated = arr->remove(key, (arr->getCount() > 1));
        } else {
          escalated = arr->remove(key.toKey(), (arr->getCount() > 1));
        }
        if (escalated) {
          set(escalated);
        }
      }
    }
    break;
  default:
    lvalInvalid();
    break;
  }
}

void Variant::remove(CVarRef key) {
  switch(key.getType()) {
  case KindOfInt64:
    removeImpl(key.toInt64());
    return;
  case KindOfString:
  case KindOfStaticString:
    removeImpl(key.toString());
    return;
  default:
    break;
  }
  // Trouble cases: Array, Object
  removeImpl(key);
}

///////////////////////////////////////////////////////////////////////////////
}
