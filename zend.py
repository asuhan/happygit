import os, sys

from zend_errors import *
import zend_compile
import zend_execute_API

IS_NULL = 0
IS_LONG = 1
IS_DOUBLE = 2
IS_BOOL = 3
IS_ARRAY = 4
IS_OBJECT = 5
IS_STRING = 6
IS_RESOURCE = 7
IS_CONSTANT = 8
IS_CONSTANT_ARRAY = 9
# For HPHP Variant support
IS_HPHP_REF = 10

IS_CONSTANT_TYPE_MASK = 0x0f
IS_CONSTANT_UNQUALIFIED = 0x10
IS_CONSTANT_INDEX = 0x80
IS_LEXICAL_VAR = 0x20
IS_LEXICAL_REF = 0x40

FAILURE = -1
SUCCESS = 0

ZEND_FE_RESET_VARIABLE = 1
ZEND_FE_RESET_REFERENCE = 2

ZEND_FE_FETCH_BYREF = 1
ZEND_FE_FETCH_WITH_KEY = 2

ZEND_RETURNS_FUNCTION = 1 << 0
ZEND_RETURNS_NEW = 1 << 1

ZEND_FETCH_GLOBAL = 0
ZEND_FETCH_LOCAL = 1
ZEND_FETCH_STATIC = 2
ZEND_FETCH_STATIC_MEMBER = 3
ZEND_FETCH_GLOBAL_LOCK = 4
ZEND_FETCH_LEXICAL = 5

ZEND_FETCH_CLASS_DEFAULT = 0
ZEND_FETCH_CLASS_SELF = 1
ZEND_FETCH_CLASS_PARENT = 2
ZEND_FETCH_CLASS_MAIN = 3
ZEND_FETCH_CLASS_GLOBAL = 4
ZEND_FETCH_CLASS_AUTO = 5
ZEND_FETCH_CLASS_INTERFACE = 6
ZEND_FETCH_CLASS_STATIC = 7
ZEND_FETCH_CLASS_MASK = 0x0f
ZEND_FETCH_CLASS_NO_AUTOLOAD = 0x80
ZEND_FETCH_CLASS_SILENT = 0x0100

ZEND_FETCH_MAKE_REF = 0x04000000

BP_VAR_R = 0
BP_VAR_W = 1
BP_VAR_RW = 2
BP_VAR_IS = 3
BP_VAR_NA = 4
BP_VAR_FUNC_ARG = 5
BP_VAR_UNSET = 6

ZEND_INTERNAL_CLASS = 1
ZEND_USER_CLASS = 2

ZEND_RETURN_REF = 1

ZEND_ACC_IMPLICIT_ABSTRACT_CLASS = 0x10
ZEND_ACC_EXPLICIT_ABSTRACT_CLASS = 0x20
ZEND_ACC_FINAL_CLASS = 0x40
ZEND_ACC_INTERFACE = 0x80

ZEND_ACC_ABSTRACT = 0x02
ZEND_ACC_DEPRECATED = 0x40000
ZEND_ACC_STATIC = 0x01
ZEND_ACC_CLOSURE = 0x100000
ZEND_ACC_ALLOW_STATIC = 0x10000
ZEND_ACC_SHADOW = 0x20000
ZEND_ACC_PUBLIC = 0x100
ZEND_ACC_PROTECTED = 0x200
ZEND_ACC_PRIVATE = 0x400
ZEND_ACC_PPP_MASK = (ZEND_ACC_PUBLIC | ZEND_ACC_PROTECTED | ZEND_ACC_PRIVATE)

ZEND_ACC_CHANGED = 0x800

ZEND_FETCH_ADD_LOCK = 0x08000000
ZEND_ARG_SEND_BY_REF = 1 << 0
ZEND_ARG_COMPILE_TIME_BOUND = 1 << 1
ZEND_ARG_SEND_FUNCTION = 1 << 2
ZEND_ARG_SEND_SILENT = 1 << 3

ZEND_ISSET = 1 << 0
ZEND_ISEMPTY = 1 << 1
ZEND_ISSET_ISEMPTY_MASK = (ZEND_ISSET | ZEND_ISEMPTY)
ZEND_QUICK_SET = 1<<2

INVALID_PC = -1

def zend_error_noreturn(type, format, *args):
	return zend_error(type, format, *args)

def zend_error(type, format, *args):
	error_filename = None
	error_lineno = 0

	if type == E_ERROR or type == E_WARNING:
		if zend_compile.zend_is_compiling():
			raise Exception('Not implemented yet')
		elif zend_execute_API.zend_is_executing():
			error_filename = zend_execute_API.zend_get_executed_filename()
			error_lineno = zend_execute_API.zend_get_executed_lineno();
		else:
			raise Exception('Not implemented yet')
	else:
		raise Exception('Not implemented yet')

	if not error_filename:
		raise Exception('Not implemented yet')

	# TODO: handle user error handler
	zend_error_cb(type, error_filename, error_lineno, format, *args)

class PHPErrorException(Exception):
	def __init__(self):
		pass

def zend_error_cb(type, error_filename, error_lineno, format, *args):
	# TODO: make it more like Zend
	error_type_str = None
	if type == E_ERROR:
		error_type_str = 'Fatal error'
	elif type == E_WARNING:
		error_type_str = 'Warning'
	else:
		raise Exception('Not implemented yet')

	buffer = format
	log_buffer = "PHP %s:  %s in %s on line %d\n" % (error_type_str, buffer, error_filename, error_lineno)
	os.write(2, log_buffer)
	# TODO: make it like Zend
	if type == E_ERROR:
		import global_state
		global_state.EG.exit_status = 255
		raise PHPErrorException()
