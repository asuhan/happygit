from zend import *
import global_state
import zval_utils
import happy_hash
from happy_util import zend_print_zval
import objects

def _zval_dtor_func(zvalue):
    zval_type = zval_utils.Z_TYPE_P(zvalue) & IS_CONSTANT_TYPE_MASK
    if zval_type == IS_OBJECT:
        zval_utils.Z_OBJ_HT_P(zvalue).del_ref(zvalue)
    elif zval_type == IS_ARRAY:
        if zvalue.deref().happy_ht and zvalue.deref().happy_ht != global_state.EG.symbol_table:
            happy_hash.zend_hash_destroy(zvalue.deref().happy_ht)
    else:
        # TODO
        pass

def zval_copy_ctor(zvalue):
    assert isinstance(zvalue, objects.zval_ptr)
    if zvalue.deref().get_type() <= IS_BOOL:
        return
    zval_copy_ctor_func(zvalue)

def zval_copy_ctor_func(zvalue):
    masked_type = zval_utils.Z_TYPE_P(zvalue) & IS_CONSTANT_TYPE_MASK
    if masked_type == IS_STRING:
        zvalue.deref().str = objects.MutableString(zvalue.deref().str.to_str())
    elif masked_type == IS_ARRAY:
        tmp = global_state.null_zval_ptr
        original_ht = zvalue.deref().happy_ht
        tmp_ht = None

        if zvalue.deref().happy_ht == global_state.EG.symbol_table:
            raise Exception('Not implemented yet')
        tmp_ht = happy_hash.zend_hash_init(None, happy_hash.zend_hash_num_elements(original_ht), None, None, False)
        happy_hash.zend_hash_copy(tmp_ht, original_ht, zval_add_ref, zval_utils.zpp_stack(tmp), 0)
        zvalue.deref().happy_ht = tmp_ht
    elif masked_type == IS_OBJECT:
        zval_utils.Z_OBJ_HT_P(zvalue).add_ref(zvalue)
    else:
        raise Exception('Not implemented yet')

def zval_ptr_dtor(zval_ptr):
    zv = zval_ptr.deref()
    zval_utils.Z_DELREF_P(zv)
    if zval_utils.Z_REFCOUNT_P(zv) == 0:
        if zv != global_state.EG.uninitialized_zval_ptr():
            zval_utils.zval_dtor(zv)
    else:
        if zval_utils.Z_REFCOUNT_P(zv) == 1:
            zval_utils.Z_UNSET_ISREF_P(zv)

def zend_print_variable(z):
    return zend_print_zval(z, 0)

def zval_add_ref(p):
    assert isinstance(p, objects.zval_ptr_ptr)
    zval_utils.Z_ADDREF_PP(p)

def zval_copy_property_ctor(ce):
    if ce.class_type == ZEND_INTERNAL_CLASS:
        raise Exception('Not implemented yet')
    return zval_add_ref
