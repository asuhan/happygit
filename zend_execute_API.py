def zend_is_executing():
	import global_state as gs
	return gs.EG.in_execution

def zend_get_executed_filename():
	import global_state as gs
	import happy_util
	if not happy_util.is_null_struct(gs.EG.active_op_array):
		return gs.EG.active_op_array.filename
	else:
		raise Exception('Not implemented yet!')

def zend_get_executed_lineno():
	import global_state as gs
	if gs.EG.exception:
		raise Exception('Not implemented yet!')
	import zend
	if gs.EG.current_execute_data.opline != zend.INVALID_PC:
		return gs.EG.current_execute_data.op_array.opcodes[gs.EG.current_execute_data.opline].lineno
	raise Exception('Not implemented yet!')

def zend_call_function(fci, fci_cache):
	import global_state as gs
	import happy_execute
	import happy_util
	import happy_variables
	import read_apc_dump as rad
	import targetphpstandalone as tphp
	import zend
	import zval_utils

	execute_data = tphp.zend_execute_data(None, None)

	fci.retval_ptr_ptr = gs.null_zval_ptr_ptr

	if not gs.EG.active:
		raise Exception('Not implemented yet!')

	if gs.EG.exception:
		raise Exception('Not implemented yet!')

	if gs.EG.current_execute_data:
		raise Exception('Not implemented yet!')

	if not fci_cache or not fci_cache.initialized:
		raise Exception('Not implemented yet!')

	execute_data.function_state.function = fci_cache.function_handler.op_array
	calling_scope = fci_cache.calling_scope
	called_scope = fci_cache.called_scope
	fci.object_ptr = fci_cache.object_ptr
	execute_data.object = fci.object_ptr
	if (not fci_cache.object_ptr.is_null() and
		zval_utils.Z_TYPE_P(fci.object_ptr) == zend.IS_OBJECT and
		((not gs.EG.objects_store.object_buckets) or
		(not gs.EG.objects_store.object_buckets[zval_utils.Z_OBJ_HANDLE_P(fci.object_ptr)].valid))):
		raise Exception('Not implemented yet!')

	if execute_data.function_state.function.fn_flags & (zend.ZEND_ACC_ABSTRACT | zend.ZEND_ACC_DEPRECATED):
		raise Exception('Not implemented yet!')

	if fci.param_count != 0:
		raise Exception('Not implemented yet!')

	execute_data.function_state.arguments = gs.EG.argument_stack.top()
	happy_execute.zend_vm_stack_push_argc(fci.param_count)

	current_scope = gs.EG.scope
	gs.EG.scope = calling_scope

	current_this = gs.EG.This()

	current_called_scope = gs.EG.called_scope
	if not happy_util.is_null_struct(called_scope):
		gs.EG.called_scope = called_scope
	else:
		raise Exception('Not implemented yet!')

	if not fci.object_ptr.is_null():
		if execute_data.function_state.function.fn_flags & zend.ZEND_ACC_STATIC:
			raise Exception('Not implemented yet!')
		else:
			gs.EG.SetThis(fci.object_ptr)

			if not zval_utils.PZVAL_IS_REF(gs.EG.This()):
				zval_utils.Z_ADDREF_P(gs.EG.This())
			else:
				raise Exception('Not implemented yet!')
	else:
		raise Exception('Not implemented yet!')

	gs.EG.prev_execute_data = gs.EG.current_execute_data
	gs.EG.current_execute_data = execute_data

	if execute_data.function_state.function.type == rad.APCFile.ZEND_function.ZEND_USER_FUNCTION:
		calling_symbol_table = gs.EG.active_symbol_table
		gs.EG.scope = execute_data.function_state.function.scope
		gs.EG.active_symbol_table = fci.symbol_table

		original_return_value = gs.EG.return_value_ptr_ptr
		original_op_array = gs.EG.active_op_array
		gs.EG.return_value_ptr_ptr = fci.retval_ptr_ptr
		gs.EG.active_op_array = execute_data.function_state.function
		# TODO: Make it more like Zend
		gs.EG.executor.execute(gs.EG.active_op_array)
		if not fci.symbol_table and gs.EG.active_symbol_table:
			raise Exception('Not implemented yet!')
		gs.EG.active_symbol_table = calling_symbol_table
		gs.EG.active_op_array = original_op_array
		gs.EG.return_value_ptr_ptr = original_return_value
		# TODO: is opline_ptr useful?
	else:
		raise Exception('Not implemented yet!')
	happy_execute.zend_vm_stack_clear_multiple()

	if not gs.EG.This().is_null():
		happy_variables.zval_ptr_dtor(zval_utils.zpp_stack(gs.EG.This()))
	gs.EG.called_scope = current_called_scope
	gs.EG.scope = current_scope
	gs.EG.SetThis(current_this)
	gs.EG.current_execute_data = execute_data.prev_execute_data

	if gs.EG.exception:
		raise Exception('Not implemented yet!')
	return zend.SUCCESS
