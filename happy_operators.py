from zend import *
import zval_utils
import os
import read_apc_dump as rad
import happy_hash
from happy_util import SUCCESS, ZEND_NORMALIZE_BOOL
import objects
import read_apc_dump as rad

def convert_to_string(op):
    # TODO
    if op.deref().get_type() == IS_LONG:
        op.deref().str = objects.MutableString(str(op.deref().lval))

def zendi_convert_scalar_to_number(op, holder, result):
    assert(isinstance(op, objects.zval_ptr_ptr))
    assert(isinstance(holder, rad.APCFile.ZEND_zval))
    assert(isinstance(result, objects.zval_ptr))
    if op.deref() == result:
        raise Exception('Not implemented yet')
    else:
        if zval_utils.Z_TYPE_PP(op) == IS_STRING:
            st, lval, dval = zval_utils.Z_STRVAL_PP(op).is_numeric_string(allow_errors=True)
            zval_utils.Z_SET_TYPE(holder, st)
            if (zval_utils.Z_TYPE(holder) != IS_LONG and zval_utils.Z_TYPE(holder) != IS_DOUBLE):
                zval_utils.Z_SET_TYPE(holder, IS_LONG)
                zval_utils.Z_SET_LVAL(holder, 0)
            elif zval_utils.Z_TYPE(holder) == IS_LONG:
                zval_utils.Z_SET_TYPE(holder, IS_LONG)
                zval_utils.Z_SET_LVAL(holder, lval)
            elif zval_utils.Z_TYPE(holder) == IS_DOUBLE:
                zval_utils.Z_SET_TYPE(holder, IS_DOUBLE)
                zval_utils.Z_SET_DVAL(holder, dval)
            else:
                raise Exception('Not implemented yet')
            op.assign(zval_utils.zp_stack(holder))
        elif (zval_utils.Z_TYPE_PP(op) == IS_BOOL or zval_utils.Z_TYPE_PP(op) == IS_RESOURCE):
            raise Exception('Not implemented yet')
        elif zval_utils.Z_TYPE_PP(op) == IS_NULL:
            raise Exception('Not implemented yet')
        elif zval_utils.Z_TYPE_PP(op) == IS_OBJECT:
            raise Exception('Not implemented yet')

def Z_ARRVAL(zval):
    assert isinstance(zval, rad.APCFile.ZEND_zval)
    return zval.happy_ht

def Z_ARRVAL_P(zp):
    assert isinstance(zp, objects.zval_ptr)
    return Z_ARRVAL(zp.deref())

def Z_ARRVAL_PP(zpp):
    assert isinstance(zpp, objects.zval_ptr_ptr)
    return Z_ARRVAL(zpp.deref().deref())

def zendi_convert_to_boolean(op, holder, result):
    assert(isinstance(op, objects.zval_ptr_ptr))
    assert(isinstance(holder, rad.APCFile.ZEND_zval))
    assert(isinstance(result, objects.zval_ptr))
    if op.deref() == result:
        raise Exception('Not implemented yet')
    elif zval_utils.Z_TYPE_PP(op) != IS_BOOL:
        if zval_utils.Z_TYPE_PP(op) == IS_LONG:
            zval_utils.Z_SET_LVAL(holder, 1 if zval_utils.Z_LVAL_PP(op) else 0)
        elif zval_utils.Z_TYPE_PP(op) == IS_DOUBLE:
            zval_utils.Z_SET_LVAL(holder, 1 if zval_utils.Z_DVAL_PP(op) else 0)
        elif zval_utils.Z_TYPE_PP(op) == IS_STRING:
            if zval_utils.Z_STRLEN_PP(op) == 0 or (zval_utils.Z_STRLEN_PP(op) == 1 and
                zval_utils.Z_STRVAL_PP(op).get(0) == '0'):
                zval_utils.Z_SET_LVAL(holder, 0)
            else:
                zval_utils.Z_SET_LVAL(holder, 1)
        else:
            zval_utils.Z_SET_LVAL(holder, 1 if happy_hash.zend_hash_num_elements(Z_ARRVAL_PP(op)) else 0)
        zval_utils.Z_SET_TYPE(holder, IS_BOOL)
        op.assign(zval_utils.zp_stack(holder))

def zendi_convert_to_long(op, holder, result):
    assert(isinstance(op, objects.zval_ptr_ptr))
    assert(isinstance(holder, rad.APCFile.ZEND_zval))
    assert(isinstance(result, objects.zval_ptr))
    if op.deref() == result:
        raise Exception('Not implemented yet')
    elif zval_utils.Z_TYPE_PP(op) != IS_LONG:
        if zval_utils.Z_TYPE_PP(op) == IS_BOOL:
            zval_utils.Z_SET_LVAL(holder, zval_utils.Z_LVAL_PP(op))
        else:
            raise Exception('Not implemented yet')

def add_function(result, op1, op2):
    op1_copy = zval_utils.make_empty_zval()
    op2_copy = zval_utils.make_empty_zval()
    converted = False

    while True:
        if (zval_utils.Z_TYPE_P(op1), zval_utils.Z_TYPE_P(op2)) == (IS_LONG, IS_LONG):
            lval = zval_utils.Z_LVAL_P(op1) + zval_utils.Z_LVAL_P(op2)

            # TODO: check for overflow
            zval_utils.ZVAL_LONG(result, lval)

            return SUCCESS

        elif (zval_utils.Z_TYPE_P(op1), zval_utils.Z_TYPE_P(op2)) == (IS_LONG, IS_DOUBLE):
            zval_utils.ZVAL_DOUBLE(result, float(zval_utils.Z_LVAL_P(op1)) + zval_utils.Z_DVAL_P(op2))
            return SUCCESS

        elif (zval_utils.Z_TYPE_P(op1), zval_utils.Z_TYPE_P(op2)) == (IS_DOUBLE, IS_LONG):
            zval_utils.ZVAL_DOUBLE(result, float(zval_utils.Z_DVAL_P(op1)) + zval_utils.Z_LVAL_P(op2))
            return SUCCESS

        elif (zval_utils.Z_TYPE_P(op1), zval_utils.Z_TYPE_P(op2)) == (IS_DOUBLE, IS_DOUBLE):
            raise Exception('Not implemented yet')

        elif (zval_utils.Z_TYPE_P(op1), zval_utils.Z_TYPE_P(op2)) == (IS_ARRAY, IS_ARRAY):
            raise Exception('Not implemented yet')

        else:
            if not converted:
                # TODO: inline code? Python lacks macros
                op1_ptr = zval_utils.zpp_stack(op1)
                zendi_convert_scalar_to_number(op1_ptr, op1_copy, result)
                op1 = op1_ptr.deref()
                op2_ptr = zval_utils.zpp_stack(op2)
                zendi_convert_scalar_to_number(op2_ptr, op2_copy, result)
                op2 = op2_ptr.deref()
                converted = True
            else:
                raise Exception('Not implemented yet')

def sub_function(result, op1, op2):
    converted = False

    while True:
        if (zval_utils.Z_TYPE_P(op1), zval_utils.Z_TYPE_P(op2)) == (IS_LONG, IS_LONG):
            lval = zval_utils.Z_LVAL_P(op1) - zval_utils.Z_LVAL_P(op2)

            # TODO: check for overflow
            zval_utils.ZVAL_LONG(result, lval)

            return 0
        else:
            raise Exception('Not implemented yet')

def mul_function(result, op1, op2):
    converted = False

    while True:
        if (zval_utils.Z_TYPE_P(op1), zval_utils.Z_TYPE_P(op2)) == (IS_LONG, IS_LONG):
            lval = zval_utils.Z_LVAL_P(op1) * zval_utils.Z_LVAL_P(op2)

            # TODO: check for overflow
            zval_utils.ZVAL_LONG(result, lval)

            return SUCCESS
        else:
            raise Exception('Not implemented yet')

def div_function(result, op1, op2):
    # TODO: implement it correctly
    converted = False

    while True:
        if (zval_utils.Z_TYPE_P(op1), zval_utils.Z_TYPE_P(op2)) == (IS_LONG, IS_LONG):
            if zval_utils.Z_LVAL_P(op2) == 0:
                zval_utils.ZVAL_BOOL(result, 0)
                return FAILURE

            lval = zval_utils.Z_LVAL_P(op1) / zval_utils.Z_LVAL_P(op2)

            # TODO: check for overflow
            zval_utils.ZVAL_LONG(result, lval)

            return SUCCESS
        else:
            raise Exception('Not implemented yet')

def mod_function(result, op1, op2):
    # TODO: implement it (more) correctly
    op1_copy = zval_utils.make_empty_zval()
    op2_copy = zval_utils.make_empty_zval()

    op1_ptr = zval_utils.zpp_stack(op1)
    zendi_convert_to_long(op1_ptr, op1_copy, result)
    op1 = op1_ptr.deref()
    op1_lval = zval_utils.Z_LVAL_P(op1)
    op2_ptr = zval_utils.zpp_stack(op2)
    zendi_convert_to_long(op2_ptr, op2_copy, result)
    op2 = op2_ptr.deref()

    if zval_utils.Z_LVAL_P(op2) == 0:
        # TODO: emit error
        zval_utils.ZVAL_BOOL(result, 0)
        return FAILURE

    if zval_utils.Z_LVAL_P(op2) == -1:
        # Prevent overflow error / crash if op1 == LONG_MIN
        zval_utils.ZVAL_LONG(result, 0)
        return SUCCESS

    zval_utils.ZVAL_LONG(result, op1_lval % zval_utils.Z_LVAL_P(op2))
    return SUCCESS

def boolean_not_function(result, op1):
    op1_copy = zval_utils.zval_copy(op1.deref())

    op1_ptr = zval_utils.zpp_stack(op1)
    zendi_convert_to_boolean(op1_ptr, op1_copy, result)
    op1 = op1_ptr.deref()
    zval_utils.ZVAL_BOOL(result, 1 if zval_utils.Z_LVAL_P(op1) == 0 else 0)
    return SUCCESS

def bitwise_not_function(result, op1):
    op1_copy = zval_utils.zval_copy(op1.deref())

    op1 = zval_utils.zp_stack(op1_copy)

    if zval_utils.Z_TYPE_P(op1) == IS_LONG:
        zval_utils.ZVAL_LONG(result, ~zval_utils.Z_LVAL_P(op1))
        return SUCCESS
    raise Exception('Not implemented yet')

def bitwise_or_function(result, op1, op2):
    op1_copy = zval_utils.make_empty_zval()
    op2_copy = zval_utils.make_empty_zval()
    op1_lval = 0

    if zval_utils.Z_TYPE_P(op1) == IS_STRING and zval_utils.Z_TYPE_P(op2) == IS_STRING:
        raise Exception('Not implemented yet')


    op1_ptr = zval_utils.zpp_stack(op1)
    zendi_convert_to_long(op1_ptr, op1_copy, result)
    op1 = op1_ptr.deref()
    op1_lval = zval_utils.Z_LVAL_P(op1)
    op2_ptr = zval_utils.zpp_stack(op2)
    zendi_convert_to_long(op2_ptr, op2_copy, result)
    op2 = op2_ptr.deref()

    zval_utils.ZVAL_LONG(result, op1_lval | zval_utils.Z_LVAL_P(op2))
    return SUCCESS

def bitwise_and_function(result, op1, op2):
    op1_copy = zval_utils.make_empty_zval()
    op2_copy = zval_utils.make_empty_zval()
    op1_lval = 0

    if zval_utils.Z_TYPE_P(op1) == IS_STRING and zval_utils.Z_TYPE_P(op2) == IS_STRING:
        raise Exception('Not implemented yet')


    op1_ptr = zval_utils.zpp_stack(op1)
    zendi_convert_to_long(op1_ptr, op1_copy, result)
    op1 = op1_ptr.deref()
    op1_lval = zval_utils.Z_LVAL_P(op1)
    op2_ptr = zval_utils.zpp_stack(op2)
    zendi_convert_to_long(op2_ptr, op2_copy, result)
    op2 = op2_ptr.deref()

    zval_utils.ZVAL_LONG(result, op1_lval & zval_utils.Z_LVAL_P(op2))
    return SUCCESS

def bitwise_xor_function(result, op1, op2):
    op1_copy = zval_utils.make_empty_zval()
    op2_copy = zval_utils.make_empty_zval()
    op1_lval = 0

    if zval_utils.Z_TYPE_P(op1) == IS_STRING and zval_utils.Z_TYPE_P(op2) == IS_STRING:
        raise Exception('Not implemented yet')


    op1_ptr = zval_utils.zpp_stack(op1)
    zendi_convert_to_long(op1_ptr, op1_copy, result)
    op1 = op1_ptr.deref()
    op1_lval = zval_utils.Z_LVAL_P(op1)
    op2_ptr = zval_utils.zpp_stack(op2)
    zendi_convert_to_long(op2_ptr, op2_copy, result)
    op2 = op2_ptr.deref()

    zval_utils.ZVAL_LONG(result, op1_lval ^ zval_utils.Z_LVAL_P(op2))
    return SUCCESS

def shift_left_function(result, op1, op2):
    op1_copy = zval_utils.make_empty_zval()
    op2_copy = zval_utils.make_empty_zval()
    op1_lval = 0

    op1_ptr = zval_utils.zpp_stack(op1)
    zendi_convert_to_long(op1_ptr, op1_copy, result)
    op1 = op1_ptr.deref()
    op1_lval = zval_utils.Z_LVAL_P(op1)
    op2_ptr = zval_utils.zpp_stack(op2)
    zendi_convert_to_long(op2_ptr, op2_copy, result)
    op2 = op2_ptr.deref()
    zval_utils.ZVAL_LONG(result, op1_lval << zval_utils.Z_LVAL_P(op2))
    return SUCCESS

def shift_right_function(result, op1, op2):
    op1_copy = zval_utils.make_empty_zval()
    op2_copy = zval_utils.make_empty_zval()
    op1_lval = 0

    op1_ptr = zval_utils.zpp_stack(op1)
    zendi_convert_to_long(op1_ptr, op1_copy, result)
    op1 = op1_ptr.deref()
    op1_lval = zval_utils.Z_LVAL_P(op1)
    op2_ptr = zval_utils.zpp_stack(op2)
    zendi_convert_to_long(op2_ptr, op2_copy, result)
    op2 = op2_ptr.deref()
    zval_utils.ZVAL_LONG(result, op1_lval >> zval_utils.Z_LVAL_P(op2))
    return SUCCESS

def add_char_to_string(result, op1, op2):
    assert isinstance(result, objects.zval_ptr)
    assert isinstance(op1, objects.zval_ptr)
    assert isinstance(op2, objects.zval_ptr)
    lhs = zval_utils.Z_STRVAL_P(op1)
    if not lhs:
        lhs = objects.MutableString('')
    zval_utils.Z_SET_STRVAL_P(result,
        objects.append_strings(lhs, objects.MutableString(chr(zval_utils.Z_LVAL_P(op2)))))
    zval_utils.Z_SET_TYPE_P(result, IS_STRING)
    return SUCCESS

def add_string_to_string(result, op1, op2):
    assert isinstance(result, objects.zval_ptr)
    assert isinstance(op1, objects.zval_ptr)
    assert isinstance(op2, objects.zval_ptr)
    lhs = zval_utils.Z_STRVAL_P(op1)
    if not lhs:
        lhs = objects.MutableString('')
    rhs = zval_utils.Z_STRVAL_P(op2)
    if not rhs:
        rhs = objects.MutableString('')
    zval_utils.Z_SET_STRVAL_P(result, objects.append_strings(lhs, rhs))
    zval_utils.Z_SET_TYPE_P(result, IS_STRING)
    return SUCCESS

def concat_function(result, op1, op2):
    import global_state
    EG = global_state.EG

    op1_copy_ptr = zval_utils.zp_stack(zval_utils.make_empty_zval())
    op2_copy_ptr = zval_utils.zp_stack(zval_utils.make_empty_zval())
    use_copy1 = False
    use_copy2 = False

    from happy_util import zend_make_printable_zval
    if zval_utils.Z_TYPE_P(op1) != IS_STRING:
        use_copy1 = zend_make_printable_zval(op1, op1_copy_ptr)
    if zval_utils.Z_TYPE_P(op2) != IS_STRING:
        use_copy2 = zend_make_printable_zval(op2, op2_copy_ptr)

    if use_copy1:
        # We have created a converted copy of op1. Therefore, op1 won't become the result so
        # we have to free it.
        if result == op1:
            raise Exception('Not implemented yet')
        op1 = op1_copy_ptr
    if use_copy2:
        op2 = op2_copy_ptr
    if result == op1:
        zval_utils.Z_STRVAL_P(result).append(zval_utils.Z_STRVAL_P(op2))
    else:
        zval_utils.Z_SET_TYPE_P(result, IS_STRING)
        zval_utils.Z_SET_STRVAL_P(result, objects.append_strings(zval_utils.Z_STRVAL_P(op1),
            zval_utils.Z_STRVAL_P(op2)))
    if use_copy1:
        zval_utils.zval_dtor(op1)
    if use_copy2:
        zval_utils.zval_dtor(op2)
    return 0

def is_identical_function(result, op1, op2):
    assert isinstance(op1, objects.zval_ptr)
    assert isinstance(op2, objects.zval_ptr)
    assert isinstance(result, objects.zval_ptr)
    zval_utils.Z_SET_TYPE_P(result, IS_BOOL)
    if zval_utils.Z_TYPE_P(op1) != zval_utils.Z_TYPE_P(op2):
        zval_utils.Z_SET_LVAL_P(result, 0)
        return SUCCESS
    op1_type = zval_utils.Z_TYPE_P(op1)
    if op1_type == IS_LONG:
        zval_utils.Z_SET_LVAL_P(result, 1 if zval_utils.Z_LVAL_P(op1) == zval_utils.Z_LVAL_P(op2) else 0)
        return SUCCESS
    raise Exception('Not implemented yet')

def is_equal_function(result, op1, op2):
    assert isinstance(op1, objects.zval_ptr)
    assert isinstance(op2, objects.zval_ptr)
    assert isinstance(result, objects.zval_ptr)
    if compare_function(result, op1, op2) == FAILURE:
        return FAILURE
    zval_utils.ZVAL_BOOL(result, 1 if zval_utils.Z_LVAL_P(result) == 0 else 0)
    return SUCCESS

def compare_function(result, op1, op2):
    converted = False
    op1_copy = zval_utils.make_empty_zval()
    op2_copy = zval_utils.make_empty_zval()

    while True:
        if (zval_utils.Z_TYPE_P(op1), zval_utils.Z_TYPE_P(op2)) == (IS_LONG, IS_LONG):
            lval = 1 if zval_utils.Z_LVAL_P(op1) > zval_utils.Z_LVAL_P(op2) else \
                (-1 if zval_utils.Z_LVAL_P(op1) < zval_utils.Z_LVAL_P(op2) else 0)
            zval_utils.ZVAL_LONG(result, lval)
            return SUCCESS

        elif (zval_utils.Z_TYPE_P(op1), zval_utils.Z_TYPE_P(op2)) == (IS_DOUBLE, IS_LONG):
            zval_utils.Z_SET_DVAL_P(result, zval_utils.Z_DVAL_P(op1) - float(zval_utils.Z_LVAL_P(op2)))
            zval_utils.ZVAL_LONG(result, ZEND_NORMALIZE_BOOL(zval_utils.Z_DVAL_P(result)))
            return SUCCESS

        elif (zval_utils.Z_TYPE_P(op1), zval_utils.Z_TYPE_P(op2)) == (IS_LONG, IS_DOUBLE):
            zval_utils.Z_SET_DVAL_P(result, float(zval_utils.Z_LVAL_P(op1)) - zval_utils.Z_DVAL_P(op2))
            zval_utils.ZVAL_LONG(result, ZEND_NORMALIZE_BOOL(zval_utils.Z_DVAL_P(result)))
            return SUCCESS

        elif (zval_utils.Z_TYPE_P(op1), zval_utils.Z_TYPE_P(op2)) == (IS_DOUBLE, IS_DOUBLE):
            zval_utils.Z_SET_DVAL_P(result, zval_utils.Z_DVAL_P(op1) - zval_utils.Z_DVAL_P(op2))
            zval_utils.ZVAL_LONG(result, ZEND_NORMALIZE_BOOL(zval_utils.Z_DVAL_P(result)))
            return SUCCESS

        elif (zval_utils.Z_TYPE_P(op1), zval_utils.Z_TYPE_P(op2)) == (IS_ARRAY, IS_ARRAY):
            raise Exception('Not implemented yet')

        elif (zval_utils.Z_TYPE_P(op1), zval_utils.Z_TYPE_P(op2)) == (IS_NULL, IS_NULL):
            zval_utils.ZVAL_LONG(result, 0)
            return SUCCESS

        elif (zval_utils.Z_TYPE_P(op1), zval_utils.Z_TYPE_P(op2)) == (IS_BOOL, IS_NULL):
            zval_utils.ZVAL_LONG(result, 1 if zval_utils.Z_LVAL_P(op1) else 0)
            return SUCCESS

        elif (zval_utils.Z_TYPE_P(op1), zval_utils.Z_TYPE_P(op2)) == (IS_BOOL, IS_BOOL):
            zval_utils.ZVAL_LONG(result, ZEND_NORMALIZE_BOOL(zval_utils.Z_LVAL_P(op1) - zval_utils.Z_LVAL_P(op2)))
            return SUCCESS

        elif (zval_utils.Z_TYPE_P(op1), zval_utils.Z_TYPE_P(op2)) == (IS_STRING, IS_STRING):
            zendi_smart_strcmp(result, op1, op2)
            return SUCCESS

        elif (zval_utils.Z_TYPE_P(op1), zval_utils.Z_TYPE_P(op2)) == (IS_NULL, IS_STRING):
            zval_utils.ZVAL_LONG(result, zend_binary_strcmp(objects.MutableString(''), 0,
                zval_utils.Z_STRVAL_P(op2), zval_utils.Z_STRLEN_P(op2)))
            return SUCCESS

        elif (zval_utils.Z_TYPE_P(op1), zval_utils.Z_TYPE_P(op2)) == (IS_STRING, IS_NULL):
            zval_utils.ZVAL_LONG(result, zend_binary_strcmp(zval_utils.Z_STRVAL_P(op1), zval_utils.Z_STRLEN_P(op1),
                objects.MutableString(''), 0))
            return SUCCESS

        elif (zval_utils.Z_TYPE_P(op1), zval_utils.Z_TYPE_P(op2)) == (IS_OBJECT, IS_NULL):
            zval_utils.ZVAL_LONG(result, 1)
            return SUCCESS

        elif (zval_utils.Z_TYPE_P(op1), zval_utils.Z_TYPE_P(op2)) == (IS_NULL, IS_OBJECT):
            zval_utils.ZVAL_LONG(result, -1)
            return SUCCESS

        elif (zval_utils.Z_TYPE_P(op1), zval_utils.Z_TYPE_P(op2)) == (IS_OBJECT, IS_OBJECT):
            raise Exception('Not implemented yet')

        else:
            if zval_utils.Z_TYPE_P(op1) == IS_OBJECT:
                raise Exception('Not implemented yet')
            if zval_utils.Z_TYPE_P(op2) == IS_OBJECT:
                raise Exception('Not implemented yet')
            if not converted:
                if zval_utils.Z_TYPE_P(op1) == IS_NULL:
                    op2_ptr = zval_utils.zpp_stack(op2)
                    zendi_convert_to_boolean(op2_ptr, op2_copy, result)
                    op2 = op2_ptr.deref()
                    zval_utils.ZVAL_LONG(result, -1 if zval_utils.Z_LVAL_P(op2) else 0)
                    return SUCCESS
                elif zval_utils.Z_TYPE_P(op2) == IS_NULL:
                    op1_ptr = zval_utils.zpp_stack(op1)
                    zendi_convert_to_boolean(op1_ptr, op1_copy, result)
                    op1 = op1_ptr.deref()
                    zval_utils.ZVAL_LONG(result, 1 if zval_utils.Z_LVAL_P(op1) else 0)
                    return SUCCESS
                elif zval_utils.Z_TYPE_P(op1) == IS_BOOL:
                    op2_ptr = zval_utils.zpp_stack(op2)
                    zendi_convert_to_boolean(op2_ptr, op2_copy, result)
                    op2 = op2_ptr.deref()
                    zval_utils.ZVAL_LONG(result, ZEND_NORMALIZE_BOOL(zval_utils.Z_LVAL_P(op1) - zval_utils.Z_LVAL_P(op2)))
                    return SUCCESS
                elif zval_utils.Z_TYPE_P(op2) == IS_BOOL:
                    op1_ptr = zval_utils.zpp_stack(op1)
                    zendi_convert_to_boolean(op1_ptr, op1_copy, result)
                    op1 = op1_ptr.deref()
                    zval_utils.ZVAL_LONG(result, ZEND_NORMALIZE_BOOL(zval_utils.Z_LVAL_P(op1) - zval_utils.Z_LVAL_P(op2)))
                    return SUCCESS
                else:
                    op1_ptr = zval_utils.zpp_stack(op1)
                    zendi_convert_scalar_to_number(op1_ptr, op1_copy, result)
                    op1 = op1_ptr.deref()
                    op2_ptr = zval_utils.zpp_stack(op2)
                    zendi_convert_scalar_to_number(op2_ptr, op2_copy, result)
                    op2 = op2_ptr.deref()
                    converted = True
            else:
                raise Exception('Not implemented yet')

def zend_binary_strcmp(s1, len1, s2, len2):
    # TODO: check this
    str1 = s1.to_str()
    str2 = s2.to_str()
    if str1 == str2:
        return 0
    return -1 if str1 < str2 else 1

def zend_binary_zval_strcmp(s1, s2):
    return zend_binary_strcmp(zval_utils.Z_STRVAL_P(s1), zval_utils.Z_STRLEN_P(s1),
        zval_utils.Z_STRVAL_P(s2), zval_utils.Z_STRLEN_P(s2))

def zendi_smart_strcmp(result, s1, s2):
    assert isinstance(result, objects.zval_ptr)
    assert isinstance(s1, objects.zval_ptr)
    assert isinstance(s2, objects.zval_ptr)
    # TODO: shortcircuit this
    ret1, lval1, dval1 = zval_utils.Z_STRVAL_P(s1).is_numeric_string(allow_errors=False)
    ret2, lval2, dval2 = zval_utils.Z_STRVAL_P(s2).is_numeric_string(allow_errors=False)
    if ret1 and ret2:
        if ret1 == IS_DOUBLE or ret2 == IS_DOUBLE:
            if ret1 != IS_DOUBLE:
                dval1 = float(lval1)
            elif ret2 != IS_DOUBLE:
                dval2 = float(lval2)
            # TODO: check if dval1 is finite
            zval_utils.Z_SET_DVAL_P(result, dval1 - dval2)
            zval_utils.ZVAL_LONG(result, ZEND_NORMALIZE_BOOL(zval_utils.Z_DVAL_P(result)))
        else:
            zval_utils.ZVAL_LONG(result, 1 if lval1 > lval2 else (-1 if lval1 < lval2 else 0))
    else:
        zval_utils.Z_SET_LVAL_P(result, zend_binary_zval_strcmp(s1, s2))
        zval_utils.ZVAL_LONG(result, ZEND_NORMALIZE_BOOL(zval_utils.Z_LVAL_P(result)))

def increment_function(op1):
    assert isinstance(op1, objects.zval_ptr)
    if zval_utils.Z_TYPE_P(op1) == IS_LONG:
        # TODO: handle overflow
        op1.deref().lval += 1
    elif zval_utils.Z_TYPE_P(op1) == IS_NULL:
        zval_utils.ZVAL_LONG(op1, 1)
    elif zval_utils.Z_TYPE_P(op1) == IS_STRING:
        s = zval_utils.Z_STRVAL_P(op1)
        if s.length() == 0:
            zval_utils.ZVAL_STRING(op1, objects.MutableString("1"))
            return SUCCESS

        st, lval, dval = s.is_numeric_string(allow_errors=False)
        if st == IS_LONG:
            zval_utils.ZVAL_LONG(op1, lval + 1)
            return SUCCESS
        elif st == IS_DOUBLE:
            zval_utils.ZVAL_DOUBLE(op1, dval + 1)
            return SUCCESS
        else:
            assert st == IS_NULL, "Expected IS_NULL, got: %d" % (st)
            s.increment()
            return SUCCESS
    else:
        raise Exception('Not implemented yet')
    return FAILURE

def decrement_function(op1):
    assert isinstance(op1, objects.zval_ptr)
    if zval_utils.Z_TYPE_P(op1) == IS_LONG:
        # TODO: handle overflow
        op1.deref().lval -= 1
    elif zval_utils.Z_TYPE_P(op1) == IS_STRING:
        s = zval_utils.Z_STRVAL_P(op1)
        if s.length() == 0:
            zval_utils.ZVAL_LONG(op1, -1)
            return SUCCESS

        st, lval, dval = s.is_numeric_string(allow_errors=False)
        if st == IS_LONG:
            # TODO: handle overflow: convert to double if it's the case
            zval_utils.ZVAL_LONG(op1, lval - 1)
            return SUCCESS
        elif st == IS_DOUBLE:
            zval_utils.ZVAL_DOUBLE(op1, dval - 1)
            return SUCCESS
        else:
            # do nothing
            assert st == IS_NULL, "Expected IS_NULL, got: %d" % (st)
            return SUCCESS
    else:
        raise Exception('Not implemented yet')
    return FAILURE

# TODO: implement zend_spprintf and replace this
def printf_as_float(param):
    result = '%f' % param
    off = 0
    for off in range(len(result) - 1, -1, -1):
        if result[off] != '0':
            if result[off] == '.':
                off -= 1
            break
    if off < 0:
        return ''
    return result[:off + 1]

def zend_locale_sprintf_double(op):
    assert isinstance(op, objects.zval_ptr)
    zval_utils.Z_SET_STRVAL_P(op, objects.MutableString(printf_as_float(zval_utils.Z_DVAL_P(op))))

def zend_str_tolower_dup(source, length):
    assert isinstance(source, objects.MutableString)
    return objects.MutableString(source.to_str().lower())
