
import ext_api

with open('extensions/happy_api.h', 'w') as hdr:
    print >>hdr, '#ifndef __HAPPY_API_H__'
    print >>hdr, '#define __HAPPY_API_H__'
    print >>hdr
    print >>hdr, 'namespace HAPPY {'
    print >>hdr
    print >>hdr, ext_api.get_api_struct()
    print >>hdr
    print >>hdr, '}'
    print >>hdr
    print >>hdr, '#endif // __HAPPY_API_H__'

