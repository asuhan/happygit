import glob, os, re, sys

def main():
    prog = re.compile("\-\-TEST\-\-.*(\<\?php.*)(\-\-EXPECT.*)", flags = re.DOTALL)
    filelist = glob.glob(os.path.join(sys.argv[1], "*.phpt"))
    for filename in filelist:
        f = open(filename)
        contents = f.read()
        m = prog.match(contents)
        if not m:
            continue
        out_filename = os.path.join(sys.argv[2], os.path.basename(filename)[:-4] + "php")
        of = open(out_filename, "w")
        of.write(m.group(1))

if __name__ == "__main__":
    main()
