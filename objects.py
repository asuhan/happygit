from pypy.rlib.objectmodel import r_dict, compute_hash

import hphp_string
import read_apc_dump as rad

# This is a 'pseudo' pointer, we could get rid of it anyway.
class ulong_ptr:
    def __init__(self, longval, null):
        assert isinstance(longval, int)
        self.longval = longval
        self.null = null

    def is_null(self):
        return self.null

    def assign(self, longval):
        assert isinstance(longval, int)
        if self.null:
            raise Exception('null pointer assignment')
        self.longval = longval

    def deref(self):
        if self.null:
            raise Exception('null pointer dereference')
        return self.longval

    def __nonzero__(self):
        raise Exception('use is_null() for casting to bool')


class double_ptr:
    def __init__(self, doubleval, null):
        assert isinstance(doubleval, float)
        self.doubleval = doubleval
        self.null = null

    def is_null(self):
        return self.null

    def assign(self, doubleval):
        assert isinstance(doubleval, float)
        if self.null:
            raise Exception('null pointer assignment')
        self.doubleval = doubleval

    def deref(self):
        if self.null:
            raise Exception('null pointer dereference')
        return self.doubleval

    def __nonzero__(self):
        raise Exception('use is_null() for casting to bool')


class zend_function_ptr:
    def __init__(self, zend_function, null):
        assert isinstance(zend_function, rad.APCFile.ZEND_function)
        self.__zend_function = zend_function
        self.__null = null

    def is_null(self):
        return self.__null

    def assign(self, zend_function):
        assert isinstance(zend_function, rad.APCFile.ZEND_function)
        if self.__null:
            raise Exception('null pointer assignment')
        self.__zend_function = zend_function

    def deref(self):
        if self.__null:
            raise Exception('null pointer dereference')
        return self.__zend_function

    def __nonzero__(self):
        raise Exception('use is_null() for casting to bool')


# Keep the RPython inference happy since pointers can either
# point to other pointers or a rad.APCFile.CStruct
class ptr(rad.APCFile.CStruct):
    def __init__(self):
        pass


class zval_ptr(ptr):
    def __init__(self, zval, null):
        assert isinstance(zval, rad.APCFile.CStruct)
        self.zval = zval
        self.null = null

    def is_null(self):
        return self.null

    def assign(self, zval):
        assert isinstance(zval, rad.APCFile.CStruct)
        if self.null:
            raise Exception('null pointer assignment')

        import zval_utils
        self.zval = zval_utils.zval_copy(zval)

    def deref(self):
        if self.null:
            raise Exception('null pointer dereference')
        return self.zval

    def __nonzero__(self):
        raise Exception('use is_null() for casting to bool')

    def copy(self):
        return zval_ptr(self.zval, self.null)


class zval_ptr_ptr(ptr):
    def __init__(self, zp, null):
        assert isinstance(zp, zval_ptr)
        self.zp = zp
        self.null = null

    def is_null(self):
        return self.null

    def assign(self, zp):
        assert isinstance(zp, ptr)
        if self.null:
            raise Exception('null pointer assignment')
        self.zp = zp

    def deref(self):
        if self.null:
            raise Exception('null pointer dereference')
        return self.zp

    def __nonzero__(self):
        raise Exception('use is_null() for casting to bool')

    def copy(self):
        return zval_ptr_ptr(self.zp, self.null)


class zval_ptr_ptr_ptr(ptr):
    def __init__(self, zpp, null):
        assert isinstance(zpp, zval_ptr_ptr)
        self.zpp = zpp
        self.null = null

    def is_null(self):
        return self.null

    def assign(self, zpp):
        assert isinstance(zpp, zval_ptr_ptr)
        if self.null:
            raise Exception('null pointer assignment')
        self.zpp = zpp

    def deref(self):
        if self.null:
            raise Exception('null pointer dereference')
        return self.zpp

    def __nonzero__(self):
        raise Exception('use is_null() for casting to bool')


class zend_object(rad.APCFile.CStruct):
    def __init__(self):
        self.ce = None
        self.properties = None
        self.guards = None


class zend_object_ptr(ptr):
    def __init__(self, obj):
        self.__obj = obj

    def deref(self):
        return self.__obj

    def assign(self, obj):
        assert isinstance(obj, zend_object)
        self.__obj = obj


class zend_object_value:
    def __init__(self):
        self.handle = 0
        self.handlers = None

    def get_copy(self):
        # TODO: check if this is really needed
        copy = zend_object_value()
        copy.handle = self.handle
        copy.handlers = self.handlers
        return copy


class MutableString:
    def __init__(self, s=None):
        if s is None:
            self.__sd = hphp_string.null_string
        else:
            assert isinstance(s, str)
            self.__sd = hphp_string.new_string_data(s)

    def length(self):
        return hphp_string.string_data_get_length(self.__sd)

    def assign(self, idx, ch):
        assert(idx >= 0)
        assert len(ch) == 1
        hphp_string.string_data_set_char(self.__sd, idx, ch)

    def get(self, idx):
        return hphp_string.string_data_get_char(self.__sd, idx)

    def to_str(self):
        return hphp_string.string_data_to_str(self.__sd)

    def get_hash(self):
        return hphp_string.string_data_get_hash(self.__sd)

    def append(self, o):
        hphp_string.string_data_append(self.__sd, o.__sd)

    def get_copy(self):
        copy = MutableString()
        copy.__sd = hphp_string.string_data_copy(self.__sd)
        return copy

    def increment(self):
        hphp_string.string_data_increment(self.__sd)

    def is_numeric_string(self, allow_errors=True):
        return hphp_string.string_data_is_numeric(self.__sd, allow_errors)

    def get_string_data(self):
      return self.__sd

    @staticmethod
    def from_string_data(sd):
      ms = MutableString()
      ms.__sd = sd
      return ms

def append_strings(lhs, rhs):
    assert isinstance(lhs, MutableString)
    assert isinstance(rhs, MutableString)
    copy = lhs.get_copy()
    copy.append(rhs)
    return copy

