#!/bin/sh

set -e

TRANSLATE_PATH=$1
PYPY_PYTHON=${PYPY_PYTHON:-python}

make -C extensions
shift
$PYPY_PYTHON $TRANSLATE_PATH/translate.py -O2 "$@" ./targetphpstandalone.py

