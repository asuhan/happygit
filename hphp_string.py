
import os

from pypy.rpython.lltypesystem import rffi, lltype
from pypy.rpython import annlowlevel
from pypy.translator.tool.cbuild import ExternalCompilationInfo

from zend import *
import hphp_kinds

_wrapper_funcs = """
typedef void StringData;

struct NumericValue {
    int64_t lval;
    double dval;
};

extern StringData *new_string_data(char*);
extern void delete_string_data(StringData*);
extern char string_data_get_char(StringData*, int);
extern void string_data_set_char(StringData*, int, char);
extern char *string_data_to_str(StringData*);
extern int64_t string_data_get_hash(StringData*);
extern StringData *string_data_copy(StringData*);
extern void string_data_append(StringData*, StringData*);
extern int string_data_get_length(StringData*);
extern int string_data_is_numeric(StringData*, struct NumericValue*, int);
extern void string_data_increment(StringData*);
"""

_curr_path = os.path.realpath(os.path.dirname(__file__))
_string_eci = ExternalCompilationInfo(
        libraries = ['test1'],
        library_dirs = ['%s/extensions' % _curr_path],
        post_include_bits = [_wrapper_funcs],
        use_cpp_linker = True
    )
_string_eci = _string_eci.compile_shared_lib()

_sd_ptr_type = rffi.VOIDP
null_string = lltype.nullptr(_sd_ptr_type.TO)

def sd_api(res_type, arg_types):
    def decorate(fn):
        fn_name = fn.func_name
        ex_fn = rffi.llexternal(fn_name,
            arg_types, res_type,
            compilation_info=_string_eci, _nowrapper=True)
        def fn_with_ex(*args):
            return fn(ex_fn, *args)
        return fn_with_ex

    return decorate

@sd_api(_sd_ptr_type, [rffi.CCHARP])
def new_string_data(ex_fn, s):
    cs = rffi.str2charp(s)
    sd = ex_fn(cs)
    return sd

@sd_api(lltype.Void, [_sd_ptr_type])
def delete_string_data(ex_fn, sd):
    if sd is not null_string:
        ex_fn(sd)

@sd_api(rffi.CHAR, [_sd_ptr_type, lltype.Signed])
def string_data_get_char(ex_fn, sd, idx):
    ch = ex_fn(sd, idx)
    return chr(rffi.cast(lltype.Signed, ch))

@sd_api(lltype.Void, [_sd_ptr_type, lltype.Signed, rffi.CHAR])
def string_data_set_char(ex_fn, sd, idx, ch):
    assert len(ch) == 1
    ex_fn(sd, idx, rffi.cast(rffi.CHAR, ord(ch)))

@sd_api(rffi.CCHARP, [_sd_ptr_type])
def string_data_to_str(ex_fn, sd):
    cs = ex_fn(sd)
    return rffi.charp2str(cs)

@sd_api(lltype.SignedLongLong, [_sd_ptr_type])
def string_data_get_hash(ex_fn, sd):
    return ex_fn(sd)

@sd_api(_sd_ptr_type, [_sd_ptr_type])
def string_data_copy(ex_fn, sd):
    return ex_fn(sd)

@sd_api(lltype.Void, [_sd_ptr_type, _sd_ptr_type])
def string_data_append(ex_fn, lsd, rsd):
    ex_fn(lsd, rsd)

@sd_api(lltype.Signed, [_sd_ptr_type])
def string_data_get_length(ex_fn, sd):
    return ex_fn(sd)

_numeric_value_ptr = rffi.CStructPtr(
        'NumericValue',
            ('lval', lltype.SignedLongLong),
            ('dval', lltype.Float),
        )

_numeric_zval_dict = {
        hphp_kinds.KindOfInt64:  IS_LONG,
        hphp_kinds.KindOfDouble: IS_DOUBLE,
        hphp_kinds.KindOfUninit: IS_NULL,
        hphp_kinds.KindOfNull:   IS_NULL,
    }

@sd_api(lltype.Signed, [_sd_ptr_type, _numeric_value_ptr, lltype.Signed])
def string_data_is_numeric(ex_fn, sd, allow_errors,
        num_val=lltype.nullptr(_numeric_value_ptr.TO)):
    if not num_val:
        num_val = lltype.malloc(_numeric_value_ptr.TO,
                flavor='raw',
                # FIXME: immortal=True)
                )
    allow_errors = 1 if allow_errors else 0
    val_kind = ex_fn(sd, num_val, allow_errors)
    return _numeric_zval_dict[val_kind], num_val.c_lval, num_val.c_dval

@sd_api(lltype.Void, [_sd_ptr_type])
def string_data_increment(ex_fn, sd):
    return ex_fn(sd)


