'''
PHP standalone target.
'''


import sys

from pypy.rlib.rarithmetic import intmask
from pypy.rlib.unroll import unrolling_iterable

from zend import *
from zend_errors import *
import global_state as gs
import opcodes as ops
import read_apc_dump as rad
import ext_api
import ext_array
import ext_string
import ext_var
import zend_builtin_functions
import zend_object_handlers
import happy_variables
import request_variables
import zval_utils
import objects
import happy_hash
import happy_execute
import happy_operators
import happy_util
import happy_iterators
import zend_API
import zend_constants
import zend_objects_API


class CompiledVariables:
    def __init__(self, cv_count, has_active_symbol_table):
        # zval_ptr_ptr
        self.__backend = []
        for i in range(0, cv_count):
            self.__backend.append(zval_utils.zppp_stack(gs.null_zval_ptr_ptr))
        # zval_ptr
        self.__backend_ext = []
        if not has_active_symbol_table:
            for i in range(0, cv_count):
                self.__backend_ext.append(zval_utils.zpp_stack(gs.null_zval_ptr))

    def address_of(self, i):
        return self.__backend[i]

    def address_of_ext(self, i):
        return self.__backend_ext[i]

    def get(self, i):
        return self.__backend[i].deref()

    def get_ext(self, i):
        return self.__backend_ext[i].deref()

    def set(self, i, cv):
        self.__backend[i].assign(cv)


class zend_execute_data:
    def __init__(self, op_array, has_active_symbol_table):
        if not happy_util.is_null_struct(op_array):
            cv_count = op_array.last_var
            self.CVs = CompiledVariables(cv_count, has_active_symbol_table)
            self.Ts = []
            for i in range(0, op_array.T):
                self.Ts.append(temp_variable())
        else:
            self.CVs = None
            self.Ts = None
        self.fbc = None
        self.object = gs.null_zval_ptr
        self.called_scope = None
        self.current_this = gs.null_zval_ptr
        self.current_object = gs.null_zval_ptr
        self.current_scope = None
        self.current_called_scope = None
        self.op_array = op_array
        self.function_state = zend_function_state(None)
        self.opline = 0
        self.prev_execute_data = None
        self.nested = False
        self.original_return_value = gs.null_zval_ptr_ptr
        self.call_opline = 0xFFFFFFFF
        self.symbol_table = happy_hash.zend_hash_init(None, 0, None, None, False)


class ZendOpcode:
    @staticmethod
    def pass_two(op_array, apc_dump):
        for op in op_array.opcodes:
            if op.get_op1().get_op_type() == rad.APCFile.ZEND_znode.IS_CONST:
                zval_utils.Z_SET_ISREF(op.get_op1().get_constant())
                zval_utils.Z_SET_REFCOUNT(op.get_op1().get_constant(), 2)
            if op.get_op2().get_op_type() == rad.APCFile.ZEND_znode.IS_CONST:
                zval_utils.Z_SET_ISREF(op.get_op2().get_constant())
                zval_utils.Z_SET_REFCOUNT(op.get_op2().get_constant(), 2)
            if op.opcode == ops.ZEND_JMP:
                for op_target in op_array.opcodes:
                    dict_key = op.get_op1().jmp_addr, op_target.get_dict_key()
                    if op_target == apc_dump.offset_struct_dict[dict_key]:
                        op.get_op1().jmp_addr = op_array.opcodes.index(op_target)
                        break
            elif op.opcode == ops.ZEND_JMPZ or op.opcode == ops.ZEND_JMPNZ or op.opcode == ops.ZEND_JMPZ_EX or \
                op.opcode == ops.ZEND_JMPNZ_EX or op.opcode == ops.ZEND_JMP_SET:
                for op_target in op_array.opcodes:
                    dict_key = op.get_op2().jmp_addr, op_target.get_dict_key()
                    if op_target == apc_dump.offset_struct_dict[dict_key]:
                        op.get_op2().jmp_addr = op_array.opcodes.index(op_target)
                        break


class zend_free_op:
    def __init__(self, zp):
        assert isinstance(zp, objects.zval_ptr)
        self.__var = zval_utils.zpp_stack(zp)
        self.__is_temp = False

    def set_var(self, zp, is_temp = False):
        assert isinstance(zp, objects.zval_ptr)
        self.__is_temp = is_temp
        self.__var.assign(zp)

    def var(self):
        return self.__var.deref()

    def var_ptr(self):
        return self.__var

    def is_temp(self):
        return self.__is_temp

    @staticmethod
    def make_new():
        return zend_free_op(gs.null_zval_ptr)


class temp_variable:
    def __init__(self):
        # all
        self.__tmp_var = zval_utils.zp_stack(zval_utils.make_empty_zval())
        # var
        self.__ptr_ptr = zval_utils.zppp_stack(gs.null_zval_ptr_ptr)
        self.__ptr = zval_utils.zpp_stack(gs.null_zval_ptr)
        self.fcall_returned_reference = 0
        # str_offset
        self.str = gs.null_zval_ptr
        self.offset = 0
        # fe
        self.fe_pos = happy_hash.HashPosition()
        # all
        self.class_entry = None

    def tmp_var_ptr(self):
        return self.__tmp_var

    def get_ptr_ptr(self):
        return self.__ptr_ptr.deref()

    def set_ptr_ptr(self, ptr_ptr):
        assert isinstance(ptr_ptr, objects.zval_ptr_ptr)
        self.__ptr_ptr.assign(ptr_ptr)

    def address_of_ptr(self):
        return self.__ptr

    def get_ptr(self):
        return self.address_of_ptr().deref()

    def set_ptr(self, ptr):
        assert isinstance(ptr, objects.zval_ptr)
        self.__ptr.assign(ptr)


class ZendExecutor:
    TEMP_SZ = 40

    def __init__(self):
        self.nested = False
        self.original_in_execution = False

    def opcode_handler(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]
        if opline.get_opcode() == ops.ZEND_ADD:
            return self.ZEND_VM_HANDLER_ADD(execute_data)
        elif opline.get_opcode() == ops.ZEND_SUB:
           return self.ZEND_VM_HANDLER_SUB(execute_data)
        elif opline.get_opcode() == ops.ZEND_MUL:
            return self.ZEND_VM_HANDLER_MUL(execute_data)
        elif opline.get_opcode() == ops.ZEND_DIV:
            return self.ZEND_VM_HANDLER_DIV(execute_data)
        elif opline.get_opcode() == ops.ZEND_MOD:
            return self.ZEND_VM_HANDLER_MOD(execute_data)
        elif opline.get_opcode() == ops.ZEND_SL:
            return self.ZEND_VM_HANDLER_SL(execute_data)
        elif opline.get_opcode() == ops.ZEND_SR:
            return self.ZEND_VM_HANDLER_SR(execute_data)
        elif opline.get_opcode() == ops.ZEND_CONCAT:
            return self.ZEND_VM_HANDLER_CONCAT(execute_data)
        elif opline.get_opcode() == ops.ZEND_IS_IDENTICAL:
            return self.ZEND_VM_HANDLER_IS_IDENTICAL(execute_data)
        elif opline.get_opcode() == ops.ZEND_IS_NOT_IDENTICAL:
            return self.ZEND_VM_HANDLER_IS_NOT_IDENTICAL(execute_data)
        elif opline.get_opcode() == ops.ZEND_IS_EQUAL:
            return  self.ZEND_VM_HANDLER_IS_EQUAL(execute_data)
        elif opline.get_opcode() == ops.ZEND_IS_NOT_EQUAL:
            return  self.ZEND_VM_HANDLER_IS_NOT_EQUAL(execute_data)
        elif opline.get_opcode() == ops.ZEND_IS_SMALLER:
            return self.ZEND_VM_HANDLER_IS_SMALLER(execute_data)
        elif opline.get_opcode() == ops.ZEND_IS_SMALLER_OR_EQUAL:
            return self.ZEND_VM_HANDLER_IS_SMALLER_OR_EQUAL(execute_data)
        elif opline.get_opcode() == ops.ZEND_BW_OR:
            return self.ZEND_VM_HANDLER_BW_OR(execute_data)
        elif opline.get_opcode() == ops.ZEND_BW_AND:
            return self.ZEND_VM_HANDLER_BW_AND(execute_data)
        elif opline.get_opcode() == ops.ZEND_BW_XOR:
            return self.ZEND_VM_HANDLER_BW_XOR(execute_data)
        elif opline.get_opcode() == ops.ZEND_BW_NOT:
            return self.ZEND_VM_HANDLER_BW_NOT(execute_data)
        elif opline.get_opcode() == ops.ZEND_BOOL_NOT:
            return self.ZEND_VM_HANDLER_BOOL_NOT(execute_data)
        elif opline.get_opcode() == ops.ZEND_ASSIGN_ADD:
            return self.ZEND_VM_HANDLER_ASSIGN_ADD(execute_data)
        elif opline.get_opcode() == ops.ZEND_ASSIGN_SUB:
            return self.ZEND_VM_HANDLER_ASSIGN_SUB(execute_data)
        elif opline.get_opcode() == ops.ZEND_ASSIGN_MUL:
            return self.ZEND_VM_HANDLER_ASSIGN_MUL(execute_data)
        elif opline.get_opcode() == ops.ZEND_ASSIGN_DIV:
            return self.ZEND_VM_HANDLER_ASSIGN_DIV(execute_data)
        elif opline.get_opcode() == ops.ZEND_ASSIGN_MOD:
            return self.ZEND_VM_HANDLER_ASSIGN_MOD(execute_data)
        elif opline.get_opcode() == ops.ZEND_ASSIGN_SL:
            return self.ZEND_VM_HANDLER_ASSIGN_SL(execute_data)
        elif opline.get_opcode() == ops.ZEND_ASSIGN_SR:
            return self.ZEND_VM_HANDLER_ASSIGN_SR(execute_data)
        elif opline.get_opcode() == ops.ZEND_ASSIGN_CONCAT:
            return self.ZEND_VM_HANDLER_ASSIGN_CONCAT(execute_data)
        elif opline.get_opcode() == ops.ZEND_ASSIGN_BW_OR:
            return self.ZEND_VM_HANDLER_ASSIGN_BW_OR(execute_data)
        elif opline.get_opcode() == ops.ZEND_ASSIGN_BW_AND:
            return self.ZEND_VM_HANDLER_ASSIGN_BW_AND(execute_data)
        elif opline.get_opcode() == ops.ZEND_ASSIGN_BW_XOR:
            return self.ZEND_VM_HANDLER_ASSIGN_BW_XOR(execute_data)
        elif opline.get_opcode() == ops.ZEND_PRE_INC_OBJ:
            return self.ZEND_VM_HANDLER_PRE_INC_OBJ(execute_data)
        elif opline.get_opcode() == ops.ZEND_POST_INC_OBJ:
            return self.ZEND_VM_HANDLER_POST_INC_OBJ(execute_data)
        elif opline.get_opcode() == ops.ZEND_PRE_INC:
            return self.ZEND_VM_HANDLER_PRE_INC(execute_data)
        elif opline.get_opcode() == ops.ZEND_PRE_DEC:
            return self.ZEND_VM_HANDLER_PRE_DEC(execute_data)
        elif opline.get_opcode() == ops.ZEND_POST_INC:
            return self.ZEND_VM_HANDLER_POST_INC(execute_data)
        elif opline.get_opcode() == ops.ZEND_POST_DEC:
            return self.ZEND_VM_HANDLER_POST_DEC(execute_data)
        elif opline.get_opcode() == ops.ZEND_FETCH_DIM_R:
            return self.ZEND_VM_HANDLER_FETCH_DIM_R(execute_data)
        elif opline.get_opcode() == ops.ZEND_FETCH_DIM_W:
            return self.ZEND_VM_HANDLER_FETCH_DIM_W(execute_data)
        elif opline.get_opcode() == ops.ZEND_FETCH_DIM_FUNC_ARG:
            return self.ZEND_VM_HANDLER_FETCH_DIM_FUNC_ARG(execute_data)
        elif opline.get_opcode() == ops.ZEND_FETCH_OBJ_R:
            return self.ZEND_VM_HANDLER_FETCH_OBJ_R(execute_data)
        elif opline.get_opcode() == ops.ZEND_FETCH_OBJ_FUNC_ARG:
            return self.ZEND_VM_HANDLER_FETCH_OBJ_FUNC_ARG(execute_data)
        elif opline.get_opcode() == ops.ZEND_ASSIGN_OBJ:
            return self.ZEND_VM_HANDLER_ASSIGN_OBJ(execute_data)
        elif opline.get_opcode() == ops.ZEND_ASSIGN_DIM:
            return self.ZEND_VM_HANDLER_ASSIGN_DIM(execute_data)
        elif opline.get_opcode() == ops.ZEND_ASSIGN_REF:
            return self.ZEND_VM_HANDLER_ASSIGN_REF(execute_data)
        elif opline.get_opcode() == ops.ZEND_ASSIGN:
            return self.ZEND_VM_HANDLER_ASSIGN(execute_data)
        elif opline.get_opcode() == ops.ZEND_ECHO:
            return self.ZEND_VM_HANDLER_ECHO(execute_data)
        elif opline.get_opcode() == ops.ZEND_PRINT:
            return self.ZEND_VM_HANDLER_PRINT(execute_data)
        elif opline.get_opcode() == ops.ZEND_FETCH_R:
            return self.ZEND_VM_HANDLER_FETCH_R(execute_data)
        elif opline.get_opcode() == ops.ZEND_FETCH_W:
            return self.ZEND_VM_HANDLER_FETCH_W(execute_data)
        elif opline.get_opcode() == ops.ZEND_FETCH_RW:
            return self.ZEND_VM_HANDLER_FETCH_RW(execute_data)
        elif opline.get_opcode() == ops.ZEND_JMP:
            return self.ZEND_VM_HANDLER_JMP(execute_data)
        elif opline.get_opcode() == ops.ZEND_JMPZ:
            return self.ZEND_VM_HANDLER_JMPZ(execute_data)
        elif opline.get_opcode() == ops.ZEND_JMPNZ:
            return self.ZEND_VM_HANDLER_JMPNZ(execute_data)
        elif opline.get_opcode() == ops.ZEND_JMPZNZ:
            return self.ZEND_VM_HANDLER_JMPZNZ(execute_data)
        elif opline.get_opcode() == ops.ZEND_JMPZ_EX:
            return self.ZEND_VM_HANDLER_JMPZ_EX(execute_data)
        elif opline.get_opcode() == ops.ZEND_JMPNZ_EX:
            return self.ZEND_VM_HANDLER_JMPNZ_EX(execute_data)
        elif opline.get_opcode() == ops.ZEND_ADD_CHAR:
            return self.ZEND_VM_HANDLER_ADD_CHAR(execute_data)
        elif opline.get_opcode() == ops.ZEND_ADD_STRING:
            return self.ZEND_VM_HANDLER_ADD_STRING(execute_data)
        elif opline.get_opcode() == ops.ZEND_FREE:
            return self.ZEND_VM_HANDLER_FREE(execute_data)
        elif opline.get_opcode() == ops.ZEND_ADD_VAR:
            return self.ZEND_VM_HANDLER_ADD_VAR(execute_data)
        elif opline.get_opcode() == ops.ZEND_FETCH_CLASS:
            return self.ZEND_VM_HANDLER_FETCH_CLASS(execute_data)
        elif opline.get_opcode() == ops.ZEND_INIT_METHOD_CALL:
            return self.ZEND_VM_HANDLER_INIT_METHOD_CALL(execute_data)
        elif opline.get_opcode() == ops.ZEND_INIT_STATIC_METHOD_CALL:
            return self.ZEND_VM_HANDLER_INIT_STATIC_METHOD_CALL(execute_data)
        elif opline.get_opcode() == ops.ZEND_INIT_FCALL_BY_NAME:
            return self.ZEND_VM_HANDLER_INIT_FCALL_BY_NAME(execute_data)
        elif opline.get_opcode() == ops.ZEND_DO_FCALL_BY_NAME:
            return self.ZEND_VM_HANDLER_DO_FCALL_BY_NAME(execute_data)
        elif opline.get_opcode() == ops.ZEND_DO_FCALL:
            return self.ZEND_VM_HANDLER_DO_FCALL(execute_data)
        elif opline.get_opcode() == ops.ZEND_RETURN:
            return self.ZEND_VM_HANDLER_RETURN(execute_data)
        elif opline.get_opcode() == ops.ZEND_SEND_VAL:
            return self.ZEND_VM_HANDLER_SEND_VAL(execute_data)
        elif opline.get_opcode() == ops.ZEND_SEND_VAR_NO_REF:
            return self.ZEND_VM_HANDLER_SEND_VAR_NO_REF(execute_data)
        elif opline.get_opcode() == ops.ZEND_SEND_REF:
            return self.ZEND_VM_HANDLER_SEND_REF(execute_data)
        elif opline.get_opcode() == ops.ZEND_SEND_VAR:
            return self.ZEND_VM_HANDLER_SEND_VAR(execute_data)
        elif opline.get_opcode() == ops.ZEND_RECV:
            return self.ZEND_VM_HANDLER_RECV(execute_data)
        elif opline.get_opcode() == ops.ZEND_BOOL:
            return self.ZEND_VM_HANDLER_BOOL(execute_data)
        elif opline.get_opcode() == ops.ZEND_BRK:
            return self.ZEND_VM_HANDLER_BRK(execute_data)
        elif opline.get_opcode() == ops.ZEND_CONT:
            return self.ZEND_VM_HANDLER_CONT(execute_data)
        elif opline.get_opcode() == ops.ZEND_NEW:
            return self.ZEND_VM_HANDLER_NEW(execute_data)
        elif opline.get_opcode() == ops.ZEND_CASE:
            return self.ZEND_VM_HANDLER_CASE(execute_data)
        elif opline.get_opcode() == ops.ZEND_SWITCH_FREE:
            return self.ZEND_VM_HANDLER_SWITCH_FREE(execute_data)
        elif opline.get_opcode() == ops.ZEND_FETCH_CONSTANT:
            return self.ZEND_VM_FETCH_CONSTANT(execute_data)
        elif opline.get_opcode() == ops.ZEND_ADD_ARRAY_ELEMENT:
            return self.ZEND_VM_ADD_ARRAY_ELEMENT(execute_data)
        elif opline.get_opcode() == ops.ZEND_INIT_ARRAY:
            return self.ZEND_VM_HANDLER_INIT_ARRAY(execute_data)
        elif opline.get_opcode() == ops.ZEND_CAST:
            return self.ZEND_VM_HANDLER_CAST(execute_data)
        elif opline.get_opcode() == ops.ZEND_FE_RESET:
            return self.ZEND_VM_HANDLER_FE_RESET(execute_data)
        elif opline.get_opcode() == ops.ZEND_FE_FETCH:
            return self.ZEND_VM_HANDLER_FE_FETCH(execute_data)
        elif opline.get_opcode() == ops.ZEND_ISSET_ISEMPTY_VAR:
            return self.ZEND_VM_HANDLER_ISSET_ISEMPTY_VAR(execute_data)
        elif opline.get_opcode() == ops.ZEND_BEGIN_SILENCE:
            return self.ZEND_VM_HANDLER_BEGIN_SILENCE(execute_data)
        elif opline.get_opcode() == ops.ZEND_END_SILENCE:
            return self.ZEND_VM_HANDLER_END_SILENCE(execute_data)
        elif opline.get_opcode() == ops.ZEND_NOP:
            return self.ZEND_VM_HANDLER_NOP(execute_data)
        else:
            raise Exception('Unsupported opcode ' +
                ops.opnames[opline.get_opcode()])

    def zend_vm_enter(self, op_array):
        while(True):
            execute_data = zend_execute_data(op_array, gs.EG.active_symbol_table)
            execute_data.op_array = op_array
            execute_data.prev_execute_data = gs.EG.current_execute_data
            gs.EG.prev_execute_data = gs.EG.current_execute_data
            execute_data.symbol_table = gs.EG.active_symbol_table
            gs.EG.current_execute_data = execute_data
            execute_data.nested = self.nested
            self.nested = True

            if op_array.this_var != 0xFFFFFFFF and not gs.EG.This().is_null():
                zval_utils.Z_ADDREF_P(gs.EG.This())
                if not gs.EG.active_symbol_table:
                    execute_data.CVs.set(op_array.this_var, execute_data.CVs.address_of_ext(op_array.this_var))
                    execute_data.CVs.get(op_array.this_var).assign(gs.EG.This())
                else:
                    raise Exception('Not implemented yet!')
            execute_data.function_state = zend_function_state(op_array)

            while True:
                ret = self.opcode_handler(execute_data)
                if ret > 0:
                    if ret == 1:
                        gs.EG.in_execution = self.original_in_execution
                        return
                    elif ret == 2:
                        op_array = gs.EG.active_op_array
                        break
                    elif ret == 3:
                        execute_data = gs.EG.current_execute_data
                    else:
                        pass

    def execute(self, op_array):
        self.nested = False
        self.original_in_execution = gs.EG.in_execution

        if gs.EG.exception:
            return

        gs.EG.in_execution = True

        self.zend_vm_enter(op_array)

    def zend_send_by_var_helper(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]
        free_op1 = zend_free_op.make_new()
        varptr = happy_execute.GET_ZVAL_PTR(opline.get_op1(), BP_VAR_R, free_op1, execute_data)
        
        if varptr == gs.EG.uninitialized_zval_ptr():
            raise Exception('Not implemented yet')
        elif zval_utils.PZVAL_IS_REF(varptr):
            original_var = varptr

            varptr = zval_utils.zp_stack(zval_utils.make_empty_zval())
            varptr.assign(original_var.deref())
            zval_utils.Z_UNSET_ISREF_P(varptr)
            zval_utils.Z_SET_REFCOUNT_P(varptr, 0)
            happy_variables.zval_copy_ctor(varptr)
        zval_utils.Z_ADDREF_P(varptr)
        happy_execute.zend_vm_stack_push(varptr)
        happy_execute.FREE_OP_NODE(opline.get_op1(), free_op1)

        execute_data.opline += 1
        return 0

    def zend_do_fcall_common_helper(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]
        should_change_scope = False

        if execute_data.function_state.function.fn_flags & \
            (ZEND_ACC_ABSTRACT | ZEND_ACC_DEPRECATED):
            if execute_data.function_state.function.fn_flags & ZEND_ACC_ABSTRACT:
                err_msg = 'Cannot call abstract method %s::%s()' % \
                    (execute_data.function_state.function.scope.name.to_str(),
                     execute_data.function_state.function.function_name)
                zend_error_noreturn(E_ERROR, err_msg)
                execute_data.opline += 1
                return 0
            raise Exception('Not implemented yet')
        if not happy_util.is_null_struct(execute_data.function_state.function.scope) and \
            not (execute_data.function_state.function.fn_flags & ZEND_ACC_STATIC) and \
            execute_data.object.is_null():

            if execute_data.function_state.function.fn_flags & ZEND_ACC_ALLOW_STATIC:
                # TODO: throw error when E_STRICT is enabled
                pass
            else:
                raise Exception('Not implemented yet')

        if execute_data.function_state.function.type == rad.APCFile.ZEND_function.ZEND_USER_FUNCTION or \
            not happy_util.is_null_struct(execute_data.function_state.function.scope):
            should_change_scope = True
            execute_data.current_this = gs.EG.This()
            execute_data.current_scope = gs.EG.scope
            execute_data.current_called_scope = gs.EG.called_scope
            gs.EG.SetThis(execute_data.object)
            if execute_data.function_state.function.type == rad.APCFile.ZEND_function.ZEND_USER_FUNCTION or \
                not execute_data.object:
                gs.EG.scope = execute_data.function_state.function.scope
            else:
                raise Exception('Not implemented yet')
            gs.EG.called_scope = execute_data.called_scope

        happy_execute.zend_arg_types_stack_3_pop(gs.EG.arg_types_stack, execute_data)
        execute_data.function_state.arguments = happy_execute.zend_vm_stack_push_args(opline.get_extended_value())

        if execute_data.function_state.function.type == rad.APCFile.ZEND_function.ZEND_INTERNAL_FUNCTION:
            execute_data.Ts[opline.result.var / ZendExecutor.TEMP_SZ].set_ptr(
                zval_utils.zp_stack(zval_utils.zval_copy(gs.EG.zval_used_for_init)))
            execute_data.Ts[opline.result.var / ZendExecutor.TEMP_SZ].set_ptr_ptr(
                execute_data.Ts[opline.result.var / ZendExecutor.TEMP_SZ].address_of_ptr())
            execute_data.Ts[opline.result.var / ZendExecutor.TEMP_SZ].fcall_returned_reference = \
                execute_data.function_state.function.return_reference
            # TODO: check arg info
            execute_data.function_state.function.internal_fn_ptr(
                opline.get_extended_value(),
                execute_data.Ts[opline.result.var / ZendExecutor.TEMP_SZ].get_ptr(),
                execute_data.Ts[opline.result.var / ZendExecutor.TEMP_SZ].get_ptr_ptr() if \
                    execute_data.function_state.function.return_reference else gs.null_zval_ptr_ptr,
                execute_data.object,
                happy_execute.RETURN_VALUE_USED(opline))

            if not happy_execute.RETURN_VALUE_USED(opline):
                happy_variables.zval_ptr_dtor(
                    zval_utils.zpp_stack(execute_data.Ts[opline.result.var / ZendExecutor.TEMP_SZ].get_ptr()))
        elif execute_data.function_state.function.type == rad.APCFile.ZEND_function.ZEND_USER_FUNCTION:
            execute_data.original_return_value = gs.EG.return_value_ptr_ptr
            gs.EG.active_symbol_table = None
            gs.EG.active_op_array = execute_data.function_state.function
            gs.EG.return_value_ptr_ptr = gs.null_zval_ptr_ptr
            if happy_execute.RETURN_VALUE_USED(opline):
                # TODO: check
                gs.EG.return_value_ptr_ptr = execute_data.Ts[opline.get_result().var / ZendExecutor.TEMP_SZ].address_of_ptr()
                execute_data.Ts[opline.get_result().var / ZendExecutor.TEMP_SZ].set_ptr(gs.null_zval_ptr)
                execute_data.Ts[opline.get_result().var / ZendExecutor.TEMP_SZ].set_ptr_ptr(
                    execute_data.Ts[opline.get_result().var / ZendExecutor.TEMP_SZ].address_of_ptr())
                execute_data.Ts[opline.get_result().var / ZendExecutor.TEMP_SZ].fcall_returned_reference = \
                    execute_data.function_state.function.return_reference

            if not gs.EG.exception:
                execute_data.call_opline = opline.get_opcode()
                return 2
            else:
                raise Exception('Not implemented yet')
        else:
            raise Exception('Not implemented yet')

        execute_data.function_state.function = execute_data.op_array
        execute_data.function_state.arguments = 0

        if should_change_scope:
            raise Exception('Not implemented yet')

        execute_data.object = execute_data.current_object
        # TODO: decode called_scope

        happy_execute.zend_vm_stack_clear_multiple()

        if gs.EG.exception:
            raise Exception('Not implemented yet')

        execute_data.opline += 1
        return 0

    def zend_leave_helper(self, execute_data):
        op_array = execute_data.op_array

        gs.EG.current_execute_data = execute_data.prev_execute_data
        if not gs.EG.active_symbol_table:
            for cv_idx in range(execute_data.op_array.last_var):
                cv = execute_data.CVs.get(cv_idx)
                if not cv.is_null():
                    happy_variables.zval_ptr_dtor(cv)

        if (op_array.fn_flags & ZEND_ACC_CLOSURE) and not happy_util.is_null_struct(op_array.prototype):
            raise Exception('Not implemented yet')

        nested = execute_data.nested

        happy_execute.zend_vm_stack_free()

        if nested:
            execute_data = gs.EG.current_execute_data

            if execute_data.call_opline == ops.ZEND_INCLUDE_OR_EVAL:
                raise Exception('Not implemented yet')
            else:
                gs.EG.active_op_array = execute_data.op_array
                gs.EG.return_value_ptr_ptr = execute_data.original_return_value
                if gs.EG.active_symbol_table:
                    # TODO: Zend specific perf stuff; check if nop is correct
                    pass
                gs.EG.active_symbol_table = execute_data.symbol_table

                execute_data.function_state.function = execute_data.op_array

                if not gs.EG.This().is_null():
                    if gs.EG.exception:
                        raise Exception('Not implemented yet')
                    happy_variables.zval_ptr_dtor(zval_utils.zpp_stack(gs.EG.This()))
                gs.EG.SetThis(execute_data.current_this)
                gs.EG.scope = execute_data.current_scope
                gs.EG.called_scope = execute_data.current_called_scope

                execute_data.object = execute_data.current_object
                execute_data.called_scope = happy_execute.DECODE_CTOR(execute_data.called_scope)

                happy_execute.zend_vm_stack_clear_multiple()

                if gs.EG.exception:
                    raise Exception('Not implemented yet')

                execute_data.opline += 1
                return 3
        return 1

    def zend_binary_assign_op_helper(self, execute_data, binary_op):
        opline = execute_data.op_array.opcodes[execute_data.opline]
        free_op1 = zend_free_op.make_new()
        free_op2 = zend_free_op.make_new()

        if opline.get_extended_value() == ops.ZEND_ASSIGN_OBJ:
            raise Exception('Not implemented yet')
        elif opline.get_extended_value() == ops.ZEND_ASSIGN_DIM:
            raise Exception('Not implemented yet')
        else:
            value = happy_execute.GET_ZVAL_PTR(opline.get_op2(), BP_VAR_R, free_op2, execute_data)
            var_ptr = happy_execute.GET_ZVAL_PTR_PTR(opline.get_op1(), BP_VAR_RW, free_op1, execute_data)

        if var_ptr.is_null():
            raise Exception('Not implemented yet')

        if var_ptr.deref() == gs.EG.error_zval_ptr:
            raise Exception('Not implemented yet')

        zval_utils.SEPARATE_ZVAL_IF_NOT_REF(var_ptr)

        if zval_utils.Z_TYPE_PP(var_ptr) == IS_OBJECT:
            raise Exception('Not implemented yet')
        else:
            binary_op(var_ptr.deref(), var_ptr.deref(), value)

        if not happy_execute.RETURN_VALUE_UNUSED(opline.get_result()):
            zval_utils.AI_SET_PTR(execute_data.Ts[opline.get_result().var / ZendExecutor.TEMP_SZ], var_ptr.deref())
            zval_utils.PZVAL_LOCK(var_ptr.deref())
        happy_execute.FREE_OP_NODE(opline.get_op2(), free_op2)

        if opline.get_extended_value() == ops.ZEND_ASSIGN_DIM:
            raise Exception('Not implemented yet')
        happy_execute.FREE_OP_VAR_PTR(free_op1)
        execute_data.opline += 1
        return 0

    def zend_fetch_var_address_helper(self, execute_data, type):
        opline = execute_data.op_array.opcodes[execute_data.opline]
        free_op1 = zend_free_op.make_new()
        varname = happy_execute.GET_ZVAL_PTR(opline.get_op1(), BP_VAR_R, free_op1, execute_data)
        retval_ptr = zval_utils.zppp_stack(gs.null_zval_ptr_ptr)
        tmp_varname_ptr = zval_utils.zp_stack(zval_utils.make_empty_zval())

        if opline.get_op1().get_op_type() == rad.APCFile.ZEND_znode.IS_CONST and \
            zval_utils.Z_TYPE_P(varname) != IS_STRING:
            raise Exception('Not implemented yet')

        if opline.get_op2().EA_type == ZEND_FETCH_STATIC_MEMBER:
            retval = zend_object_handlers.zend_std_get_static_property(
                execute_data.Ts[opline.get_op2().var / ZendExecutor.TEMP_SZ].class_entry,
                zval_utils.Z_STRVAL_P(varname), zval_utils.Z_STRLEN_P(varname), False)
            retval_ptr.assign(retval)
            happy_execute.FREE_OP_NODE(opline.get_op1(), free_op1)
        else:
            target_symbol_table = happy_execute.zend_get_target_symbol_table(opline)
            if happy_hash.zend_hash_find(target_symbol_table, zval_utils.Z_STRVAL_P(varname), zval_utils.Z_STRLEN_P(varname),
                retval_ptr) == FAILURE:
                if type == BP_VAR_W:
                    new_zval_ptr = zval_utils.zpp_stack(gs.EG.uninitialized_zval_ptr())

                    zval_utils.Z_ADDREF_P(new_zval_ptr.deref())
                    happy_hash.zend_hash_update(target_symbol_table, zval_utils.Z_STRVAL_P(varname),
                        zval_utils.Z_STRLEN_P(varname) + 1, new_zval_ptr, 0, retval_ptr)
                else:
                    raise Exception('Not implemented yet')
            if opline.get_op2().EA_type == ZEND_FETCH_GLOBAL_LOCK:
                if opline.get_op1().get_op_type() == rad.APCFile.ZEND_znode.IS_VAR and not free_op1.var().is_null():
                    raise Exception('Not implemented yet')
            elif opline.get_op2().EA_type == ZEND_FETCH_LOCAL:
                happy_execute.FREE_OP_NODE(opline.get_op1(), free_op1)
            elif opline.get_op2().EA_type == ZEND_FETCH_STATIC:
                # TODO: implement & use zval_update_constant
                pass
            else:
                raise Exception('Not implemented yet')

        if opline.get_op1().get_op_type() != rad.APCFile.ZEND_znode.IS_CONST and varname == tmp_varname_ptr:
            raise Exception('Not implemented yet')
        if not happy_execute.RETURN_VALUE_UNUSED(opline.get_result()):
            if opline.get_extended_value() & ZEND_FETCH_MAKE_REF:
                raise Exception('Not implemented yet')
            zval_utils.PZVAL_LOCK(retval_ptr.deref().deref())
            if type == BP_VAR_R:
                zval_utils.AI_SET_PTR(execute_data.Ts[opline.get_result().var / ZendExecutor.TEMP_SZ], retval_ptr.deref().deref())
            elif type == BP_VAR_IS:
                raise Exception('Not implemented yet')
            elif type == BP_VAR_UNSET:
                raise Exception('Not implemented yet')
            else:
                execute_data.Ts[opline.get_result().var / ZendExecutor.TEMP_SZ].set_ptr_ptr(retval_ptr.deref())
        execute_data.opline += 1
        return 0

    @staticmethod
    def zend_verify_arg_type(zf, arg_num, arg, fetch_type):
        # TODO
        pass

    @staticmethod
    def ARG_SHOULD_BE_SENT_BY_REF(zf, arg_num):
        # TODO
        return False

    @staticmethod
    def ARG_MUST_BE_SENT_BY_REF(zf, arg_num):
        # TODO
        return False

    @staticmethod
    def ARG_MAY_BE_SENT_BY_REF(zf, arg_num):
        # TODO
        return False

    def ZEND_VM_HANDLER_ADD(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]
        free_op1 = zend_free_op.make_new()
        free_op2 = zend_free_op.make_new()
        happy_operators.add_function(execute_data.Ts[opline.result.var / ZendExecutor.TEMP_SZ].tmp_var_ptr(),
            happy_execute.GET_ZVAL_PTR(opline.get_op1(), BP_VAR_R, free_op1, execute_data),
            happy_execute.GET_ZVAL_PTR(opline.get_op2(), BP_VAR_R, free_op2, execute_data))
        happy_execute.FREE_OP_NODE(opline.get_op1(), free_op1)
        happy_execute.FREE_OP_NODE(opline.get_op2(), free_op2)
        execute_data.opline += 1
        return 0

    def ZEND_VM_HANDLER_SUB(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]
        free_op1 = zend_free_op.make_new()
        free_op2 = zend_free_op.make_new()
        happy_operators.sub_function(execute_data.Ts[opline.result.var / ZendExecutor.TEMP_SZ].tmp_var_ptr(),
            happy_execute.GET_ZVAL_PTR(opline.get_op1(), BP_VAR_R, free_op1, execute_data),
            happy_execute.GET_ZVAL_PTR(opline.get_op2(), BP_VAR_R, free_op2, execute_data))
        happy_execute.FREE_OP_NODE(opline.get_op1(), free_op1)
        happy_execute.FREE_OP_NODE(opline.get_op2(), free_op2)
        execute_data.opline += 1
        return 0

    def ZEND_VM_HANDLER_MUL(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]
        free_op1 = zend_free_op.make_new()
        free_op2 = zend_free_op.make_new()
        happy_operators.mul_function(execute_data.Ts[opline.result.var / ZendExecutor.TEMP_SZ].tmp_var_ptr(),
            happy_execute.GET_ZVAL_PTR(opline.get_op1(), BP_VAR_R, free_op1, execute_data),
            happy_execute.GET_ZVAL_PTR(opline.get_op2(), BP_VAR_R, free_op2, execute_data))
        happy_execute.FREE_OP_NODE(opline.get_op1(), free_op1)
        happy_execute.FREE_OP_NODE(opline.get_op2(), free_op2)
        execute_data.opline += 1
        return 0

    def ZEND_VM_HANDLER_DIV(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]
        free_op1 = zend_free_op.make_new()
        free_op2 = zend_free_op.make_new()

        happy_operators.div_function(execute_data.Ts[opline.result.var / ZendExecutor.TEMP_SZ].tmp_var_ptr(),
            happy_execute.GET_ZVAL_PTR(opline.get_op1(), BP_VAR_R, free_op1, execute_data),
            happy_execute.GET_ZVAL_PTR(opline.get_op2(), BP_VAR_R, free_op2, execute_data))
        happy_execute.FREE_OP_NODE(opline.get_op1(), free_op1)
        happy_execute.FREE_OP_NODE(opline.get_op2(), free_op2)
        execute_data.opline += 1
        return 0

    def ZEND_VM_HANDLER_MOD(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]
        free_op1 = zend_free_op.make_new()
        free_op2 = zend_free_op.make_new()

        happy_operators.mod_function(execute_data.Ts[opline.result.var / ZendExecutor.TEMP_SZ].tmp_var_ptr(),
            happy_execute.GET_ZVAL_PTR(opline.get_op1(), BP_VAR_R, free_op1, execute_data),
            happy_execute.GET_ZVAL_PTR(opline.get_op2(), BP_VAR_R, free_op2, execute_data))
        happy_execute.FREE_OP_NODE(opline.get_op1(), free_op1)
        happy_execute.FREE_OP_NODE(opline.get_op2(), free_op2)
        execute_data.opline += 1
        return 0

    def ZEND_VM_HANDLER_SL(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]
        free_op1 = zend_free_op.make_new()
        free_op2 = zend_free_op.make_new()

        happy_operators.shift_left_function(execute_data.Ts[opline.result.var / ZendExecutor.TEMP_SZ].tmp_var_ptr(),
            happy_execute.GET_ZVAL_PTR(opline.get_op1(), BP_VAR_R, free_op1, execute_data),
            happy_execute.GET_ZVAL_PTR(opline.get_op2(), BP_VAR_R, free_op2, execute_data))
        happy_execute.FREE_OP_NODE(opline.get_op1(), free_op1)
        happy_execute.FREE_OP_NODE(opline.get_op2(), free_op2)
        execute_data.opline += 1
        return 0

    def ZEND_VM_HANDLER_SR(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]
        free_op1 = zend_free_op.make_new()
        free_op2 = zend_free_op.make_new()

        happy_operators.shift_right_function(execute_data.Ts[opline.result.var / ZendExecutor.TEMP_SZ].tmp_var_ptr(),
            happy_execute.GET_ZVAL_PTR(opline.get_op1(), BP_VAR_R, free_op1, execute_data),
            happy_execute.GET_ZVAL_PTR(opline.get_op2(), BP_VAR_R, free_op2, execute_data))
        happy_execute.FREE_OP_NODE(opline.get_op1(), free_op1)
        happy_execute.FREE_OP_NODE(opline.get_op2(), free_op2)
        execute_data.opline += 1
        return 0

    def ZEND_VM_HANDLER_CONCAT(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]
        free_op1 = zend_free_op.make_new()
        free_op2 = zend_free_op.make_new()

        happy_operators.concat_function(execute_data.Ts[opline.result.var / ZendExecutor.TEMP_SZ].tmp_var_ptr(),
            happy_execute.GET_ZVAL_PTR(opline.get_op1(), BP_VAR_R, free_op1, execute_data),
            happy_execute.GET_ZVAL_PTR(opline.get_op2(), BP_VAR_R, free_op2, execute_data))
        happy_execute.FREE_OP_NODE(opline.get_op1(), free_op1)
        happy_execute.FREE_OP_NODE(opline.get_op2(), free_op2)
        execute_data.opline += 1
        return 0

    def ZEND_VM_HANDLER_IS_IDENTICAL(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]
        free_op1 = zend_free_op.make_new()
        free_op2 = zend_free_op.make_new()
        result = execute_data.Ts[opline.get_result().var / ZendExecutor.TEMP_SZ].tmp_var_ptr()

        happy_operators.is_identical_function(execute_data.Ts[opline.result.var / ZendExecutor.TEMP_SZ].tmp_var_ptr(),
            happy_execute.GET_ZVAL_PTR(opline.get_op1(), BP_VAR_R, free_op1, execute_data),
            happy_execute.GET_ZVAL_PTR(opline.get_op2(), BP_VAR_R, free_op2, execute_data))
        happy_execute.FREE_OP_NODE(opline.get_op1(), free_op1)
        happy_execute.FREE_OP_NODE(opline.get_op2(), free_op2)
        execute_data.opline += 1
        return 0

    def ZEND_VM_HANDLER_IS_NOT_IDENTICAL(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]
        free_op1 = zend_free_op.make_new()
        free_op2 = zend_free_op.make_new()
        result = execute_data.Ts[opline.get_result().var / ZendExecutor.TEMP_SZ].tmp_var_ptr()

        happy_operators.is_identical_function(execute_data.Ts[opline.result.var / ZendExecutor.TEMP_SZ].tmp_var_ptr(),
            happy_execute.GET_ZVAL_PTR(opline.get_op1(), BP_VAR_R, free_op1, execute_data),
            happy_execute.GET_ZVAL_PTR(opline.get_op2(), BP_VAR_R, free_op2, execute_data))
        zval_utils.Z_SET_LVAL_P(result, 1 if zval_utils.Z_LVAL_P(result) == 0 else 0)
        happy_execute.FREE_OP_NODE(opline.get_op1(), free_op1)
        happy_execute.FREE_OP_NODE(opline.get_op2(), free_op2)
        execute_data.opline += 1
        return 0

    def ZEND_VM_HANDLER_IS_EQUAL(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]
        free_op1 = zend_free_op.make_new()
        free_op2 = zend_free_op.make_new()
        result = execute_data.Ts[opline.get_result().var / ZendExecutor.TEMP_SZ].tmp_var_ptr()

        happy_operators.compare_function(execute_data.Ts[opline.result.var / ZendExecutor.TEMP_SZ].tmp_var_ptr(),
            happy_execute.GET_ZVAL_PTR(opline.get_op1(), BP_VAR_R, free_op1, execute_data),
            happy_execute.GET_ZVAL_PTR(opline.get_op2(), BP_VAR_R, free_op2, execute_data))
        zval_utils.ZVAL_BOOL(result, zval_utils.Z_LVAL_P(result) == 0)
        happy_execute.FREE_OP_NODE(opline.get_op1(), free_op1)
        happy_execute.FREE_OP_NODE(opline.get_op2(), free_op2)
        execute_data.opline += 1
        return 0

    def ZEND_VM_HANDLER_IS_NOT_EQUAL(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]
        free_op1 = zend_free_op.make_new()
        free_op2 = zend_free_op.make_new()
        result = execute_data.Ts[opline.get_result().var / ZendExecutor.TEMP_SZ].tmp_var_ptr()

        happy_operators.compare_function(execute_data.Ts[opline.result.var / ZendExecutor.TEMP_SZ].tmp_var_ptr(),
            happy_execute.GET_ZVAL_PTR(opline.get_op1(), BP_VAR_R, free_op1, execute_data),
            happy_execute.GET_ZVAL_PTR(opline.get_op2(), BP_VAR_R, free_op2, execute_data))
        zval_utils.ZVAL_BOOL(result, zval_utils.Z_LVAL_P(result) != 0)
        happy_execute.FREE_OP_NODE(opline.get_op1(), free_op1)
        happy_execute.FREE_OP_NODE(opline.get_op2(), free_op2)
        execute_data.opline += 1
        return 0

    def ZEND_VM_HANDLER_IS_SMALLER(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]
        free_op1 = zend_free_op.make_new()
        free_op2 = zend_free_op.make_new()
        result = execute_data.Ts[opline.get_result().var / ZendExecutor.TEMP_SZ].tmp_var_ptr()

        happy_operators.compare_function(execute_data.Ts[opline.result.var / ZendExecutor.TEMP_SZ].tmp_var_ptr(),
            happy_execute.GET_ZVAL_PTR(opline.get_op1(), BP_VAR_R, free_op1, execute_data),
            happy_execute.GET_ZVAL_PTR(opline.get_op2(), BP_VAR_R, free_op2, execute_data))
        zval_utils.ZVAL_BOOL(result, zval_utils.Z_LVAL_P(result) < 0)
        happy_execute.FREE_OP_NODE(opline.get_op1(), free_op1)
        happy_execute.FREE_OP_NODE(opline.get_op2(), free_op2)
        execute_data.opline += 1
        return 0

    def ZEND_VM_HANDLER_IS_SMALLER_OR_EQUAL(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]
        free_op1 = zend_free_op.make_new()
        free_op2 = zend_free_op.make_new()
        result = execute_data.Ts[opline.get_result().var / ZendExecutor.TEMP_SZ].tmp_var_ptr()

        happy_operators.compare_function(execute_data.Ts[opline.result.var / ZendExecutor.TEMP_SZ].tmp_var_ptr(),
            happy_execute.GET_ZVAL_PTR(opline.get_op1(), BP_VAR_R, free_op1, execute_data),
            happy_execute.GET_ZVAL_PTR(opline.get_op2(), BP_VAR_R, free_op2, execute_data))
        zval_utils.ZVAL_BOOL(result, zval_utils.Z_LVAL_P(result) <= 0)
        happy_execute.FREE_OP_NODE(opline.get_op1(), free_op1)
        happy_execute.FREE_OP_NODE(opline.get_op2(), free_op2)
        execute_data.opline += 1
        return 0

    def ZEND_VM_HANDLER_BW_OR(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]
        free_op1 = zend_free_op.make_new()
        free_op2 = zend_free_op.make_new()

        happy_operators.bitwise_or_function(execute_data.Ts[opline.result.var / ZendExecutor.TEMP_SZ].tmp_var_ptr(),
            happy_execute.GET_ZVAL_PTR(opline.get_op1(), BP_VAR_R, free_op1, execute_data),
            happy_execute.GET_ZVAL_PTR(opline.get_op2(), BP_VAR_R, free_op2, execute_data))
        happy_execute.FREE_OP_NODE(opline.get_op1(), free_op1)
        happy_execute.FREE_OP_NODE(opline.get_op2(), free_op2)
        execute_data.opline += 1
        return 0

    def ZEND_VM_HANDLER_BW_AND(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]
        free_op1 = zend_free_op.make_new()
        free_op2 = zend_free_op.make_new()

        happy_operators.bitwise_and_function(execute_data.Ts[opline.result.var / ZendExecutor.TEMP_SZ].tmp_var_ptr(),
            happy_execute.GET_ZVAL_PTR(opline.get_op1(), BP_VAR_R, free_op1, execute_data),
            happy_execute.GET_ZVAL_PTR(opline.get_op2(), BP_VAR_R, free_op2, execute_data))
        happy_execute.FREE_OP_NODE(opline.get_op1(), free_op1)
        happy_execute.FREE_OP_NODE(opline.get_op2(), free_op2)
        execute_data.opline += 1
        return 0

    def ZEND_VM_HANDLER_BW_XOR(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]
        free_op1 = zend_free_op.make_new()
        free_op2 = zend_free_op.make_new()

        happy_operators.bitwise_xor_function(execute_data.Ts[opline.result.var / ZendExecutor.TEMP_SZ].tmp_var_ptr(),
            happy_execute.GET_ZVAL_PTR(opline.get_op1(), BP_VAR_R, free_op1, execute_data),
            happy_execute.GET_ZVAL_PTR(opline.get_op2(), BP_VAR_R, free_op2, execute_data))
        happy_execute.FREE_OP_NODE(opline.get_op1(), free_op1)
        happy_execute.FREE_OP_NODE(opline.get_op2(), free_op2)
        execute_data.opline += 1
        return 0

    def ZEND_VM_HANDLER_BW_NOT(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]
        free_op1 = zend_free_op.make_new()

        happy_operators.bitwise_not_function(execute_data.Ts[opline.result.var / ZendExecutor.TEMP_SZ].tmp_var_ptr(),
            happy_execute.GET_ZVAL_PTR(opline.get_op1(), BP_VAR_R, free_op1, execute_data))
        happy_execute.FREE_OP_NODE(opline.get_op1(), free_op1)
        execute_data.opline += 1
        return 0

    def ZEND_VM_HANDLER_BOOL_NOT(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]
        free_op1 = zend_free_op.make_new()

        happy_operators.boolean_not_function(execute_data.Ts[opline.result.var / ZendExecutor.TEMP_SZ].tmp_var_ptr(),
            happy_execute.GET_ZVAL_PTR(opline.get_op1(), BP_VAR_R, free_op1, execute_data))
        happy_execute.FREE_OP_NODE(opline.get_op1(), free_op1)
        execute_data.opline += 1
        return 0

    def ZEND_VM_HANDLER_ASSIGN_ADD(self, execute_data):
        return self.zend_binary_assign_op_helper(execute_data, happy_operators.add_function)

    def ZEND_VM_HANDLER_ASSIGN_SUB(self, execute_data):
        return self.zend_binary_assign_op_helper(execute_data, happy_operators.sub_function)

    def ZEND_VM_HANDLER_ASSIGN_MUL(self, execute_data):
        return self.zend_binary_assign_op_helper(execute_data, happy_operators.mul_function)

    def ZEND_VM_HANDLER_ASSIGN_DIV(self, execute_data):
        return self.zend_binary_assign_op_helper(execute_data, happy_operators.div_function)

    def ZEND_VM_HANDLER_ASSIGN_MOD(self, execute_data):
        return self.zend_binary_assign_op_helper(execute_data, happy_operators.mod_function)

    def ZEND_VM_HANDLER_ASSIGN_SL(self, execute_data):
        return self.zend_binary_assign_op_helper(execute_data, happy_operators.shift_left_function)

    def ZEND_VM_HANDLER_ASSIGN_SR(self, execute_data):
        return self.zend_binary_assign_op_helper(execute_data, happy_operators.shift_right_function)

    def ZEND_VM_HANDLER_ASSIGN_CONCAT(self, execute_data):
        return self.zend_binary_assign_op_helper(execute_data, happy_operators.concat_function)

    def ZEND_VM_HANDLER_ASSIGN_BW_OR(self, execute_data):
        return self.zend_binary_assign_op_helper(execute_data, happy_operators.bitwise_or_function)

    def ZEND_VM_HANDLER_ASSIGN_BW_AND(self, execute_data):
        return self.zend_binary_assign_op_helper(execute_data, happy_operators.bitwise_and_function)

    def ZEND_VM_HANDLER_ASSIGN_BW_XOR(self, execute_data):
        return self.zend_binary_assign_op_helper(execute_data, happy_operators.bitwise_xor_function)

    def zend_pre_incdec_property_helper(self, execute_data, incdec_op):
        opline = execute_data.op_array.opcodes[execute_data.opline]
        free_op1 = zend_free_op.make_new()
        free_op2 = zend_free_op.make_new()
        object_ptr = happy_execute.GET_OBJ_ZVAL_PTR_PTR(opline.get_op1(), BP_VAR_W, free_op1, execute_data)
        property = happy_execute.GET_ZVAL_PTR(opline.get_op2(), BP_VAR_R, free_op2, execute_data)
        retval = execute_data.Ts[opline.result.var / ZendExecutor.TEMP_SZ].address_of_ptr()
        have_get_ptr = False

        if opline.get_op1().get_op_type() == rad.APCFile.ZEND_znode.IS_VAR and not object_ptr.is_null():
            raise Exception('Not implemented yet')

        happy_execute.make_real_object(object_ptr)
        object = object_ptr.deref()

        if zval_utils.Z_TYPE_P(object) != IS_OBJECT:
            raise Exception('Not implemented yet')

        # here we are sure we are dealing with an object

        if happy_execute.IS_OP_TMP_FREE(opline.get_op2()):
            raise Exception('Not implemented yet')

        if zval_utils.Z_OBJ_HT_P(object).get_property_ptr_ptr:
            zptr = zval_utils.Z_OBJ_HT_P(object).get_property_ptr_ptr(object, property)
            if not zptr.is_null():
                zval_utils.SEPARATE_ZVAL_IF_NOT_REF(zptr)

                have_get_ptr = True
                incdec_op(zptr.deref())
                if not happy_execute.RETURN_VALUE_UNUSED(opline.get_result()):
                    retval.assign(zptr.deref())
                    zval_utils.PZVAL_LOCK(retval.deref())

        if not have_get_ptr:
            raise Exception('Not implemented yet')

        if happy_execute.IS_OP_TMP_FREE(opline.get_op2()):
            raise Exception('Not implemented yet')
        else:
            happy_execute.FREE_OP_NODE(opline.get_op2(), free_op2)
        happy_execute.FREE_OP_VAR_PTR(free_op1)
        execute_data.opline += 1
        return 0

    def zend_post_incdec_property_helper(self, execute_data, incdec_op):
        opline = execute_data.op_array.opcodes[execute_data.opline]
        free_op1 = zend_free_op.make_new()
        free_op2 = zend_free_op.make_new()
        object_ptr = happy_execute.GET_OBJ_ZVAL_PTR_PTR(opline.get_op1(), BP_VAR_W, free_op1, execute_data)
        property = happy_execute.GET_ZVAL_PTR(opline.get_op2(), BP_VAR_R, free_op2, execute_data)
        retval = execute_data.Ts[opline.result.var / ZendExecutor.TEMP_SZ].tmp_var_ptr()
        have_get_ptr = False

        if opline.get_op1().get_op_type() == rad.APCFile.ZEND_znode.IS_VAR and not object_ptr.is_null():
            raise Exception('Not implemented yet')

        happy_execute.make_real_object(object_ptr)
        object = object_ptr.deref()

        if zval_utils.Z_TYPE_P(object) != IS_OBJECT:
            raise Exception('Not implemented yet')

        # here we are sure we are dealing with an object

        if happy_execute.IS_OP_TMP_FREE(opline.get_op2()):
            raise Exception('Not implemented yet')

        if zval_utils.Z_OBJ_HT_P(object).get_property_ptr_ptr:
            zptr = zval_utils.Z_OBJ_HT_P(object).get_property_ptr_ptr(object, property)
            if not zptr.is_null():
                have_get_ptr = True
                zval_utils.SEPARATE_ZVAL_IF_NOT_REF(zptr)

                retval.assign(zptr.deref().deref())
                zval_utils.i_zval_copy_ctor(retval)

                incdec_op(zptr.deref())

        if not have_get_ptr:
            raise Exception('Not implemented yet')

        if happy_execute.IS_OP_TMP_FREE(opline.get_op2()):
            raise Exception('Not implemented yet')
        else:
            happy_execute.FREE_OP_NODE(opline.get_op2(), free_op2)
        happy_execute.FREE_OP_VAR_PTR(free_op1)
        execute_data.opline += 1
        return 0

    def ZEND_VM_HANDLER_PRE_INC_OBJ(self, execute_data):
        return self.zend_pre_incdec_property_helper(execute_data, happy_operators.increment_function)

    def ZEND_VM_HANDLER_POST_INC_OBJ(self, execute_data):
        return self.zend_post_incdec_property_helper(execute_data, happy_operators.increment_function)

    def ZEND_VM_HANDLER_PRE_INC(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]
        free_op1 = zend_free_op.make_new()
        var_ptr = happy_execute.GET_ZVAL_PTR_PTR(opline.get_op1(), BP_VAR_RW, free_op1, execute_data)
        assert isinstance(var_ptr, objects.zval_ptr_ptr)

        if opline.get_op1().get_op_type() == rad.APCFile.ZEND_znode.IS_VAR and var_ptr.is_null():
            raise Exception('Not implemented yet')
        if opline.get_op1().get_op_type() == rad.APCFile.ZEND_znode.IS_VAR and \
           var_ptr.deref() == gs.EG.error_zval_ptr:
            raise Exception('Not implemented yet')

        zval_utils.SEPARATE_ZVAL_IF_NOT_REF(var_ptr)

        if zval_utils.Z_TYPE_PP(var_ptr) == IS_OBJECT:
            raise Exception('Not implemented yet')
        else:
            happy_operators.increment_function(var_ptr.deref())

        if not happy_execute.RETURN_VALUE_UNUSED(opline.get_result()):
            zval_utils.AI_SET_PTR(execute_data.Ts[opline.get_result().var / ZendExecutor.TEMP_SZ], var_ptr.deref())
            zval_utils.PZVAL_LOCK(var_ptr.deref())

        happy_execute.FREE_OP_VAR_PTR(free_op1)
        execute_data.opline += 1
        return 0

    def ZEND_VM_HANDLER_PRE_DEC(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]
        free_op1 = zend_free_op.make_new()
        var_ptr = happy_execute.GET_ZVAL_PTR_PTR(opline.get_op1(), BP_VAR_RW, free_op1, execute_data)
        assert isinstance(var_ptr, objects.zval_ptr_ptr)

        if opline.get_op1().get_op_type() == rad.APCFile.ZEND_znode.IS_VAR and var_ptr.is_null():
            raise Exception('Not implemented yet')
        if opline.get_op1().get_op_type() == rad.APCFile.ZEND_znode.IS_VAR and \
           var_ptr.deref() == gs.EG.error_zval_ptr:
            raise Exception('Not implemented yet')

        zval_utils.SEPARATE_ZVAL_IF_NOT_REF(var_ptr)

        if zval_utils.Z_TYPE_PP(var_ptr) == IS_OBJECT:
            raise Exception('Not implemented yet')
        else:
            happy_operators.decrement_function(var_ptr.deref())

        if not happy_execute.RETURN_VALUE_UNUSED(opline.get_result()):
            zval_utils.AI_SET_PTR(execute_data.Ts[opline.get_result().var / ZendExecutor.TEMP_SZ], var_ptr.deref())
            zval_utils.PZVAL_LOCK(var_ptr.deref())

        happy_execute.FREE_OP_VAR_PTR(free_op1)
        execute_data.opline += 1
        return 0

    def ZEND_VM_HANDLER_POST_INC(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]
        free_op1 = zend_free_op.make_new()
        var_ptr = happy_execute.GET_ZVAL_PTR_PTR(opline.get_op1(), BP_VAR_RW, free_op1, execute_data)
        assert isinstance(var_ptr, objects.zval_ptr_ptr)

        if opline.get_op1().get_op_type() == rad.APCFile.ZEND_znode.IS_VAR and var_ptr.is_null():
            raise Exception('Not implemented yet')
        if opline.get_op1().get_op_type() == rad.APCFile.ZEND_znode.IS_VAR and \
           var_ptr.deref() == gs.EG.error_zval_ptr:
            raise Exception('Not implemented yet')

        execute_data.Ts[opline.get_result().var / ZendExecutor.TEMP_SZ].tmp_var_ptr().assign(var_ptr.deref().deref())
        zval_utils.i_zval_copy_ctor(
            execute_data.Ts[opline.get_result().var / ZendExecutor.TEMP_SZ].tmp_var_ptr())

        zval_utils.SEPARATE_ZVAL_IF_NOT_REF(var_ptr)

        if zval_utils.Z_TYPE_PP(var_ptr) == IS_OBJECT:
            raise Exception('Not implemented yet')
        else:
            happy_operators.increment_function(var_ptr.deref())

        happy_execute.FREE_OP_IF_VAR(free_op1)
        execute_data.opline += 1
        return 0

    def ZEND_VM_HANDLER_POST_DEC(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]
        free_op1 = zend_free_op.make_new()
        var_ptr = happy_execute.GET_ZVAL_PTR_PTR(opline.get_op1(), BP_VAR_RW, free_op1, execute_data)
        assert isinstance(var_ptr, objects.zval_ptr_ptr)

        if opline.get_op1().get_op_type() == rad.APCFile.ZEND_znode.IS_VAR and var_ptr.is_null():
            raise Exception('Not implemented yet')
        if opline.get_op1().get_op_type() == rad.APCFile.ZEND_znode.IS_VAR and \
           var_ptr.deref() == gs.EG.error_zval_ptr:
            raise Exception('Not implemented yet')

        execute_data.Ts[opline.get_result().var / ZendExecutor.TEMP_SZ].tmp_var_ptr().assign(var_ptr.deref().deref())
        zval_utils.i_zval_copy_ctor(
            execute_data.Ts[opline.get_result().var / ZendExecutor.TEMP_SZ].tmp_var_ptr())

        zval_utils.SEPARATE_ZVAL_IF_NOT_REF(var_ptr)

        if zval_utils.Z_TYPE_PP(var_ptr) == IS_OBJECT:
            raise Exception('Not implemented yet')
        else:
            happy_operators.decrement_function(var_ptr.deref())

        happy_execute.FREE_OP_IF_VAR(free_op1)
        execute_data.opline += 1
        return 0

    def ZEND_VM_HANDLER_FETCH_DIM_R(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]
        free_op1 = zend_free_op.make_new()
        free_op2 = zend_free_op.make_new()
        dim = happy_execute.GET_ZVAL_PTR(opline.get_op2(), BP_VAR_R, free_op2, execute_data)

        if opline.get_extended_value() == ZEND_FETCH_ADD_LOCK:
            raise Exception('Not implemented yet')
        container = happy_execute.GET_ZVAL_PTR_PTR(opline.get_op1(), BP_VAR_R, free_op1, execute_data)
        if opline.get_op1().get_op_type() == rad.APCFile.ZEND_znode.IS_VAR and happy_util.is_null_struct(container):
            raise Exception('Not implemented yet')
        happy_execute.zend_fetch_dimension_address_read(
            None if happy_execute.RETURN_VALUE_UNUSED(opline.get_result()) \
            else execute_data.Ts[opline.get_result().var / ZendExecutor.TEMP_SZ],
            container,
            dim,
            happy_execute.IS_OP_TMP_FREE(opline.get_op2()),
            BP_VAR_R
        )
        happy_execute.FREE_OP_NODE(opline.get_op2(), free_op2)
        happy_execute.FREE_OP_VAR_PTR(free_op1)
        execute_data.opline += 1
        return 0

    def ZEND_VM_HANDLER_FETCH_DIM_W(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]
        free_op1 = zend_free_op.make_new()
        free_op2 = zend_free_op.make_new()
        dim = happy_execute.GET_ZVAL_PTR(opline.get_op2(), BP_VAR_R, free_op2, execute_data)
        container = happy_execute.GET_ZVAL_PTR_PTR(opline.get_op1(), BP_VAR_W, free_op1, execute_data)

        if opline.get_op1().get_op_type() == rad.APCFile.ZEND_znode.IS_VAR and container.is_null():
            raise Exception('Not implemented yet')
        happy_execute.zend_fetch_dimension_address(execute_data.Ts[opline.get_result().var / ZendExecutor.TEMP_SZ],
            container, dim, happy_execute.IS_TMP_FREE(free_op2), BP_VAR_W)
        happy_execute.FREE_OP_NODE(opline.get_op2(), free_op2)
        happy_execute.FREE_OP_VAR_PTR(free_op1)

        # We are going to assign the result by reference
        if opline.get_extended_value() and \
            not execute_data.Ts[opline.get_result().var / ZendExecutor.TEMP_SZ].get_ptr_ptr().is_null():
            zval_utils.Z_DELREF_PP(execute_data.Ts[opline.get_result().var / ZendExecutor.TEMP_SZ].get_ptr_ptr())
            zval_utils.SEPARATE_ZVAL_TO_MAKE_IS_REF(
                execute_data.Ts[opline.get_result().var / ZendExecutor.TEMP_SZ].get_ptr_ptr())
            zval_utils.Z_ADDREF_PP(execute_data.Ts[opline.get_result().var / ZendExecutor.TEMP_SZ].get_ptr_ptr())

        execute_data.opline += 1
        return 0

    def ZEND_VM_HANDLER_FETCH_DIM_FUNC_ARG(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]
        free_op1 = zend_free_op.make_new()
        free_op2 = zend_free_op.make_new()
        dim = happy_execute.GET_ZVAL_PTR(opline.get_op2(), BP_VAR_R, free_op2, execute_data)

        if self.ARG_SHOULD_BE_SENT_BY_REF(execute_data.fbc, opline.get_extended_value()):
            raise Exception('Not implemented yet')
        else:
            if opline.get_op2().get_op_type() == rad.APCFile.ZEND_znode.IS_UNUSED:
                raise Exception('Not implemented yet')
            container = happy_execute.GET_ZVAL_PTR_PTR(opline.get_op1(), BP_VAR_R, free_op1, execute_data)
            if opline.get_op1().get_op_type() == rad.APCFile.ZEND_znode.IS_VAR and container.is_null():
                raise Exception('Not implemented yet')
            happy_execute.zend_fetch_dimension_address_read(
                execute_data.Ts[opline.get_result().var / ZendExecutor.TEMP_SZ], container,
                dim, happy_execute.IS_OP_TMP_FREE(opline.get_op2()), BP_VAR_R)
        happy_execute.FREE_OP_NODE(opline.get_op2(), free_op2)
        happy_execute.FREE_OP_VAR_PTR(free_op1)
        execute_data.opline += 1
        return 0

    def zend_fetch_property_address_read_helper(self, execute_data, type):
        opline = execute_data.op_array.opcodes[execute_data.opline]
        free_op1 = zend_free_op.make_new()
        container = happy_execute.GET_OBJ_ZVAL_PTR(opline.get_op1(), type, free_op1, execute_data)
        free_op2 = zend_free_op.make_new()
        offset = happy_execute.GET_ZVAL_PTR(opline.get_op2(), BP_VAR_R, free_op2, execute_data)

        if (zval_utils.Z_TYPE_P(container) != IS_OBJECT or
            not zval_utils.Z_OBJ_HT_P(container).read_property):
            raise Exception('Not implemented yet')
        else:
            if happy_execute.IS_OP_TMP_FREE(opline.get_op2()):
                raise Exception('Not implemented yet')

            retval = zval_utils.Z_OBJ_HT_P(container).read_property(container, offset, type)

            if happy_execute.RETURN_VALUE_UNUSED(opline.get_result()):
                raise Exception('Not implemented yet')
            else:
                zval_utils.AI_SET_PTR(execute_data.Ts[opline.get_result().var / ZendExecutor.TEMP_SZ], retval)
                zval_utils.PZVAL_LOCK(retval)

        execute_data.opline += 1
        return 0

    def ZEND_VM_HANDLER_FETCH_OBJ_R(self, execute_data):
        return self.zend_fetch_property_address_read_helper(execute_data, BP_VAR_R)

    def ZEND_VM_HANDLER_FETCH_OBJ_FUNC_ARG(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]

        if self.ARG_SHOULD_BE_SENT_BY_REF(execute_data.fbc, opline.get_extended_value()):
            raise Exception('Not implemented yet')
        else:
            return self.zend_fetch_property_address_read_helper(execute_data, BP_VAR_R)

    def ZEND_VM_HANDLER_ECHO(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]
        free_op1 = zend_free_op.make_new()
        z = happy_execute.GET_ZVAL_PTR(opline.get_op1(), BP_VAR_R, free_op1, execute_data)

        if opline.get_op1().get_op_type() != rad.APCFile.ZEND_znode.IS_CONST and \
            zval_utils.Z_TYPE_P(z) == IS_OBJECT:
            raise Exception('Not implemented yet')
        else:
            happy_variables.zend_print_variable(z)

        happy_execute.FREE_OP_NODE(opline.get_op1(), free_op1)
        execute_data.opline += 1
        return 0

    def ZEND_VM_HANDLER_PRINT(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]

        zval_utils.Z_SET_LVAL_P(execute_data.Ts[opline.get_result().var / ZendExecutor.TEMP_SZ].tmp_var_ptr(), 1)
        zval_utils.Z_SET_TYPE_P(execute_data.Ts[opline.get_result().var / ZendExecutor.TEMP_SZ].tmp_var_ptr(), IS_LONG)

        return self.ZEND_VM_HANDLER_ECHO(execute_data)

    def ZEND_VM_HANDLER_FETCH_R(self, execute_data):
        return self.zend_fetch_var_address_helper(execute_data, BP_VAR_R)

    def ZEND_VM_HANDLER_FETCH_W(self, execute_data):
        return self.zend_fetch_var_address_helper(execute_data, BP_VAR_W)

    def ZEND_VM_HANDLER_FETCH_RW(self, execute_data):
        return self.zend_fetch_var_address_helper(execute_data, BP_VAR_RW)

    def ZEND_VM_HANDLER_ASSIGN_OBJ(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]
        op_data = execute_data.op_array.opcodes[execute_data.opline + 1]
        free_op1 = zend_free_op.make_new()
        free_op2 = zend_free_op.make_new()
        object_ptr = happy_execute.GET_ZVAL_PTR_PTR(opline.get_op1(), BP_VAR_W, free_op1, execute_data)
        property_name = happy_execute.GET_ZVAL_PTR(opline.get_op2(), BP_VAR_R, free_op2, execute_data)

        if happy_execute.IS_OP_TMP_FREE(opline.get_op2()):
            raise Exception('Not implemented yet')
        if opline.get_op1().get_op_type() == rad.APCFile.ZEND_znode.IS_VAR and not object_ptr.is_null():
            raise Exception('Not implemented yet')
        happy_execute.zend_assign_to_object(opline.get_result(), object_ptr, property_name, op_data.get_op1(),
            execute_data.Ts, ops.ZEND_ASSIGN_OBJ)
        if happy_execute.IS_OP_TMP_FREE(opline.get_op2()):
            raise Exception('Not implemented yet')
        else:
            happy_execute.FREE_OP_NODE(opline.get_op2(), free_op2)
        happy_execute.FREE_OP_VAR_PTR(free_op1)
        # assign_obj has two opcodes!
        execute_data.opline += 2
        return 0

    def ZEND_VM_HANDLER_ASSIGN_DIM(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]
        op_data = execute_data.op_array.opcodes[execute_data.opline + 1]
        free_op1 = zend_free_op.make_new()
        object_ptr = happy_execute.GET_ZVAL_PTR_PTR(opline.get_op1(), BP_VAR_W, free_op1, execute_data)

        if opline.get_op1().get_op_type() == rad.APCFile.ZEND_znode.IS_VAR and object_ptr.is_null():
            raise Exception('Not implemented yet')
        if zval_utils.Z_TYPE_PP(object_ptr) == IS_OBJECT:
            raise Exception('Not implemented yet')
        else:
            free_op2 = zend_free_op.make_new()
            free_op_data1 = zend_free_op.make_new()
            free_op_data2 = zend_free_op.make_new()
            dim = happy_execute.GET_ZVAL_PTR(opline.get_op2(), BP_VAR_R, free_op2, execute_data)

            happy_execute.zend_fetch_dimension_address(execute_data.Ts[op_data.get_op2().var / ZendExecutor.TEMP_SZ],
                object_ptr, dim, happy_execute.IS_OP_TMP_FREE(opline.get_op2()), BP_VAR_W)
            happy_execute.FREE_OP_NODE(opline.get_op2(), free_op2)

            value = happy_execute.get_zval_ptr(op_data.get_op1(), execute_data.Ts, free_op_data1, BP_VAR_R)
            variable_ptr_ptr = happy_execute.get_zval_ptr_ptr_var(op_data.get_op2(), execute_data.Ts, free_op_data2)
            if variable_ptr_ptr.is_null():
                if happy_execute.zend_assign_to_string_offset(
                    execute_data.Ts[op_data.get_op2().var / ZendExecutor.TEMP_SZ],
                    value,
                    op_data.get_op1().get_op_type()):
                    if not happy_execute.RETURN_VALUE_UNUSED(opline.get_result()):
                        raise Exception('Not implemented yet')
                else:
                    raise Exception('Not implemented yet')
            else:
                happy_execute.zend_assign_to_variable(variable_ptr_ptr, value,
                    happy_execute.IS_TMP_FREE(free_op_data1))
                if not happy_execute.RETURN_VALUE_UNUSED(opline.get_result()):
                    raise Exception('Not implemented yet')
            happy_execute.FREE_OP_VAR_PTR(free_op_data2)
            happy_execute.FREE_OP_IF_VAR(free_op_data1)

        # assign_dim has two opcodes!
        execute_data.opline += 2
        return 0

    def ZEND_VM_HANDLER_ASSIGN(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]
        free_op1 = zend_free_op.make_new()
        free_op2 = zend_free_op.make_new()
        value = happy_execute.GET_ZVAL_PTR(opline.get_op2(), BP_VAR_R, free_op2, execute_data)
        variable_ptr_ptr = happy_execute.GET_ZVAL_PTR_PTR(opline.get_op1(), BP_VAR_W, free_op1, execute_data)

        if opline.get_op1().get_op_type() == rad.APCFile.ZEND_znode.IS_VAR and variable_ptr_ptr.is_null():
            raise Exception('Not implemented yet')
        else:
            value = happy_execute.zend_assign_to_variable(variable_ptr_ptr, value,
                happy_execute.IS_OP_TMP_FREE(opline.get_op2()))
            if not happy_execute.RETURN_VALUE_UNUSED(opline.get_result()):
                zval_utils.AI_SET_PTR(execute_data.Ts[opline.get_result().var / ZendExecutor.TEMP_SZ], value)
                zval_utils.PZVAL_LOCK(value)

        happy_execute.FREE_OP_VAR_PTR(free_op1)

        happy_execute.FREE_OP_IF_VAR(free_op2)

        execute_data.opline += 1
        return 0

    def ZEND_VM_HANDLER_ASSIGN_REF(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]
        free_op1 = zend_free_op.make_new()
        free_op2 = zend_free_op.make_new()
        value_ptr_ptr = happy_execute.GET_ZVAL_PTR_PTR(opline.get_op2(), BP_VAR_W, free_op2, execute_data)

        if opline.get_op2().get_op_type() == rad.APCFile.ZEND_znode.IS_VAR and \
            not value_ptr_ptr.is_null() and \
            not zval_utils.Z_ISREF_PP(value_ptr_ptr) and \
            opline.get_extended_value() == ZEND_RETURNS_FUNCTION:
            raise Exception('Not implemented yet')
        elif opline.get_op2().get_op_type() == rad.APCFile.ZEND_znode.IS_VAR and \
            opline.get_extended_value() == ZEND_RETURNS_NEW:
            raise Exception('Not implemented yet')
        if opline.get_op1().get_op_type() == rad.APCFile.ZEND_znode.IS_VAR and \
            execute_data.Ts[opline.get_op1().var / ZendExecutor.TEMP_SZ].get_ptr_ptr() == \
            execute_data.Ts[opline.get_op1().var / ZendExecutor.TEMP_SZ].address_of_ptr():
            raise Exception('Not implemented yet')

        variable_ptr_ptr = happy_execute.GET_ZVAL_PTR_PTR(opline.get_op1(), BP_VAR_W, free_op1, execute_data)
        if (opline.get_op2().get_op_type() == rad.APCFile.ZEND_znode.IS_VAR and value_ptr_ptr.is_null()) or \
            (opline.get_op1().get_op_type() == rad.APCFile.ZEND_znode.IS_VAR and variable_ptr_ptr.is_null()):
            raise Exception('Not implemented yet')
        happy_execute.zend_assign_to_variable_reference(variable_ptr_ptr, value_ptr_ptr)

        if opline.get_op2().get_op_type() == rad.APCFile.ZEND_znode.IS_VAR and \
            opline.get_extended_value() == ZEND_RETURNS_NEW:
            raise Exception('Not implemented yet')

        if not happy_execute.RETURN_VALUE_UNUSED(opline.get_result()):
            raise Exception('Not implemented yet')

        happy_execute.FREE_OP_VAR_PTR(free_op1)
        happy_execute.FREE_OP_VAR_PTR(free_op2)

        execute_data.opline += 1
        return 0

    def ZEND_VM_HANDLER_JMP(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]
        execute_data.opline = opline.get_op1().jmp_addr
        return 0

    def ZEND_VM_HANDLER_JMPZ(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]
        free_op1 = zend_free_op.make_new()
        val = happy_execute.GET_ZVAL_PTR(opline.get_op1(), BP_VAR_R, free_op1, execute_data)

        if opline.get_op1().get_op_type() == rad.APCFile.ZEND_znode.IS_TMP_VAR and zval_utils.Z_TYPE_P(val) == IS_BOOL:
            ret = zval_utils.Z_LVAL_P(val)
        else:
            ret = happy_execute.i_happy_is_true(val)
            happy_execute.FREE_OP_NODE(opline.get_op1(), free_op1)
            if gs.EG.exception:
                return 0
        if not ret:
            execute_data.opline = opline.get_op2().jmp_addr
            return 0

        execute_data.opline += 1
        return 0

    def ZEND_VM_HANDLER_JMPNZ(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]
        free_op1 = zend_free_op.make_new()
        val = happy_execute.GET_ZVAL_PTR(opline.get_op1(), BP_VAR_R, free_op1, execute_data)

        if opline.get_op1().get_op_type() == rad.APCFile.ZEND_znode.IS_TMP_VAR and zval_utils.Z_TYPE_P(val) == IS_BOOL:
            ret = zval_utils.Z_LVAL_P(val)
        else:
            ret = happy_execute.i_happy_is_true(val)
            happy_execute.FREE_OP_NODE(opline.get_op1(), free_op1)
            if gs.EG.exception:
                return 0
        if ret:
            execute_data.opline = opline.get_op2().jmp_addr
            return 0

        execute_data.opline += 1
        return 0

    def ZEND_VM_HANDLER_JMPZNZ(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]
        free_op1 = zend_free_op.make_new()
        val = happy_execute.GET_ZVAL_PTR(opline.get_op1(), BP_VAR_R, free_op1, execute_data)
        retval = 0

        if opline.get_op1().get_op_type() == rad.APCFile.ZEND_znode.IS_TMP_VAR and \
           zval_utils.Z_TYPE_P(val) == IS_BOOL:
            retval = zval_utils.Z_LVAL_P(val)
        else:
            retval = happy_execute.i_happy_is_true(val)
            happy_execute.FREE_OP_NODE(opline.get_op1(), free_op1)
            if gs.EG.exception:
                raise Exception('Not implemented yet')
        if retval:
            execute_data.opline = opline.get_extended_value()
            return 0
        else:
            execute_data.opline = opline.get_op2().opline_num
            return 0

    def ZEND_VM_HANDLER_JMPZ_EX(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]
        free_op1 = zend_free_op.make_new()
        val = happy_execute.GET_ZVAL_PTR(opline.get_op1(), BP_VAR_R, free_op1, execute_data)
        retval = 0

        if opline.get_op1().get_op_type() == rad.APCFile.ZEND_znode.IS_TMP_VAR and \
           zval_utils.Z_TYPE_P(val) == IS_BOOL:
            retval = zval_utils.Z_LVAL_P(val)
        else:
            retval = happy_execute.i_happy_is_true(val)
            happy_execute.FREE_OP_NODE(opline.get_op1(), free_op1)
            if gs.EG.exception:
                raise Exception('Not implemented yet')
        zval_utils.Z_SET_TYPE_P(execute_data.Ts[opline.get_result().var / ZendExecutor.TEMP_SZ].tmp_var_ptr(), IS_BOOL)
        zval_utils.Z_SET_LVAL_P(execute_data.Ts[opline.get_result().var / ZendExecutor.TEMP_SZ].tmp_var_ptr(),
            retval)
        if not retval:
            execute_data.opline = opline.get_op2().jmp_addr
            return 0
        execute_data.opline += 1
        return 0

    def ZEND_VM_HANDLER_JMPNZ_EX(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]
        free_op1 = zend_free_op.make_new()
        val = happy_execute.GET_ZVAL_PTR(opline.get_op1(), BP_VAR_R, free_op1, execute_data)
        retval = 0

        if opline.get_op1().get_op_type() == rad.APCFile.ZEND_znode.IS_TMP_VAR and \
           zval_utils.Z_TYPE_P(val) == IS_BOOL:
            retval = zval_utils.Z_LVAL_P(val)
        else:
            retval = happy_execute.i_happy_is_true(val)
            happy_execute.FREE_OP_NODE(opline.get_op1(), free_op1)
            if gs.EG.exception:
                raise Exception('Not implemented yet')
        zval_utils.Z_SET_TYPE_P(execute_data.Ts[opline.get_result().var / ZendExecutor.TEMP_SZ].tmp_var_ptr(), IS_BOOL)
        zval_utils.Z_SET_LVAL_P(execute_data.Ts[opline.get_result().var / ZendExecutor.TEMP_SZ].tmp_var_ptr(),
            retval)
        if retval:
            execute_data.opline = opline.get_op2().jmp_addr
            return 0
        execute_data.opline += 1
        return 0

    def ZEND_VM_HANDLER_FREE(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]
        zval_utils.i_zval_dtor(
            execute_data.Ts[opline.get_op1().var / ZendExecutor.TEMP_SZ].tmp_var_ptr().deref())
        execute_data.opline += 1
        return 0

    def ZEND_VM_HANDLER_ADD_CHAR(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]
        str = execute_data.Ts[opline.get_result().var / ZendExecutor.TEMP_SZ].tmp_var_ptr()

        if opline.get_op1().get_op_type() == rad.APCFile.ZEND_znode.IS_UNUSED:
            zval_utils.Z_SET_STRVAL_P(str, None)
            zval_utils.Z_SET_TYPE_P(str, IS_STRING)

            zval_utils.INIT_PZVAL(str)

        happy_operators.add_char_to_string(str, str, zval_utils.zp_stack(opline.get_op2().constant))

        execute_data.opline += 1
        return 0

    def ZEND_VM_HANDLER_ADD_STRING(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]
        str = execute_data.Ts[opline.get_result().var / ZendExecutor.TEMP_SZ].tmp_var_ptr()

        if opline.get_op1().get_op_type() == rad.APCFile.ZEND_znode.IS_UNUSED:
            zval_utils.Z_SET_STRVAL_P(str, None)
            zval_utils.Z_SET_TYPE_P(str, IS_STRING)

            zval_utils.INIT_PZVAL(str)

        happy_operators.add_string_to_string(str, str, zval_utils.zp_stack(opline.get_op2().constant))

        # FREE_OP is missing intentionally here - we're always working on the same temporary variable
        execute_data.opline += 1
        return 0

    def ZEND_VM_HANDLER_ADD_VAR(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]
        free_op2 = zend_free_op.make_new()
        str = execute_data.Ts[opline.get_result().var / ZendExecutor.TEMP_SZ].tmp_var_ptr()
        var = happy_execute.GET_ZVAL_PTR(opline.get_op2(), BP_VAR_R, free_op2, execute_data)
        var_copy_ptr = zval_utils.zp_stack(zval_utils.make_empty_zval())
        use_copy = False

        if opline.get_op1().get_op_type() == rad.APCFile.ZEND_znode.IS_UNUSED:
            zval_utils.Z_SET_STRVAL_P(str, None)
            zval_utils.Z_SET_TYPE_P(str, IS_STRING)

            zval_utils.INIT_PZVAL(str)

        if zval_utils.Z_TYPE_P(var) != IS_STRING:
            use_copy = happy_util.zend_make_printable_zval(var, var_copy_ptr)

            if use_copy:
                var = var_copy_ptr
        happy_operators.add_string_to_string(str, str, var)

        if use_copy:
            zval_utils.zval_dtor(var)
        happy_execute.FREE_OP_NODE(opline.get_op2(), free_op2)

        execute_data.opline += 1
        return 0

    def ZEND_VM_HANDLER_FETCH_CLASS(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]


        if opline.get_op2().get_op_type() == rad.APCFile.ZEND_znode.IS_UNUSED:
            execute_data.Ts[opline.get_result().var / ZendExecutor.TEMP_SZ].class_entry = \
                happy_execute.zend_fetch_class(None, 0, opline.get_extended_value())
            execute_data.opline += 1
            return 0
        else:
            free_op2 = zend_free_op.make_new()
            class_name = happy_execute.GET_ZVAL_PTR(opline.get_op2(), BP_VAR_R, free_op2, execute_data)

            if opline.get_op2().get_op_type() == rad.APCFile.ZEND_znode.IS_CONST and \
                zval_utils.Z_TYPE_P(class_name) == IS_OBJECT:
                raise Exception('Not implemented yet')
            elif zval_utils.Z_TYPE_P(class_name) == IS_STRING:
                execute_data.Ts[opline.get_result().var / ZendExecutor.TEMP_SZ].class_entry = happy_execute.zend_fetch_class(
                    zval_utils.Z_STRVAL_P(class_name), zval_utils.Z_STRLEN_P(class_name), opline.get_extended_value())
            else:
                raise Exception('Not implemented yet')

            happy_execute.FREE_OP_NODE(opline.get_op2(), free_op2)
            execute_data.opline += 1
            return 0

    def ZEND_VM_HANDLER_INIT_METHOD_CALL(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]
        free_op1 = zend_free_op.make_new()
        free_op2 = zend_free_op.make_new()

        happy_execute.zend_ptr_stack_3_push(gs.EG.arg_types_stack, execute_data.fbc, execute_data.object,
            execute_data.called_scope)

        function_name = happy_execute.GET_ZVAL_PTR(opline.get_op2(), BP_VAR_R, free_op2, execute_data)

        if zval_utils.Z_TYPE_P(function_name) != IS_STRING:
            raise Exception('Not implemented yet')

        function_name_strval = zval_utils.Z_STRVAL_P(function_name)
        function_name_strlen = zval_utils.Z_STRLEN_P(function_name)

        execute_data.object = happy_execute.GET_OBJ_ZVAL_PTR(opline.get_op1(), BP_VAR_R, free_op1, execute_data)

        if not execute_data.object.is_null() and \
            zval_utils.Z_TYPE_P(execute_data.object) == IS_OBJECT:
            if not zval_utils.Z_OBJ_HT_P(execute_data.object).get_method:
                raise Exception('Not implemented yet')

            execute_data.fbc = zval_utils.Z_OBJ_HT_P(execute_data.object).get_method(
                zval_utils.zpp_stack(execute_data.object), function_name_strval, function_name_strlen)
            if happy_util.is_null_struct(execute_data.fbc):
                raise Exception('Not implemented yet')

            execute_data.called_scope = zval_utils.Z_OBJCE_P(execute_data.object)
        else:
            raise Exception('Not implemented yet')

        if execute_data.fbc.fn_flags & ZEND_ACC_STATIC:
            raise Exception('Not implemented yet')
        else:
            if not zval_utils.PZVAL_IS_REF(execute_data.object):
                zval_utils.Z_ADDREF_P(execute_data.object)
            else:
                raise Exception('Not implemented yet')

        happy_execute.FREE_OP_NODE(opline.get_op2(), free_op2)
        happy_execute.FREE_OP_IF_VAR(free_op1)

        execute_data.opline += 1
        return 0

    def ZEND_VM_HANDLER_INIT_STATIC_METHOD_CALL(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]

        happy_execute.zend_ptr_stack_3_push(gs.EG.arg_types_stack, execute_data.fbc, execute_data.object,
            execute_data.called_scope)

        if opline.get_op1().get_op_type() == rad.APCFile.ZEND_znode.IS_CONST:
            ce = happy_execute.zend_fetch_class(zval_utils.Z_STRVAL(opline.get_op1().get_constant()),
                zval_utils.Z_STRLEN(opline.get_op1().get_constant()), opline.get_extended_value())
            if happy_util.is_null_struct(ce):
                raise Exception('Not implemented yet')
            execute_data.called_scope = ce
        else:
            ce = execute_data.Ts[opline.get_op1().var / ZendExecutor.TEMP_SZ].class_entry

            if opline.get_op1().EA_type == ZEND_FETCH_CLASS_PARENT or opline.get_op1().EA_type == ZEND_FETCH_CLASS_SELF:
                execute_data.called_scope = gs.EG.called_scope
            else:
                raise Exception('Not implemented yet')
        if opline.get_op2().get_op_type() != rad.APCFile.ZEND_znode.IS_UNUSED:
            function_name_strval = None
            function_name_strlen = 0
            free_op2 = zend_free_op.make_new()

            if opline.get_op2().get_op_type() == rad.APCFile.ZEND_znode.IS_CONST:
                function_name_strval = zval_utils.Z_STRVAL(opline.get_op2().get_constant())
                function_name_strlen = zval_utils.Z_STRLEN(opline.get_op2().get_constant())
            else:
                raise Exception('Not implemented yet')

            if function_name_strval:
                if not happy_util.is_null_struct(ce.get_static_method):
                    raise Exception('Not implemented yet')
                else:
                    execute_data.fbc = zend_object_handlers.zend_std_get_static_method(ce, function_name_strval,
                        function_name_strlen)
                if happy_util.is_null_struct(execute_data.fbc):
                    raise Exception('Not implemented yet')

            if opline.get_op2().get_op_type() != rad.APCFile.ZEND_znode.IS_CONST:
                happy_execute.FREE_OP_NODE(opline.get_op2(), free_op2)
        else:
            if happy_util.is_null_struct(ce.constructor):
                raise Exception('Not implemented yet')
            if (not gs.EG.This().is_null() and zval_utils.Z_OBJCE_P(gs.EG.This()) != ce.constructor.op_array.scope
                and ce.constructor.op_array.fn_flags & ZEND_ACC_PRIVATE):
                raise Exception('Not implemented yet')
            execute_data.fbc = ce.constructor.op_array

        if execute_data.fbc.fn_flags & ZEND_ACC_STATIC:
            execute_data.object = gs.null_zval_ptr
        else:
            # TODO: do the PHP4 compatibility hack
            execute_data.object = gs.EG.This()
            if not execute_data.object.is_null():
                zval_utils.Z_ADDREF_P(execute_data.object)
                execute_data.called_scope = zval_utils.Z_OBJCE_P(execute_data.object)

        execute_data.opline += 1
        return 0

    def ZEND_VM_HANDLER_INIT_FCALL_BY_NAME(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]

        happy_execute.zend_ptr_stack_3_push(gs.EG.arg_types_stack, execute_data.fbc, execute_data.object,
            execute_data.called_scope)

        if opline.get_op2().get_op_type() == rad.APCFile.ZEND_znode.IS_CONST:
            fname = objects.MutableString(zval_utils.Z_STRVAL(opline.get_op2().constant).to_str().lower())
            if fname in gs.EG.function_table:
                execute_data.fbc = gs.EG.function_table[fname]
            else:
                raise Exception('Not implemented yet')
        else:
            raise Exception('Not implemented yet')

        self.object = gs.null_zval_ptr
        execute_data.opline += 1
        return 0

    def ZEND_VM_HANDLER_DO_FCALL(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]
        free_op1 = zend_free_op.make_new()
        fname = happy_execute.GET_ZVAL_PTR(opline.get_op1(), BP_VAR_R, free_op1, execute_data)

        happy_execute.zend_ptr_stack_3_push(gs.EG.arg_types_stack, execute_data.fbc, execute_data.object,
            execute_data.called_scope)

        fname_str = zval_utils.Z_STRVAL_P(fname)

        if not fname_str in gs.EG.function_table:
            raise Exception('Not implemented yet')
        else:
            execute_data.function_state = zend_function_state(gs.EG.function_table[fname_str])
        execute_data.object = gs.null_zval_ptr

        happy_execute.FREE_OP_NODE(opline.get_op1(), free_op1)

        return self.zend_do_fcall_common_helper(execute_data)

    def ZEND_VM_HANDLER_DO_FCALL_BY_NAME(self, execute_data):
        execute_data.function_state.function = execute_data.fbc
        return self.zend_do_fcall_common_helper(execute_data)

    def ZEND_VM_HANDLER_RETURN(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]
        free_op1 = zend_free_op.make_new()

        if gs.EG.active_op_array.return_reference == ZEND_RETURN_REF:
            raise Exception('Not implemented yet')
        else:
            retval_ptr = happy_execute.GET_ZVAL_PTR(opline.get_op1(), BP_VAR_R, free_op1, execute_data)

            if gs.EG.return_value_ptr_ptr.is_null():
                if opline.get_op1().get_op_type() == rad.APCFile.ZEND_znode.IS_TMP_VAR:
                    raise Exception('Not implemented yet')
            elif not happy_execute.IS_OP_TMP_FREE(opline.get_op1()):
                if opline.get_op1().get_op_type() == rad.APCFile.ZEND_znode.IS_CONST or \
                    gs.EG.active_op_array.return_reference == ZEND_RETURN_REF or \
                    (zval_utils.PZVAL_IS_REF(retval_ptr) and zval_utils.Z_REFCOUNT_P(retval_ptr) > 0):
                    ret = zval_utils.zp_stack(zval_utils.make_empty_zval())
                    happy_execute.INIT_PZVAL_COPY(ret, retval_ptr)
                    happy_variables.zval_copy_ctor(ret)
                    gs.EG.return_value_ptr_ptr.assign(ret)
                elif (opline.get_op1().get_op_type() == rad.APCFile.ZEND_znode.IS_CV or \
                    opline.get_op1().get_op_type() == rad.APCFile.ZEND_znode.IS_VAR) and \
                    retval_ptr == gs.EG.uninitialized_zval_ptr():
                    raise Exception('Not implemented yet')
                else:
                    # TODO: check
                    gs.EG.return_value_ptr_ptr.assign(retval_ptr)
                    zval_utils.Z_ADDREF_P(retval_ptr)
            else:
                ret = zval_utils.zp_stack(zval_utils.make_empty_zval())
                happy_execute.INIT_PZVAL_COPY(ret, retval_ptr)
                gs.EG.return_value_ptr_ptr.assign(ret)
        happy_execute.FREE_OP_IF_VAR(free_op1)
        return self.zend_leave_helper(execute_data)

    def ZEND_VM_HANDLER_RECV(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]
        arg_num = zval_utils.Z_LVAL(opline.get_op1().get_constant())
        param = happy_execute.zend_vm_stack_get_arg(arg_num)

        if param.is_null():
            raise Exception('Not implemented yet')
        else:
            free_res = zend_free_op.make_new()

            self.zend_verify_arg_type(gs.EG.active_op_array, arg_num, param, opline.get_extended_value())
            var_ptr = happy_execute.get_zval_ptr_ptr(opline.get_result(), execute_data.Ts, free_res, BP_VAR_W)
            zval_utils.Z_DELREF_PP(var_ptr)
            var_ptr.assign(param)
            zval_utils.Z_ADDREF_PP(var_ptr)

        execute_data.opline += 1
        return 0

    def ZEND_VM_HANDLER_BOOL(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]
        free_op1 = zend_free_op.make_new()

        zval_utils.Z_SET_TYPE_P(execute_data.Ts[opline.get_result().var / ZendExecutor.TEMP_SZ].tmp_var_ptr(), IS_BOOL)
        zval_utils.Z_SET_LVAL_P(execute_data.Ts[opline.get_result().var / ZendExecutor.TEMP_SZ].tmp_var_ptr(),
            happy_execute.i_happy_is_true(happy_execute.GET_ZVAL_PTR(opline.get_op1(), BP_VAR_R, free_op1,
                execute_data)))
        happy_execute.FREE_OP_NODE(opline.get_op1(), free_op1)

        execute_data.opline += 1
        return 0

    def ZEND_VM_HANDLER_BRK(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]
        free_op2 = zend_free_op.make_new()

        el = happy_execute.zend_brk_cont(
            happy_execute.GET_ZVAL_PTR(opline.get_op2(), BP_VAR_R, free_op2, execute_data),
            opline.get_op1().opline_num, execute_data.op_array, execute_data.Ts)

        happy_execute.FREE_OP_NODE(opline.get_op2(), free_op2)
        execute_data.opline = el.brk.int_value
        return 0

    def ZEND_VM_HANDLER_CONT(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]
        free_op2 = zend_free_op.make_new()

        el = happy_execute.zend_brk_cont(
            happy_execute.GET_ZVAL_PTR(opline.get_op2(), BP_VAR_R, free_op2, execute_data),
            opline.get_op1().opline_num, execute_data.op_array, execute_data.Ts)

        happy_execute.FREE_OP_NODE(opline.get_op2(), free_op2)
        execute_data.opline = el.cont.int_value
        return 0

    def ZEND_VM_HANDLER_NEW(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]

        if (execute_data.Ts[opline.get_op1().var / ZendExecutor.TEMP_SZ].class_entry.ce_flags &
            (ZEND_ACC_INTERFACE |
            ZEND_ACC_IMPLICIT_ABSTRACT_CLASS |
            ZEND_ACC_EXPLICIT_ABSTRACT_CLASS)):
            raise Exception('Not implemented yet')
        object_zval = zval_utils.zp_stack(zval_utils.make_empty_zval())
        zend_API.object_init_ex(object_zval, execute_data.Ts[opline.get_op1().var / ZendExecutor.TEMP_SZ].class_entry)
        zval_utils.INIT_PZVAL(object_zval)

        constructor = zval_utils.Z_OBJ_HT_P(object_zval).get_constructor(object_zval)

        if happy_util.is_null_struct(constructor):
            if happy_execute.RETURN_VALUE_USED(opline):
                zval_utils.AI_SET_PTR(execute_data.Ts[opline.get_result().var / ZendExecutor.TEMP_SZ], object_zval)
            else:
                raise Exception('Not implemented yet')
            if not gs.EG.exception:
                execute_data.opline = opline.get_op2().opline_num
            return 0
        else:
            if happy_execute.RETURN_VALUE_USED(opline):
                zval_utils.AI_SET_PTR(execute_data.Ts[opline.get_result().var / ZendExecutor.TEMP_SZ], object_zval)
                zval_utils.PZVAL_LOCK(object_zval)

            # TODO: encode ctor
            happy_execute.zend_ptr_stack_3_push(gs.EG.arg_types_stack, execute_data.fbc, execute_data.object,
                execute_data.called_scope)

            execute_data.object = object_zval
            execute_data.fbc = constructor.op_array
            execute_data.called_scope = execute_data.Ts[opline.get_op1().var / ZendExecutor.TEMP_SZ].class_entry

            execute_data.opline += 1
            return 0

    def ZEND_VM_HANDLER_CASE(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]
        switch_expr_is_overloaded = False
        free_op1 = zend_free_op.make_new()
        free_op2 = zend_free_op.make_new()

        if opline.get_op1().get_op_type() == rad.APCFile.ZEND_znode.IS_VAR:
            raise Exception('Not implemented yet')
        happy_operators.is_equal_function(
            execute_data.Ts[opline.get_result().var / ZendExecutor.TEMP_SZ].tmp_var_ptr(),
            happy_execute.GET_ZVAL_PTR(opline.get_op1(), BP_VAR_R, free_op1, execute_data),
            happy_execute.GET_ZVAL_PTR(opline.get_op2(), BP_VAR_R, free_op2, execute_data))

        happy_execute.FREE_OP_NODE(opline.get_op2(), free_op2)
        if switch_expr_is_overloaded:
            happy_execute.FREE_OP_NODE(opline.get_op1(), free_op1)
            execute_data.Ts[opline.get_op1().var / ZendExecutor.TEMP_SZ].set_ptr_ptr(gs.null_zval_ptr_ptr)
            execute_data.Ts[opline.get_op1().var / ZendExecutor.TEMP_SZ].set_ptr(gs.null_zval_ptr)
        execute_data.opline += 1
        return 0

    def ZEND_VM_HANDLER_SWITCH_FREE(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]

        happy_execute.zend_switch_free(execute_data.Ts[opline.get_op1().var / ZendExecutor.TEMP_SZ],
            opline.get_extended_value())
        execute_data.opline += 1
        return 0

    def ZEND_VM_HANDLER_SEND_VAL(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]
        free_op1 = zend_free_op.make_new()

        if opline.get_extended_value() == ops.ZEND_DO_FCALL_BY_NAME and \
            ZendExecutor.ARG_MUST_BE_SENT_BY_REF(execute_data.fbc, execute_data.opline):
            raise Exception('Not implemented yet')

        value = happy_execute.GET_ZVAL_PTR(opline.get_op1(), BP_VAR_R, free_op1, execute_data)

        valptr = zval_utils.zp_stack(zval_utils.make_empty_zval())
        happy_execute.INIT_PZVAL_COPY(valptr, value)
        if not happy_execute.IS_OP_TMP_FREE(opline.get_op1()):
            happy_variables.zval_copy_ctor(valptr)
        happy_execute.zend_vm_stack_push(valptr)
        happy_execute.FREE_OP_IF_VAR(free_op1)
        execute_data.opline += 1
        return 0

    def ZEND_VM_HANDLER_SEND_VAR_NO_REF(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]
        free_op1 = zend_free_op.make_new()

        extended_value = intmask(opline.get_extended_value())
        if extended_value & ZEND_ARG_COMPILE_TIME_BOUND:
            if not extended_value & ZEND_ARG_SEND_BY_REF:
                return self.zend_send_by_var_helper(execute_data)
        elif not self.ARG_SHOULD_BE_SENT_BY_REF(execute_data.fbc, opline.get_op2().opline_num):
            return self.zend_send_by_var_helper(execute_data)

        if opline.get_op1().get_op_type() == rad.APCFile.ZEND_znode.IS_VAR and \
            extended_value & ZEND_ARG_SEND_FUNCTION and \
            execute_data.Ts[opline.get_op1().var].fcall_returned_reference and \
            execute_data.Ts[opline.get_op1().var].get_ptr():
            raise Exception('Not implemented yet')
        else:
            varptr = happy_execute.GET_ZVAL_PTR(opline.get_op1(), BP_VAR_R, free_op1, execute_data)

        if ((not (extended_value & ZEND_ARG_SEND_FUNCTION)) or
            execute_data.Ts[opline.get_op1().var].fcall_returned_reference) and \
            varptr != gs.EG.uninitialized_zval_ptr() and \
            (zval_utils.PZVAL_IS_REF(varptr) or
                (zval_utils.Z_REFCOUNT_P(varptr) == 1 and
                (opline.get_op1().get_op_type() == rad.APCFile.ZEND_znode.IS_CV or not free_op1.var().is_null()))):
            raise Exception('Not implemented yet')
        else:
            if extended_value & ZEND_ARG_COMPILE_TIME_BOUND:
                if not (extended_value & ZEND_ARG_SEND_SILENT):
                    # TODO: raise error for E_STRICT mode
                    pass
            elif not ZendExecutor.ARG_MAY_BE_SENT_BY_REF(execute_data.fbc, opline.get_op2().opline_num):
                raise Exception('Not implemented yet')
            valptr = zval_utils.zp_stack(zval_utils.make_empty_zval())
            happy_execute.INIT_PZVAL_COPY(valptr, varptr)
            if not happy_execute.IS_OP_TMP_FREE(opline.get_op1()):
                happy_variables.zval_copy_ctor(valptr)
            happy_execute.zend_vm_stack_push(valptr)
        happy_execute.FREE_OP_IF_VAR(free_op1)
        execute_data.opline += 1
        return 0

    def ZEND_VM_HANDLER_SEND_REF(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]
        free_op1 = zend_free_op.make_new()
        varptr_ptr = happy_execute.GET_ZVAL_PTR_PTR(opline.get_op1(), BP_VAR_W, free_op1, execute_data)
        assert isinstance(varptr_ptr, objects.zval_ptr_ptr)

        if opline.get_op1().get_op_type() == rad.APCFile.ZEND_znode.IS_VAR and varptr_ptr.is_null():
            raise Exception('Not implemented yet')

        if opline.get_op1().get_op_type() == rad.APCFile.ZEND_znode.IS_VAR and \
            varptr_ptr.deref() == gs.EG.error_zval_ptr:
            raise Exception('Not implemented yet')

        if execute_data.function_state.function.type == rad.APCFile.ZEND_function.ZEND_INTERNAL_FUNCTION:
            raise Exception('Not implemented yet')

        zval_utils.SEPARATE_ZVAL_TO_MAKE_IS_REF(varptr_ptr)
        varptr = varptr_ptr.deref()
        zval_utils.Z_ADDREF_P(varptr)
        happy_execute.zend_vm_stack_push(varptr)

        happy_execute.FREE_OP_VAR_PTR(free_op1)
        execute_data.opline += 1
        return 0

    def ZEND_VM_HANDLER_SEND_VAR(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]

        if opline.get_extended_value() == ops.ZEND_DO_FCALL_BY_NAME and \
           self.ARG_SHOULD_BE_SENT_BY_REF(execute_data.fbc, opline.get_op2().opline_num):
            raise Exception('Not implemented yet')
        return self.zend_send_by_var_helper(execute_data)

    def ZEND_VM_HANDLER_INIT_ARRAY(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]

        happy_util.array_init(execute_data.Ts[opline.get_result().var / ZendExecutor.TEMP_SZ].tmp_var_ptr())
        if opline.get_op1().get_op_type() == rad.APCFile.ZEND_znode.IS_UNUSED:
            execute_data.opline += 1
            return 0
        else:
            return self.ZEND_VM_ADD_ARRAY_ELEMENT(execute_data)

    def ZEND_VM_HANDLER_CAST(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]
        free_op1 = zend_free_op.make_new()
        expr = happy_execute.GET_ZVAL_PTR(opline.get_op1(), BP_VAR_R, free_op1, execute_data)
        result = execute_data.Ts[opline.get_result().var / ZendExecutor.TEMP_SZ].tmp_var_ptr()

        if opline.get_extended_value() != IS_STRING:
            raise Exception('Not implemented yet')
        if opline.get_extended_value() == IS_STRING:
            var_copy_ptr = zval_utils.zp_stack(zval_utils.make_empty_zval())
            use_copy = happy_util.zend_make_printable_zval(expr, var_copy_ptr)
            if use_copy:
                raise Exception('Not implemented yet')
            else:
                result.assign(zval_utils.zval_copy(expr.deref()))
                if not happy_execute.IS_OP_TMP_FREE(opline.get_op1()):
                    zval_utils.i_zval_copy_ctor(result)
        else:
            raise Exception('Not implemented yet')
        happy_execute.FREE_OP_IF_VAR(free_op1)
        execute_data.opline += 1
        return 0

    def ZEND_VM_HANDLER_FE_RESET(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]
        free_op1 = zend_free_op.make_new()
        iter = None
        ce = None
        is_empty = 0

        if opline.get_extended_value() & ZEND_FE_RESET_VARIABLE:
            array_ptr_ptr = happy_execute.GET_ZVAL_PTR_PTR(opline.get_op1(), BP_VAR_R, free_op1, execute_data)
            if array_ptr_ptr.is_null() or array_ptr_ptr == gs.EG.uninitialized_zval_ptr_ptr():
                raise Exception('Not implemented yet')
            elif zval_utils.Z_TYPE_PP(array_ptr_ptr) == IS_OBJECT:
                raise Exception('Not implemented yet')
            else:
                if zval_utils.Z_TYPE_PP(array_ptr_ptr) == IS_ARRAY:
                    zval_utils.SEPARATE_ZVAL_IF_NOT_REF(array_ptr_ptr)
                    if opline.get_extended_value() & ZEND_FE_FETCH_BYREF:
                        zval_utils.Z_SET_ISREF_PP(array_ptr_ptr)
                    else:
                        raise Exception('Not implemented yet')
                assert isinstance(array_ptr_ptr, objects.zval_ptr_ptr)
                array_ptr = array_ptr_ptr.deref()
                zval_utils.Z_ADDREF_P(array_ptr)
        else:
            array_ptr = happy_execute.GET_ZVAL_PTR(opline.get_op1(), BP_VAR_R, free_op1, execute_data)
            OP1_type = opline.get_op1().get_op_type()
            if happy_execute.IS_OP_TMP_FREE(opline.get_op1()):
                tmp = zval_utils.zp_stack(zval_utils.make_empty_zval())
                happy_execute.INIT_PZVAL_COPY(tmp, array_ptr)
                array_ptr = tmp
                if zval_utils.Z_TYPE_P(array_ptr) == IS_OBJECT:
                    raise Exception('Not implemented yet')
            elif zval_utils.Z_TYPE_P(array_ptr) == IS_OBJECT:
                raise Exception('Not implemented yet')
            elif (OP1_type == rad.APCFile.ZEND_znode.IS_CONST or
                ((OP1_type == rad.APCFile.ZEND_znode.IS_CV or OP1_type == rad.APCFile.ZEND_znode.IS_VAR) and
                not zval_utils.Z_ISREF_P(array_ptr) and
                zval_utils.Z_REFCOUNT_P(array_ptr) > 1)):
                tmp = zval_utils.zp_stack(zval_utils.make_empty_zval())
                happy_execute.INIT_PZVAL_COPY(tmp, array_ptr)
                happy_variables.zval_copy_ctor(tmp)
                array_ptr = tmp
            else:
                zval_utils.Z_ADDREF_P(array_ptr)

        if ce:
            raise Exception('Not implemented yet')

        zval_utils.AI_SET_PTR(execute_data.Ts[opline.get_result().var / ZendExecutor.TEMP_SZ], array_ptr)
        zval_utils.PZVAL_LOCK(array_ptr)

        if iter:
            raise Exception('Not implemented yet')
        else:
            fe_ht = zend_API.HASH_OF(array_ptr)
            if fe_ht:
                happy_hash.zend_hash_internal_pointer_reset(fe_ht)
                if ce:
                    raise Exception('Not implemented yet')
                is_empty = 1 if happy_hash.zend_hash_has_more_elements(fe_ht) == FAILURE else 0
                happy_hash.zend_hash_get_pointer(fe_ht,
                    execute_data.Ts[opline.get_result().var / ZendExecutor.TEMP_SZ].fe_pos)
            else:
                is_empty = 1
                zend_error(E_WARNING, 'Invalid argument supplied for foreach()')

        if opline.get_extended_value() & ZEND_FE_RESET_VARIABLE:
            happy_execute.FREE_OP_VAR_PTR(free_op1)
        else:
            happy_execute.FREE_OP_IF_VAR(free_op1)
        if is_empty:
            if not gs.EG.exception:
                execute_data.opline = opline.get_op2().opline_num
            return 0
        else:
            execute_data.opline += 1
            return 0

    def ZEND_VM_HANDLER_FE_FETCH(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]
        array = execute_data.Ts[opline.get_op1().var / ZendExecutor.TEMP_SZ].get_ptr()
        value_ptr = zval_utils.zppp_stack(gs.null_zval_ptr_ptr)
        int_key_ptr = objects.ulong_ptr(0, False)
        fe_ht = None
        key_type = 0
        use_key = opline.get_extended_value() & ZEND_FE_FETCH_WITH_KEY

        if happy_iterators.iterator_unwrap(array) == happy_iterators.object_iterator_kind.ITER_PLAIN_ARRAY:
            fe_ht = zend_API.HASH_OF(array)
            happy_hash.zend_hash_set_pointer(fe_ht, execute_data.Ts[opline.get_op1().var / ZendExecutor.TEMP_SZ].fe_pos)
            if happy_hash.zend_hash_get_current_data(fe_ht, value_ptr) == FAILURE:
                if not gs.EG.exception:
                    execute_data.opline = opline.get_op2().opline_num
                return 0
            if use_key:
                key_type = happy_hash.zend_hash_get_current_key_ex(fe_ht, None, None, int_key_ptr, 0, None)
            happy_hash.zend_hash_move_forward(fe_ht)
            happy_hash.zend_hash_get_pointer(fe_ht, execute_data.Ts[opline.get_op1().var / ZendExecutor.TEMP_SZ].fe_pos)
        else:
            raise Exception('Not implemented yet')

        if opline.get_extended_value() & ZEND_FE_FETCH_BYREF:
            zval_utils.SEPARATE_ZVAL_IF_NOT_REF(value_ptr.deref())
            zval_utils.Z_SET_ISREF_PP(value_ptr.deref())
            execute_data.Ts[opline.get_result().var / ZendExecutor.TEMP_SZ].set_ptr_ptr(value_ptr.deref())
            zval_utils.Z_ADDREF_PP(value_ptr.deref())
        else:
            zval_utils.AI_SET_PTR(execute_data.Ts[opline.get_result().var / ZendExecutor.TEMP_SZ],
                value_ptr.deref().deref())
            zval_utils.PZVAL_LOCK(value_ptr.deref().deref())

        if use_key:
            op_data = execute_data.op_array.opcodes[execute_data.opline + 1]
            key = execute_data.Ts[op_data.get_result().var / ZendExecutor.TEMP_SZ].tmp_var_ptr()

            if key_type == happy_hash.HASH_KEY_IS_LONG:
                zval_utils.Z_SET_TYPE_P(key, IS_LONG)
                zval_utils.Z_SET_LVAL_P(key, int_key_ptr.deref())
            else:
                raise Exception('Not implemented yet')

        execute_data.opline += 2
        return 0

    def ZEND_VM_HANDLER_ISSET_ISEMPTY_VAR(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]
        isset = 1

        if (opline.get_op1().get_op_type() == rad.APCFile.ZEND_znode.IS_CV and
            (opline.get_extended_value() & ZEND_QUICK_SET)):
            value_ptr = zval_utils.zppp_stack(execute_data.CVs.get(opline.get_op1().var))
            if not value_ptr.deref().is_null():
                pass
            elif gs.EG.active_symbol_table:
                cv = happy_execute.CV_DEF_OF(opline.get_op1().var)

                if happy_hash.zend_hash_quick_find(gs.EG.active_symbol_table, cv.name,
                    cv.name.length() + 1, 0, value_ptr) == FAILURE:
                    isset = 0
            else:
                isset = 0
        else:
            raise Exception('Not implemented yet')

        zval_utils.Z_SET_TYPE(execute_data.Ts[opline.get_result().var / ZendExecutor.TEMP_SZ].tmp_var_ptr().deref(),
            IS_BOOL)

        if opline.get_extended_value() & ZEND_ISSET_ISEMPTY_MASK == ZEND_ISSET:
            if isset and zval_utils.Z_TYPE_PP(value_ptr.deref()) == IS_NULL:
                zval_utils.Z_SET_LVAL(execute_data.Ts[opline.get_result().var / ZendExecutor.TEMP_SZ].tmp_var_ptr().deref(), 0)
            else:
                zval_utils.Z_SET_LVAL(execute_data.Ts[opline.get_result().var / ZendExecutor.TEMP_SZ].tmp_var_ptr().deref(), isset)
        else:
            raise Exception('Not implemented yet')
        execute_data.opline += 1
        return 0

    def ZEND_VM_HANDLER_BEGIN_SILENCE(self, execute_data):
        # TODO: implement it correctly
        gs.EG.silence = True
        execute_data.opline += 1
        return 0

    def ZEND_VM_HANDLER_END_SILENCE(self, execute_data):
        # TODO: implement it correctly
        gs.EG.silence = False
        execute_data.opline += 1
        return 0

    def ZEND_VM_HANDLER_NOP(self, execute_data):
        execute_data.opline += 1
        return 0

    def ZEND_VM_FETCH_CONSTANT(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]

        if opline.get_op1().get_op_type() == rad.APCFile.ZEND_znode.IS_UNUSED:
            if not zend_constants.zend_get_constant_ex(zval_utils.Z_STRVAL(opline.get_op2().constant),
                zval_utils.Z_STRLEN(opline.get_op2().constant),
                execute_data.Ts[opline.get_result().var / ZendExecutor.TEMP_SZ].tmp_var_ptr(),
                None,
                opline.get_extended_value()):
                raise Exception('Not implemented yet')
        execute_data.opline += 1
        return 0

    def ZEND_VM_ADD_ARRAY_ELEMENT(self, execute_data):
        opline = execute_data.op_array.opcodes[execute_data.opline]
        free_op1 = zend_free_op.make_new()
        free_op2 = zend_free_op.make_new()
        array_ptr = execute_data.Ts[opline.get_result().var / ZendExecutor.TEMP_SZ].tmp_var_ptr()
        expr_ptr = gs.null_zval_ptr
        expr_ptr_ptr = gs.null_zval_ptr_ptr
        offset = happy_execute.GET_ZVAL_PTR(opline.get_op2(), BP_VAR_R, free_op2, execute_data)

        if opline.get_op1().get_op_type() == rad.APCFile.ZEND_znode.IS_VAR or \
            opline.get_op1().get_op_type() == rad.APCFile.ZEND_znode.IS_CV:
            if opline.extended_value:
                expr_ptr_ptr = happy_execute.GET_ZVAL_PTR_PTR(opline.get_op1(), BP_VAR_W, free_op1, execute_data)
                expr_ptr = expr_ptr_ptr.deref()
            else:
                expr_ptr = happy_execute.GET_ZVAL_PTR(opline.get_op1(), BP_VAR_R, free_op1, execute_data)
        else:
            expr_ptr = happy_execute.GET_ZVAL_PTR(opline.get_op1(), BP_VAR_R, free_op1, execute_data)

        if happy_execute.IS_OP_TMP_FREE(opline.get_op1()):
            new_expr = zval_utils.zp_stack(zval_utils.make_empty_zval())
            happy_execute.INIT_PZVAL_COPY(new_expr, expr_ptr)
            expr_ptr = new_expr
        else:
            if (opline.get_op1().get_op_type() == rad.APCFile.ZEND_znode.IS_VAR or
                opline.get_op1().get_op_type() == rad.APCFile.ZEND_znode.IS_CV) and opline.extended_value:
                zval_utils.SEPARATE_ZVAL_TO_MAKE_IS_REF(expr_ptr_ptr)
                expr_ptr = expr_ptr_ptr.deref()
                zval_utils.Z_ADDREF_P(expr_ptr)
            elif opline.get_op1().get_op_type() == rad.APCFile.ZEND_znode.IS_CONST or zval_utils.PZVAL_IS_REF(expr_ptr):
                new_expr = zval_utils.zp_stack(zval_utils.make_empty_zval())
                happy_execute.INIT_PZVAL_COPY(new_expr, expr_ptr)
                expr_ptr = new_expr
                zval_utils.i_zval_copy_ctor(expr_ptr)
            else:
                zval_utils.Z_ADDREF_P(expr_ptr)
        if not offset.is_null():
            if zval_utils.Z_TYPE_P(offset) == IS_STRING:
                happy_hash.zend_symtable_update(happy_operators.Z_ARRVAL_P(array_ptr), zval_utils.Z_STRVAL_P(offset),
                    zval_utils.Z_STRLEN_P(offset), zval_utils.zpp_stack(expr_ptr), 0, gs.null_zval_ptr_ptr_ptr)
            else:
                raise Exception('Not implemented yet')
        else:
            happy_hash.zend_hash_next_index_insert(happy_operators.Z_ARRVAL_P(array_ptr), zval_utils.zpp_stack(expr_ptr), 0,
                gs.null_zval_ptr_ptr_ptr)
        if opline.get_extended_value():
            raise Exception('Not implemented yet')
        else:
            happy_execute.FREE_OP_IF_VAR(free_op1)
        execute_data.opline += 1
        return 0


class zend_function_state:
    def __init__(self, function):
        self.function = function # like ZEND_op_array
        self.arguments = 0


def PHP_FUNCTION(fn_impl):
    op_array = rad.APCFile.ZEND_op_array('')
    op_array.fn_flags = ZEND_ACC_PUBLIC
    op_array.scope = rad.APCFile.NullStruct()
    op_array.type = rad.APCFile.ZEND_function.ZEND_INTERNAL_FUNCTION
    op_array.return_reference = 0
    op_array.internal_fn_ptr = fn_impl
    return op_array


def init_builtins(executor):
    gs.EG.function_table[objects.MutableString('strlen')] = \
        PHP_FUNCTION(zend_builtin_functions.PHP_FUNCTION_strlen)
    gs.EG.function_table[objects.MutableString('define')] = \
        PHP_FUNCTION(zend_builtin_functions.PHP_FUNCTION_define)
    gs.EG.function_table[objects.MutableString('defined')] = \
        PHP_FUNCTION(zend_builtin_functions.PHP_FUNCTION_defined)
    gs.EG.function_table[objects.MutableString('get_class')] = \
        PHP_FUNCTION(zend_builtin_functions.PHP_FUNCTION_get_class)
    gs.EG.function_table[objects.MutableString('func_num_args')] = \
        PHP_FUNCTION(zend_builtin_functions.PHP_FUNCTION_func_num_args)
    gs.EG.function_table[objects.MutableString('count')] = \
        PHP_FUNCTION(ext_array.PHP_FUNCTION_count)
    gs.EG.function_table[objects.MutableString('current')] = \
        PHP_FUNCTION(ext_array.PHP_FUNCTION_current)
    gs.EG.function_table[objects.MutableString('implode')] = \
        PHP_FUNCTION(ext_string.PHP_FUNCTION_implode)
    gs.EG.function_table[objects.MutableString('var_dump')] = \
        PHP_FUNCTION(ext_var.PHP_FUNCTION_var_dump)
    for fname, fn in ext_api.extensions.iteritems():
        gs.EG.function_table[objects.MutableString(fname)] = PHP_FUNCTION(fn)


def zval_call_destructor(zv):
    assert isinstance(zv, objects.zval_ptr_ptr)
    if zval_utils.Z_TYPE_PP(zv) == IS_OBJECT and zval_utils.Z_REFCOUNT_PP(zv) == 1:
        return happy_hash.ZEND_HASH_APPLY_REMOVE
    else:
        return happy_hash.ZEND_HASH_APPLY_KEEP


def shutdown_destructors():
    # TODO: guard with try / catch
    symbols = happy_hash.zend_hash_num_elements(gs.EG.symbol_table)
    happy_hash.zend_hash_reverse_apply(gs.EG.symbol_table, zval_call_destructor)
    # TODO: zend_objects_store_call_destructors


def zend_call_destructors():
    # TODO: guard with try / catch
    shutdown_destructors()


def php_request_shutdown(dummy):
    gs.EG.opline = INVALID_PC
    gs.EG.active_op_array = None
    # TODO: do everything Zend does
    # TODO: guard with try / catch
    zend_call_destructors()


class sapi_request_info:
    def __init__(self):
        self.argc = 0


class sapi_globals_struct:
    def __init__(self):
        self.request_info = sapi_request_info()


def happy_request_startup(SG):
    request_variables.request_hash_environment(SG)
    return SUCCESS


def do_inheritance_helper(apc_class, apc_classes):
    parent_name = objects.MutableString(apc_class.parent_name.to_str().lower())
    name = objects.MutableString(apc_class.name.to_str().lower())
    if len(parent_name.to_str()) != 0:
        class_entry = gs.EG.class_table[name]
        parent_class_entry = gs.EG.class_table[parent_name]
        # TODO: this shouldn't be needed, but for some reason the parent is null here
        class_entry.parent = parent_class_entry
        for kv in parent_class_entry.function_table.dict:
            happy_hash.zend_hash_add(class_entry.function_table, kv.strval, len(kv.strval.to_str()),
                parent_class_entry.function_table.dict[kv][0], 0, gs.null_zval_ptr_ptr)
        for kv in parent_class_entry.default_properties.dict:
            happy_hash.zend_hash_add(class_entry.default_properties, kv.strval, len(kv.strval.to_str()),
                parent_class_entry.default_properties.dict[kv][0], 0, gs.null_zval_ptr_ptr)
        for kv in parent_class_entry.properties_info.dict:
            happy_hash.zend_hash_add(class_entry.properties_info, kv.strval, len(kv.strval.to_str()),
                parent_class_entry.properties_info.dict[kv][0], 0, gs.null_zval_ptr_ptr)
        for cl in apc_classes:
            if cl.name.to_str() == parent_name.to_str():
                do_inheritance_helper(cl, apc_classes)
                break


def do_inheritance(apc_classes):
    for apc_class in apc_classes:
        do_inheritance_helper(apc_class, apc_classes)


def entry_point(argv):
    try:
        ext_api.init_ext_api()
        executor = ZendExecutor()
        apc_dump = rad.APCFile(argv[1])
        php_module = apc_dump.apc_bd.entries[0].value
        ZendOpcode.pass_two(php_module.op_array, apc_dump)
        gs.EG.active_op_array = php_module.op_array
        gs.EG.executor = executor
        for php_function in php_module.functions:
            ZendOpcode.pass_two(php_function.function.op_array, apc_dump)
            gs.EG.function_table[php_function.name] = php_function.function.op_array
        for php_class in php_module.classes:
            gs.EG.class_table[php_class.name] = php_class.class_entry
            for k in php_class.class_entry.function_table.dict:
                ZendOpcode.pass_two(php_class.class_entry.function_table.dict[k][0].deref().op_array, apc_dump)
        do_inheritance(php_module.classes)
        # Init extension API; disabled for now
        init_builtins(executor)
        SG = sapi_globals_struct()
        SG.request_info.argc = len(argv) - 1
        if happy_request_startup(SG) == FAILURE:
            raise Exception('Not implemented yet')
        executor.execute(php_module.op_array)
        php_request_shutdown(None)
    except PHPErrorException:
        return -1
    return 0


# _____ Define and setup target ___
def target(*args):
    return entry_point, None


if __name__== '__main__':
    entry_point(sys.argv)
