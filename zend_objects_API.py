import zend_objects
import zval_utils
import objects

class zend_object_store_bucket:
    def __init__(self):
        self.destructor_called = False
        self.valid = False
        # store_object obj
        self.object = None
        self.dtor = None
        self.free_storage = None
        self.clone = None
        self.handlers = None
        self.refcount = 0
        # free_list
        self.next = 0


class zend_objects_store:
    def __init__(self, init_size):
        self.object_buckets = []
        for i in range(init_size):
            self.object_buckets.append(zend_object_store_bucket())
        self.top = 1
        self.size = init_size
        self.free_list_head = -1


def zend_object_store_get_object(zobject):
    assert isinstance(zobject, objects.zval_ptr)
    import global_state

    handle = zval_utils.Z_OBJ_HANDLE_P(zobject)
    return global_state.EG.objects_store.object_buckets[handle].object


def zend_objects_store_put(object, dtor, free_storage, clone):
    import global_state
    EG = global_state.EG

    if EG.objects_store.free_list_head != -1:
        raise Exception('Not implemented yet')
    else:
        if EG.objects_store.top == EG.objects_store.size:
            raise Exception('Not implemented yet')
        handle = EG.objects_store.top
        EG.objects_store.top += 1
    obj = EG.objects_store.object_buckets[handle]
    EG.objects_store.object_buckets[handle].destructor_called = False
    EG.objects_store.object_buckets[handle].valid = True

    obj.refcount = 1
    obj.object = object
    # FIXME
    obj.dtor = dtor if dtor else zend_objects.zend_objects_destroy_object
    obj.free_storage = free_storage
    obj.clone = clone
    obj.handlers = None

    return handle
