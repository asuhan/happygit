#!/bin/sh

pypy=`cd $1; pwd`
happy=`pwd`
ln -sf $happy/*.py $pypy/pypy/translator/goal
ln -sf $happy/*.sh $pypy
ln -sf $happy/*.php $pypy
ln -sf $happy/extensions $pypy

