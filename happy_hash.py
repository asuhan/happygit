
from pypy.rlib.objectmodel import r_dict, compute_hash
import functools

from zend import *
import objects
import zval_utils

class HashTableKey:
    pass

class HashTableStringKey(HashTableKey):
    def __init__(self, strval):
        assert isinstance(strval, objects.MutableString)
        self.strval = strval

    def get_hash(self):
        return compute_hash(self.strval.to_str())

class HashTableIntKey(HashTableKey):
    def __init__(self, intval):
        self.intval = intval

    def get_hash(self):
        return compute_hash(self.intval)

def _ht_key_eq(key1, key2):
    if (isinstance(key1, HashTableIntKey) and
            isinstance(key2, HashTableIntKey)):
        return key1.intval == key2.intval
    if (isinstance(key1, HashTableStringKey) and
            isinstance(key2, HashTableStringKey)):
        return key1.strval.to_str() == key2.strval.to_str()
    return False

def _ht_key_hash(key):
    return key.get_hash()

class HashTable:
    def __init__(self, pDestructor, persistent, bApplyProtection):
        self.pDestructor = pDestructor
        self.persistent = persistent
        self.bApplyProtection = bApplyProtection
        self.reset()

    def replace(self, key, value):
        assert isinstance(value, objects.zval_ptr_ptr)
        assert key in self.dict
        old_data, prev_key, _, next_key = self.dict[key]
        if self.pDestructor is not None:
            self.pDestructor(old_data)
        self.dict[key] = value, prev_key, key, next_key

    def add(self, key, value):
        assert isinstance(value, objects.ptr)
        assert not key in self.dict
        if len(self.dict) is 0:
            self.dict[key] = value, None, key, None
            self.first_key = self.last_key = key
        else:
            # Update the next pointer of the last key
            assert self.last_key is not None
            last_data, last_prev_key, _, _ = self.dict[self.last_key]
            self.dict[self.last_key] = last_data, last_prev_key, self.last_key, key
            # Add the new key to the dictionary
            self.dict[key] = value, self.last_key, key, None
            self.last_key = key

    def prepend(self, value):
        key = HashTableIntKey(0)
        if len(self.dict) is 0:
            self.dict[key] = value, None, key, None
            self.first_key = self.last_key = key
        else:
            assert self.first_key is not None
            first_data, _, _, first_next_key = self.dict[self.first_key]
            self.dict[self.first_key] = first_data, key, self.first_key, first_next_key
            # Add the new key to the dictionary
            self.dict[key] = value, None, key, self.first_key
            self.first_key = key


    def delete(self, key):
        assert key in self.dict
        data, prev_key, _, next_key = self.dict[key]
        del self.dict[key]
        if self.pDestructor:
            self.pDestructor(data)

        if key == self.first_key:
            self.first_key = next_key
        if key == self.last_key:
            self.last_key = prev_key

        if prev_key is not None:
            assert prev_key in self.dict
            prev_data, prev_prev_key, _, prev_next_key = self.dict[prev_key]
            assert prev_next_key == key
            self.dict[prev_key] = prev_data, prev_prev_key, prev_key, next_key
        if next_key is not None:
            assert next_key in self.dict
            next_data, next_prev_key, _, next_next_key = self.dict[next_key]
            assert next_prev_key == key
            self.dict[next_key] = next_data, prev_key, next_key, next_next_key

    def reset(self):
        self.dict = r_dict(_ht_key_eq, _ht_key_hash)
        self.first_key = None
        self.last_key = None
        self.next_free = 0
        self.current_key = None

    def size(self):
        return len(self.dict)

    # This is a funky function needed by the extensions:
    # given a temporary key, it returns the permanent equivalent key
    # from the dictionary; this key is used as a pointer to that item
    def get_perm_key(self, key):
        if key is None or key not in self.dict:
            return None

        _, _, kk, _ = self.dict[key]
        assert _ht_key_eq(key, kk)
        return kk

    def renumber(self):
        idx = 0
        k = self.first_key
        while k is not None:
            data, prev_key, _, next_key = self.dict[k]
            if isinstance(k, HashTableIntKey) and k.intval != idx:
                # Need to remove element first, because we change the key
                del self.dict[k]
                k.intval = idx
                idx += 1
                self.dict[k] = data, prev_key, k, next_key
            k = next_key
        self.next_free = idx

    def is_vector(self):
        idx = 0
        k = self.first_key
        while k is not None:
            _, _, _, next_key = self.dict[k]
            if not isinstance(k, HashTableIntKey) or k.intval != idx:
                return False
            idx += 1
            k = next_key
        return True

def zend_hash_init_ex(ht, nSize, pHashFunction, pDestructor, persistent,
        bApplyProtection=False):
    return HashTable(pDestructor, persistent, bApplyProtection)

# zend_hash_init is the same, without bApplyProtection
zend_hash_init = zend_hash_init_ex

def zend_hash_copy(target, source, pCopyConstructor, tmp, size):
    from global_state import null_zval_ptr_ptr
    target.pDestructor = source.pDestructor
    new_entry_ptr = zval_utils.zppp_stack(null_zval_ptr_ptr)
    for k, (pData, _, _, _) in source.dict.iteritems():
        if isinstance(k, HashTableStringKey):
            zend_hash_quick_update(target, k.strval, k.strval.length(), 0,
                                   pData, 0, new_entry_ptr)
        else:
            zend_hash_index_update(target, k.intval, pData, 0, new_entry_ptr)
        if pCopyConstructor:
            pCopyConstructor(new_entry_ptr.deref())

def zend_hash_destroy(ht):
    if ht.pDestructor:
        for value in ht.dict.itervalues():
            ht.pDestructor(value[0])
    ht.reset()

zend_hash_clean = zend_hash_destroy

_HASH_UPDATE      = (1<<0)
_HASH_ADD         = (1<<1)
_HASH_NEXT_INSERT = (1<<2)

def _zend_hash_add_or_update(ht, arKey, nKeyLength,
                             pData, nDataSize, pDest, flag):
    assert isinstance(pDest, objects.ptr)
    assert isinstance(pData, objects.ptr)
    if nKeyLength <= 0:
        return FAILURE
    is_add = flag is _HASH_ADD
    key = HashTableStringKey(arKey)
    p = pData.copy()
    if key in ht.dict:
        if is_add:
            return FAILURE
        ht.replace(key, p)
        if pDest:
            pDest.assign(p)
        return SUCCESS
    ht.add(key, p)
    if not pDest.is_null():
        pDest.assign(p)
    return SUCCESS

def zend_hash_add(ht, arKey, nKeyLength, pData, nDataSize, pDest):
    return _zend_hash_add_or_update(ht, arKey, nKeyLength, pData, nDataSize,
        pDest, _HASH_ADD)

def zend_hash_update(ht, arKey, nKeyLength, pData, nDataSize, pDest):
    return _zend_hash_add_or_update(ht, arKey, nKeyLength, pData, nDataSize,
        pDest, _HASH_UPDATE)

def zend_symtable_update(ht, arKey, nKeyLength, pData, nDataSize, pDest):
    # TODO: ZEND_HANDLE_NUMERIC
    return zend_hash_update(ht, arKey, nKeyLength, pData, nDataSize, pDest)

def zend_symtable_find(ht, arKey, nKeyLength, pData):
    # TODO: ZEND_HANDLE_NUMERIC
    return zend_hash_find(ht, arKey, nKeyLength, pData)

def _zend_hash_index_update_or_next_insert(ht, h, pData, nDataSize, pDest, flag):
    assert isinstance(pDest, objects.zval_ptr_ptr_ptr)
    assert isinstance(pData, objects.zval_ptr_ptr)
    if flag & _HASH_NEXT_INSERT:
        h = ht.next_free
    key = HashTableIntKey(h)
    p = pData.copy()
    if key in ht.dict:
        if (flag & _HASH_NEXT_INSERT) or (flag & _HASH_ADD):
            return FAILURE

        ht.replace(key, p)
        if h >= ht.next_free:
            # TODO: clamp at LONG_MAX
            ht.next_free = h + 1
        if not pDest.is_null():
            pDest.assign(p)
        return SUCCESS

    ht.add(key, p)
    if h >= ht.next_free:
        # TODO: clamp at LONG_MAX
        ht.next_free = h + 1
    if not pDest.is_null():
        pDest.assign(p)
    return SUCCESS

def zend_hash_index_update(ht, h, pData, nDataSize, pDest):
    return _zend_hash_index_update_or_next_insert(ht, h, pData, nDataSize, pDest, _HASH_UPDATE)

def zend_hash_next_index_insert(ht, pData, nDataSize, pDest):
    assert isinstance(pDest, objects.zval_ptr_ptr_ptr)
    assert isinstance(pData, objects.zval_ptr_ptr)
    return _zend_hash_index_update_or_next_insert(ht, 0, pData, nDataSize, pDest, _HASH_NEXT_INSERT)

def _zend_hash_quick_add_or_update(ht, arKey, nKeyLength, h,
                                   pData, nDataSize, pDest, flag):
    assert isinstance(pDest, objects.zval_ptr_ptr_ptr)
    assert isinstance(pData, objects.zval_ptr_ptr)
    if not nKeyLength:
        return zend_hash_index_update(ht, h, pData, nDataSize, pDest)
    return _zend_hash_add_or_update(ht, arKey, nKeyLength,
                                    pData, nDataSize, pDest, flag)

zend_hash_quick_add = functools.partial(_zend_hash_quick_add_or_update,
        flag=_HASH_ADD)

def zend_hash_quick_update(ht, arKey, nKeyLength, h, pData, nDataSize, pDest):
    return _zend_hash_quick_add_or_update(ht, arKey, nKeyLength, h, pData, nDataSize, pDest, _HASH_UPDATE)

def zend_hash_add_empty_element(ht, arKey, nKeyLength):
    return zend_hash_add(ht, arKey, nKeyLength, None, 0, None)

def zend_hash_graceful_destroy(ht):
    # TODO: later
    pass

def zend_hash_graceful_reverse_destroy(ht):
    # TODO: later
    pass

ZEND_HASH_APPLY_KEEP   = 0
ZEND_HASH_APPLY_REMOVE = (1<<0)
ZEND_HASH_APPLY_STOP   = (1<<1)

def zend_hash_apply(ht, apply_func):
    # TODO: later
    pass

def zend_hash_apply_deleter(ht, q):
    pData, _, _, next_key = ht.dict[q]
    # TODO: remove entry from the hash
    if ht.pDestructor:
        ht.pDestructor(pData)

def zend_hash_apply_with_argument(ht, apply_func, argument):
    # TODO: later
    pass

def zend_hash_apply_with_arguments(ht, apply_func, num_args, *args):
    p = ht.first_key
    while p:
        pData, _, _, next_key = ht.dict[p]
        result = apply_func(pData, num_args, args, p)
        if result:
            raise Exception('Not implemented yet')
        p = next_key

# TODO: get rid of the EG parameter
def zend_hash_reverse_apply(ht, apply_func):
    p = ht.last_key
    while p:
        pData, prev_key, _, _ = ht.dict[p]
        result = apply_func(pData)

        q = p
        if result & ZEND_HASH_APPLY_REMOVE:
            zend_hash_apply_deleter(ht, q)
        if result & ZEND_HASH_APPLY_STOP:
            raise Exception('Not implemented yet')
        p = prev_key

HASH_DEL_KEY = 0
HASH_DEL_INDEX = 1
HASH_DEL_KEY_QUICK = 2

def zend_hash_del_key_or_index(ht, arKey, nKeyLength, h, flag):
    key = HashTableIntKey(h) if nKeyLength is 0 else HashTableStringKey(arKey)
    if key not in ht.dict:
        return FAILURE
    ht.delete(key)
    return SUCCESS

def zend_hash_del(ht, arKey, nKeyLength):
    return zend_hash_del_key_or_index(ht, arKey, nKeyLength, 0, HASH_DEL_KEY)

def zend_hash_quick_del(ht, arKey, nKeyLength, h):
    return zend_hash_del_key_or_index(ht, arKey, nKeyLength, h,
        HASH_DEL_KEY_QUICK)

def zend_hash_index_del(ht, h):
    return zend_hash_del_key_or_index(ht, None, 0, h, HASH_DEL_INDEX)

def zend_get_hash_value(arKey, nKeyLength):
    # TODO: later
    pass

def zend_hash_find(ht, arKey, nKeyLength, pData):
    assert isinstance(arKey, objects.MutableString)
    key = HashTableStringKey(arKey)
    if key not in ht.dict:
        return FAILURE
    if not pData.is_null():
        data, _, _, _ = ht.dict[key]
        pData.assign(data)
    return SUCCESS

def zend_hash_quick_find(ht, arKey, nKeyLength, h, pData):
    if nKeyLength == 0:
        return zend_hash_index_find(ht, h, pData)
    return zend_hash_find(ht, arKey, nKeyLength, pData)

def zend_hash_index_find(ht, h, pData):
    assert isinstance(pData, objects.zval_ptr_ptr_ptr)
    key = HashTableIntKey(h)
    if key not in ht.dict:
        return FAILURE
    if not pData.is_null():
        data, _, _, _ = ht.dict[key]
        assert isinstance(data, objects.zval_ptr_ptr)
        pData.assign(data)
    return SUCCESS

def zend_hash_exists(ht, arKey, nKeyLength):
    return HashTableStringKey(arKey) in ht.dict

def zend_hash_quick_exists(ht, arKey, nKeyLength, h):
    key = HashTableIntKey(h) if nKeyLength is 0 else HashTableStringKey(arKey)
    return key in ht.dict

def zend_hash_index_exists(ht, h):
    return HashTableIntKey(h) in ht.dict

def zend_hash_next_free_element(ht):
    return ht.next_free

HASH_KEY_IS_STRING    = 1
HASH_KEY_IS_LONG      = 2
HASH_KEY_NON_EXISTANT = 3

HASH_UPDATE_KEY_IF_NONE   = 0
HASH_UPDATE_KEY_IF_BEFORE = 1
HASH_UPDATE_KEY_IF_AFTER  = 2
HASH_UPDATE_KEY_ANYWAY    = 3

class HashPosition:
    def __init__(self):
        self.key = None

def zend_hash_move_forward_ex(ht, pos):
    key = pos.key if pos else ht.current_key
    if key is None:
        return FAILURE
    assert key in ht.dict
    _, _, _, next_key = ht.dict[key]

    if pos:
        pos.key = next_key
    else:
        ht.current_key = next_key
    return SUCCESS

def zend_hash_move_backwards_ex(ht, pos):
    key = pos.key if pos else ht.current_key
    assert key in ht.dict
    _, prev_key, _, _ = ht.dict[key]
    if prev_key is None:
        return FAILURE

    if pos:
        pos.key = next_key
    else:
        ht.current_key = next_key
    return SUCCESS

def zend_hash_get_current_key_ex(ht, str_index, str_length,
                                 num_index, duplicate, pos):
    key = pos.key if pos else ht.current_key
    if key is None:
        return HASH_KEY_NON_EXISTANT
    if isinstance(key, HashTableStringKey):
        raise Exception('Not implemented yet')
    elif isinstance(key, HashTableIntKey):
        if not num_index.is_null():
            num_index.assign(key.intval)
        return HASH_KEY_IS_LONG
    assert(False)

def zend_hash_get_current_key_type_ex(ht, pos):
    key = pos.key if pos else ht.current_key
    if key is None:
        return HASH_KEY_NON_EXISTANT
    if isinstance(key, HashTableStringKey):
        return HASH_KEY_IS_STRING
    elif isinstance(key, HashTableIntKey):
        return HASH_KEY_IS_LONG
    else:
        raise Exception('Invalid key type!')

def zend_hash_get_current_data_ex(ht, pData, pos):
    key = pos.key if pos else ht.current_key
    if key is None:
        return FAILURE
    if not pData.is_null():
        data, _, _, _ = ht.dict[key]
        pData.assign(data)
    return SUCCESS

def zend_hash_internal_pointer_reset_ex(ht, pos):
    if pos:
        pos.key = ht.first_key
    else:
        ht.current_key = ht.first_key

def zend_hash_internal_pointer_end_ex(ht, pos):
    if pos:
        pos.key = ht.last_key
    else:
        ht.current_key = ht.last_key

def zend_hash_update_current_key_ex(ht, key_type, str_index, str_length,
        num_index, mode, pos):
    key = pos.key if pos else ht.current_key
    if key_type is HASH_KEY_LONG and key == num_index:
        return SUCCESS
    if key_type is HASH_KEY_IS_STRING and key == str_index:
        return SUCCESS
    if key_type not in [HASH_KEY_IS_STRING, HASH_KEY_IS_LONG]:
        return FAILURE

    if key in ht.dict:
        ht.delete(key)
    # TODO: implement rest later
    return SUCCESS


def zend_hash_has_more_elements_ex(ht, pos):
    # FIXME: FAILURE == 0?
    return FAILURE if (zend_hash_get_current_key_type_ex(ht, pos)
                       == HASH_KEY_NON_EXISTANT) else SUCCESS

#HashPointer = HashPosition

def zend_hash_get_pointer(ht, ptr):
    ptr.key = ht.current_key
    return ptr.key is not None

def zend_hash_set_pointer(ht, ptr):
    if ptr.key != ht.current_key:
        if ptr.key not in ht.dict:
            return 0
        ht.current_key = ptr.key
    return 1 

def zend_hash_has_more_elements(ht):
    return zend_hash_has_more_elements_ex(ht, None)

def zend_hash_move_forward(ht):
    return zend_hash_move_forward_ex(ht, None)

zend_hash_move_backwards = functools.partial(
        zend_hash_move_backwards_ex, pos=None)
zend_hash_get_current_key = functools.partial(
        zend_hash_get_current_key_ex, pos=None)
zend_hash_get_current_key_type = functools.partial(
        zend_hash_get_current_key_type_ex, pos=None)

def zend_hash_get_current_data(ht, pData):
    return zend_hash_get_current_data_ex(ht, pData, None)

def zend_hash_internal_pointer_reset(ht):
    return zend_hash_internal_pointer_reset_ex(ht, None)

zend_hash_internal_pointer_end = functools.partial(
        zend_hash_internal_pointer_end_ex, pos=None)
zend_hash_update_current_key = functools.partial(
        zend_hash_update_current_key_ex, pos=None)

# Skipped: zend_hash_copy, zend_hash_merge, zend_hash_sort, zend_hash_compare

def zend_hash_num_elements(ht):
    return len(ht.dict)

def zend_hash_rehash(ht):
    # TODO: later
    pass

# TODO: add Zend hash function

# TODO: add zend_symtable_XXX functions


