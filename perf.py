import os, sys, glob, time

abspath = os.popen('cd ' + sys.argv[1] + '; pwd').read().strip()
runs = 5
if len(sys.argv) > 2:
	runs = int(sys.argv[2])
os.system('./cleanup.sh ' + abspath)
os.system('./create_dumps.sh ' + abspath)

txtred = os.popen('tput setaf 1').read()
txtgrn=os.popen('tput setaf 2').read()
txtrst=os.popen('tput sgr0').read()

php_filelist = glob.glob(abspath + '/*.php')
print 'File, HappyJIT avg, HappyJIT min, HappyJIT max, Zend avg, Zend min, Zend max, Avg speedup'
for php_file in php_filelist:
	sys.stderr.write('Testing ' + php_file + '...\n')
	php_dumpfile = php_file + '.dump'
	happy_results = []
	zend_results = []
	success = True
	for run in range(runs):
		start = time.time()
		happy_out = os.popen('./targetphpstandalone-c ' + php_dumpfile).read().strip()
		happy_time = time.time() - start
		start = time.time()
		zend_out = os.popen('php ' + php_file).read().strip()
		zend_time = time.time() - start
		if happy_out != zend_out:
			success = False
			break
		happy_results.append(happy_time)
		zend_results.append(zend_time)
	if success:
		sys.stderr.write(txtgrn + 'PASS' + txtrst + '\n')
		happy_avg = sum(happy_results) / len(happy_results)
		zend_avg = sum(zend_results) / len(zend_results)
		print php_file.split('/')[-1] + ', ' +\
		str(happy_avg) + ', ' +\
		str(min(happy_results)) + ', ' + str(max(happy_results)) + ', ' +\
		str(zend_avg) + ', ' +\
		str(min(zend_results)) + ', ' + str(max(zend_results)) + ', ' +\
		str(zend_avg / happy_avg)
	else:
		sys.stderr.write(txtred + 'FAIL' + txtrst + '\n')
