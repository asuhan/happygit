from zend import *
import global_state
import zval_utils
import happy_hash
import happy_operators
from objects import zval_ptr, MutableString, append_strings

def PHP_FUNCTION_implode(ht, return_value, return_value_ptr, this_ptr, return_value_used):
    assert isinstance(return_value, zval_ptr)
    EG = global_state.EG

    arg_count = EG.argument_stack.top_elem().deref().lval
    if arg_count != 2:
        raise Exception('Not implemented yet')
    top_idx = EG.argument_stack.top()
    assert top_idx - 1 - arg_count >= 0
    arg1 = EG.argument_stack.getItem(top_idx - 1 - arg_count + 1)
    arg2 = EG.argument_stack.getItem(top_idx - 1 - arg_count)
    if arg2.is_null():
        raise Exception('Not implemented yet')
    if zval_utils.Z_TYPE_P(arg1) != IS_ARRAY:
        raise Exception('Not implemented yet')
    if zval_utils.Z_TYPE_P(arg2) != IS_STRING:
        raise Exception('Not implemented yet')
    arr = arg1
    delim = arg2
    implode_impl(delim, arr, return_value)

def implode_impl(delim, arr, return_value):
    assert isinstance(return_value, zval_ptr)
    tmp_ptr = zval_utils.zppp_stack(global_state.null_zval_ptr_ptr)
    pos = happy_hash.HashPosition()
    implstr = MutableString('')
    i = 0

    numelems = happy_hash.zend_hash_num_elements(happy_operators.Z_ARRVAL_P(arr))

    if numelems == 0:
        raise Exception('Not implemented yet')

    happy_hash.zend_hash_internal_pointer_reset_ex(happy_operators.Z_ARRVAL_P(arr), pos)
    while happy_hash.zend_hash_get_current_data_ex(happy_operators.Z_ARRVAL_P(arr), tmp_ptr, pos) == happy_hash.SUCCESS:
        if zval_utils.Z_TYPE_PP(tmp_ptr.deref()) == IS_STRING:
            implstr = append_strings(implstr, zval_utils.Z_STRVAL_PP(tmp_ptr.deref()))
        elif zval_utils.Z_TYPE_PP(tmp_ptr.deref()) == IS_LONG:
            implstr = append_strings(implstr, MutableString(str(zval_utils.Z_LVAL_PP(tmp_ptr.deref()))))
        else:
            raise Exception('Not implemented yet')
        i += 1
        if i != numelems:
            implstr = append_strings(implstr, zval_utils.Z_STRVAL_P(delim))
        happy_hash.zend_hash_move_forward_ex(happy_operators.Z_ARRVAL_P(arr), pos)

    zval_utils.ZVAL_STRING(return_value, implstr)
