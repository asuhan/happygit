<?php

class foo
{
    private function printItem($string)
    {
        echo 'Foo: ' . $string . PHP_EOL;
    }

    public function printHandler($string)
    {
        $this->printItem($string);
    }
}

class bar extends foo
{
    public function printItem($string)
    {
        echo 'Bar: ' . $string . PHP_EOL;
    }
}

$bar = new bar();
$bar->printItem('baz'); // Output: 'Bar: baz'
$bar->printHandler('bar');

?>

