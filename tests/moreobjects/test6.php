<?php

class SimpleClass
{
    // property declaration
    public $var = 'a default value';
}

class ExtendClass extends SimpleClass
{
    public function displayVar() {
        echo $this->var;
    }
}

$extended = new ExtendClass();
$extended->displayVar();

?>

