<?php

//$x = test1_fn1('aaa');
//var_dump($x);
#$x = test1_fn1(12);
#$y = test1_fn1();
//var_dump($y);
// Should output: 14
//echo test1_fn2(2, 'abc', array(5, 'ab', 7));

#echo plus1(7) . "\n";
#plus1(12);

#$pi = pi();
#echo "$pi\n";
#echo sin($pi / 6.) . "\n";

echo abs(-13) . "\n";

// Output: b26e
echo dechex(45678) . "\n";

// Output: 3 2
echo min(5, 3) . "\n";
$a = array(5, 7, 2, 8);
echo min($a) . "\n";

echo strrev("alphabeta") . "\n";
echo ord("A") . chr(97) . "\n";

// Output: 35
printf("%d\n", 12 + 23);

$a1 = "alphabeta";
$a2 = "deltagamma";
$c = 0;
$b1 = str_replace("a", "q", $a1);
$b2 = str_replace("a", "q", $a2);
# FIXME: disabled for now, enable later
#$b2 = str_replace("a", "q", $a2, $c);
echo "$b1 $b2 $c\n";

$s = "This is a sentence!!!";
$c = str_word_count($s);
$sa = str_word_count($s, 1);
var_dump($c, $sa);

?>

