<?php
/*
 $Id: fibo.php,v 1.4 2005-04-25 19:01:38 igouy-guest Exp $
 http://shootout.alioth.debian.org/
*/
function fibo($n){
    return(($n < 2) ? 1 : fibo($n - 2) + fibo($n - 1));
}
$n = (int) $argv[1];
$r = fibo($n);
echo "$r\n";
?>
