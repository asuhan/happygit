<?php
/* The Great Computer Language Shootout
   http://shootout.alioth.debian.org/
   contributed by Isaac Gouy 

   php -q sieve.php 9000
*/ 


$n = (int) $argv[1];

$stop = 8192;
$count = 0;

$isPrime = array();

while ($n-- > 0) {
   $count = 0;
   $i = $stop; while ($i--) $isPrime[$i] = 1;

   $i = 2; while ($i++ < $stop){
      if ($isPrime[$i]){
         for ($k=$i+$i; $k<=$stop; $k+=$i) $isPrime[$k] = 0;
         $count++;
      }
   }
} 
echo "Count: $count\n";

?>
