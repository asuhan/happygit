<?
/* The Great Computer Language Shootout
   http://shootout.alioth.debian.org/
   contributed by Isaac Gouy 

   php -q ary.php 9000
*/ 


$n = 3;

$x = array();
for ($i=0; $i<$n; $i++) $x[$i] = $i + 1;

$y = array();
for ($i=0; $i < $n; $i++) {
    $y[] = 0;
}
for ($k=0; $k < 2000000; $k++){
    $j = $n;
    while ($j--)
        $y[$j] = $y[$j] + $x[$j];
}

printf("%d %d\n", $y[0], $y[$n-1]);

?>
