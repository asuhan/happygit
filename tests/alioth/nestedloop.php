<?php
/* The Great Computer Language Shootout
   http://shootout.alioth.debian.org/
   contributed by Isaac Gouy 

   php -q nestedloop.php 18
*/ 


$n = (int) $argv[1];

$x = 0;
$a = $n; while ($a--){
   $b = $n; while ($b--){
      $c = $n; while ($c--){
         $d = $n; while ($d--){
            $e = $n; while ($e--){
               $f = $n; while ($f--) 
                  $x++; }}}}}
 
echo "$x\n";

?>
