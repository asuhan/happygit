<?php /* The Computer Language Shootout
   http://shootout.alioth.debian.org/
   contributed by Peter Baltruschat
*/

function primes($size)
{
   $flags = array();
   for ($i = 1; $i <= ($size>>5) + 1; $i++)
      $flags[] = -1;
   $count = 0;
   for($i = 2; $i < $size; ++$i)
   {
      $offset = $i>>5;
      $mask = 1<<($i - ($offset<<5));
      if($flags[$offset] & $mask)
      {
         ++$count;
         for($j = $i<<1; $j <= $size; $j += $i)
         {
            $offset = $j>>5;
            $mask = 1<<($j - ($offset<<5));
            if($flags[$offset] & $mask)
            {
               $flags[$offset] = $flags[$offset] ^ $mask;
            }
         }
      }
   }
   printf("Primes up to %8d %8d\n", $size, $count);
}

$n = (int) $argv[1];

$size = 10000 * (1<<$n);
primes($size);
primes($size>>1);
primes($size>>2);
?>
