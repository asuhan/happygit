<?php
  $a = array(array(array(1)),
             array(array(2)),
             array(array(3)),
             array(array(4)),
             array(array(5)));
  $i = 42;
  foreach ($a as &$x) {
    $x[0] = $i;
    $i++;
  }
  var_dump($a);
?>
