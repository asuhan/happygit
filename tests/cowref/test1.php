<?php
  function f(&$x) {
    foreach ($x as $x[0] => &$x[1]) {
      var_dump($x);
    }
  }

  function g($x) {
    foreach ($x as $x[0] => &$x[1]) {
      var_dump($x);
    }
  }

  $a = $b = array(array(array(1)),
                  array(array(2)),
                  array(array(3)),
                  array(array(4)),
                  array(array(5)));
  f($a);
  var_dump($a);
  g($b);
  var_dump($b);
?>
