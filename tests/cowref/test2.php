<?php
function f(&$x) {
    var_dump($x);
    foreach ($x as &$y) {
      var_dump($x);
      $y = array(42);
      $y[0] = 7;
      var_dump($x);
    }
    $x[0] = 7;
  }

  $a = array(array(array(1)),
             array(array(2)),
             array(array(3)),
             array(array(4)),
             array(array(5)));
  f($a);
  var_dump($a);
?>
