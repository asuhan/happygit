<?php
  $x = array(array(array(1)),
             array(array(2)),
             array(array(3)),
             array(array(4)),
             array(array(5)));
  $i = 42;
  foreach ($x as &$y) {
    foreach ($y as &$z) {
      $z[0] = $i;
      $i++;
    }
  }
  var_dump($x);
?>
