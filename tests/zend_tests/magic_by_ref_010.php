<?php

class Foo {
    function __call($method, $args)
    {
        print $args[0]."\n";
        $args[0] = 5;
        print $args[0]."\n";
        return true;
    }
}

$v = 'str';
$o = new Foo();
$o->test(&$v);

var_dump($v);

echo "Done\n";
?>
