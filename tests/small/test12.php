<?php

function fibo($n) {
  if ($n <= 2)
    return 1;
  else
    return fibo($n - 1) + fibo($n - 2);
}

class A {
  public $x;

  function foo($x) {
    return fibo($x + $this->x);
  }
}

$a = new A();
$a->x = 2;
echo $a->foo(6);
$a->x = 3;
echo $a->x;

?>

