<?php

$x = 7;

function foo() {
  global $x;
  $x++;
  static $x = 0;
  $x++;
  echo $x;
}

foo();
foo();
echo $x;

?>
