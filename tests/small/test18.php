<?php

$a = array(false, true, null, 0, 1, 2, 0.0, 1.1, 2.2, "0", "", "1", "11", "1.1",
    "1.1 values", "0.1", "1e-1", "0 stuff", "none");

foreach ($a as $kx => $x) {
    foreach ($a as $ky => $y) {
        $is_eq = ($x == $y);
        $is_lt = ($x <  $y);
        $is_le = ($x <= $y);
        echo "Comparing `$x`[$kx] with `$y`[$ky]: eq:`$is_eq` lt:`$is_lt` le:`$is_le`\n";
    }
}

?>
