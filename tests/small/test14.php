<?php

class A {
    public $x = 1;
}

$a1 = new A();
$a1->x = 1;
$b1 = new A();
$b1->x = 1;
$c1 = $a1;
$c1 = $b1;
$b1->x++;
echo "{$a1->x}{$b1->x}{$c1->x}\n";
// Output: 122

$a2 = new A();
$a2->x = 1;
$b2 = new A();
$b2->x = 1;
$c2 =& $a2;
$c2 = $b2;
$b2->x++;
echo "{$a2->x}{$b2->x}{$c2->x}";
// Output: 222

?>
