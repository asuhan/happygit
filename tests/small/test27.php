<?php

class A {
    function __construct() {
        echo "Constructed A\n";
    }

    function __destruct() {
        echo "Destroyed A\n";
    }
}

$a = new A();

?>

