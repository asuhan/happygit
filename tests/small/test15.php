<?php

# FIXME: add this back when we have constants
#const NUM_TESTS = 20;

function foo($x) {
    $y = (string) $x;
    for ($i = 0; $i < 200; $i++) {
        echo $y;
        $y++;
    }
    echo "\n";
}

foo("12ab");
foo("1ab2");
foo("q");
foo("8");
foo("1/c/");
foo("-x");
foo("-3");

?>

