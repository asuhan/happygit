<?php

$t = 1;
do {
    $a = 'abc';
    $b = 'def';
    $a .= $b;
    $b .= $a . $b . $a . $b;
    $a = "$a $b $b $a";
    $a .= 'x' . 'y' . 'z';
    $b .= "$a$b" . 'hij';
} while (--$t !== 0);

echo "$a$b";

?>
