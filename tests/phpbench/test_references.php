<?php

$t = 1;
do {
    $a = 42;
    $b = &$a;
    $b = 69;
    $c = &$b;
    $a = 17;
    $c = 21;
} while (--$t !== 0);

echo $a.$b.$c;

?>
