<?php

$t = 1;

do {
    $a = 'abcdefghijklmnop';
    $b = $a;
    $c = $a . $b;
    $d = $c . $a;
    $e = $a . $c . $d . $b;
    $f = $a . $c;
    $g = $b . $d;
    $h = $a . $b . $c . $d . $e . $f . $g;
    $i = $a;
    $j = $b;
    $k = $c;
    $l = $d;
    $m = $e;
    $n = $f;
    $o = $g;
    $p = $h;
} while (--$t !== 0);
var_dump("$a$b$c$d$e$f$g$h$i$j$k$l$m$n$o$p");

?>
