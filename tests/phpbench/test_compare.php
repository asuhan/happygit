<?php

$a = 42;
$b = '69';

$t = 1;
do {
    $c = ($a == $b);
    $d = ($a === $b);
    $e = ($a != $b);	
    $f = ($a !== $b);
    $g = ($a < $b);	
    $h = ($a > $b);
    $i = ($a <= $b);
    $j = ($a >= $b);
} while (--$t !== 0);

var_dump($c);
var_dump($d);
var_dump($e);
var_dump($f);
var_dump($g);
var_dump($h);
var_dump($i);
var_dump($j);
var_dump("$c $d $e $f $g $h $i $j");

?>
