from zend import *
import objects
import zend_objects_API
import zend_object_handlers as zoh
import happy_util
import happy_variables
import zend_interfaces
import zval_utils
import global_state

ZEND_DESTRUCTOR_FUNC_NAME = "__destruct"

def zend_objects_destroy_object(object, handle):
    EG = global_state.EG

    destructor = object.ce.destructor if not happy_util.is_null_struct(object) else None

    if not happy_util.is_null_struct(destructor) and not happy_util.is_null_struct(destructor):
        if destructor.op_array.fn_flags & (ZEND_ACC_PRIVATE | ZEND_ACC_PROTECTED):
            if destructor.op_array.fn_flags & ZEND_ACC_PRIVATE:
                # Ensure that if we're calling a private function, we're allowed to do so
                if object.ce != EG.scope:
                    ce = object.ce

                    err_msg = "Call to private %s::__destruct() from context '%s'%s" % (
                        ce.name.to_str(),
                        EG.scope.name.to_str() if not happy_util.is_null_struct(EG.scope) else '',
                        '' if EG.in_execution else ' during shutdown ignored')
                    zend_error(E_ERROR if EG.in_execution else E_WARNING, err_msg)
                    return
            else:
                if not zoh.zend_check_protected(destructor.op_array.scope, EG.scope):
                    ce = object.ce

                    err_msg = "Call to protected %s::__destruct() from context '%s'%s" % (
                        ce.name.to_str(),
                        EG.scope.name.to_str() if not happy_util.is_null_struct(EG.scope) else '',
                        '' if EG.in_execution else ' during shutdown ignored')
                    zend_error(E_ERROR if EG.in_execution else E_WARNING, err_msg)
    
        obj_ptr = zval_utils.zpp_stack(zval_utils.zp_stack(zval_utils.make_empty_zval()))
        zval_utils.Z_SET_TYPE_P(obj_ptr.deref(), IS_OBJECT)
        obj_ptr.deref().deref().obj = objects.zend_object_value()
        zval_utils.Z_SET_OBJ_HANDLE_P(obj_ptr.deref(), handle)
        if not EG.objects_store.object_buckets[handle].handlers:
            raise Exception('Not implemented yet')
        zval_utils.Z_SET_OBJ_HT_P(obj_ptr.deref(), EG.objects_store.object_buckets[handle].handlers)
        happy_variables.zval_copy_ctor(obj_ptr.deref())

        old_exception = None
        if EG.exception:
            raise Exception('Not implemented yet')
        zend_interfaces.zend_call_method_with_0_params(obj_ptr, object.ce,
            objects.zend_function_ptr(destructor, False), ZEND_DESTRUCTOR_FUNC_NAME, global_state.null_zval_ptr_ptr)
        if old_exception:
            raise Exception('Not implemented yet')
        happy_variables.zval_ptr_dtor(obj_ptr)

def zend_objects_free_object_storage(object):
    raise Exception('Not implemented yet')

def zend_objects_new(object_ptr, class_type):
    assert isinstance(object_ptr, objects.zend_object_ptr)

    object_ptr.assign(objects.zend_object())
    object_ptr.deref().ce = class_type
    retval = objects.zend_object_value()
    retval.handle = zend_objects_API.zend_objects_store_put(object_ptr.deref(), zend_objects_destroy_object,
        zend_objects_destroy_object, None)
    retval.handlers = zoh.std_object_handlers
    object_ptr.deref().guards = None
    return retval
