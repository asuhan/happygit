from zend import *
import happy_execute
import happy_variables
import targetphpstandalone as tphp
import zend_API
import zend_execute_API
import zval_utils

def zend_call_method(object_pp, obj_ce, fn_proxy, function_name, function_name_len, retval_ptr_ptr,
    param_count, arg1, arg2):
    import global_state
    import happy_util
    result = 0
    fci = zend_API.zend_fcall_info()
    z_fname = zval_utils.make_empty_zval()
    retval = global_state.null_zval_ptr
    function_table = None

    params = []

    params.append(zval_utils.zpp_stack(arg1))
    params.append(zval_utils.zpp_stack(arg2))

    # fci.function_table = NULL; will be read form zend_class_entry of object if needed
    fci.object_ptr = object_pp.deref() if not object_pp.is_null() else global_state.null_zval_ptr
    fci.function_name = zval_utils.zp_stack(z_fname)
    fci.retval_ptr_ptr = retval_ptr_ptr if not retval_ptr_ptr.is_null() else zval_utils.zpp_stack(retval)
    fci.param_count = param_count
    fci.params = params
    fci.no_separation = True
    fci.symbol_table = None

    if fn_proxy.is_null() and happy_util.is_null_struct(obj_ce):
        raise Exception('Not implemented yet')
    else:
        fcic = zend_API.zend_fcall_info_cache()

        fcic.initialized = True
        if happy_util.is_null_struct(obj_ce):
            raise Exception('Not implemented yet')
        if not happy_util.is_null_struct(obj_ce):
            function_table = obj_ce.function_table
        else:
            raise Exception('Not implemented yet')
        if fn_proxy.is_null() or happy_util.is_null_struct(fn_proxy.deref()):
            raise Exception('Not implemented yet')
        else:
            fcic.function_handler = fn_proxy.deref()
        fcic.calling_scope = obj_ce
        if not object_pp.is_null():
            fcic.called_scope = zval_utils.Z_OBJCE_PP(object_pp)
        else:
            raise Exception('Not implemented yet')
        fcic.object_ptr = global_state.null_zval_ptr if object_pp.is_null() else object_pp.deref()
        result = zend_execute_API.zend_call_function(fci, fcic)
    if result == FAILURE:
        raise Exception('Not implemented yet')
    if retval_ptr_ptr.is_null():
        if not retval.is_null():
            happy_variables.zval_ptr_dtor(zval_utils.zpp_stack(retval))
        return global_state.null_zval_ptr
    return retval_ptr_ptr.deref()

def zend_call_method_with_0_params(obj, obj_ce, fn_proxy, function_name, retval):
    import global_state
    return zend_call_method(obj, obj_ce, fn_proxy, function_name, len(function_name), retval, 0,
        global_state.null_zval_ptr, global_state.null_zval_ptr)
