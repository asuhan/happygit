#!/bin/bash

abspath=`cd $1; pwd`
./cleanup.sh $abspath:
./create_dumps.sh $abspath

txtred=$(tput setaf 1)    # Red
txtgrn=$(tput setaf 2)    # Green
txtrst=$(tput sgr0)       # Text reset

for php in $abspath/*.php
do
	echo "Testing "$php"..."
	python pypy/translator/goal/targetphpstandalone.py $php.dump > my1 2> my2
	php $php > php1 2> php2
	diffcount1=`diff my1 php1 | wc -l`
	diffcount2=`diff my2 php2 | wc -l`
	if [[ $diffcount1 -eq 0 && $diffcount2 -eq 0 ]]; then
		echo "${txtgrn}PASS${txtrst}"
	else
		echo "${txtred}FAIL${txtrst}"
	fi
	rm -f my php
done

