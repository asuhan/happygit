from zend import *
import happy_util
from objects import MutableString, zval_ptr
import zend_constants
import zval_utils
import zend_API

def PHP_FUNCTION_strlen(ht, return_value, return_value_ptr, this_ptr, return_value_used):
    # TODO: implement it correctly
    import global_state
    EG = global_state.EG

    arg_count = EG.argument_stack.top_elem().deref().lval
    if arg_count != 1:
        raise Exception('Not implemented yet')
    top_idx = EG.argument_stack.top()
    assert top_idx - 1 - arg_count >= 0
    arg = EG.argument_stack.getItem(top_idx - 1 - arg_count)
    if zval_utils.Z_TYPE_P(arg) == IS_STRING:
        zval_utils.ZVAL_LONG(return_value, zval_utils.Z_STRLEN_P(arg))
    elif zval_utils.Z_TYPE_P(arg) == IS_LONG:
        zval_utils.ZVAL_LONG(return_value, len(str(arg.deref().lval)))
    else:
        raise Exception('Not implemented yet')


def PHP_FUNCTION_define(ht, return_value, return_value_ptr, this_ptr, return_value_used):
    # TODO: implement it correctly
    import global_state
    EG = global_state.EG

    arg_count = EG.argument_stack.top_elem().deref().lval
    if arg_count != 2:
        raise Exception('Not implemented yet')
    top_idx = EG.argument_stack.top()
    assert top_idx - 1 - arg_count >= 0

    case_sensitive = zend_constants.CONST_CS
    name = EG.argument_stack.getItem(top_idx - 1 - arg_count)
    val = EG.argument_stack.getItem(top_idx - 1 - arg_count + 1)

    if (zval_utils.Z_TYPE_P(val) == IS_LONG or
        zval_utils.Z_TYPE_P(val) == IS_DOUBLE or
        zval_utils.Z_TYPE_P(val) == IS_STRING or
        zval_utils.Z_TYPE_P(val) == IS_BOOL or
        zval_utils.Z_TYPE_P(val) == IS_RESOURCE or
        zval_utils.Z_TYPE_P(val) == IS_NULL):
        pass
    else:
        raise Exception('Not implemented yet')

    name_str = zval_utils.Z_STRVAL_P(name)
    assert isinstance(name_str, MutableString)
    c = zend_constants.zend_constant(name_str, val, case_sensitive, zend_constants.PHP_USER_CONSTANT)
    if zend_constants.zend_register_constant(c) == happy_util.SUCCESS:
        zval_utils.ZVAL_BOOL(return_value, 1)
    else:
        zval_utils.ZVAL_BOOL(return_value, 0)


def PHP_FUNCTION_defined(ht, return_value, return_value_ptr, this_ptr, return_value_used):
    # TODO: implement it correctly
    import global_state
    EG = global_state.EG

    arg_count = EG.argument_stack.top_elem().deref().lval
    if arg_count != 1:
        raise Exception('Not implemented yet')
    top_idx = EG.argument_stack.top()
    assert top_idx - 1 - arg_count >= 0

    name = EG.argument_stack.getItem(top_idx - 1 - arg_count)

    c_ptr = zval_utils.zp_stack(zval_utils.make_empty_zval())

    if zend_constants.zend_get_constant_ex(zval_utils.Z_STRVAL_P(name), zval_utils.Z_STRLEN_P(name), c_ptr, None,
        ZEND_FETCH_CLASS_SILENT):
        zval_utils.zval_dtor(c_ptr)
        zval_utils.ZVAL_BOOL(return_value, 1)
    else:
        zval_utils.ZVAL_BOOL(return_value, 0)


def PHP_FUNCTION_get_class(ht, return_value, return_value_ptr, this_ptr, return_value_used):
    # TODO: implement it correctly
    import global_state
    EG = global_state.EG

    arg_count = EG.argument_stack.top_elem().deref().lval
    if arg_count != 1:
        raise Exception('Not implemented yet')
    top_idx = EG.argument_stack.top()
    assert top_idx - 1 - arg_count >= 0

    obj = EG.argument_stack.getItem(top_idx - 1 - arg_count)

    if obj.is_null():
        raise Exception('Not implemented yet')

    dup, name = zend_API.zend_get_object_classname(obj)

    zval_utils.ZVAL_STRING(return_value, name.get_copy() if dup else name)


def PHP_FUNCTION_func_num_args(ht, return_value, return_value_ptr, this_ptr, return_value_used):
    import global_state
    ex = global_state.EG.current_execute_data.prev_execute_data

    if ex and ex.function_state.arguments:
        p = global_state.EG.argument_stack.getItem(ex.function_state.arguments - 1)
        arg_count = zval_utils.Z_LVAL_P(p)
        zval_utils.ZVAL_LONG(return_value, arg_count)
    else:
        raise Exception('Not implemented yet')
