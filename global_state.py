from pypy.rlib.objectmodel import r_dict, compute_hash

from zend import *
import objects
import read_apc_dump as rad
import zval_utils

null_zval_ptr = objects.zval_ptr(zval_utils.make_empty_zval(), True)
null_zval_ptr_ptr = objects.zval_ptr_ptr(null_zval_ptr, True)
null_zval_ptr_ptr_ptr = objects.zval_ptr_ptr_ptr(null_zval_ptr_ptr, True)


class ZendVmStack:
    def __init__(self):
        self.backend = []
        for i in range(1024):
            self.backend.append(null_zval_ptr)
        self.top_idx = 0

    def append(self, ptr):
        self.backend[self.top_idx] = ptr
        self.top_idx += 1

    def top(self):
        return self.top_idx

    def top_elem(self):
        return self.backend[self.top_idx - 1]

    def set_top(self, top_idx):
        self.top_idx = top_idx

    def getLen(self):
        return self.top_idx

    def getItem(self, key):
        return self.backend[key]

    def setItem(self, key, value):
        self.backend[key] = value


def _ms_key_eq(key1, key2):
    return key1.to_str() == key2.to_str()


def _ms_key_hash(key):
    return key.get_hash()


class zend_executor_globals:
    def make_zval_used_for_init(self):
        self.zval_used_for_init = zval_utils.make_empty_zval()
        zval_utils.Z_UNSET_ISREF(self.zval_used_for_init)
        zval_utils.Z_SET_REFCOUNT(self.zval_used_for_init, 1)
        zval_utils.Z_SET_TYPE(self.zval_used_for_init, IS_NULL)

    def INIT_ZVAL(self):
        return zval_utils.zval_copy(self.zval_used_for_init)

    def uninitialized_zval(self):
        return self.__uninitialized_zval_ptr_ptr.deref().deref()

    def uninitialized_zval_ptr(self):
        return self.__uninitialized_zval_ptr_ptr.deref()

    def uninitialized_zval_ptr_ptr(self):
        return self.__uninitialized_zval_ptr_ptr

    def This(self):
        return self.__This.deref()

    def AddressOfThis(self):
        return self.__This

    def SetThis(self, This):
        assert isinstance(This, objects.zval_ptr)
        self.__This.assign(This)

    def __init__(self):
    	from happy_hash import zend_hash_init
    	from happy_variables import zval_ptr_dtor
    	from zend_objects_API import zend_objects_store
        self.make_zval_used_for_init()
        self.__uninitialized_zval_ptr_ptr = zval_utils.zpp_stack(zval_utils.zp_stack(self.INIT_ZVAL()))
        zval_utils.Z_ADDREF_PP(self.__uninitialized_zval_ptr_ptr)
        self.error_zval = self.INIT_ZVAL()
        self.error_zval_ptr = zval_utils.zp_stack(self.error_zval)
        self.argument_stack = ZendVmStack()
        self.arg_types_stack = []
        self.return_value_ptr_ptr = null_zval_ptr_ptr
        self.current_execute_data = None
        self.prev_execute_data = None
        self.symbol_table = zend_hash_init(None, 0, None, zval_ptr_dtor, False)
        self.active_symbol_table = self.symbol_table
        self.active_op_array = None
        # TODO: init with False, implement init_executor
        self.active = True
        self.function_table = r_dict(_ms_key_eq, _ms_key_hash)
        self.class_table = r_dict(_ms_key_eq, _ms_key_hash)
        # TODO: add constant destructor
        self.zend_constants = zend_hash_init(None, 0, None, None, False)
        self.scope = None
        self.called_scope = None
        self.__This = zval_utils.zpp_stack(null_zval_ptr)
        self.return_value_ptr_ptr = null_zval_ptr_ptr
        self.exception = None
        self.in_execution = False
        self.objects_store = zend_objects_store(1024)
        self.std_property_info = rad.APCFile.ZEND_property_info('')
        # TODO: provisional dummy flag
        self.silence = False
        self.exit_status = 0
        self.ext_frames = []

EG = zend_executor_globals()
