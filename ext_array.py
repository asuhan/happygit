from zend import *
import happy_hash
import happy_operators
import zval_utils

def PHP_FUNCTION_count(ht, return_value, return_value_ptr, this_ptr, return_value_used):
    import global_state
    EG = global_state.EG
    if ht != 1:
        raise Exception('Not implemented yet')
    arg_count = EG.argument_stack.top_elem().deref().lval
    if arg_count != 1:
        raise Exception('Not implemented yet')
    top_idx = EG.argument_stack.top()
    assert top_idx - 1 - arg_count >= 0
    arg = EG.argument_stack.getItem(top_idx - 1 - arg_count)
    if zval_utils.Z_TYPE_P(arg) == IS_ARRAY:
        zval_utils.ZVAL_LONG(return_value, arg.deref().happy_ht.size())
    else:
        raise Exception('Not implemented yet')

def PHP_FUNCTION_current(ht, return_value, return_value_ptr, this_ptr, return_value_used):
    import global_state
    EG = global_state.EG
    if ht != 1:
        raise Exception('Not implemented yet')
    arg_count = EG.argument_stack.top_elem().deref().lval
    if arg_count != 1:
        raise Exception('Not implemented yet')
    top_idx = EG.argument_stack.top()
    assert top_idx - 1 - arg_count >= 0
    arg = EG.argument_stack.getItem(top_idx - 1 - arg_count)
    if zval_utils.Z_TYPE_P(arg) != IS_ARRAY:
        raise Exception('Not implemented yet')
    entry_ptr = zval_utils.zppp_stack(global_state.null_zval_ptr_ptr)
    if happy_hash.zend_hash_get_current_data(happy_operators.Z_ARRVAL_P(arg), entry_ptr) == FAILURE:
        zval_utils.ZVAL_BOOL(return_value, 0)
        return
    raise Exception('Not implemented yet')
