from zend import *
import zend_API
import zval_utils


class object_iterator_kind:
    ITER_PLAIN_ARRAY = 0


def iterator_unwrap(array_ptr):
    if zval_utils.Z_TYPE_P(array_ptr) == IS_ARRAY:
        if zend_API.HASH_OF(array_ptr):
            return object_iterator_kind.ITER_PLAIN_ARRAY
    raise Exception('Not implemented yet')

