from pypy.rlib.rarithmetic import intmask
from zend import *
import read_apc_dump as rad
import happy_hash as hh
import happy_operators
import happy_util
import happy_variables
import objects
import global_state as gs
import zend_API
import zend_objects_API
import zval_utils

class zend_object_handlers:
    def __init__(self, **kwargs):
        self.add_ref = kwargs['add_ref']
        self.del_ref = kwargs['del_ref']
        self.read_property = kwargs['read_property']
        self.write_property = kwargs['write_property']
        self.get_method = kwargs['get_method']
        self.get_constructor = kwargs['get_constructor']
        self.get_class_entry = kwargs['get_class_entry']
        self.get_class_name = kwargs['get_class_name']
        self.get_property_ptr_ptr = kwargs['get_property_ptr_ptr']
        self.set = None

def Z_OBJ_P(zval_p):
    return zend_objects_get_address(zval_p)

def zend_objects_store_add_ref(object):
    handle = zval_utils.Z_OBJ_HANDLE_P(object)
    gs.EG.objects_store.object_buckets[handle].refcount += 1

def zend_objects_store_del_ref(zobject):
    handle = zval_utils.Z_OBJ_HANDLE_P(zobject)

    zval_utils.Z_ADDREF_P(zobject)
    zend_objects_store_del_ref_by_handle_ex(handle, zval_utils.Z_OBJ_HT_P(zobject))
    zval_utils.Z_DELREF_P(zobject)

def zend_objects_store_del_ref_by_handle_ex(handle, handlers):
    EG = gs.EG

    if not EG.objects_store.object_buckets:
        raise Exception('Not implemented yet')

    obj = EG.objects_store.object_buckets[handle]

    if EG.objects_store.object_buckets[handle].valid:
        if obj.refcount == 1:
            if not EG.objects_store.object_buckets[handle].destructor_called:
                EG.objects_store.object_buckets[handle].destructor_called = True

                if obj.dtor:
                    if handlers and not obj.handlers:
                        obj.handlers = handlers
                    # TODO: try / catch
                    obj.dtor(obj.object, handle)
    else:
        raise Exception('Not implemented yet')

def zend_verify_property_access(property_info, ce):
    if property_info.deref().flags & ZEND_ACC_PPP_MASK == ZEND_ACC_PUBLIC:
        return True
    elif property_info.deref().flags & ZEND_ACC_PROTECTED:
        return zend_check_protected(property_info.deref().ce, gs.EG.scope)
    elif property_info.deref().flags & ZEND_ACC_PPP_MASK == ZEND_ACC_PRIVATE:
        if ce == gs.EG.scope or property_info.deref().ce == gs.EG.scope and not happy_util.is_null_struct(gs.EG.scope):
            return True
        else:
            return False

    raise Exception('Not implemented yet')

def is_derived_class(child_class, parent_class):
    child_class = child_class.parent
    while not happy_util.is_null_struct(child_class):
        if child_class == parent_class:
            return True
        child_class = child_class.parent

    return False

def zend_check_protected(ce, scope):
    fbc_scope = ce

    # Is the context that's calling the function, the same as one of
    # the function's parents?
    while not happy_util.is_null_struct(fbc_scope):
        if fbc_scope == scope:
            return True
        fbc_scope = fbc_scope.parent

    # Is the function's scope the same as our current object context,
    # or any of the parents of our context?
    while not happy_util.is_null_struct(scope):
        if scope == ce:
            return True
        scope = scope.parent
    return False

def zend_get_function_root_class(fbc):
    return fbc.op_array.prototype.scope if not happy_util.is_null_struct(fbc.op_array.prototype) else fbc.op_array.scope

def zend_get_property_info(ce, member, silent):
    property_info_ptr = zval_utils.zpp_stack(gs.null_zval_ptr)
    scope_property_info_ptr = zval_utils.zpp_stack(gs.null_zval_ptr)

    if not zval_utils.Z_STRVAL_P(member).to_str()[0]:
        raise Exception('Not implemented yet')
    if hh.zend_hash_quick_find(ce.properties_info, zval_utils.Z_STRVAL_P(member),
        zval_utils.Z_STRLEN_P(member), 0, property_info_ptr) == SUCCESS:
        property_info = property_info_ptr.deref()
        if property_info.deref().flags & ZEND_ACC_SHADOW:
            raise Exception('Not implemented yet')
        else:
            if zend_verify_property_access(property_info, ce):
                if (property_info.deref().flags & ZEND_ACC_CHANGED
                    and not (property_info.deref().flags & ZEND_ACC_PRIVATE)):
                    # We still need to make sure that we're not in a context
                    # where the right property is a different 'statically linked' private
                    # continue checking below...
                    pass
                else:
                    if property_info.deref().flags & ZEND_ACC_STATIC:
                        raise Exception('Not implemented yet')
                    return property_info_ptr.deref()
            else:
                raise Exception('Not implemented yet')
    if (gs.EG.scope != ce
        and is_derived_class(ce, gs.EG.scope)
        and not happy_util.is_null_struct(gs.EG.scope)
        and hh.zend_hash_quick_find(gs.EG.scope.properties_info, zval_utils.Z_STRVAL_P(member),
            zval_utils.Z_STRLEN_P(member) + 1, 0, scope_property_info_ptr) == SUCCESS
        and (scope_property_info_ptr.deref().deref().flags & ZEND_ACC_PRIVATE)):
        return scope_property_info_ptr.deref()
    else:
        gs.EG.std_property_info.flags = ZEND_ACC_PUBLIC
        gs.EG.std_property_info.name = zval_utils.Z_STRVAL_P(member)
        gs.EG.std_property_info.name_length = zval_utils.Z_STRLEN_P(member)
        gs.EG.std_property_info.h = rad.UIntWrapper(0)  # TODO
        gs.EG.std_property_info.ce = ce
        property_info_ptr = zval_utils.zpp_stack(zval_utils.zp_stack(gs.EG.std_property_info))
    return property_info_ptr.deref()

def zend_objects_get_address(zobject):
    assert isinstance(zobject, objects.zval_ptr)
    return zend_objects_API.zend_object_store_get_object(zobject)

def zend_std_read_property(object, member, type):
    assert isinstance(object, objects.zval_ptr)
    tmp_member = gs.null_zval_ptr
    retval_ptr = zval_utils.zppp_stack(gs.null_zval_ptr_ptr)

    zobj = Z_OBJ_P(object)

    if zval_utils.Z_TYPE_P(member) != IS_STRING:
        raise Exception('Not implemented yet')

    property_info = zend_get_property_info(zobj.ce, member, zobj.ce.uuget)

    if (property_info.is_null() or hh.zend_hash_quick_find(zobj.properties, property_info.deref().name,
        property_info.deref().name.length(), 0, retval_ptr) == FAILURE):
        raise Exception('Not implemented yet')
    if not tmp_member.is_null():
        raise Exception('Not implemented yet')
    return retval_ptr.deref().deref()

def zend_std_write_property(object, member, value):
    assert isinstance(object, objects.zval_ptr)
    tmp_member = gs.null_zval_ptr
    variable_ptr_ptr = zval_utils.zppp_stack(gs.null_zval_ptr_ptr)

    zobj = Z_OBJ_P(object)

    if zval_utils.Z_TYPE_P(member) != IS_STRING:
        raise Exception('Not implemented yet')

    property_info = zend_get_property_info(zobj.ce, member, zobj.ce.uuset)

    if (not property_info.is_null() and
        hh.zend_hash_quick_find(zobj.properties, property_info.deref().name,
            property_info.deref().name.length() + 1, 0, variable_ptr_ptr) == SUCCESS):
        if variable_ptr_ptr.deref().deref() != value:
            if zval_utils.PZVAL_IS_REF(variable_ptr_ptr.deref().deref()):
                raise Exception('Not implemented yet')
            else:
                garbage = variable_ptr_ptr.deref().deref()

                # if we assign referenced variable, we should separate it
                zval_utils.Z_ADDREF_P(value)
                if zval_utils.PZVAL_IS_REF(value):
                    raise Exception('Not implemented yet')
                variable_ptr_ptr.deref().assign(value)
                happy_variables.zval_ptr_dtor(zval_utils.zpp_stack(garbage))
    else:
        if not happy_util.is_null_struct(zobj.ce.uuset):
            raise Exception('Not implemented yet')
        elif not happy_util.is_null_struct(property_info):
            # if we assign referenced variable, we should separate it
            zval_utils.Z_ADDREF_P(value)
            if zval_utils.PZVAL_IS_REF(value):
                raise Exception('Not implemented yet')
            # TODO: use property_info.deref().h.int_value for the call below
            hh.zend_hash_quick_update(zobj.properties, property_info.deref().name, property_info.deref().name_length + 1,
                0, zval_utils.zpp_stack(value), 0,
                zval_utils.zppp_stack(gs.null_zval_ptr_ptr))
        else:
            raise Exception('Not implemented yet')

    if not tmp_member.is_null():
        raise Exception('Not implemented yet')

def zend_std_get_property_ptr_ptr(object, member):
    assert isinstance(object, objects.zval_ptr)
    assert isinstance(member, objects.zval_ptr)
    tmp_member_ptr = zval_utils.zp_stack(zval_utils.make_empty_zval())
    retval_ptr = zval_utils.zppp_stack(gs.null_zval_ptr_ptr)

    zobj = Z_OBJ_P(object)

    if zval_utils.Z_TYPE_P(member) != IS_STRING:
        raise Exception('Not implemented yet')

    property_info = zend_get_property_info(zobj.ce, member, zobj.ce.uuget)

    if (property_info.is_null() or
        hh.zend_hash_quick_find(zobj.properties, property_info.deref().name,
            property_info.deref().name.length() + 1, 0, retval_ptr) == FAILURE):
        raise Exception('Not implemented yet')
    if member == tmp_member_ptr:
        raise Exception('Not implemented yet')
    return retval_ptr.deref()

def zend_std_object_get_class(object):
    assert isinstance(object, objects.zval_ptr)
    zobj = Z_OBJ_P(object)

    return zobj.ce

def zend_std_object_get_class_name(object, parent):
    assert isinstance(object, objects.zval_ptr)
    zobj = Z_OBJ_P(object)

    if parent:
        raise Exception('Not implemented yet')
    else:
        ce = zobj.ce

    class_name = ce.name.get_copy()
    return (SUCCESS, class_name)

def zend_std_get_method(object_ptr, method_name, method_len):
    assert isinstance(object_ptr, objects.zval_ptr_ptr)
    fbc_ptr = zval_utils.zpp_stack(gs.null_zval_ptr)
    object = object_ptr.deref()

    lc_method_name = objects.MutableString(method_name.to_str().lower())

    zobj = Z_OBJ_P(object)
    if hh.zend_hash_find(zobj.ce.function_table, lc_method_name, method_len + 1, fbc_ptr) == FAILURE:
        raise Exception('Not implemented yet')

    fbc = fbc_ptr.deref().deref()

    if fbc.op_array.fn_flags & ZEND_ACC_PRIVATE:
        raise Exception('Not implemented yet')
    else:
        # Ensure that we haven't overridden a private function and end up calling
        # the overriding public function...
        priv_fbc_ptr = zval_utils.zpp_stack(gs.null_zval_ptr)

        if (not happy_util.is_null_struct(gs.EG.scope) and is_derived_class(fbc.op_array.scope, gs.EG.scope) and
            fbc.op_array.fn_flags & ZEND_ACC_CHANGED):
            if (hh.zend_hash_find(gs.EG.scope.function_table, lc_method_name, method_len + 1, priv_fbc_ptr) == SUCCESS
                and (priv_fbc_ptr.deref().deref().op_array.fn_flags & ZEND_ACC_PRIVATE)
                and (priv_fbc_ptr.deref().deref().op_array.scope == gs.EG.scope)):
                fbc = priv_fbc_ptr.deref().deref()
        if fbc.op_array.fn_flags & ZEND_ACC_PROTECTED:
            # Ensure that if we're calling a protected function, we're allowed to do so.
            # If we're not and __call() handler exists, invoke it, otherwise error out.
            if not zend_check_protected(zend_get_function_root_class(fbc), gs.EG.scope):
                raise Exception('Not implemented yet')

    return fbc.op_array

def zend_std_get_constructor(object):
    assert isinstance(object, objects.zval_ptr)
    zobj = Z_OBJ_P(object)
    constructor = zobj.ce.constructor

    if not happy_util.is_null_struct(constructor):
        if constructor.op_array.fn_flags & ZEND_ACC_PUBLIC:
            pass
        elif constructor.op_array.fn_flags & ZEND_ACC_PRIVATE:
            # Ensure that if we're calling a private function, we're allowed to do so.
            if constructor.op_array.scope != gs.EG.scope:
                if gs.EG.scope:
                    err_msg = "Call to private %s::%s() from context '%s'" % (
                        constructor.op_array.scope.name.to_str(),
                        constructor.op_array.function_name,
                        gs.EG.scope.name.to_str())
                    zend_error(E_ERROR, err_msg)
                else:
                    err_msg = "Call to private %s::%s() from invalid context" % (
                        constructor.op_array.scope.name.to_str(),
                        constructor.op_array.function_name)
                    zend_error(E_ERROR, err_msg)
        elif constructor.op_array.fn_flags & ZEND_ACC_PROTECTED:
            # Ensure that if we're calling a protected function, we're allowed to do so.
            # Constructors only have prototype if they are defined by an interface but
            # it is the compilers responsibility to take care of the prototype.
            #
            if not zend_check_protected(zend_get_function_root_class(constructor), gs.EG.scope):
                if not happy_util.is_null_struct(gs.EG.scope):
                    raise Exception('Not implemented yet')
                else:
                    err_msg = 'Call to protected %s::%s() from invalid context' % \
                        (constructor.op_array.scope.name.to_str(), constructor.op_array.function_name)
                    zend_error(E_ERROR, err_msg)

    return constructor

def zend_std_get_static_method(ce, function_name_strval, function_name_strlen):
    fbc_ptr = zval_utils.zpp_stack(gs.null_zval_ptr)

    lc_function_name = objects.MutableString(function_name_strval.get_copy().to_str().lower())

    if function_name_strlen == len(ce.name.to_str()) and not happy_util.is_null_struct(ce.constructor):
        lc_class_name = happy_operators.zend_str_tolower_dup(ce.name, ce.name_length)
        # Only change the method to the constructor if the constructor isn't called __construct
        # we check for __ so we can be binary safe for lowering, we should use ZEND_CONSTRUCTOR_FUNC_NAME
        #
        if lc_class_name.to_str() == lc_function_name.to_str() and ce.constructor.op_array.function_name[:2] == "__":
            fbc_ptr = zval_utils.zpp_stack(ce.constructor)
    if fbc_ptr.deref().is_null() and hh.zend_hash_find(ce.function_table, lc_function_name,
        function_name_strlen + 1, fbc_ptr) == FAILURE:
        raise Exception('Not implemented yet')

    if fbc_ptr.deref().deref().op_array.fn_flags & ZEND_ACC_PUBLIC:
        pass
    elif fbc_ptr.deref().deref().op_array.fn_flags & ZEND_ACC_PUBLIC:
        raise Exception('Not implemented yet')
    elif fbc_ptr.deref().deref().op_array.fn_flags & ZEND_ACC_PROTECTED:
        # TODO: implement the check
        pass

    return fbc_ptr.deref().deref().op_array

def zend_std_get_static_property(ce, property_name, property_name_len, silent):
    retval_ptr = zval_utils.zppp_stack(gs.null_zval_ptr_ptr)
    tmp_ce = ce
    property_info_ptr = zval_utils.zpp_stack(gs.null_zval_ptr)

    if hh.zend_hash_find(ce.properties_info, property_name, property_name_len + 1, property_info_ptr) == FAILURE:
        raise Exception('Not implemented yet')
    property_info = property_info_ptr.deref()

    if not zend_verify_property_access(property_info, ce):
        raise Exception('Not implemented yet')

    zend_API.zend_update_class_constants(tmp_ce)

    hh.zend_hash_quick_find(zend_API.CE_STATIC_MEMBERS(tmp_ce), property_info.deref().name,
        property_info.deref().name.length() + 1, 0, retval_ptr)

    retval = retval_ptr.deref()

    if retval.is_null():
        raise Exception('Not implemented yet')

    return retval

std_object_handlers = zend_object_handlers(
    add_ref = zend_objects_store_add_ref,
    del_ref = zend_objects_store_del_ref,
    read_property = zend_std_read_property,
    write_property = zend_std_write_property,
    get_property_ptr_ptr = zend_std_get_property_ptr_ptr,
    get_method = zend_std_get_method,
    get_constructor = zend_std_get_constructor,
    get_class_entry = zend_std_object_get_class,
    get_class_name = zend_std_object_get_class_name)
